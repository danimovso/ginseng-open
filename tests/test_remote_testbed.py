from etc.PySock import Client
import time
import threading
from testbed.remote import remote_testbed
from exp.prog.MemoryConsumer import MemoryConsumerServer
from testbed.remote.remote_testbed import RemoteTestbedServer

argv = ["debug", "MemoryConsumer", 0.4, 300, 3, 0.1]

if __name__ == "__main__":
    print "=== STARTING TEST MODE! ==="
    mc = Client("localhost", MemoryConsumerServer.port)
    rtb = Client("localhost", RemoteTestbedServer.port)

    def test_func():
        time.sleep(5)
        mc.send_recv("set-load:10")
        rtb.send_recv("collect-start")
        time.sleep(3)

        print "collect-end: ", rtb.send_recv("collect-end")
        print "get-mem: ", rtb.send_recv("get-mem")
        print "performance: ", mc.send_recv("get-perf")

        rtb.send("finish")
        rtb.close()
        mc.close()

    trd = threading.Thread(target=test_func)
    trd.start()
    remote_testbed.remote_testbed(*argv)
    trd.join()
