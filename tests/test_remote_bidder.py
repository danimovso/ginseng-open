'''
Created on Jul 28, 2012

@author: eyal
'''
from exp.remote import remote_bidder.remote_bidder
import os

if __name__ == '__main__':
    verbosity = "debug"
    prog = "MemoryConsumer"
    adviser = {"name": "AdviserProfit", "profiler": "doc/profiler-memcached.xml",
               "entry": "throughput", "revenue_function": "lambda x: 10*x",
               "memory_delta": 1, "load_smooth": 1, "memory_smooth": 3,
               "load_d": 1, "memory_d": 3, "base_mem":650}

    remote_bidder("%s/moc" %  os.path.expanduser("~"), verbosity, prog,
                  *str(adviser).split())
