'''
Created on Jul 27, 2012

@author: eyal
'''
import os
from exp.prog.Programs import Guest_MemoryConsumer
import exp.core.Loads
from exp.common.exp_compare import exp_compare
from mom.LogUtils import LogUtils
from exp.core.Experiment import set_out_dir

verbosity = "info"
prog_args = Guest_MemoryConsumer(consume_percent = 0.5, saturation = 800,
    update_interval = 2, sleep_after_write = 0.1).command_args

prifiler_file = 'doc/profiler-mc.xml'
profiler_entry = 'throughput'
base_mem = 1100

if __name__ == "__main__":
    LogUtils(verbosity)
    name = "exp-compare-test-" + prog_args[0]
    out_dir = set_out_dir("test", name)

    adviser = {'name': 'AdviserProfit', 'profiler': prifiler_file,
               'entry': profiler_entry, 'revenue_function': 'lambda x: 10*x',
               'base_mem': base_mem, 'memory_delta': 10, 'load_smooth': 0,
               'memory_smooth': 0, 'load_d': 5, 'memory_d':10}

    exp_compare(name, 0, prog_args, adviser,
                alpha = 1,
                t_auction = 10,
                rounds = 2,
                t_load = 100,
                load_intence = exp.core.Loads.loadupto40,
                load_interval = 5,
                auction_base0 = base_mem,
                start_cpu = 0,
                divide_memory = True,
                verbosity = verbosity,
                out_dir = out_dir)
