'''
Created on Jul 28, 2012

@author: eyal
'''
import threading
import logging
import collections
import time

class ParallelInvoker(object):

    logger = logging.getLogger("ParallelInvoker")

    def __init__(self, iterable, name = lambda *i: "ParallelInvokerThread"):
        self.threads = []
        self.iterable = iterable
        self.name = name

    def start(self, method, args = lambda *i: (), kwargs = lambda *i: {},
              invoke_interval = 0):
        # start all threads
        for item in self.iterable:
            # convert item into an argument list if it isn't one.
            if not isinstance(item, collections.Iterable):
                item = (item,)
            # initiate and start the thread
            trd = threading.Thread(target = method(*item),
                    args = args(*item), kwargs = kwargs(*item),
                    name = self.name(*item))
            trd.start()
            self.threads.append(trd)
            time.sleep(invoke_interval)

    def join(self, timeout = None):
        # wait for threads
        while self.threads:
            trd = self.threads.pop()
            # try to join a thread
            trd.join(timeout)
            # if still alive after timeout, hard-killing it
            if timeout and trd.is_alive():
                self.logger.warn("Join timeout, killing %s", trd.name)
                threading.Thread._Thread__stop(trd) #@UndefinedVariable

    def start_and_join(self, method, args = lambda *i: (), 
                       kwargs = lambda *i: {},
                       timeout = None, invoke_interval = 0):
        self.start(method, args, kwargs, invoke_interval)
        self.join(timeout)
