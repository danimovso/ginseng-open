'''
Created on Mar 12, 2015

@author: Liran Funaro <fonaro@cs.technion.ac.il>
'''
from subprocess import Popen, PIPE
from os.path import join, isfile
import re
import atexit
import logging
from etc.decorators import Singleton
from etc.shell import run, append_to_file, run_shell

class CacheDriver(object):
    __metaclass__ = Singleton
    '''
    Resource Directory Technology (RDT) Driver for Cache Allocation
    Technology (CAT).

    This feature is currently supported by a limited set of Intel's CPUs and
    can be utilized using Vikas Shivappa <vikas.shivappa@linux.intel.com> patch
    for the Linux kernel: https://lkml.org/lkml/2015/3/12/978.

    This technology allows to limit a specific CPU cache usage.
    This CPU will allocate new lines of cache, only in a sequential set of ways
    selected for it, from the available ways of the Last Level Cache (LLC).
    Vikas's patch allows to apply this limitations per task/cgroup.
    More information can be found in the Intel SDM Volume 3 section 17.15

    Since this technology only limits the cache allocations, a CPU that uses
    memory that is already allocated in different cache ways (other then the
    ones allocated to it), will use those cache ways and will mark them as
    most-recently-used.
    For some situations, this behavior is unwanted. For that, this controller
    also support cleaning a set of ways.
    This feature requires that the "cleancache" application will be installed
    on the system (this is not a stock application, it was developed for this
    specific purpose).

    Usage (singleton)
    ===========================================================================
    Allocation:
    ---------------------------------------------------------------------------
    CacheDriver().set_cgroup_cache("mycgroup",w1,w2)

    Where "mycgroup" is the name of the cgroup and w1 <= w2.

    The cgroup "mycgroup" will be limited from now on to the cache ways in the
    sequence [w1,w2].

    Cleaning Cache Ways:
    ---------------------------------------------------------------------------
    CacheDriver().cleancache(w1,w2)

    Where w1 >= w2.
    '''

    CGROUP_DIR = "/sys/fs/cgroup"
    CGROUP_CACHE_DIR = join(CGROUP_DIR,"rdt")
    CGROUP_CLEANCACHE_SUBDIR = join(CGROUP_CACHE_DIR,"cleancache")

    CLEANCACHE_SERVICE = "cleancache"

    CBM_FILE_NAME = "rdt.cbm"
    TASKS_FILE_NAME = "tasks"
    MIN_CBM_BITS = 2

    def __init__(self):
        self.logger = logging.getLogger( self.__class__.__name__ )

        # Default value in case the check will fail
        self.cbmlength = float("inf")
        self.closs = None
        self.clean_cache = None

        self.rdt_initiated = self.init_rdt()
        self.init_cleancache()

    def init_rdt(self):
        self.check_rdt()

        mkdir = run(["mkdir", "-p", self.CGROUP_CACHE_DIR], as_root = True)
        # Will not return error if already exists
        if mkdir.err:
            self.logger.error("Could not create rdt directory - out: '%s', err: '%s'" % (mkdir.out,mkdir.err) )
            return False

        ignore_fail_mount = False

        is_mount = run_shell("mount | grep rdt")
        if is_mount.err:
            self.logger.warn("Could not check if rdt is already mounted, will ignore if fail to mount  - out: %s, err: %s" % (is_mount.out,is_mount.err))
            ignore_fail_mount = True
        elif is_mount.out:
            # We will assume that if something with rdt is mount, it will be our rdt-cgroup
            self.logger.info("cgroup rdt was already mounted: %s" % is_mount.out)
            return True

        mount = run(["mount", "-t", "cgroup", "-ordt", "rdt", self.CGROUP_CACHE_DIR], as_root = True)
        if mount.err:
            if ignore_fail_mount:
                self.logger.warn("Could not mount cgroups rdt directory (ignoring) - out: '%s', err: '%s'" % (mount.out,mount.err) )
            else:
                self.logger.error("Could not mount cgroups rdt directory - out: '%s', err: '%s'" % (mount.out,mount.err) )
                return False
        else:
            self.logger.info("Successfully mounted cgroup rdt: %s" % mount.out)

        return True

    def init_cleancache(self):
        if not self.rdt_initiated:
            self.logger.warn("Did not start cleancache because rdt is not supported")
            return

        self.terminate_cleancache()

        if isfile(self.CLEANCACHE_SERVICE):
            try:
                self.clean_cache = Popen(self.CLEANCACHE_SERVICE, stdin=PIPE, stdout=PIPE, shell=False)
            except Exception as err:
                self.logger.error("Could not start cleancache: %s" % err)
            else:
                atexit.register(self.terminate_cleancache)

                mkdir = run(["mkdir", "-p", self.CGROUP_CLEANCACHE_SUBDIR], as_root = True)
                # Will not return error if already exists
                if mkdir.err:
                    self.logger.error("Could not create cleancache cgroup directory - out: '%s', err: '%s'" % (mkdir.out,mkdir.err) )
                else:
                    self._assign_task_to_cgroup(self.CGROUP_CLEANCACHE_SUBDIR, self.clean_cache.pid)

    def terminate_cleancache(self):
        if self.clean_cache is not None:
            self.clean_cache.kill()
            self.clean_cache = None

    def cleancache(self, first_way, last_way):
        if self.clean_cache:
            self._set_cgroup_cache(self.CGROUP_CLEANCACHE_SUBDIR, first_way, last_way)
            self.clean_cache.stdin.write("%s-%s\n" % (first_way, last_way))
            return self.self.clean_cache.stdout.readline()
        else:
            self.logger.warn("Could not clean cache ways: %s. Cleancache was never started." % [first_way, last_way])

    def check_rdt(self):
        '''
        According to documentation by Vikas Shivappa
        =====================================================================
        To check if CAT was enabled on your system

        dmesg | grep -i intel_rdt
        should output : intel_rdt: cbmlength:xx, Closs:xx
        the length of cbm and CLOS should depend on the system you use.
        '''
        dmesg = run_shell("dmesg | grep -i intel_rdt")
        if dmesg.err or not dmesg.out:
            self.logger.warn("Could not check rdt - out: %s, err: %s" % (dmesg.out,dmesg.err))
            return False

        dmesg_pattern = re.compile("\s*intel_rdt:\s*cbmlength:\s*([0-9]+),\s*Closs:\s*([0-9]+)")
        result = dmesg_pattern.match(dmesg.out)
        if result:
            self.cbmlength = int(result.group(1))
            self.closs = int(result.group(2))
            self.logger.info("RDT support detected - CBM Length: %s, CLOS: %s" % (self.cbmlength, self.closs))
        else:
            self.logger.warn("dmesg output does not match expectations - out: %s" % dmesg.out)

    @property
    def max_cache_ways(self):
        return self.cbmlength

    @property
    def max_different_cache_allocations(self):
        return self.closs

    def _get_cbm_mask(self, first_way, last_way):
        bit_count = last_way - first_way + 1

        if bit_count < self.MIN_CBM_BITS:
            raise ValueError("Must allocate at least 2 cache ways, used %s", bit_count)

        if bit_count > self.cbmlength:
            raise ValueError("Can allocate not more then %s cache ways, used %s", bit_count)

        return ((1 << bit_count) - 1) << first_way

    def __set_cbm_file(self, cbm_file_path, first_way, last_way):
        cbm_mask = self._get_cbm_mask(first_way, last_way)
        set_cbm = append_to_file(cbm_file_path, hex(cbm_mask), as_root = True)

        if set_cbm.err:
            self.logger.error("Could not apply cgroup cache - out: '%s', err: '%s'" % (set_cbm.out,set_cbm.err) )
            return False
        elif set_cbm.out:
            self.logger.info("Applied cgroup cache - '%s'" % set_cbm.out )

        return True

    def __assign_task_file(self, tasks_file_path, pid):
        add_task = append_to_file(tasks_file_path, pid, as_root = True)

        if add_task.err:
            self.logger.error("Could not add task to cgroup - out: '%s', err: '%s'" % (add_task.out,add_task.err) )
            return False
        elif add_task.out:
            self.logger.info("Add task to cgroup - '%s'" % add_task.out )

        return True

    def __get_cgroup_path(self, cgroup, file_name):
        return join(self.CGROUP_CACHE_DIR, cgroup, file_name)

    def set_cgroup_cache(self, cgroup, first_way, last_way):
        if not self.rdt_initiated:
            self.logger.warn("Could not set cache ways: %s to cgroup: %s. RDT is not supported." % ([first_way, last_way], cgroup))
            return

        cbm_file_path = self.__get_cgroup_path(cgroup, self.CBM_FILE_NAME)
        self.__set_cbm_file(cbm_file_path, first_way, last_way)

    def assign_task_to_cgroup(self, cgroup, pid):
        if not self.rdt_initiated:
            self.logger.warn("Could not assign pid: %s to cgroup: %s. RDT is not supported." % (pid, cgroup))
            return

        tasks_file_path = self.__get_cgroup_path(cgroup, self.TASKS_FILE_NAME)
        self.__assign_task_file(tasks_file_path, pid)

class LibvirtCacheDriver(CacheDriver):
    '''
    Cache Allocation Technology (CAT) driver specifically for libvirt
    virtual machines.

    Usage (singleton)
    ===========================================================================
    Allocation:
    ---------------------------------------------------------------------------
    LibvirtCacheDriver().set_vm_cache("my-vm",w1,w2)

    Where "my-vm" is the name of the virtual machine and w1 <= w2.

    The virtual machine "my-vm" will be limited from now on to the cache ways
    in the sequence [w1,w2].
    '''
    CGROUP_LIBVIRT_MACHINES_SUBDIR = "libvirt/qemu"

    def __init__(self):
        CacheDriver.__init__(self)

    def __get_machine_cgroup(self, vm_name):
        return join(self.CGROUP_LIBVIRT_MACHINES_SUBDIR, vm_name)

    def set_vm_cache(self, vm_name, first_way, last_way):
        machine_cgroup = self.__get_machine_cgroup(vm_name)
        return self.set_cgroup_cache(machine_cgroup, first_way, last_way)

if __name__ == '__main__':
    logging.basicConfig()
    # Test Init
    c = LibvirtCacheDriver()

    # Test allocation
    c.set_vm_cache("master",1,3)