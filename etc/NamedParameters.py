'''
Created on Jul 13, 2014

@author: liran
'''

import copy
import sys

class NamedParametersException(Exception): pass

class NamedParameters(object):
    '''
    Help parametrize a script
    '''

    def __init__(self, parameter_groups, show = True, do_prompt = True, post_actions = None):
        '''
        Constructor
        '''
        self.parameter_groups = {}
        self.post_actions = []

        self.show = show
        self.do_prompt = do_prompt

        self.add_groups(parameter_groups, post_actions)

    def add_groups(self, new_parameter_groups, post_actions = None):
        if isinstance(new_parameter_groups, NamedParameters):
            self.parameter_groups.update( copy.deepcopy(new_parameter_groups.parameter_groups) )
            self.post_actions.extend(new_parameter_groups.post_actions)
        else:
            assert isinstance(new_parameter_groups, dict)
            self.parameter_groups.update( copy.deepcopy(new_parameter_groups) )

        if post_actions:
            if isinstance(post_actions, list):
                self.post_actions.extend(post_actions)
            else:
                self.post_actions.append(post_actions)

    def parse(self, input_params = []):
        ret = parse_parameters(self, input_params)

        for p in self.post_actions:
            p(ret)

        return ret

    def inquire_parameters(self, parameters):
        for param in parameters:
            if not self.inquire_param(param):
                raise NamedParametersException("Unused parameter: %s" % param)

    def inquire_param(self, parameter):
        param_used = False
        for _, group in self.parameter_groups.iteritems():
            param_used |= group.inquire_param(parameter)

        return param_used

    def get_group(self, group_name):
        return self.parameter_groups[group_name]

    def prompt(self, name = None):
        params = []

        if self.do_prompt:
            for group_name, group in self.parameter_groups.iteritems():
                fail = True
                while fail:
                    try:
                        params += group.prompt(group_name)
                        fail = False
                    except NamedParametersException:
                        fail = True

        return params

    def prompt_paramers(self, name = None):
        for group_name, group in self.parameter_groups.iteritems():
            group.prompt_paramers(group_name)

    def prompt_all(self, name = None):
        self.prompt_paramers(name)

        user_input = filter(None, raw_input("Enter Parameters: ").split(" "))

        params = []
        for param in user_input:
            if not self.inquire_param(param):
                user_input = raw_input("[ERROR] Unused parameter: %s. Ignore? (y/n): " % param)
                if not user_input == "y":
                    raise NamedParametersException("Unused parameter: %s" % param)
            else:
                params.append(param)

        return params

    def __getitem__(self, index):
        return self.parameter_groups[index].value

    def __repr__(self):
        return "\n".join([ "%s: %s" % (key, group.value) for key,group in self.parameter_groups.iteritems() if group.show])

    @property
    def value(self):
        ret = dict()
        for key, group in self.parameter_groups.iteritems():
            ret[key] = group.value

        return ret

    def duplicate_as_base(self):
        return copy.deepcopy(self)

class ParameterGroup(object):
    '''
    Define a group of parameters for an experiment
    '''

    def __init__(self, param_dict, default_values, init_value, show = True, do_prompt = True):
        '''
        Constructor
        '''
        self.init_value = init_value
        self.restart()

        if not isinstance(default_values, list):
            default_values = [ default_values ]
        self.default_values = default_values

        assert isinstance(param_dict, dict)
        self.param_dict = param_dict

        self.show = show
        self.do_prompt = do_prompt

    def restart(self):
        self.included = []
        self._value = copy.copy(self.init_value)

    def inquire_param(self, param):
        '''
        Inquire if a parameter is part of this group.
        If so, adds its value to the current value
        '''
        if param in self.param_dict:
            self.include_param(param, self.param_dict[param])
            self.included.append(param)
            return True
        else: return False

    @property
    def value(self):
        if not self.included:
            base = self.duplicate_as_base()

            for param in self.default_values:
                base.inquire_param(param)

            return base._value
        else:
            return self._value

    def prompt_paramers(self, name):
        if self.do_prompt:
            if not self.included:
                    keys = [ k if k not in self.default_values else "\033[96m\033[1m%s\033[0m" % k for k in self.param_dict.keys() ]
            else:
                keys = [ k if k not in self.included else "\033[92m\033[1m%s\033[0m" % k for k in self.param_dict.keys() ]

            print "\033[93m\033[1m%s\033[0m (%s): " % (name, ",".join(keys))

    def prompt(self, name):
        params = []

        if self.do_prompt:
            if not self.included:
                keys = [ k if k not in self.default_values else "\033[96m\033[1m%s\033[0m" % k for k in self.param_dict.keys() ]
            else:
                keys = [ k if k not in self.included else "\033[92m\033[1m%s\033[0m" % k for k in self.param_dict.keys() ]

            user_input = filter(None, raw_input("\033[93m\033[1m%s\033[0m (%s): " % (name, ",".join(keys))).split(" "))

            for param in user_input:
                if not self.inquire_param(param):
                    user_input = raw_input("[ERROR] Unused parameter: %s. Ignore? (y: yes, otherwise: try again): " % param)
                    if not user_input == "y":
                        raise NamedParametersException("Unused parameter: %s" % param)
                else:
                    params.append(param)

        return params

    def __repr__(self):
        return str(self.value)

    def include_param(self, param_key, param_value):
        '''
        Include a parameter in the group value.
        Should be implemented based on the group
        '''
        pass

    def duplicate_as_base(self):
        return copy.deepcopy(self)

class SingleParameterGroup(ParameterGroup):
    def __init__(self, param_dict, default_values = [], init_value = None, show = True, do_prompt = True):
        ParameterGroup.__init__(self, param_dict, default_values, init_value, show, do_prompt)

    def include_param(self, param_key, param_value):
        self._value = param_value

class BooleanParameterGroup(ParameterGroup):
    def __init__(self, availible_parameters, default_values = [], show = True, do_prompt = True):
        assert isinstance(availible_parameters, list)
        init_value = dict()
        for param in availible_parameters:
            init_value[param] = False

        ParameterGroup.__init__(self, init_value, default_values, init_value, show, do_prompt)

    def include_param(self, param_key, param_value):
        self._value[param_key] = True

class ListSingleParameterGroup(ParameterGroup):
    def __init__(self, param_dict, default_values = [], init_value = [], show = True, do_prompt = True):
        assert isinstance(init_value, list)
        ParameterGroup.__init__(self, param_dict, default_values, init_value, show, do_prompt)

    def include_param(self, param_key, param_value):
        if param_value not in self._value:
            self._value.append(param_value)

class ListMultiParameterGroup(ListSingleParameterGroup):
    def __init__(self, param_dict, default_value = [], init_value = [], show = True, do_prompt = True):
        ListSingleParameterGroup.__init__(self, param_dict, default_value, init_value, show, do_prompt)

    def include_param(self, param_key, param_value):
        if isinstance(param_value, list):
            for p in param_value:
                ListSingleParameterGroup.include_param(self, param_key, p)
        else: ListSingleParameterGroup.include_param(self, param_key, param_value)

def parse_parameters(params, input_params = []):
    ret_params = []
    sub_params = []

    parent_stack = []
    child_stack = []
    operand_count = 0

    for p in input_params:
        if p == '{':
            operand_count += 1
            if operand_count == 1:
                continue
        if p == '}':
            operand_count -= 1
            if operand_count == 0:
                continue

        if operand_count == 0:
            if p == '+':
                sub_params.append((parent_stack, child_stack))
                parent_stack = []
                child_stack = []
            else:
                parent_stack.append(p)
        else:
            child_stack.append(p)

    if parent_stack or child_stack:
        sub_params.append((parent_stack, child_stack))

    while True:
        cur_params = params.duplicate_as_base()
        parent_params = []
        child_params = []

        try:
            if sub_params:
                parent_params, child_params = sub_params.pop(0)
                cur_params.inquire_parameters(parent_params)
            else:
                user_input = raw_input("Do you want to add an experiment? (y/n) (default: n): ")
                if user_input != 'y':
                    break

                parent_params = cur_params.prompt_all()
        except Exception as e:
            exit(str(e))

        print "Input Parameters: %s" % " ".join(parent_params)
        print "Selected Parameters:\n%s\n" % cur_params

        if child_params:
            use_as_base = True
        else:
            user_input = raw_input("What do to? (b: use as base, a: add as is - defualt): ")
            use_as_base = user_input == 'b'

        if not use_as_base:
            ret_params.append(cur_params)
        else:
            ret_params += parse_parameters(cur_params, child_params)

    return ret_params

if __name__ == '__main__':
    ''' Unit Test'''
    params = NamedParameters({
        "boolean" : BooleanParameterGroup(["b1", "b2", "b3"],
                    default_values = ["b1"], show = True),

        "list_single_w_list" : ListSingleParameterGroup(dict(
            lsw1 = [1,1,1],
            lsw2 = [2,2,2],
            lsw3 = [3],
        ), "lsw1"),

       "list_single" : ListSingleParameterGroup(dict(
            ls1 = 1,
            ls2 = 2,
            ls3 = 3,
        ), "ls1"),

        "list_multi" : ListMultiParameterGroup(dict(
            lm1 = [1],
            lm123 = [1,2,3],
            lm456 = [4,5,6],
            lm6 = [6],
        ), "lm1"),

        "multi_dict" : NamedParameters(dict(
            md1 = ListMultiParameterGroup(dict(
                md1_1 = [1],
                md1_2 = [2,2,2],
            ), "md1_1", do_prompt = False),

            md2 = ListSingleParameterGroup(dict(
                md2_1 = 1,
                md2_2 = 2,
            ), "md2_1", do_prompt = False),

            md3 = ListSingleParameterGroup(dict(
                md3_true = True,
                md3_false = False,
            ), "md3_true", do_prompt = False),
        )),
    })

    print parse_parameters(params, sys.argv[1:])
