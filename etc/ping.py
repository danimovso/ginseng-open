'''
Created on Feb 20, 2013

@author: eyal
'''
from subprocess import Popen, PIPE
from threading import Thread
import time
from etc.shell import TimeoutError

def __ping (ip, logger, out):
    try:
        start = time.time()
        out = Popen(["ping", "-c", "1", ip], stdout = PIPE).communicate()[0]
        t = time.time() - start
        out = out.split("\n")
        line = filter(lambda l: l.find("packets transmitted") != -1, out)[0].strip()
        loss = int(line.split(",")[2].strip().split("%")[0])
    except ValueError, TimeoutError:
        loss = 100
    if logger:
        if loss == 0:
            logger.info("ping %s OK, delay: %.2fs", ip, t)
        else:
            logger.warn("ping %s ERROR: packet loss", ip)
    else:
        print ip, ":", "OK, delay: %.2fs" % t if loss == 0 else "Unreachable"

def ping(ip, logger = None):
    out = []
    Thread(target = __ping, args = (ip, logger, out)).start()

if __name__ == "__main__":
    ping("www.google.com")
    ping("1.1.0.1")
