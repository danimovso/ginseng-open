'''
Common decorators

@author: Liran Funaro <fonaro@cs.technion.ac.il>
'''
import os

def dict_represented(cls):
    '''
    A class that implements the method @property as_dict(self) can use this decorator
    to automatically implement 2 methods:
      1. __repr__(self): will return the class representation as string based
              on the result from as_dict().
      2. write_info(self, out_dir): writes the result of as_dict() in an info
              file located in the out_dir path.
    '''
    cls.__INFO_DEFAULT_FILE_NAME__ = "info"

    def dict_repr(self_obj):
        return str(self_obj.as_dict)

    def write_info(self_obj, out_dir):
        file(os.path.join(out_dir, self_obj.__INFO_DEFAULT_FILE_NAME__), "w").write(str(self_obj))

    cls.__repr__ = dict_repr
    cls.write_info = write_info

    return cls

def synchronized(func):
    '''
    This decorator will make a function to synchronized with a global class lock
    '''
    def synced_func(self, *args, **kws):
        with self._lock:
            return func(self, *args, **kws)

    synced_func.__name__ = func.__name__

    return synced_func

class Singleton(type):
    '''
    A singleton class as suggested here:
    http://stackoverflow.com/questions/6760685/creating-a-singleton-in-python

    Usage:
    =====================================================

    In Python2:
    -----------
    class MyClass(BaseClass):
        __metaclass__ = Singleton

    In Python3:
    -----------
    class MyClass(BaseClass, metaclass=Singleton):
        pass
    '''
    _instances = {}

    def __call__(self, *args, **kwargs):
        if self not in self._instances:
            self._instances[self] = super(Singleton, self).__call__(*args, **kwargs)
        return self._instances[self]
