'''
Created on Mar 21, 2012

@author: eyal
'''
from datetime import datetime

def get_time_now():
    return '-'.join(str(datetime.now()).split('.')[0].split(' '))
