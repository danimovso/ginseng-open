'''
Created on Jul 27, 2012

@author: eyal
'''
import logging
import threading
from subprocess import Popen, PIPE
import shlex

def make_sendable_command_string(obj):
    """
    Make a sendable and executable command.
    """
    s = str(obj)
    for c in ["'", '"', "(", ")", "<", ">"]:
        s = s.replace(c, "\\" + c)
    return s

def get_command_line_args(cmd, make_sendable = False):
    # convert cmd to a list of arguments
    if isinstance(cmd, str):
        cmd = shlex.split(cmd)

    if not isinstance(cmd, list):
        raise ValueError("Command line was not a string or a list: %s" % cmd)

    if make_sendable:
        return map(make_sendable_command_string,cmd)
    else:
        return map(str,cmd)

def get_command_line_string(cmd):
    if isinstance(cmd, str):
        return cmd
    elif isinstance(cmd, list):
        return " ".join(map(str, cmd))
    else:
        raise ValueError("Command line was not a string or a list: %s" % cmd)

class ShellCommand:
    def __init__(self, cmd, as_root = False, make_sendable = False, **kwarg):
        self.cmd_args = get_command_line_args(cmd, make_sendable)

        if as_root:
            self.cmd_args = ["sudo"] + self.cmd_args

        self.cmd_str = get_command_line_string(self.cmd_args)

class run(ShellCommand):
    def __init__(self, cmd, as_root = False, **kwarg):
        ShellCommand.__init__(self, cmd, as_root = as_root,
                              make_sendable = False)

        kwarg["stdout"] = PIPE
        kwarg["stderr"] = PIPE

        out, err = Popen(self.cmd_args, **kwarg).communicate()

        self.out = out.strip()
        self.err = err.strip()

class run_shell(run):
    SHELL_CMD = ["sh", "-c"]

    def __init__(self, cmd, as_root = False, **kwarg):
        self.shell_cmd = get_command_line_string(cmd)

        run.__init__(self,
                     self.SHELL_CMD + [self.shell_cmd],
                     as_root = as_root,
                     **kwarg)

class run_on_server(run):
    def __init__(self, cmd, server, **kwarg):
        self.ssh_cmd = get_command_line_string(cmd)

        run.__init__(self,
                     ['ssh', server] + [self.ssh_cmd],
                     as_root = False,
                     **kwarg)

class run_with_cpus(run):
    def __init__(self, cmd, cpus = None, as_root = False, **kwarg):
        self.prog_cmd_args = get_command_line_args(cmd)
        self.set_cpus(cpus)

        run.__init__(self,
                     self.taskset_cmd_args + self.prog_cmd_args,
                     as_root = as_root,
                     **kwarg)

    def set_cpus(self, cpus):
        self.cpus = cpus
        self.cpus_bit_vector = 0
        self.taskset_cmd_args = []

        if self.cpus is not None:
            if not isinstance(self.cpus,list):
                self.cpus = [self.cpus]

            if len(self.cpus) > 0:
                for cpu in self.cpus:
                    self.cpus_bit_vector |= ( 1 << int(cpu) )

                self.taskset_cmd_args = ["taskset" ,"-a", hex(self.cpus_bit_vector)]

class append_to_file(run_shell):
    APPEND_CMD = "echo '%s' > %s"

    def __init__(self, file_name, data, as_root = False, **kwarg):
        self.data = make_sendable_command_string(data)
        self.append_cmd = self.APPEND_CMD % (self.data, file_name)

        run.__init__(self,
                     self.append_cmd,
                     as_root = as_root,
                     **kwarg)

class TimeoutError(Exception): pass

class SSHCommunicator(ShellCommand, threading.Thread):

    def __init__(self, cmd, user, ip, name = "SSH-Communicator",
                 output_file = None, verbose = True):
        ShellCommand.__init__(self, cmd, make_sendable = True)
        threading.Thread.__init__(self, name = name)

        self.verbose = verbose
        self.logger = logging.getLogger(name)

        # ssh as the user using the user's private key information - this hack avoids password prompt when running as root
        self.args = ["ssh", "%s@%s" % (user, ip)] + self.cmd_args
        self.popen = None
        self.output_file = output_file
        self.out = None
        self.err = None

    def run(self):
        if self.verbose:
            self.logger.info("Starting: %s", self.args)

        fo = file(self.output_file, "a") if self.output_file else PIPE

        self.popen = Popen(self.args, stdout = fo, stderr = fo)
        self.out, self.err = self.popen.communicate()

        if fo != PIPE:
            fo.close()

        if self.verbose:
            self.logger.info("Ended")

    def join(self, timeout = None):
        threading.Thread.join(self, timeout = timeout)
        if self.is_alive():
            raise TimeoutError("Join timeout")

    def communicate(self, timeout = None):
        self.start()
        self.join(timeout)
        if self.is_alive():
            self.terminate()
        return self.out, self.err

    def terminate(self):
        if self.popen:
            self.logger.warning("Killing ssh **********************************************************************")
            self.popen.kill()

if __name__ == "__main__":
    from mom.LogUtils import LogUtils
    LogUtils("info")
    try:
        comm = SSHCommunicator(["sleep", "3", ";", "echo", "1"], "eyal", "localhost").communicate(10)
        print comm
    except TimeoutError:
        print "Timeout"
        comm.terminate()
        print "Terminated"
    else:
        print "shouldn't be here..."
