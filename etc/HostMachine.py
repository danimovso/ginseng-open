'''
Created on Aug 13, 2014

@author: Liran Funaro <fonaro@cs.technion.ac.il>
'''
from etc.decorators import Singleton
from mom.LogUtils import LogUtils
import logging
from etc.shell import run, append_to_file
import socket
import re
import os
from timeit import itertools
import copy
import threading
from etc.decorators import synchronized, dict_represented

@dict_represented
class HostMachine(object):
    '''
    HostMachine is a singleton.
    Manage the host machine resources during single/multiple experiment(s).
    '''
    __metaclass__ = Singleton

    # lscpu -p : CPU,Core,Socket,Node,,L1d,L1i,L2,L3
    lscpu_structure = re.compile("^(?P<CPU>[0-9]+),(?P<Core>[0-9]+),(?P<Socket>[0-9]+),(?P<Node>[0-9]+),,?(,(?P<L1d>[0-9]+))?(,(?P<L1i>[0-9]+))?(,(?P<L2>[0-9]+))?(,(?P<L3>[0-9]+))?$", re.MULTILINE)

    def __init__(self, verbosity = "info", system_cpus_count = 2):
        '''
        Constructor is called only once (singleton)
        '''
        # Allows reentrant by the same thread
        self._lock = threading.RLock()

        LogUtils(verbosity)
        self.logger = logging.getLogger(self.__class__.__name__)

        self._active_experiments = set()

        self._init_cpus()

        self.set_system_cpus_count(system_cpus_count)

    @property
    @synchronized
    def as_dict(self):
        return dict(
                    hostname = self.hostname,
                    cpus_hierarcy = self.cpus_hierarchy,
                    system_cpus = self.system_cpus
                    )

    @classmethod
    def _sort_by(cls, dct_list, attr):
        return sorted(dct_list, key=lambda x: x[attr])

    @classmethod
    def _group_by(cls, dct_list, attr):
        return itertools.groupby(cls._sort_by(dct_list, attr), lambda x: x[attr])

    @classmethod
    def _group_by_cpus(cls, cpus, attr):
        ret = cls._group_by(cpus, attr)

        groups = {}

        for key, cpus in ret:
            groups[key] = list(cpus)

        return groups

    def group_by_cpus(self, attr):
        return self._group_by_cpus(self._cpus,attr)

    @classmethod
    def _multi_group_by(cls, dct_list, *keys, **kwargs):
        if len(keys) == 0:
            list_func = kwargs.setdefault("list_func",list)
            return list_func(dct_list)

        cpus_group = cls._group_by_cpus(dct_list, keys[0])

        return dict([ (key, cls._multi_group_by(cpus, *keys[1:], **kwargs)) for key, cpus in cpus_group.iteritems()])

    def multi_group_by(self, *keys, **kwargs):
        return self._multi_group_by(self._cpus,*keys,**kwargs)

    @classmethod
    def _subset(cls, dct_list, *keys):
        subset_of = keys[-1]
        group_by_keys = keys[:-1]
        return cls._multi_group_by(dct_list, *group_by_keys, list_func=lambda lst: sorted(set( [ dct[subset_of] for dct in lst] )) )

    def subset(self, *keys):
        return self._subset(self._cpus, *keys)

    @classmethod
    def _subset_count(cls, dct_list, *keys):
        return cls._multi_group_by(dct_list, *keys, list_func=len)

    def subset_count(self, *keys):
        return self._subset_count(self._cpus, *keys)

    def _init_cpus(self):
        self._cpus = self.fetch_cpus()

        self._cpus_hierarchy = self.subset("Node", "Socket", "Core", "CPU")

        self._machine_core_subset = {0: self.subset("Core")}
        self._node_core_subset =  self.subset("Node", "Core")
        self._socket_core_subset =  self.subset("Socket", "Core")
        self._core_core_subset = self.subset("Core", "Core")

        self._core_cpu_subset = self.subset("Core", "CPU")

        self._machine_availability = {0: len(self._cpus)}
        self._node_availability = self.subset_count("Node")
        self._socket_availability = self.subset_count("Socket")
        self._core_availability = self.subset_count("Core")
        self._cpu_availability = [True] * len(self._cpus)

        self._system_cpus = []

    @synchronized
    def _hold_cpu(self, cpu):
        if self._cpu_availability[cpu]:
            cpu_info = self._cpus[cpu]
            machine = 0
            node = cpu_info["Node"]
            socket = cpu_info["Socket"]
            core = cpu_info["Core"]

            self._machine_availability[machine] -= 1
            self._core_availability[core] -= 1
            self._socket_availability[socket] -= 1
            self._node_availability[node] -= 1
            self._cpu_availability[cpu] = False

    @synchronized
    def _release_cpu(self, cpu):
        if not self._cpu_availability[cpu]:
            cpu_info = self._cpus[cpu]
            machine = 0
            node = cpu_info["Node"]
            socket = cpu_info["Socket"]
            core = cpu_info["Core"]

            self._machine_availability[machine] += 1
            self._core_availability[core] += 1
            self._socket_availability[socket] += 1
            self._node_availability[node] += 1
            self._cpu_availability[cpu] = True

    @property
    def hostname(self):
        return socket.gethostname()

    @property
    @synchronized
    def active_experiments_count(self):
        return len(self._active_experiments)

    @property
    @synchronized
    def active_experiments(self):
        return copy.copy(self._active_experiments)

    @property
    @synchronized
    def system_cpus(self):
        return copy.deepcopy(self._system_cpus)

    @property
    def cpus(self):
        '''
        Retrieve the CPUs in the host machine with the following info:
            CPU, Core, Socket, Node, L1d, L1i, L2, L3
            arranged by CPU id
        '''
        return copy.deepcopy(self._cpus)

    @property
    def cpus_hierarchy(self):
        '''
        Retrieve the CPUs hierarchy in the host machine arranged by nodes then cores

        Example output: {
                            0: {                 # Node 0
                                0: {             # Socket 0
                                    0: [1, 2],   # Core 0
                                    1: [3, 4]    # Core 1
                                }
                            },
                            1: {                 # Node 1
                                1: {             # Socket 1
                                    2: [5, 6],   # Core 2
                                    3: [7, 8]    # Core 3
                                }
                            }
                        }
        '''
        return copy.deepcopy(self._cpus_hierarchy)

    @classmethod
    def fetch_cpus(cls):
        '''
        Retrieve the CPUs in the host machine with the following info:
            CPU, Core, Socket, Node, L1d, L1i, L2, L3
        '''
        lscpu_raw = os.popen("lscpu -p").read()

        cpus_info = [match.groupdict() for match in cls.lscpu_structure.finditer(lscpu_raw)]

        # Convert all the data to integer
        for cpu in cpus_info:
            for key in cpu:
                if cpu[key]:
                    cpu[key] = int(cpu[key])

        return sorted(cpus_info, key=lambda x: x["CPU"])

    @synchronized
    def filter_non_available_cpus(self, cpus):
        return filter(lambda cpu: self._cpu_availability[cpu],  cpus)

    @synchronized
    def set_system_cpus_count(self, system_cpus_count):
        if len(self._system_cpus) != system_cpus_count:
            self.release_cpus(self._system_cpus)
            self._system_cpus = self.auto_select_cpus(system_cpus_count)

    @synchronized
    def auto_select_cpus(self, cpu_count):
        '''
        Select CPUs from the available CPUs poll.
        The returned CPUs will not be available for the following calls.
        When an experiment ends, the CPUs expected to be return using release_cpus()

        Output: a list of CPUs (e.g. [0,2,...,8])
        '''
        if cpu_count <= 0:
            return self.system_cpus

        select_hierarchy = [
                    (self._core_availability, self._core_core_subset),
                    (self._socket_availability, self._socket_core_subset),
                    (self._node_availability, self._node_core_subset),
                    (self._machine_availability, self._machine_core_subset),
                    ]

        available_cores = None

        for subset_availability, subset_cores in select_hierarchy:
            if available_cores is not None:
                break

            for subset, availability in subset_availability.iteritems():
                if availability >= cpu_count:
                    available_cores = subset_cores[subset]
                    break

        if available_cores is None:
            self.logger.warn("Could not select CPUs. No cores available.")
            return []

        cpus = []

        # We will always select only full cores
        cores = []
        selected_cores_cpu_count = 0

        # Arrange cores from the most available cpus to the least
        available_cores = sorted(copy.deepcopy(available_cores), key=lambda core: -(self._core_availability[core]))

        for core in available_cores:
            if selected_cores_cpu_count >= cpu_count:
                # We already have what we need
                break

            if self._core_availability[core] == 0:
                continue

            cores.append( self.filter_non_available_cpus(self._core_cpu_subset[core]) )
            selected_cores_cpu_count += self._core_availability[core]

        # CPUs from the same core should be far apart
        while len(cpus) < cpu_count and len(cores) > 0:
            new_cores = []

            for core in cores:
                if core and len(cpus) < cpu_count:
                    cpus.append(core.pop(0))

                if core:
                    new_cores.append(core)

            cores = new_cores

        if len(cpus) < cpu_count:
            self.logger.warn("Could not select %s CPUs. Only %s available.", cpu_count, len(cpus))

        return self.select_cpus(cpus)

    @synchronized
    def select_cpus(self, cpus):
        '''
        Select a list of CPUs.
        This function will return the subset of CPUs that are available for use.
        The returned CPUs will not be available for the following calls.
        When an experiment ends, the CPUs expected to be return using release_cpus()
        '''
        available_cpus_subset = self.filter_non_available_cpus(cpus)

        if len(available_cpus_subset) < len(cpus):
            self.logger.warn("Could not select all of the requested CPUs. Only %s are available.", available_cpus_subset)

        for cpu in available_cpus_subset:
            self._hold_cpu(cpu)

        return available_cpus_subset

    @synchronized
    def release_cpus(self, cpus):
        '''
        Release the given CPUs back the available CPUs poll.
        '''
        for cpu in cpus:
            self._release_cpu(cpu)

    def drop_caches(self):
        sync = run("sync", as_root = True)

        if sync.err:
            self.logger.error("Fail to sync cache to secondary memory: %s", sync.err)
            return

        drop_caches = append_to_file("/proc/sys/vm/drop_caches", 3, as_root = True)
        if drop_caches.err:
            self.logger.error("Fail to drop caches: %s", drop_caches.err)
            return

        self.logger.info("Dropped caches %s %s",
                         ("[sync: %s]" % sync.out) if sync.out else "",
                         ("[drop: %s]" % drop_caches.out) if drop_caches.out else "")

    def disable_cron(self):
        stop_cron = run(["service", "cron", "stop"], as_root = True)

        if stop_cron.err:
            self.logger.error("Fail to disable cron: %s", stop_cron.err)
        else:
            self.logger.info("Disabled cron %s",
                             "[%s]" % stop_cron.out if stop_cron.out else "")

    def enable_cron(self):
        enable_cron = run(["service", "cron", "start"], as_root = True)

        if enable_cron.err:
            self.logger.error("Fail to enable cron: %s", enable_cron.err)
        else:
            self.logger.info("Enabled cron %s",
                             "[%s]" % enable_cron.out if enable_cron.out else "")

    @synchronized
    def begin_experiment(self, exp_obj):
        if self.active_experiments_count == 0:
            self.drop_caches()
            self.disable_cron()

        self._active_experiments.add(exp_obj)

    @synchronized
    def end_experiment(self, exp_obj):
        self._active_experiments.remove(exp_obj)

        if self.active_experiments_count == 0:
            self.enable_cron()
