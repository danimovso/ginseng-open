# Memory Overcommitment Manager
# Copyright (C) 2010 Adam Litke, IBM Corporation
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

from etc.mrow import ReadWriteLock

class EntityError(Exception):
    def __init__(self, message):
        self.message = message

class Entity:
    """
    An entity is an object that is designed to be inserted into the rule-
    processing namespace.  The properties and statistics elements allow it to
    contain a snapshot of Monitor data that can be used as inputs to rules.  The
    rule-accessible methods provide a simple syntax for referencing data.
    """
    def __init__(self, monitor = None):
        self.properties = {}
        self.variables = {}
        self.statistics = []
        self.controls = {}
        self.monitor = monitor
        self.__property_lock = ReadWriteLock()
        self.__variable_lock = ReadWriteLock()
        self.__statistics_lock = ReadWriteLock()
        self.__control_lock = ReadWriteLock()

    def _set_property(self, name, val):
        try:
            self.__property_lock.acquireWrite()
            self.properties[name] = val
        finally:
            self.__property_lock.release()

    def _set_variable(self, name, val):
        try:
            self.__variable_lock.acquireWrite()
            self.variables[name] = val
        finally:
            self.__variable_lock.release()

    def _set_statistics(self, stats):
        try:
            self.__statistics_lock.acquireWrite()
            for row in stats:
                self.statistics.append(row)
        finally:
            self.__statistics_lock.release()

    def _store_variables(self):
        """
        Pass rule-defined variables back to the Monitor for storage
        """
        if self.monitor is not None:
            self.monitor.update_variables(self.variables)

    # TODO: understand how to make this thread-safe
    def _finalize(self):
        """
        Once all data has been added to the Entity, perform any extra processing
        """
        # Add the most-recent stats to the top-level namespace for easy access
        # from within rules scripts.
        if len(self.statistics) > 0:
            for stat in self.statistics[-1].keys():
                setattr(self, stat, self.statistics[-1][stat])

    def _disp(self, name = ''):
        """
        Debugging function to display the structure of an Entity.
        """
        try:
            self.__statistics_lock.acquireRead()
            prop_str = ""
            stat_str = ""
            for i in self.properties.keys():
                prop_str = prop_str + " " + i

            if len(self.statistics) > 0:
                for i in self.statistics[0].keys():
                    stat_str = stat_str + " " + i
            else:
                stat_str = ""
            print "Entity: %s {" % name
            print "    properties = { %s }" % prop_str
            print "    statistics = { %s }" % stat_str
            print "}"
        finally:
            self.__statistics_lock.release()

    ### Rule-accessible Methods 
    def Prop(self, name):
        """
        Get the value of a single property
        """
        try:
            self.__property_lock.acquireRead()
            return self.properties[name]
        finally:
            self.__property_lock.release()    

    def Stat(self, name):
        """
        Get the most-recently recorded value of a statistic
        Returns None if no statistics are available
        """
        try:
            self.__statistics_lock.acquireRead()
            if len(self.statistics) > 0:
                return self.statistics[-1][name]
            else:
                return None
        finally:
            self.__statistics_lock.release()

    def StatAvg(self, name):
        """
        Calculate the average value of a statistic using all recent values.
        """
        try:
            self.__statistics_lock.acquireRead()
            if (len(self.statistics) == 0):
                raise EntityError("Statistic '%s' not available" % name)
            total = 0
            for row in self.statistics:
                total = total + row[name]
            return float(total / len(self.statistics))
        finally:
            self.__statistics_lock.release()

    def SetVar(self, name, val):
        """
        Store a named value in this Entity.
        """
        try:
            self.__variable_lock.acquireWrite()
            self.variables[name] = val
        finally:
            self.__variable_lock.release()

    def UpdateVars(self, vars):
        """
        takes a dictionary of variables and values and updates them all.
        """
        try:
            self.__variable_lock.acquireWrite()
            self.variables.update(vars)
        finally:
            self.__variable_lock.release()


    def GetVar(self, name):
        """
        Get the value of a potential variable in this instance.
        Returns None if the variable has not been defined.
        """
        try:
            self.__variable_lock.acquireRead()
            if name in self.variables:
                return self.variables[name]
            else:
                return None
        finally:
            self.__variable_lock.release()

    def Control(self, name, val):
        """
        Set a control variable in this instance.
        """
        try:
            self.__control_lock.acquireWrite()
            self.controls[name] = val
        finally:
            self.__control_lock.release()

    def GetControl(self, name):
        """
        Get the value of a control variable in this instance if it exists.
        Returns None if the control has not been set.
        """
        try:
            self.__control_lock.acquireRead()
            if name in self.controls:
                res = self.controls[name]
                return res
            else:
                return None
        finally:
            self.__control_lock.release()
