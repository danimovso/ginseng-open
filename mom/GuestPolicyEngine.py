import threading
import logging
from mom.Adviser import get_adviser
import time

class GuestPolicyEngine():
    """
    At a regular interval, this thread triggers system reconfiguration by
    sampling host and guest data, evaluating the policy and reporting the
    results to all enabled Controller plugins.
    """
    def __init__(self, config):
        self.config = config
        self.logger = logging.getLogger('GuestPolicyEngine')
        self.adviser = get_adviser(**eval(config.get("main", "adviser")))
        self.policy_lock = threading.Lock()

    def next_valuation(self):
        self.adviser.next_valuation()

    def get_valuation(self):
        return self.adviser.current_valuation

    def get_bill_estimate_mem(self, q):
        return self.adviser.get_last_bill_estimate_mem(q)

    def get_profit_estimate_mem(self, q):
        return self.adviser.get_last_profit_estimate_mem(q)
        
    def get_valuation_estimate_mem(self, q):
        return self.adviser.get_last_valuation_estimate_mem(q)
        
    def get_valuation_mem(self):
        return self.adviser.get_valuation_for_allocation_q()

    def do_controls(self, data):
        """
        Sample host and guest data, process the rule set and feed the results
        into each configured Controller.
        """
        if data is None:
            return None

        # evaluate
        results = ()
        with self.policy_lock:
            start = time.time()
            p, rq = self.adviser.advice(data)
            duration = time.time() - start
            return {'bid_round_mem': data['auction_round_mem'],
                    'bid_p_mem': p, 'bid_ranges_mem': rq,
                    "bid_duration": duration}

    def get_base_mem(self):
        return self.adviser.base_mem
