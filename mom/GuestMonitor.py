# Memory Overcommitment Manager
# Copyright (C) 2010 Adam Litke, IBM Corporation
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

import threading
import time
import re
import logging
from mom.Monitor import Monitor
import subprocess
from mom.Comm.GuestCommnicator import GuestCommunicator
from exp.core.VMDesc import get_vm_desc
from mom.Comm.GuestServer import GuestServer
from mom.Comm.Messages import MessageWelcome

class GuestMonitor(Monitor, threading.Thread):
    """
    A GuestMonitor thread collects and reports statistics about 1 running guest
    """
    def __init__(self, config, id, libvirt_iface):
        self.config = config
        self.libvirt_iface = libvirt_iface
        self.id = id
        self.logger = logging.getLogger("GuestMonitor-%s" % id)
        self.dom = self.libvirt_iface.getDomainFromID(id)

        self.info = self.get_guest_info()
        if self.info is None:
            raise ValueError(
                "GuestMonitor-id%i - failed to get information" % id)

        # thread's name is important to Plotter, must be:
        # guest_name-Monitor
        threading.Thread.__init__(self, name = "%s-Monitor" % self.info['name'])
        Monitor.__init__(self, config, self.info['name'])

        self.start()

    @property
    def _properties(self):
        properties = dict(self.info)
        name = properties['name']
        self.desc = get_vm_desc(name)
        if self.desc is None:
            raise ValueError(
                "GuestMonitor-%s - failed to get description" % name)

        ### add properties to guest:
        # get default memory allocation for specific guest
        try:
            base_mem = int(self.info['base_mem'])
        except KeyError:
            self.logger.warn("No base_mem in description file - Vm-Desc: %s" % self.desc)
            base_mem = 0

        properties.update({
            'id': self.id,
            'base_mem': base_mem,
            'libvirt_iface': self.libvirt_iface,
            'config' : self.config,
            })

        # add guest communicator
        ip = properties['ip']
        timeout = eval(self.config.get('communication', 'timeout'))
        comm_mem = None
        if self.config.has_section('mem'):
            comm_mem = GuestCommunicator(ip, GuestServer.port_mem, timeout, name)

        # communicate with client, tell it the current base_mem, so it can calculate
        # the difference, and reply with the new base_mem
        self.logger.info("Sending welcome message with memory: %i.", base_mem)
        while True:
            reply_mem = {}
            try:
                if comm_mem:
                    reply_mem = comm_mem.communicate(MessageWelcome(current_mem = base_mem))
            except Exception as e:
                self.logger.warning("Welcome message error, trying again %s", e)
                time.sleep(3)
                continue
            if comm_mem and len(reply_mem) == 0:
                self.logger.info("Empty welcome memory message reply from memory auction bidder")
                time.sleep(3)
                continue
            break

        reply = reply_mem
        # We could take both from the same one (use only reply_mem)
        properties['base_mem'] = reply["base_mem"] 
        self.logger.info("Guest %s requested base_mem %s MB", name, reply["base_mem"])

        if comm_mem:
            properties['comm_mem'] = comm_mem

        return properties

    def _collectors_list(self, config):
        return config.get('guest', 'collectors')

    def get_guest_info(self):
        """
        Collect some basic properties about this guest
        Returns: A dict of properties on success, None otherwise
        """
        if self.dom is None:
            return None
        data = {}
        data['uuid'] = self.libvirt_iface.domainGetUUID(self.dom)
        data['name'] = self.libvirt_iface.domainGetName(self.dom)
        data['pid'] = self.get_guest_pid(data['uuid'])
        data['base_mem'] = self.libvirt_iface.domainGetMemoryStats(self.dom)['actual'] >> 10
        if None in data.values():
            return None

        # The IP address is optional
        data['ip'] = self.get_guest_ip(data['name'])
        return data

    def run(self):
        self.logger.info("Started")
        interval = self.config.getint('main', 'guest-monitor-interval')
        self.logger.debug("monitor interval: %i sec", interval)
        while self.should_run:
            self.collect()
            time.sleep(interval)
        # close guest-communicator
        if self.properties.has_key('comm_mem'):
            self.properties['comm_mem'].close()  
        self.logger.info("Ended")

    def get_guest_pid(self, uuid):
        """
        This is an ugly way to find the pid of the qemu process associated with
        this guest.  Scan ps output looking for our uuid and record the pid.
        Something is probably wrong if more or less than 1 match is returned.
        """
        if bool(self.config.get('main', 'local-test').lower() == "true"):
            import os
            return os.getpid()

        p1 = subprocess.Popen(["ps", "axww"],
                              stdout = subprocess.PIPE).communicate()[0]
        matches = re.findall("^\s*(\d+)\s+.*" + uuid, p1, re.M)
        if len(matches) < 1:
            self.logger.warn("No matching process for domain with uuid %s", \
                             uuid)
            return None
        elif len(matches) > 1:
            self.logger.warn("Too many process matches for domain with uuid %s", \
                             uuid)
            return None
        return int(matches[0])

    def get_guest_ip(self, name):
        return name
#        output = None
#        if self.config.has_option('guest', 'name-to-ip-helper'):
#            prog = self.config.get('guest', 'name-to-ip-helper')
#            try:
#                output = subprocess.Popen([prog, name],
#                                          stdout = subprocess.PIPE).communicate()[0]
#            except OSError as strerror:
#                self.logger.warn("Cannot call name-to-ip-helper: %s", str(strerror))
#                output = None
#
#        if output is not None:
#            matches = re.findall("^(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})", output, re.M)
#            if len(matches) is 1:
#                ip = matches[0]
#                self.logger.info("Guest %s has IP address %s", name, ip)
#                return ip
#            else:
#                self.logger.warn("Output from name-to-ip-helper %s is not an IP " \
#                                 "address. (output = '%s')", name, output)
#
#        # If no success try to use xml description file and arp command to get ip
#        self.logger.info('Using arp to detect ip of %s' % name)
#        # get guest mac address
#        xml = self.libvirt_iface.domainGetXmlDesc(self.dom)
#        desc = etree.fromstring(xml)
#        try:
#            mac = desc.find("devices/interface[@type='network']/mac").attrib["address"].lower().strip()
#        except Exception:
#            self.logger.warn("guest %s: couldn't find mac address in xml description" % self.name)
#
#        # translate to ip through arm -n
#        output = subprocess.Popen(["arp", "-n"], stdout = subprocess.PIPE).communicate()[0]
#        lines = [line.split() for line in output.split("\n")[1:]]
#        ip = None
#        for line in lines:
#            if line and (line[2] == mac):
#                ip = line[0]
#                break
#
#        if ip is not None:
#            self.logger.info('Guest: %s, mac = %s, ip = %s' % (self.name, mac, ip))
#            return ip
#        else:
#            self.logger.warn("guest %s: couldn't find matching ip address using arp" % self.name)
