from subprocess import PIPE, Popen
from mom.Collectors.Collector import Collector, open_datafile
import logging

class HostCpu(Collector):

    def __init__(self, properties):
        self.logger = logging.getLogger("HostCPU")
        self.cpuinfo = open_datafile("/proc/stat")
        self.prev = self.get_usage_total()

    def __del__(self):
        if self.cpuinfo is not None:
            self.cpuinfo.close()

    def get_usage_total(self):
        self.cpuinfo.seek(0)
        contents = self.cpuinfo.read()
        cpu_lines = [line for line in contents.split("\n") if line.startswith("cpu")]
        dct = {}
        for line in cpu_lines:
            words = [eval(word) for word in line.split(" ")[1:] if len(word) > 0]

            name = line[:line.index(" ")]
            ind = name[3:] if len(name) > 3 else "tot"
            dct[ind] = (sum(words[0:3]), # not including idle and IO
                        sum(words[0:4]), # not including IO
                        sum(words[0:5])) # total

        return dct

    def collect(self):
        curr = self.get_usage_total()

        dct = {}
        for k in curr.iterkeys():
            try:
                dct["cpu_%s" % k] = (float(curr[k][0] - self.prev[k][0]) /
                                     (curr[k][1] - self.prev[k][1]))
            except ZeroDivisionError:
                dct["cpu_%s" % k] = 0
            try:
                dct["io_percent_%s" % k] = (float((curr[k][2] - curr[k][1]) -
                                                  (self.prev[k][2] - self.prev[k][1])) /
                                            (curr[k][2] - self.prev[k][2]))
            except ZeroDivisionError:
                dct["io_percent_%s" % k] = 0

        self.prev = curr
        return dct

    @staticmethod
    def getFields():
        cpu_count = eval(Popen("cat /proc/cpuinfo | grep processor | wc -l",
                    shell = True, stdout = PIPE).communicate()[0].strip())
        return set(["cpu_tot"] + ["cpu_%i" % i for i in range(cpu_count)] +
                   ["io_percent_%i" % i for i in range(cpu_count)])

def instance(properties):
    return HostCpu(properties)

if __name__ == "__main__":
    from time import sleep
    hc = HostCpu(None)
    print hc.getFields()
    for i in range(10):
        sleep(3)
        print hc.collect()
