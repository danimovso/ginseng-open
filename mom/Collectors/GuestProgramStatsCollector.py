
from mom.Collectors.Collector import *  #@UnusedWildImport
from subprocess import PIPE, Popen
from telnetlib import Telnet
import time

class GuestProgramStatsCollector(Collector):
    @staticmethod
    def getFields():
        return set(["prog_load", "prog_get_hits", "prog_get_total", "prog_get_hits_rate", "prog_throughput"])

class GuestMemcachedCollector(GuestProgramStatsCollector):
    def __init__(self, properties):
        self.tn = Telnet()
        self.connect()
        self.last_info = {"time": 0,
                          "cmd_get": 0,
                          "cmd_set": 0,
                          "get_hits": 0,
                          "get_misses": 0,
                          "evictions": 0,
                          "curr_connections": 0
                          }

    def __del__(self):
        self.tn.close()

    def connect(self):
        self.tn.open(host = "localhost", port = 11211)

    def collect(self):
        data = []
        for retry in range(2):  #@UnusedVariable
            try:
                self.tn.write("stats\n")
                data = self.tn.read_until("END").split("\n")
                break
            except EOFError:
                self.connect()
            except Exception:
                self.connect()
            data = []
        """
        example for data:
        STAT pid 14868
        STAT uptime 175931
        STAT time 1220540125
        STAT version 1.2.2
        STAT pointer_size 32
        STAT rusage_user 620.299700
        STAT rusage_system 1545.703017
        STAT curr_items 228
        STAT total_items 779
        STAT bytes 15525
        STAT curr_connections 92
        STAT total_connections 1740
        STAT connection_structures 165
        STAT cmd_get 7411
        STAT cmd_set 28445156
        STAT get_hits 5183
        STAT get_misses 2228
        STAT evictions 0
        STAT bytes_read 2112768087
        STAT bytes_written 1000038245
        STAT limit_maxbytes 52428800
        STAT threads 1
        END
        """
        info = self.last_info.copy()
        for d in data:
            try:
                k, v = d.split(" ")[1:3]
                info[k] = int(v.strip())
            except:
                pass
        # calculate the difference of the fields that are saved in the
        # self.last_info field.
        for k in self.last_info.keys():
            info["d_" + k] = info[k] - self.last_info[k]
            self.last_info[k] = info[k]

        # return the needed data:
        # if dt==0, we got no new data, thus return zero answer.
        dt = float(info["d_time"])
        return {
                "prog_load": info["curr_connections"],
                "prog_get_hits": info["d_get_hits"],
                "prog_get_total": info["cmd_get"],
                "prog_get_hits_rate": info["d_get_hits"] / dt,
                "prog_throughput": info["cmd_get"] / dt,
               } \
               if dt != 0 else dict((k, 0) for k in self.getFields())


def instance(properties):
    program_name = properties['config'].get('main', 'program')
    if program_name == "Memcached":
        return GuestMemcachedCollector(properties)

