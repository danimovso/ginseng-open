# monitors the current guest's valuation

from mom.Collectors.Collector import *
from mom.Comm.Messages import MessageGetValuation
import logging

#TODO: Maybe there should be one for every resource
class GuestValuation(Collector):
    def __init__(self, properties):
        Collector.__init__(self, properties)
        self.name = properties['name']
        self.logger = logging.getLogger('%s-GetValuation' % self.name)
        # use any communications channel available
        self.comm = properties['comm_mem' if 'comm_mem' in properties.keys() else 'comm_bw']
        self.msg = MessageGetValuation()

    def collect(self):
        try:
            data = self.comm.communicate(self.msg)
        except Exception as e:
            self.logger.warn("error in valuation querying: %s", e)
            data = {}
        finally:
            if 'current_valuation' in data:
                self.logger.debug("valuation: %i", data['current_valuation'])
            return data

    @staticmethod
    def getFields():
        return MessageGetValuation.response_keys

def instance(properties):
    return GuestValuation(properties)
