'''
Created on Jul 29, 2011

@author: eyal
'''
import logging
import socket
from mom.Collectors.HostMemory import HostMemory
from mom.Collectors.GuestCpu import GuestCpu
from mom.Collectors.GuestProgramStatsCollector import GuestProgramStatsCollector


def get_message(message_str):
    """
    Evaluate a message string, return the initiated message class with
    the received arguments
    TODO: SECURITY BREACH!!!!!!!
    """
    msg = eval(message_str)
    if not issubclass(msg.__class__, Message):
        raise ValueError("Got bad message! %s" % message_str)
    return msg


def sets_compare(actual, expected):
    missing = expected - actual
    redundant = actual - actual
    return missing, redundant


class MessageError(Exception): pass


class MessageTimeoutError(Exception): pass


class Message():
    """
    Abstract class for a message send-respond mechanism
    """
    # alloc_diff saves the allocation difference between the host and the guest
    # (what_host_sees - what_guest_sees).
    # it is initiated in the WelcomeMessage.
    # it is used to translate the memory amount in the communications.
    alloc_diff_mem = 0

    # message key are the keys sent from the host to the guest, response keys
    # are the keys that the guest response to the host.
    # those class fields can be overridden in each message class.
    # response - expected response key
    message_keys = set()
    response_keys = set(["ack"])

    def __init__(self, **kwrds):
        self.logger = logging.getLogger(self.name)
        # filter keyword arguments with expected message fields
        self.content = dict((k, kwrds[k]) for k in self.message_keys)

    @property
    def name(self):
        return self.__class__.__name__

    def __repr__(self):
        """
        Convert a message to string that could be sent through a connection.
        """
        return self.name + "(" + \
            ",".join([str(k) + "=" + str(v) for k, v in self.content.iteritems()]) + \
            ")"

    def communicate(self, client):
        """
        Send-response algorithm:
        Send this message through the connection to the client,
        receive data from the connection (blocking),
        then evaluate the received data and verify that it matches this
        message's response_keys set.
        @param client - of class Client (found in etc/PySock).
        @return: a dictionary whose keys are response_keys.
        """
        # communicate a message and wait for response
        # send current message by str(self) and wait for reply in result
        try:
            result = client.send_recv(repr(self))
        except socket.timeout:
            raise MessageTimeoutError()
        except socket.error as ex:
            raise MessageError("%s Communication Error: %s" % (self.name, ex))
        except Exception as ex:
            self.logger.error("Got Exception in communicate(): %s", ex)
            result = "{}"
        try:
            # TODO: SECURITY BREACH!!!!
            result = eval(result)
            assert isinstance(result, dict)
        except Exception as ex:
            raise MessageError("%s Problem in parsing response dict: %s, %s" %
                               (self.name, result, ex))

        # check expected response fields.
        missing, redundant = sets_compare(actual = set(result.keys()),
                                          expected = self.response_keys)
        if len(missing) > 0:
            self.logger.warn("Missing: %s in response", str(list(missing)))
            for key in missing:
                result[key] = None
        if len(redundant) > 0:
            self.logger.warn("Redundant: %s in response", str(list(redundant)))
            for key in redundant:
                del result[key]

        # check if ack is false
        if (result.has_key("ack") and not bool(result["ack"])):
            self.logger.warn("Other side did not understand message: %s",
                             str(self))

        # return expected dictionary
        self.logger.debug("got message: %s" % str(result))
        return result

    def process(self, data, monitor, policy):
        """
        @param data - a DictDefaultNone instance.
        @param monitor -  an Entity instance.
        Activated on the receiver's side to process the received message.
        """
        return {"ack": True}


class EchoMessage(Message): pass


class MessageWelcome(Message):
    """
    Host send an announce message
    Guest response with evaluated bid
    """
    message_keys = set(["current_mem"])
    response_keys = set(["base_mem"])

    def process(self, data, monitor, policy):
        """
        The guest evaluates its bid and returns it to the host.
        @param policy - used as the guest advisor (to determine how much the guest
                        should bid.)
        @return - dictionary that will be sent to the host
        """
        # update data with announced auction values
        self.logger.info("Got message with data: %s", str(self.content))
        mon_data = monitor.collect()
        self.logger.info("Monitor data: %s", str(mon_data))
        if "current_mem" in self.content and "mem_available" in mon_data:
            diff_mem = self.content["current_mem"] - mon_data["mem_available"]
            self.alloc_diff_mem = diff_mem
        else:
            self.logger.warn("diff_mem = 0")
            self.alloc_diff_mem = 0
        base_mem = policy.get_base_mem() + self.alloc_diff_mem
        return {"base_mem": base_mem}


class MessageStats(Message):
    """
    Host request from guest memory statistics
    Guest response with his statistics
    """
    response_keys = set.union(
                              HostMemory.getFields(),
                              GuestCpu.getFields(),
                              # GuestProgramStatsCollector.getFields(),
                              )

    def process(self, data, monitor, policy):
        """
        Guest collect statistics and send them back to host
        """
        mon_data = {}
        try:
            mon_data = monitor.collect()
        except Exception as e:
            self.logger.error("Error collecting data: %s", e)

        if mon_data is None:
            mon_data = {}

        ret = dict((k, mon_data[k]) for k in self.response_keys)
        if None in ret.values():
            self.logger.warn("Missing keys: %s in stats",
                             str([k for k, v in ret.iteritems() if v is None]))
        return ret


class MessageTargetMemory(Message):
    """

    """
    response_keys = set(["target_mem"])

    def process(self, data, monitor, policy):
        return {"target_mem": data["not_mem"]}


class MessageAnnounceMem(Message):
    """
    Host send an announce message
    Guest response with evaluated bid
    """
    message_keys = set(("auction_round_mem", "auction_mem", "base_mem",
                        "closing_time", "warmup_ended"))
    response_keys = set(("bid_round_mem", "bid_p_mem", "bid_ranges_mem",
                         "bid_duration"))

    def process(self, data, monitor, policy):
        """
        Guest evaluate bid and return to host
        """

        self.content["base_mem"] -= self.alloc_diff_mem

        # update data with announced auction values
        data.update(self.content)
        # get current status data
        data.update(monitor.collect())

        # evaluate the bid according to self data and auction data
        bid = policy.do_controls(data)

        # update data with bid
        data.update(bid)
        self.logger.debug("returned bid: %s", str(bid))
        return bid


class MessageNextValuation(Message):
    '''
    notify the guest that its valuation
    needs to change
    '''

    def process(self, data, monitor, policy):
        self.logger.debug("got switch to next valuation")
        policy.next_valuation()
        return Message.process(self, data, monitor, policy)


class MessageGetValuation(Message):
    '''
    query the guest's current valuation
    '''
    response_keys = set(('current_valuation',))
    def process(self, data, monitor, policy):
        ret = {}
        self.logger.debug("asked for my current valuation")
        ret['current_valuation'] = policy.get_valuation()
        ret['ack'] = True
        self.logger.debug("my current valuation: %i", ret['current_valuation'])
        return ret

class MessageNotifyMem(Message):
    """
    Host notify the guest on auction results
    Guest respond with ack (default)
    """

    message_keys = set(("not_round_mem", "not_bill_mem", "not_mem", "not_won_p_mem",
                        "not_tie_mem", "p_in_min_mem", "p_out_max_mem", "validity_time_mem"))
    response_keys = set(("bill_estimate_mem", "profit_estimate_mem", "valuation_estimate_mem", "valuation_mem"))

    def process(self, data, monitor, policy):
        #TODO: update policy.negotiator with p_in_min, p_out_max
        # guest update data with the host's notification

        self.content["not_mem"] -= self.alloc_diff_mem

        data.update(self.content)

        return {"bill_estimate_mem": policy.get_bill_estimate_mem(
            self.content["not_mem"]),
            "profit_estimate_mem": policy.get_profit_estimate_mem(
                self.content['not_mem']),
            "valuation_estimate_mem": policy.get_valuation_estimate_mem(
                self.content['not_mem']),
            "valuation_mem": policy.get_valuation_mem(),
            "ack": True}


class MessageExpHintLoad(Message):
    """
    Host Hint the guest on load for experiment use
    Guest respond with ack (default)
    """
    message_keys = set(("load",))

    def process(self, data, monitor, policy):
        data.update(self.content)
        return Message.process(self, data, monitor, policy)

class MessageNegotiate(Message):
    """
    One guest sends the other an offer.
    The other responds with ack and with an action:
     accept/reject/counter_offer.
    """
    # In the future, more keys can be added to the set, if they are needed for
    # other proposal types.
    message_keys = set(("sender_id", "proposal_type",
                        "direction", "compensation_sum"))
    response_keys = set(("ack", "action"))

    def process(self, data, monitor, policy):
        action = policy.do_negotiate(self.content)
        if action is None:
            return {"ack": False}
        return {"ack": True, "action": action}
