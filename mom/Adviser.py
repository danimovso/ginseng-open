'''
Created on Aug 15, 2011

@author: eyal
'''
import logging
from mom.Profiler import fromxml
import sys
import time
import numpy as np
import os
from etc.HistoryWindow import HistoryWindow

p_digits = 5
p_eps = 10 ** (-p_digits)
p_eq = lambda a, b: np.abs(np.asarray(a) - np.asarray(b)) <= p_eps

NoCosts = True


def get_adviser(**argv):
    if isinstance(argv['profiler'], str):
        argv['profiler'] = fromxml(argv['profiler'], argv['advice_entry'])

    if isinstance(argv['rev_func'], str):
        argv['rev_func'] = eval(argv['rev_func'])
    elif isinstance(argv['rev_func'], list):
        argv['rev_func'] = map(lambda f: eval(f) if isinstance(f, str) else f,
                               argv['rev_func'])

    module = __import__("mom.Advisers." + argv['name'], fromlist=argv['name'])
    adviser = getattr(module, argv['name'])
    if not argv.has_key('base_mem'):
        argv['base_mem'] = 600
    if not argv.has_key('memory_delta'):
        argv['memory_delta'] = 10

    argv['base_mem'] = int(argv['base_mem'])
    argv['memory_delta'] = int(argv['memory_delta'])
    argv['advice_entry'] = str(argv['advice_entry'])
    del argv['name']
    return adviser(**argv)


class HistoryItem:
    def __init__(self, rnd, state):
        self.round = rnd
        self.state = state
        self.results_added = False


class HistoryItemMem(HistoryItem):
    def __init__(self, rnd, state, base_mem, bid_p, bid_rq):
        HistoryItem.__init__(self, rnd, state)
        self.base_mem = int(base_mem)
        self.bid_p = round(bid_p, p_digits)
        self.bid_rq = bid_rq
        self.bill = 0
        self.won_mem = 0
        self.won_p = 0
        self.tie_winner = False
        self.p_in_min = 0
        self.p_out_max = 0

    def add_results(self, data):
        if self.results_added:
            return
        # total bill received for the round
        if abs(data['not_bill_mem']) < p_eps:
            self.bill = 0
        else:
            self.bill = data['not_bill_mem']
        self.won_mem = data['not_mem']  # total memory achieved in the round
        self.won_p = data['not_won_p_mem']
        self.tie_winner = data['not_tie_mem']  # was the guest a tie winner?
        self.p_in_min = data['p_in_min_mem']
        self.p_out_max = data['p_out_max_mem']
        self.results_added = True

    @property
    def won_q(self):
        try:
            return self.won_mem - self.base_mem
        except AttributeError:
            return 0

    def to_str(self):
        ret = "{"
        ret += "'round': " + str(self.round) + ", "
        ret += "'state': " + str(self.state) + ", "
        ret += "'base_mem': " + str(self.base_mem) + ", "
        ret += "'bid_p': " + str(self.bid_p) + ", "
        ret += "'bid_rq': " + str(self.bid_rq) + ", "
        ret += "'results_added': " + str(self.results_added) + ", "
        if self.results_added:
            ret += "'bill': " + str(self.bill) + ", "
            ret += "'won_mem': " + str(self.won_mem) + ", "
            ret += "'won_p': " + str(self.won_p) + ", "
            ret += "'tie_winner': " + str(self.tie_winner) + ", "
            ret += "'p_in_min': " + str(self.p_in_min) + ", "
            ret += "'p_out_max': " + str(self.p_out_max) + ", "
        ret += "}"
        return ret

    def __str__(self):
        return self.to_str()


class Adviser():
    def __init__(self, profiler, base_mem, advice_entry, memory_delta, rev_func,
                 **kwargs):
        self.logger = logging.getLogger("Adviser")
        self.profiler = profiler
        self.base_mem = base_mem
        self.entry = advice_entry
        self.d_mem = memory_delta
        if isinstance(rev_func, list):  # multiple valuation
            self.logger.debug("got %i valuation functions", len(rev_func))
            self.V_funcs = map(lambda f: np.vectorize(f), rev_func)
        else:  # single valuation
            self.V_funcs = [np.vectorize(rev_func)]
        self.current_valuation = 0
        self.rnd_mem = -1
        self.last_mem = -1
        self.load = -1
        self.history_window_size = 1
        self.hist_mem = HistoryWindow(self.history_window_size)
        self.ps = {0: 0}
        self.last_bill_estimate_mem = {}
        self.last_profit_estimate_mem = {}
        self.last_valuation_estimate_mem = {}

    def advice(self, data):
        self.logger.debug("advice() called")
        # get advising state:
        self.rnd_mem = int(data['auction_round_mem'])
        base_mem = int(data['base_mem']) if data['base_mem'] is not None else 0
        load = int(data['load'])
        self.load = load
        try:
            if data['warmup_ended']:
                self.logger.debug("Warmup is over, resetting")
                self.reset_mem()
        except KeyError:
            pass
        # add last round auction results to the history
        if (self.hist_mem.len() > 0) and (
                    self.hist_mem[0].round == data['not_round_mem']):
            self.hist_mem[0].add_results(data)
        self.logger.debug("History in round_mem %i:\nmem: %s\n", self.rnd_mem,
                          [h.to_str() for h in self.hist_mem.dump()])
        # state for the next bid: a vector of parameters describing the current
        # machine state.
        # in our case we use load ,memory and bandwidth, assuming constant load, and next
        # base memory and bandwidth allocation received from the auctioneer.
        state = [load, base_mem]
        # == call derived class advice function ==
        auction_mem = int(data['auction_mem'])
        bid_p, bid_rq = self.do_advice_mem(state, auction_mem)
        self.logger.info("round %i) state: %s, bid: [%s, %s]",
                         self.rnd_mem, str(state), str(bid_p), str(bid_rq))
        # add bid to history and keep history in the proper size
        if (self.hist_mem.len() == 0) or \
                        self.hist_mem[0].round != self.rnd_mem:
            self.hist_mem.add(HistoryItemMem(self.rnd_mem, state, base_mem,
                                             bid_p, bid_rq))
        self.last_mem = self.rnd_mem
        return bid_p, bid_rq

    def next_valuation(self):
        '''
        Updates the current valuation. If the valuation was changed,
        the history is reset
        @return: The index of the new valuation
        '''
        new_valuation = (self.current_valuation + 1) % len(self.V_funcs)
        if new_valuation != self.current_valuation:
            self.logger.debug("Valuation was changed. Resetting")
            self.reset_mem()
            self.current_valuation = new_valuation
        return new_valuation

    def get_last_profit_estimate_mem(self, q):
        return self._value_for_q(self.last_profit_estimate_mem, q)

    def get_last_bill_estimate_mem(self, q):
        return self._value_for_q(self.last_bill_estimate_mem, q)

    def get_last_valuation_estimate_mem(self, q):
        return self._value_for_q(self.last_valuation_estimate_mem, q)
        
    def get_valuation_for_allocation_q(self):
        mem_valuation = 0
        if (self.rnd_mem == 0):
            return mem_valuation
        else:
            last = self.hist_mem[0]
            if last.results_added:
                won_mem = last.won_mem
                base_mem = last.base_mem
                mem_valuation = self.V_func(self.profiler.interpolate([self.load, base_mem+won_mem, 0]))
                self.logger.debug("round %i V: %s", self.rnd_mem, str(mem_valuation))
            return mem_valuation

    def _value_for_q(self, src, q):
        base_mem = 0
        if self.hist_mem.len() > 0:
            base_mem = self.hist_mem[0].base_mem
        q -= base_mem
        if q in src:
            self.logger.debug("data for allocation %i: %.05f", q,
                              src[q])
            return src[q]
        else:
            try:
                qs = src.keys()
                ind = np.argmin(map(lambda x: abs(x - q), qs))
                self.logger.debug("closest quantity: %i, estimate: %.05f",
                                  qs[ind], src[qs[ind]])
                return src[qs[ind]]
            except ValueError:
                return 0

    def reset_mem(self):
        self.hist_mem.clear()
        self.do_reset_mem()

    def do_reset_mem(self):
        '''
        Override if further reset actions are required
        '''
        pass

    @property
    def V_func(self):
        return self.V_funcs[self.current_valuation]

    def do_advice_mem(self, state, auction_mem):
        """
        This function should be override to apply new advising methodologies
        @param state: a vector describing the machine parameters.
        @param auction_mem: the amount of memory available for auction.
        """
        raise NotImplemented

    def is_first_round_mem(self):
        '''
        @return: true if no data available for last round
        '''
        return (not self.hist_mem or self.hist_mem.len() == 0) or (self.last_mem == -1 or
                self.rnd_mem != self.last_mem + 1 or
                (self.hist_mem and self.hist_mem.len() > 0 and self.hist_mem[
                    0].round != self.last_mem))


if __name__ == "__main__":
    import numpy as np
    import pylab as pl
    from mom.LogUtils import LogUtils
    from etc.Settings import Settings

    LogUtils("debug")

    user_dir = Settings.user_home()

    adv_mcd = get_adviser(name="AdviserProfit",
                          profiler="%s/moc/doc/profiler-memcached-inside-spare-50-win-500k-tapuz25.xml" % user_dir,
                          rev_func=[lambda x: x, lambda x: x ** 2],
                          advice_entry="hits_rate")

    adv_mc = get_adviser(name="AdviserProfit",
                         profiler="%s/moc/doc/profiler-mc-spare-50-satur-2000-tapuz21.xml" % user_dir,
                         rev_func=lambda x: 15 * x,
                         advice_entry="hits_rate")

    base = 645
    loads_mc = (1, 10)
    load_mcd = 8
    state_lo = (loads_mc[0], base)
    state_hi = (loads_mc[1], base)
    state_mcd = (load_mcd, base)
    auction_mem = 1300

    for i in range(2):
        notification = {'auction_round_mem': i, 'auction_mem': auction_mem,
                        'base_mem': base, 'load': load_mcd, 'not_round_mem': i}
        print "MCD advice: ", adv_mcd.advice(notification)
    # print "MC advice: ", adv_mc.do_advice(state_lo, auction_mem)

    ps = np.arange(0, 10, 0.5)
    loads = [1, 10]  # range(1, 11)
    auction_mem = 1650
    base = 600
    for last_won_p in ps:
        vals = []
        for p_tar in ps:
            adv = get_adviser(name="AdviserProfit",
                              profiler="%s/moc/doc/profiler-memcached-inside-spare-50-win-500k-tapuz21.xml" % user_dir,
                              rev_func=lambda x: x, base_mem=600,
                              advice_entry="hits_rate", memory_delta=10)
            adv.won_p_hist = [last_won_p]
            adv.p_tar_hist = [p_tar]
            p, qr = adv.do_advice_mem((8, base), auction_mem)
            print "load %i, bid: %s, mem: %i" % (8, (p, qr), qr[0][1] + base)
            vals.append(p)  # qr[-1][1])
        pl.plot(ps, vals,
                c=pl.cm.jet(float(last_won_p) / ps[-1]))  # @UndefinedVariable
    pl.show()

    base = 600
    load = 8

    adv = get_adviser(name="AdviserProfit",
                      profiler="%s/moc/doc/profiler-memcached-spare-50-win-100k.xml" % user_dir,
                      rev_func=lambda x: x, base_mem=base,
                      advice_entry="hits_rate", memory_delta=10)
    #    adv.last = 0
    #    adv.rnd = 1
    #    adv.hist_mem = [HistoryItem(0, [load, base], 400, 2.39, [(0, 1600)])]
    #    adv.won_p_hist = [2.31]
    #    adv.p_tar_hist = [1.26]
    p, qr = adv.do_advice_mem((load, base), 2000)
    print "load %i, bid: %s, mem: %i" % (10, (p, qr), qr[0][1] + base)
#
##    print all(np.array(ps[1:]) >= np.array(ps[:-1]))
