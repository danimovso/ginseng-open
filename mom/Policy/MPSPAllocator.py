'''
Created on Aug 22, 2012

@author: eyal
'''
import collections
import numpy as np


# thrown when the allocation cannot be satisfied due to
# bad preallocation
class AllocationError(Exception):
    pass


class MPSPBid(collections.namedtuple("MPSPBid", "name p rqs parent")):
    '''
    @brief An MPSP bid descriptor
    @param name guest name
    @param p offered price
    @param rqs a list of allowed ranges (as [r,q] tuples)
    @param parent the parent of the bid when splitting the bill
    '''
    @property
    def is_parent(self):
        return self.parent is None

    @property
    def max_q(self):
        return self.rqs[-1][-1]

    @property
    def min_r(self):
        return self.rqs[0][0]

    def __str__(self):
        return "%s: p=%.02f, rqs=%s" % (self.name, self.p, self.rqs)


def social_welfare(p, alloc):
    return sum(np.asarray(p) * np.asarray(alloc))

# debug stub: print a bid in a readable manner
#print_bid = lambda b: "%s:%.02f:%s <- %s" % (b.name, b.p, b.rqs, print_bid(b.parent)) if b is not None else 'o'
#print_bids = lambda bids: map(print_bid, bids)

def deep_copy_bid(b):
    parent = None
    if b.parent is not None:
        parent = deep_copy_bid(b.parent)
    return MPSPBid(str(b.name), b.p, list(b.rqs), parent)


def deep_copy_bids(bids):
    return map(deep_copy_bid, bids)


def allocate(bids, qmax, extra_prev):
    """
    first call for _rec_allocate
    @param bids the bids being allocated. must be monotonic not decreasing by
    their p value
    @param qmax the allocated quantity
    @param extra_prev the previous allocation
    @return extra allocation, social welfare
    """
    if len(bids) == 0:
        return [], 0

    ps = np.array([b.p for b in bids], np.float)
    assert all(ps[:-1] - ps[1:] >= 0), "Not descending p order: %s" % str(ps)

    extra, sc = _rec_allocate(bids, qmax, prealloc = [0] * len(bids),
                              sc_min = 0, extra_prev=extra_prev)

    assert all([e >= 0 for e in extra])
    assert sc >= 0

    return list(extra), sc

def _rec_allocate(bids, qmax, prealloc, sc_min, extra_prev):
    '''
    Recursive allocation for ordered bids list
    @param bids: a list of ordered MPSPBids
    '''
    bids = deep_copy_bids(bids)
    prealloc = prealloc[:]
    
    # allocate memory with the ordered bidders
    try:
        alloc, split = _allocate(bids, qmax, prealloc, extra_prev)
    except AllocationError as e:
        return [0] * len(bids), 0

    # calculate the social welfare of the allocation
    W = social_welfare([b.p for b in bids], alloc)

    ## In case of a valid allocation:
    # no farther search is need to be done:
    if split == False:
        return alloc, W

    ## In case of invalid allocation:

    # split the bid for two bids:
    i, r = split
    original = deep_copy_bid(bids[i])

    bids_orig = deep_copy_bids(bids)
    bid_above, bid_below = split_bid(original, r, common_parent=bids[i].parent)


    prealloc_orig = prealloc[:]
    # 1nd case:
    prealloc[i] += r
    # if the bid above is a child, we must also preallocate m to the parent
    # we also remove its intervals to avoid further allocations to it
    if not bid_above.is_parent:
        prnt = bids.index(bid_above.parent)
        prealloc[prnt] += extra_prev[bid_above.name]
        _replace_bid(bids, prnt, MPSPBid(bids[prnt].name, bids[prnt].p, [(0, 0)], None))
    _replace_bid(bids, i, bid_above)
    alloc1, w1 = _rec_allocate(bids, qmax, prealloc, sc_min, extra_prev)

    # 2nd case:
    bids = bids_orig
    prealloc = prealloc_orig
    _replace_bid(bids, i, bid_below)
    for j, b in enumerate(bids):
        if b.parent == bid_below:
            _replace_bid(bids, j, MPSPBid(b.name, b.p, [(0, 0)], bid_below))
    alloc2, w2 = _rec_allocate(bids, qmax, prealloc, max(sc_min, w1), extra_prev)

    # return the best allocation of two sons
    return ((alloc1, w1), (alloc2, w2))[np.argmax((w1, w2))]

def _allocate(bids, total, prealloc, extra_prev):
    '''
    allocates by order of received bids.
    @param bids: a list of MPSPBids
    @param mem: total amount of memory for auction
    @return: arrays representing the allocation and the idex of the bidder
             being splitter because of it's r value
    '''
    # apply pre-allocation to current allocation
    alloc = prealloc[:]
    # remove pre-allocated memory from total
    remain = total - sum(prealloc)
    # check if pre-allocation is acceptable
    if remain < 0:
        raise AllocationError("More preallocated than total memory")
    split = False  # indicates one bidder with not maximal allocation value
    for i, bid in enumerate(bids):
        # stop allocating when out of memory
        if remain <= 0:
            break
        if bids[i].p < 0:
            continue
        # if this is a child, we make sure the parent was allocated
        # with enough memory. if there is not enough to fill the parent
        # and satisfy the minimal valid amount for the child, we skip it
        if not bids[i].is_parent:
            name = bid.name
            prnt = bids.index(bids[i].parent)
            if 0 == bid.min_r == bid.max_q:
                continue
            if alloc[prnt] < extra_prev[name]:
                if remain >= extra_prev[name] - alloc[prnt]:
                    remain -= extra_prev[name] - alloc[prnt]
                    alloc[prnt] = extra_prev[name]
                else:
                    alloc[prnt] += remain
                    if not is_alloc_allowed(alloc[prnt], bids[prnt].rqs):
                        # this ensures the child bids won't be examined
                        # when capping everything above this point.
                        # the parent will be preallocated with this
                        # amount when investigating the bids above
                        split = prnt, extra_prev[name]
                    remain = 0
                    continue
        add = 0
        # if guest maximal allocation is less than the available,
        # give it the maximal allocation
        if remain >= bid.max_q:
            add = bid.max_q
        # else give all remain memory to the current guest
        # if it is not in an allowed range, split the guest to the
        # minimum memory above the remain memory, and return an 'invalid'
        # allocation
        else:
            if not is_alloc_allowed(remain, bid.rqs):
                split = i, min_r_above(remain, bid.rqs)
            add = remain
        alloc[i] += add
        remain -= add
    return alloc, split


def _replace_bid(bids, i, new_bid):
    if new_bid is None:
        return
    prnt = bids[i]
    bids[i] = new_bid
    for j, b in enumerate(bids):
        if b.parent == prnt:
            _replace_bid(bids, j, MPSPBid(b.name, b.p, b.rqs, bids[i]))

def split_bid(bid, split_at, child_p_reduction=0, common_parent=None):
    """
    split the bid into two bids:
    parent, who's rq list is up to the last extra memory of the player.
    and child, whose rq list is from the last extra memory of the player,
    and its unit price is lowered by p0.
    """

    # construct the new rq list
    below = []
    above = []
    for r, q in  bid.rqs:
        if r == q == split_at:
            # annoying corner case: in this case we
            # want to ignore the interval, because the next clause will add
            # the original bid as is as the bid below, and this will cause an
            # infinite recursion in the allocation algorithm, because it will
            # retry the same bid over and over again. the part below is
            # considered in the preallocation, so it is safe to ignore the bid
            # in this stage. on the other hand, if force_parent is off, it means
            # we want to split the bid for the affine maximizer reduction.
            # ignoring the bid will result in completely ignoring it in the
            # allocation, so we must not ignore interval.
            pass
        elif q <= split_at:
            # r <= q <= split
            below.append((r, q))
        elif r < split_at:
            # r < split < q
            below.append((r, split_at))
            above.append((0, q - split_at))
        else:
            above.append((r - split_at, q - split_at))

    # if rq list is empty, add zero rq list
    if len(below) == 0: below = [(0, 0)]
    if len(above) == 0: above = [(0, 0)]

    # create the new bids:
    parent = MPSPBid(bid.name, bid.p, below, common_parent)
    child = MPSPBid(bid.name, bid.p - child_p_reduction, above, common_parent)
    return child, parent


def is_alloc_allowed(alloc, rqs):
    return any([r <= alloc <= q for r, q in rqs])


def min_r_above(alloc, rqs):
    return min(filter(lambda r: alloc < r, map(lambda x: x[0], rqs)))


def max_possible(alloc, rqs):
    if is_alloc_allowed(alloc, rqs):
        return alloc
    qs = filter(lambda q: q <= alloc, map(lambda x: x[1], rqs))
    new_alloc = max(qs) if len(qs) > 0 else 0
    assert new_alloc <= alloc
    return new_alloc
