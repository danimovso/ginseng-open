from mom.Adviser import p_eps, p_digits
from mom.Controllers.Announcer import Announcer
from mom.Controllers.Notifier import Notifier
import logging
from random import shuffle
from mom.Policy.MPSPAllocator import allocate, MPSPBid, deep_copy_bid
import time
from collections import defaultdict
from mom.LogUtils import LogUtils
from etc.HistoryWindow import NumericHistoryWindow
from math import log10

list_wo = lambda lst, i: lst[:i] + lst[i + 1:]


def list_wo2(lst, i, j):
    ret = lst[:i] + lst[i + 1: j] + lst[j + 1:]
    if len(ret) == 0:
        ret = [MPSPBid('vm-1', 0.0, [(0, 0)], None)]
    return ret


GName = lambda b: b.Prop("name") if b.GetVar("parent") is None else b.GetVar(
    "parent").Prop("name") + "+"


class Auctioneer(object):
    total_bills = defaultdict(int)

    def __init__(self, verbosity="debug", simulate=False):
        # TODO: _auction_type defaults to memory, because the auctioneer is
        # started in auction.rules (a LISP script in doc/). this goes through
        # mom/Policy/Parser.py, which constructs a new auctioneer object
        LogUtils(verbosity)
        self.logger = logging.getLogger(self.__class__.__name__)
        self.simulate = simulate
        self.announcer = Announcer(None, verbosity=verbosity)
        self.notifier = Notifier(None)
        self.bid_collection_interval = None
        self.calculation_interval = None
        self.notification_interval = None
        self.total_bills = None
        self.extra_prev = None
        self.p_in_min = None
        self.p_out_max = None
        self.base_reduction = None
        self.p0 = None
        self.unit_sw_hist = None
        self.last_alternate = None
        self.stable_rounds = None
        self.warmup_rounds = None
        self.reset_mem()

    def reset_mem(self):
        self.total_bills = defaultdict(int)
        self.extra_prev = defaultdict(int)
        self.p_in_min = 0
        self.p_out_max = 0
        self.base_reduction = 0
        self.p0 = 0
        self.unit_sw_hist = NumericHistoryWindow(50)
        self.last_alternate = defaultdict(int)
        self.stable_rounds = 1

    def wait_for_time(self, seconds_to_wait, start):
        if self.simulate: return
        current_wait = time.time() - start

        if current_wait < seconds_to_wait:
            time.sleep(seconds_to_wait - current_wait)

    def set_auction_parameters(self, host, name):
        param = host.Prop('config').getfloat('host', name)
        host.SetVar(name, param)
        return param

    # called in auction.rules
    def auction(self, host, bidders):
        t0 = time.time()
        t = 0

        self.bid_collection_interval = self.set_auction_parameters(host,
                                                                   'mem-bid-collection-interval')
        self.calculation_interval = self.set_auction_parameters(host,
                                                                'mem-calculation-interval')
        self.notification_interval = self.set_auction_parameters(host,
                                                                 'mem-notification-interval')
        self.p0 = self.set_auction_parameters(host, 'p0-percent')

        # check if we need to reset
        self.logger.debug("Checking reset timer")
        host.SetVar('warmup_ended', False)
        if self.warmup_rounds is None:
            self.logger.debug("Getting initial warmup reset counter")
            self.warmup_rounds = self.set_auction_parameters(host,
                                                             'warmup_rounds')
        else:
            self.logger.debug("Updating warmup reset counter")
            self.warmup_rounds -= 1
            self.warmup_rounds = max(self.warmup_rounds, -1)
        if self.warmup_rounds >= 0:
            self.logger.debug("Rounds till reset: %i", self.warmup_rounds)
            if self.warmup_rounds == 0:
                self.logger.debug("Warmup ended, resetting auction")
                self.warmup_rounds = -1
                self.reset_mem()
                host.SetVar('warmup_ended', True)
        else:
            # do not reset more than once
            host.SetVar('warmup_ended', False)

        # base reduction factor
        self.base_reduction = self.unit_sw_hist.average()

        # set extra_prev variable
        for b in bidders:
            extra_prev = self.extra_prev[b.Prop('name')]
            b.SetVar("alloc_prev_mem", extra_prev + b.Prop("base_mem"))

        # announce an auction
        if not self.simulate:
            try:
                self.announcer.process(host, bidders)
            except Exception as ex:
                self.logger.error("exception in announcer: %s", ex)
                raise

        t += self.bid_collection_interval
        self.wait_for_time(t, t0)

        # calculate results
        try:
            start_time = time.time()
            self.do_auction_mem(host, bidders)
            end_time = time.time()
            host.SetVar("auction_time_mem", end_time - start_time)
            self.logger.debug("auction_time_mem: %f" % (end_time - start_time))
        except Exception as ex:
            self.logger.error("exception in do_auction: %s", ex)
            raise

        t += self.calculation_interval
        self.wait_for_time(t, t0)

        # send notification back to bidders on results
        if not self.simulate:
            try:
                self.notifier.process(host, bidders)
            except Exception as ex:
                self.logger.error("exception in notifier: %s", ex)
                raise

        t += self.notification_interval
        self.wait_for_time(t, t0)
        self.logger.debug("Overall auction time is %f", time.time() - t0)

    def split_bid(self, bid, split_at, child_p_reduction=0):
        """
        split the bid into two bids:
        parent, whose rq list is up to the last extra memory of the player.
        and child, who's rq list is from the last extra memory of the player,
        and its unit price is lowered by p0.
        """
        # construct the new rq list
        below = []
        above = []
        for r, q in bid.rqs:
            if q <= split_at:  # r<=q<=s
                below.append((r, q))
            elif split_at < r:  # s<r<=q
                above.append((r - split_at, q - split_at))
            else:  # r<=s<q
                below.append((min(r, split_at), split_at))
                above.append((0, q - split_at))

        sanity_check = lambda rq: rq[0] <= rq[1] and rq[0] >= 0
        assert all(map(sanity_check, below + above)), "illegal ranges"
        assert all(map(lambda rq: rq[1] <= split_at,
                       below)), "below has a part above split point"
        # if s is a valid allocation, we want it to be considered.
        # this allows us to allocate the parent with enough memory
        # before we allocate the child.
        # we can't do that if s in an invalid allocation, because
        # then it adds a new allowed range which is in fact forbidden
        if any(map(lambda rq: rq[0] == 0, above)) and not any(
                map(lambda rq: rq[1] == split_at, below)):
            below.append((split_at, split_at))

        # if rq list is empty, add zero rq list
        if len(below) == 0: below = [(0, 0)]
        if len(above) == 0: above = [(0, 0)]

        # create the new bids:
        parent = MPSPBid(bid.name, bid.p, below, None)
        child = MPSPBid(bid.name, bid.p - child_p_reduction, above, parent)

        return parent, child

    def sort_bids_mem(self, bids):
        # compute bidders extra memory allocations
        # sort bidders for allocation preference
        # random shuffle
        shuffle(bids)
        # sort by last allocation, with less preference to children
        if len(self.extra_prev) != 0:  # first time: no previous allocation
            bids.sort(key=lambda b: self.extra_prev[b.name], reverse=True)
        # sort such that the parents will be before the children
        bids.sort(key=lambda b: 0 if b.is_parent else 1)
        # sort by p value
        bids.sort(key=lambda b: b.p, reverse=True)

    def _compare_allocations(self, current, new, exclude=None):
        '''
        Compares two allocations and returns true iff they are equal
        @param current,new the allocations to compare: guest=>allocation
        @param exclude guest to exclude (by name). If None, no one is excluded
        dictionaries
        '''
        for k in filter(lambda g: g != exclude,
                        set(current.keys() + new.keys())):
            try:
                if current[k] != new[k]:
                    return False
            except KeyError:  # if guest is missing in one of the allocations
                return False
        return True

    def _validate_allocation(self, auction_round, bidders, alloc, exclude=None):
        '''
        Checks if the given allocation is feasible
        according to the current bidders state
        @param bidders the bidders
        @param alloc the allocation examined: guest=>allocation dictionary
        @param exclude guest to exclude (if None, all will be included)
        @return True iff the allocation is possible with the current bids
        '''
        is_guest_alloc_valid = lambda bid, m: any(
            map(lambda rq: rq[0] <= m <= rq[1], bid.rqs))
        bids = filter(lambda bid: bid.name != exclude,
                      map(lambda b: self.get_bid_mem(b, auction_round),
                          bidders))
        for b in bids:
            my_alloc = alloc[b.name]
            if (my_alloc > 0) and not is_guest_alloc_valid(b, my_alloc):
                return False
        return True

    def _prepare_bids(self, auction_round, bidders, reduction=0):
        '''
        Extracts and splits bids from bidders
        @param auction_round the current round
        @param bidders guest descriptors
        @param reduction the reduction factor. If zero,
        splitting will not be done
        @return a list of sorted bids
        '''
        assert reduction > -p_eps, "Reduction factor must be non-negative"
        bids = []
        for b in bidders:
            bid = self.get_bid_mem(b, auction_round)
            prnt = None
            chld = None
            if abs(reduction) < p_eps:
                prnt = deep_copy_bid(bid)
                chld = MPSPBid(bid.name, 0, [(0, 0)], prnt)
            else:
                prnt, chld = self.split_bid(bid, self.extra_prev[bid.name],
                                            child_p_reduction=reduction)
            bids.extend([prnt, chld])
        self.sort_bids_mem(bids)
        map(lambda b: self.logger.debug("Bids after split and sort: %s",
                                        str(b)), bids)
        return bids

    def _update_child_reduction(self, src, new_reduction):
        bids = []
        for b in src:
            if b.is_parent:
                bids.append(deep_copy_bid(b))
            else:
                prnt = deep_copy_bid(
                    filter(lambda bb: bb.is_parent and bb == b.parent, bids)[0])
                bids.append(
                    MPSPBid(b.name, prnt.p - new_reduction, b.rqs, prnt))
        return bids

    def _guest_SW(self, bids, alloc, guest):
        ret = 0
        for b, a in zip(bids, alloc):
            if b.name != guest: continue
            self.logger.debug("%s p=%.05f, a=%i, parent: %i", guest, b.p, a,
                              b.is_parent)
            ret += b.p * a
        return ret

    def _SW_from_list(self, bids, alloc, exclude=None):
        ret = 0
        for b, a in zip(bids, alloc):
            if b.name != exclude:
                ret += b.p * a
        return ret

    def _SW_from_dict(self, auction_mem, bids, alloc, prev_alloc={},
                      exclude=None):
        alloc_list = self._allocation_dict_to_list(auction_mem, bids, alloc,
                                                   prev_alloc)
        return self._SW_from_list(bids, alloc_list, exclude)

    def _allocation_list_to_dict(self, bids, extras):
        ret = defaultdict(int)
        for b, a in zip(bids, extras):
            ret[b.name] += a
        return ret

    def _allocation_dict_to_list(self, auction_mem, bids, alloc_dict,
                                 prev_alloc_dict={}):
        ret = [0] * len(bids)
        for i, b in enumerate(bids):
            name = b.name
            prev_alloc = prev_alloc_dict[name] if prev_alloc_dict.has_key(name) \
                else auction_mem
            if b.is_parent:
                ret[i] = min(prev_alloc, alloc_dict[name])
            else:
                ret[i] = max(0, alloc_dict[name] - prev_alloc)
        return ret

    def _base_reduction_factor(self, p0, unit_sw):
        return p0 * unit_sw

    def _high_barrier(self):
        return self.p0

    def _low_barrier(self):
        return self.p0 / float(1 + self.stable_rounds)

    def _calculate_alloc_mem(self, auction_round, auction_mem, bidders,
                             all_bids_high, all_bids_low=None, exclude=None):
        if exclude is None:
            bids_high = all_bids_high
            bids_low = all_bids_low
        else:
            self.logger.debug("Round %i excluding %s", auction_round, exclude)
            bids_high = filter(lambda b: b.name != exclude, all_bids_high)
            if all_bids_low is not None:
                bids_low = filter(lambda b: b.name != exclude, all_bids_low)
            else:
                bids_low = None
        self.logger.debug("Round %i high bids: %s", auction_round, bids_high)
        extra, sw = allocate(bids_high, auction_mem, self.extra_prev)
        extras = self._allocation_list_to_dict(bids_high, extra)
        if bids_low is not None:
            if self._compare_allocations(self.last_alternate, extras, exclude):
                self.logger.debug("Got previous alternate, correcting SW")
                sw = self._SW_from_list(bids_low, extra, exclude)
            elif self._validate_allocation(auction_round, bidders,
                                           self.last_alternate, exclude):
                self.logger.debug("Comparing SW with last alternate")
                alternate_sw = self._SW_from_dict(auction_mem, bids_low,
                                                  self.last_alternate,
                                                  self.extra_prev, exclude)
                if alternate_sw >= sw:
                    self.logger.debug("Alternate SW is preferred")
                    extra = self._allocation_dict_to_list(auction_mem, bids_low,
                                                          self.last_alternate,
                                                          self.extra_prev)
                    sw = alternate_sw
        return extra, sw

    def do_auction_mem(self, host, bidders, show=True):
        """
        @param host: a host of type mom.Entity
                needs the following variables: auction_round_mem, auction_mem
        @param bidders: a list of type mom.Entity representing bidders
                each bidder needs the following:
                bid_p - price of required memory in cents per mb
                bid_ranges -
        @return: set values for 'control_mem' and 'control_bill_mem' for each bidder
                 in the received list.
        """
        host_round = host.GetVar('auction_round_mem')
        if host_round is None:  # No auction before one is announced
            return
        auction_mem = host.GetVar('auction_mem')
        payments = defaultdict(int)
        self.logger.debug("Memory Auction %i started", host_round)
        reduction_factor = self._high_barrier() * self.base_reduction
        self.logger.debug("Round %i high barrier: %.05f", host_round,
                          reduction_factor)
        host.SetVar('reduction_high_mem', reduction_factor)
        bids_high = self._prepare_bids(host_round, bidders, reduction_factor)
        reduction_factor = self._low_barrier() * self.base_reduction
        host.SetVar('reduction_low_mem', reduction_factor)
        self.logger.debug("Round %i low barrier: %.05f", host_round,
                          reduction_factor)
        bids_low = self._update_child_reduction(bids_high, reduction_factor)
        map(lambda b: self.logger.debug("low barrier bid: %s", str(b)),
            bids_low)
        self.logger.debug("preparing alternate bids")
        bids_alternate = self._prepare_bids(host_round, bidders)
        # get the new alternate allocation
        alternate_extra, alternate_sw = self._calculate_alloc_mem(host_round,
                                                                  auction_mem,
                                                                  bidders,
                                                                  bids_alternate)
        # get the allocation
        extras = None
        extra = None
        sw = None
        is_affine_maximizer = False
        if (sum(alternate_extra) < auction_mem) or (
            abs(reduction_factor) < p_eps):
            self.logger.debug("Round %i using alternate allocation", host_round)
            extra = list(alternate_extra)
            sw = alternate_sw
            extras = self._allocation_list_to_dict(bids_alternate, extra)
        else:
            self.logger.debug("Round %i using affine maximizer", host_round)
            extra, sw = self._calculate_alloc_mem(host_round, auction_mem,
                                                  bidders, bids_high, bids_low)
            extras = self._allocation_list_to_dict(bids_high, extra)
            is_affine_maximizer = True
        self.logger.debug("Round %i allocation: %s", host_round, str(extras))
        self.logger.debug("Round %i SW: %.05f", host_round, sw)
        # calculate the payments
        for name in map(lambda g: g.Prop('name'), bidders):
            self.logger.debug("Round %i payment for bidder %s. " +
                              "Affine maximizer used: %i", host_round,
                              name, is_affine_maximizer)
            extra_wo = None
            sw_wo = None
            sc_i = None
            if is_affine_maximizer:
                extra_wo, sw_wo = self._calculate_alloc_mem(host_round,
                                                            auction_mem,
                                                            bidders,
                                                            bids_high, bids_low,
                                                            exclude=name)
                sc_i = self._guest_SW(bids_low, extra, name)
            else:
                extra_wo, sw_wo = self._calculate_alloc_mem(host_round,
                                                            auction_mem,
                                                            bidders,
                                                            bids_alternate,
                                                            exclude=name)
                sc_i = self._guest_SW(bids_alternate, extra, name)
            self.logger.debug("Round %i excluded alloc: %s", host_round,
                              extra_wo)
            self.logger.debug("Round %i excluded SW: %.05f", host_round, sw_wo)
            payments[name] = sw_wo - (sw - sc_i)
            self.logger.debug(
                "Round %i guest %s sw=%.05f, sw_wo=%.05f, sc_i=%.05f",
                host_round, name, sw, sw_wo, sc_i)
            self.logger.debug("Round %i guest %s payment: %.05f", host_round,
                              name, payments[name])
            assert -p_eps <= payments[name] <= (1 + p_eps) * sc_i, \
                "%s got an illegal bill: %0.2f, sc_i=%0.2f, sw_wo=%.05f, sw=%.05f" \
                % (name, payments[name], sc_i, sw_wo, sw)
        alternate_alloc = self._allocation_list_to_dict(bids_alternate,
                                                        alternate_extra)
        self.logger.debug("Round %i alternate alloc: %s", host_round,
                          alternate_extra)
        self.logger.debug("Round %i alternate SW: %s", host_round, alternate_sw)
        if self._validate_allocation(host_round, bidders,
                                     self.last_alternate):
            prev_alternate_sw = self._SW_from_dict(auction_mem, bids_alternate,
                                                   self.last_alternate)
            if prev_alternate_sw >= alternate_sw:
                self.logger.debug("Round %i preferring last alternate",
                                  host_round)
                alternate_alloc = self.last_alternate
        if self._compare_allocations(self.last_alternate,
                                     alternate_alloc):
            self.stable_rounds += 1
            self.logger.debug(
                "Round %i is similar. Number of stable rounds: %i",
                host_round, self.stable_rounds)
        else:
            self.stable_rounds = 1
            self.last_alternate = alternate_alloc
            self.logger.debug("Round %i alternate is different", host_round)
        self._update_history_and_controls_mem(host_round, host, bidders,
                                              extras, self.last_alternate, sw,
                                              payments)
        if show:
            data = dict(((b.Prop('name'), (
                ("bid_p_mem", str(round(b.GetVar("bid_p_mem"), 4))),
                ("bid_ranges_mem", b.GetVar("bid_ranges_mem")),
                ("extra", b.GetVar('mem_extra')),
                ("extra_alternate", b.GetVar('mem_extra_alternate')),
                ("mem", b.GetControl('control_mem')),
                ("payment", str(round(b.GetControl('control_bill_mem'), 2))),))
                         for b in bidders))
            self.logger.info(
                "=== auction: %i, memory: %i ===\n" % (
                    host_round, auction_mem) +
                "\n".join(["    %s: {%s}" %
                           (name, ", ".join(["%s: %s" % v for v in data[name]]))
                           for name in sorted(data.keys())]))

    def _update_history_and_controls_mem(self, auction_round, host, bidders,
                                         extras, extras_alternate, sw,
                                         payments):
        # p values, unit SW, total bill, previous alloc and total bill
        self.p_in_min = float('inf')
        self.p_out_max = 0
        total_allocated = 0
        for b in bidders:
            name = b.Prop('name')
            extra = extras[name]
            extra_alternate = extras_alternate[name]
            payment = payments[name]
            alloc = extra + b.Prop('base_mem')
            # controls
            b.Control('control_mem', alloc)
            b.Control('control_bill_mem', payment)
            b.SetVar('mem_extra', extra)
            b.SetVar('mem_extra_alternate', extra_alternate)
            # databases
            self.total_bills[name] += payment
            self.extra_prev[name] = extra
            # history
            bid_p = b.GetVar('bid_p_mem')
            bid_max_q = b.GetVar('bid_ranges_mem')[-1][1]
            self.total_bills[name] += payment
            self.extra_prev[name] = extra
            total_allocated += extra
            if extra > 0:
                self.p_in_min = min(self.p_in_min, bid_p)
            if extra < bid_max_q:
                self.p_out_max = max(self.p_out_max, bid_p)
        if self.p_in_min == float('inf'):
            self.p_in_min = 0
        _flag_tie_winners_mem(bidders)
        self.unit_sw_hist.add(float(sw) / total_allocated \
                                  if total_allocated > 0 else 0)
        # parameters used by notifier later
        host.SetVar('p_in_min_mem', self.p_in_min)
        host.SetVar('p_out_max_mem', self.p_out_max)
        host.SetVar('results_round_mem', auction_round)
        self.logger.debug("Round %i p_in_min=%.05f, p_out_max=%.05f",
                          auction_round,
                          self.p_in_min, self.p_out_max)

    def get_bid_mem(self, bidder, curr_round):
        try:
            rnd = int(bidder.GetVar('bid_round_mem'))
            p = float(round(bidder.GetVar('bid_p_mem'), p_digits))
            rq = bidder.GetVar('bid_ranges_mem')
            n = len(rq)
            # check bid values
            if any(map(lambda x: x is None, (rnd, p, rq))):
                raise ValueError("Didn't get bid")
            if rnd != curr_round:
                raise ValueError("Wrong round")
            if p < 0:
                raise ValueError("p is negative")
            if n == 0:
                raise ValueError("No rq list")
            if any((rq[i - 1][1] >= rq[i][0] for i in
                    range(1, n)) or  # r_i-1 >= q_i
                           any((rq[i][0] < 0 or rq[i][1] < 0 or rq[i][0] >
                                   rq[i][1]
                                for i in range(n)))):  # r<0, q<0, r>q
                raise ValueError("rq list not ordered")
        except Exception as ex:
            self.logger.warn(
                "Bad bid value of guest %s: p: %s, qr: %s, Error: %s",
                bidder.Prop("name"), str(bidder.GetVar('bid_p_mem')),
                str(bidder.GetVar('bid_ranges_mem')), ex)
            p = 0
            rq = [(0, 0)]

        # set values in bidder entity
        bidder.SetVar('bid_p_mem', p)
        bidder.SetVar('bid_ranges_mem', rq)

        return MPSPBid(bidder.Prop("name"), p, rq, None)


def _flag_tie_winners_mem(bidders):
    # find p value for matches the limit between loosing and winning
    p_tie = None
    for b in bidders:
        if b.GetVar('mem_extra') > 0 and \
                        b.GetVar('mem_extra') not in \
                        [rq[1] for rq in b.GetVar('bid_ranges_mem')]:
            p_tie = b.GetVar('bid_p_mem')
            break
    if p_tie == None:
        return
    # find all bidders with the limit bid value
    matches = filter(lambda b: abs(b.GetVar('bid_p_mem') - p_tie) < p_eps,
                     bidders)
    # tie is only when number of bidders with p_tie is higher then 1
    if len(matches) > 1:
        for b in matches:
            b.SetVar('tie_winner_mem', True)


if __name__ == '__main__':
    # unit tests: one simple test with two guests and forbidden ranges,
    # second is a real assertion fail that occurred
    from mom.Entity import Entity

    # first test: a small 2 guests with forbidden ranges fail
    print "test 1"
    auctioneer = Auctioneer(verbosity='debug')
    host = Entity()
    host.SetVar('auction_round_mem', 86)
    host.SetVar('auction_mem', 100)
    vm1 = Entity()
    vm2 = Entity()
    vm1._set_property('name', 'vm-1')
    vm2._set_property('name', 'vm-2')
    vm1.SetVar('bid_round_mem', 86)
    vm2.SetVar('bid_round_mem', 86)
    vm1.SetVar('bid_p_mem', 100.0)
    vm1.SetVar('bid_ranges_mem', [(3, 99)])  # 4)])
    vm1._set_property('base_mem', 0)
    vm2.SetVar('bid_p_mem', 99.5)
    vm2.SetVar('bid_ranges_mem', [(98, 100)])
    vm2._set_property('base_mem', 0)
    bidders = [vm1, vm2]
    auctioneer.do_auction_mem(host, bidders, True)

    # second test: a reproduction of a real fail
    print "test 2"
    auctioneer = Auctioneer(verbosity='debug')
    host = Entity()
    host.SetVar('auction_round_mem', 86)
    host.SetVar('auction_mem', 10700)
    gnames = map(lambda n: 'vm-%i' % n, range(1, 8 + 1))
    bidders = []
    for gname in gnames:
        guest = Entity()
        guest._set_property('name', gname)
        guest._set_property('base_mem', 0)
        guest.SetVar('bid_round_mem', 86)
        bidders.append(guest)

    bidders[0].SetVar('bid_p_mem', 1641.94)
    bidders[1].SetVar('bid_p_mem', 760.28)
    bidders[2].SetVar('bid_p_mem', 503.82)
    bidders[3].SetVar('bid_p_mem', 225.92)
    bidders[4].SetVar('bid_p_mem', 234.74)
    bidders[5].SetVar('bid_p_mem', 168.62)
    bidders[6].SetVar('bid_p_mem', 0)
    bidders[7].SetVar('bid_p_mem', 202.18)

    bidders[0].SetVar('bid_ranges_mem', [(1550, 1560)])
    bidders[1].SetVar('bid_ranges_mem', [(1550, 1560)])
    bidders[2].SetVar('bid_ranges_mem', [(1550, 1560)])
    bidders[3].SetVar('bid_ranges_mem', [(1550, 1560)])
    bidders[4].SetVar('bid_ranges_mem', [(1536, 1550)])
    bidders[5].SetVar('bid_ranges_mem', [(1963, 1980)])
    bidders[6].SetVar('bid_ranges_mem', [(0, 0)])
    bidders[7].SetVar('bid_ranges_mem', [(1550, 1560)])

    auctioneer.do_auction_mem(host, bidders)

    # third test: r=q
    print "test 3"
    auctioneer = Auctioneer(verbosity='debug')
    host = Entity()
    host.SetVar('auction_round_mem', 86)
    host.SetVar('auction_mem', 100)
    host.SetVar('p0-percent', 1.0)
    host.SetVar('p0', 1)

    vm1 = Entity()
    vm2 = Entity()
    vm1._set_property('name', 'vm-1')
    vm2._set_property('name', 'vm-2')
    vm1.SetVar('bid_round_mem', 86)
    vm2.SetVar('bid_round_mem', 86)
    vm1.SetVar('bid_p_mem', 100.0)
    vm1.SetVar('bid_ranges_mem', [(99, 99)])  # 4)])
    vm1._set_property('base_mem', 0)
    vm2.SetVar('bid_p_mem', 99.5)
    vm2.SetVar('bid_ranges_mem', [(98, 100)])
    vm2._set_property('base_mem', 0)
    bidders = [vm1, vm2]
    auctioneer.extra_prev['vm-1'] = 3
    auctioneer.extra_prev['vm-2'] = 97
    auctioneer.base_reduction = 0.02
    auctioneer.do_auction_mem(host, bidders)

    print "test 4"
    auctioneer = Auctioneer(verbosity='debug')
    auctioneer.base_reduction = 5
    host = Entity()
    host.SetVar('auction_round_mem', 86)
    host.SetVar('auction_mem', 100)
    host.SetVar('p0-percent', 1.0)
    host.SetVar('p0', 1)

    vm1 = Entity()
    vm1._set_property('name', 'vm-1')
    vm1.SetVar('bid_round_mem', 86)
    vm1.SetVar('bid_p_mem', 10.0)
    vm1.SetVar('bid_ranges_mem', [(99, 99)])  # 4)])
    vm1._set_property('base_mem', 0)

    vm2 = Entity()
    vm2._set_property('base_mem', 0)
    vm2._set_property('name', 'vm-2')
    vm2.SetVar('bid_round_mem', 86)
    vm2.SetVar('bid_p_mem', 1)
    vm2.SetVar('bid_ranges_mem', [(2, 100)])
    vm2._set_property('base_mem', 0)
    auctioneer.extra_prev['vm-1'] = 3
    auctioneer.extra_prev['vm-2'] = 60
    bidders = [vm1, vm2]
    auctioneer.do_auction_mem(host, bidders)

    # [r1, q1], [r2, q2], q1<m<r2
    print "test 5"
    auctioneer = Auctioneer(verbosity='debug')
    auctioneer.base_reduction = 5
    host = Entity()
    host.SetVar('auction_round_mem', 86)
    host.SetVar('auction_mem', 100)
    host.SetVar('p0-percent', 1.0)
    host.SetVar('p0', 1)

    vm1 = Entity()
    vm1._set_property('name', 'vm-1')
    vm1.SetVar('bid_round_mem', 86)
    vm1.SetVar('bid_p_mem', 10.0)
    vm1.SetVar('bid_ranges_mem', [(30, 50), (60, 80)])
    vm1._set_property('base_mem', 0)

    vm2 = Entity()
    vm2._set_property('base_mem', 0)
    vm2._set_property('name', 'vm-2')
    vm2.SetVar('bid_round_mem', 86)
    vm2.SetVar('bid_p_mem', 4)
    vm2.SetVar('bid_ranges_mem', [(0, 100)])
    vm2._set_property('base_mem', 0)

    auctioneer.extra_prev['vm-1'] = 55
    auctioneer.extra_prev['vm-2'] = 60
    bidders = [vm1, vm2]
    auctioneer.do_auction_mem(host, bidders)

    # [r, q], r = m
    print "test 6"
    auctioneer = Auctioneer(verbosity='debug')
    auctioneer.base_reduction = 5
    host = Entity()
    host.SetVar('auction_round_mem', 86)
    host.SetVar('auction_mem', 100)
    host.SetVar('p0-percent', 1.0)
    host.SetVar('p0', 1)

    vm1 = Entity()
    vm1._set_property('name', 'vm-1')
    vm1.SetVar('bid_round_mem', 86)
    vm1.SetVar('bid_p_mem', 10.0)
    vm1.SetVar('bid_ranges_mem', [(50, 80)])
    vm1._set_property('base_mem', 0)

    vm2 = Entity()
    vm2._set_property('base_mem', 0)
    vm2._set_property('name', 'vm-2')
    vm2.SetVar('bid_round_mem', 86)
    vm2.SetVar('bid_p_mem', 5)
    vm2.SetVar('bid_ranges_mem', [(0, 100)])
    vm2._set_property('base_mem', 0)

    auctioneer.extra_prev['vm-1'] = 50
    auctioneer.extra_prev['vm-2'] = 60
    bidders = [vm1, vm2]
    auctioneer.do_auction_mem(host, bidders)

    # [r1, q1], [r2, q2], q1=m
    print "test 7"
    auctioneer = Auctioneer(verbosity='debug')
    auctioneer.base_reduction = 4
    host = Entity()
    host.SetVar('auction_round_mem', 86)
    host.SetVar('auction_mem', 100)
    host.SetVar('p0-percent', 1.0)
    host.SetVar('p0', 1)

    vm1 = Entity()
    vm1._set_property('name', 'vm-1')
    vm1.SetVar('bid_round_mem', 86)
    vm1.SetVar('bid_p_mem', 10.0)
    vm1.SetVar('bid_ranges_mem', [(30, 50), (70, 80)])
    vm1._set_property('base_mem', 0)

    vm2 = Entity()
    vm2._set_property('base_mem', 0)
    vm2._set_property('name', 'vm-2')
    vm2.SetVar('bid_round_mem', 86)
    vm2.SetVar('bid_p_mem', 5)
    vm2.SetVar('bid_ranges_mem', [(0, 100)])
    vm2._set_property('base_mem', 0)

    auctioneer.extra_prev['vm-1'] = 50
    auctioneer.extra_prev['vm-2'] = 60
    bidders = [vm1, vm2]
    auctioneer.do_auction_mem(host, bidders)

    # [r1, q1], [r2, q1], m = r2
    print "test 8"
    auctioneer = Auctioneer(verbosity='debug')
    auctioneer.base_reduction = 5
    host = Entity()
    host.SetVar('auction_round_mem', 86)
    host.SetVar('auction_mem', 100)
    host.SetVar('p0-percent', 1.0)
    host.SetVar('p0', 1)

    vm1 = Entity()
    vm1._set_property('name', 'vm-1')
    vm1.SetVar('bid_round_mem', 86)
    vm1.SetVar('bid_p_mem', 10.0)
    vm1.SetVar('bid_ranges_mem', [(30, 40), (50, 70)])
    vm1._set_property('base_mem', 0)

    vm2 = Entity()
    vm2._set_property('base_mem', 0)
    vm2._set_property('name', 'vm-2')
    vm2.SetVar('bid_round_mem', 86)
    vm2.SetVar('bid_p_mem', 5)
    vm2.SetVar('bid_ranges_mem', [(0, 100)])
    vm2._set_property('base_mem', 0)

    auctioneer.extra_prev['vm-1'] = 50
    auctioneer.extra_prev['vm-2'] = 60
    bidders = [vm1, vm2]
    auctioneer.do_auction_mem(host, bidders)

    # [r, q], r<m<q
    print "test 9"
    auctioneer = Auctioneer(verbosity='debug')
    auctioneer.base_reduction = 5
    host = Entity()
    host.SetVar('auction_round_mem', 86)
    host.SetVar('auction_mem', 100)
    host.SetVar('p0-percent', 1.0)
    host.SetVar('p0', 1)

    vm1 = Entity()
    vm1._set_property('name', 'vm-1')
    vm1.SetVar('bid_round_mem', 86)
    vm1.SetVar('bid_p_mem', 10.0)
    vm1.SetVar('bid_ranges_mem', [(40, 60)])
    vm1._set_property('base_mem', 0)

    vm2 = Entity()
    vm2._set_property('base_mem', 0)
    vm2._set_property('name', 'vm-2')
    vm2.SetVar('bid_round_mem', 86)
    vm2.SetVar('bid_p_mem', 5.0)
    vm2.SetVar('bid_ranges_mem', [(0, 40), (65, 100)])
    vm2._set_property('base_mem', 0)

    auctioneer.extra_prev['vm-1'] = 50
    auctioneer.extra_prev['vm-2'] = 60
    bidders = [vm1, vm2]
    auctioneer.do_auction_mem(host, bidders)
