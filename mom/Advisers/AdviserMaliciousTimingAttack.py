'''
Created on September 03, 2017

@author: danielle
'''
import logging
import numpy as np
import scipy as sp
from mom.Advisers.AdviserProfitEstimator import AdviserProfitEstimator
from etc.HistoryWindow import NumericHistoryWindow
from random import randint

class AdviserMaliciousTimingAttack(AdviserProfitEstimator):
    def __init__(self, profiler, base_mem, advice_entry, memory_delta, rev_func, **kwargs):
        AdviserProfitEstimator.__init__(self, profiler, base_mem, advice_entry, memory_delta, rev_func, **kwargs)
        self.p_in_min = 0

    def do_advice_mem(self, state, auction_mem):
        p_chosen = 0.5
        if ((self.rnd_mem != 0) and ((self.rnd_mem % 2) == 0)) :
            last = self.hist_mem[0]
            if last.results_added:
              self.p_in_min = last.p_in_min
            p_chosen = self.p_in_min * 1.05
        else:
            p_chosen = 0.5
        
        self.logger.debug("round %i chosen p: %s", self.rnd_mem, str(p_chosen))
        return p_chosen, [(0,1550)]