'''
Created on July 03, 2017

@author: danielle
'''
import logging
import numpy as np
import scipy as sp
from mom.Advisers.AdviserProfitEstimator import AdviserProfitEstimator
from etc.HistoryWindow import NumericHistoryWindow
from random import randint

class AdviserAttack2(AdviserProfitEstimator):
    def __init__(self, profiler, base_mem, advice_entry, memory_delta, rev_func, **kwargs):
        AdviserProfitEstimator.__init__(self, profiler, base_mem, advice_entry, memory_delta, rev_func, **kwargs)
        self.bad_ps = 0
        self.p_in_min = 0
        self.p_out_max = 0
        self.victim_p_min_in = 0
        self.bid_p = 0
        self.won_p = 0
        self.last_valuation = 0.0

    def do_advice_mem(self, state, auction_mem):
        if (self.rnd_mem == 0):
            self.last_valuation = self.V_func(self.perf(state))
            return 0.05, [(0,auction_mem)]
        else:
            last = self.hist_mem[0]
            if last.results_added:
                if (self.rnd_mem == 1):
                    self.victim_p_min_in = last.p_in_min
                self.p_in_min = last.p_in_min
                self.p_out_max = last.p_out_max
                self.bid_p = last.bid_p
                self.won_p = last.won_p
                self.bill_prev = last.bill
                self.last_valuation = self.V_func(self.perf([state[0], state[1] + (last.won_mem - last.base_mem), 0]))
                self.logger.debug("&&&&&&&&&&&&&&&&&&&&&&&&&&& round %i, self.last_valuation: %s ", self.rnd_mem, str(self.last_valuation))
            p_chosen = (self.victim_p_min_in + self.p_out_max)/2.0
            self.logger.debug("&&&&&&&&&&&&&&&&&&&&&&&&&&& round %i, p_chosen before: %s ", self.rnd_mem, str(p_chosen))
            if (self.bill_prev > 0):
                self.victim_p_min_in = self.p_out_max
                p_chosen = (self.p_out_max * 0.9)
                self.logger.debug("&&&&&&&&&&&&&&&&&&&&&&&&&&& round %i, p_chosen: %s ", self.rnd_mem, str(p_chosen))
            elif (self.bid_p >= p_chosen):
                p_chosen = (self.bid_p * 1.2)
                self.logger.debug("*************************** round %i, p_chosen: %s ", self.rnd_mem, str(p_chosen))
#            elif (self.bid_p == self.p_in_min and self.bid_p == self.p_out_max):
#                if ((p_chosen * 1.2) > self.victim_p_min_in):
#                    p_chosen = (self.bid_p + self.victim_p_min_in)/2.0
#                else:
#                    p_chosen *= 1.2
            self.logger.debug("round %i chosen p: %s", self.rnd_mem, str(p_chosen))
            return p_chosen, [(0,auction_mem)]
            
    def get_last_profit_estimate_mem(self, q):
        return (self.last_valuation - self.bill_prev)

    def get_last_bill_estimate_mem(self, q):
        return self.bill_prev

    def get_last_valuation_estimate_mem(self, q):
        return (self.last_valuation - 0.0)
