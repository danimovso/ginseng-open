'''
Created on July 03, 2017

@author: danielle
'''
import logging
import numpy as np
import scipy as sp
#from mom.Advisers.AdviserProfitEstimator import AdviserProfitEstimator
from mom.Advisers.AdviserProfit import AdviserProfit
from etc.HistoryWindow import NumericHistoryWindow
from random import randint

#class AdviserAttack(AdviserProfitEstimator):
class AdviserAttack(AdviserProfit):
    def __init__(self, profiler, base_mem, advice_entry, memory_delta, rev_func, **kwargs):
        AdviserProfit.__init__(self, profiler, base_mem, advice_entry, memory_delta, rev_func)
        #AdviserProfitEstimator.__init__(self, profiler, base_mem, advice_entry, d_mem, rev_func, **kwargs)
        self.p_in_min = 0
        self.p_out_max = 0
        if kwargs.has_key('auction_mem'):
            auction_mem = kwargs['auction_mem']
            self.first_rq = [(0,auction_mem)]

    def do_advice_mem(self, state, auction_mem):

        if (self.rnd_mem == 0):
            self.auction_mem = auction_mem
            #bid_p, bid_rq = AdviserProfitEstimator.do_advice_mem(self, state, auction_mem)
            bid_p, bid_rq = AdviserProfit.do_advice_mem(self, state, auction_mem)
            self.logger.info("round %i) state: %s, bid before change: [%s, %s]", self.rnd_mem, str(state), str(bid_p), str(bid_rq))
            self.first_rq = bid_rq
            self.logger.info("round %i) state: %s, bid after change (first_round): [%s, %s]", self.rnd_mem, str(state), str(bid_p), str(self.first_rq))
            #return bid_p, self.first_rq
            return bid_p, [(500,1640)]
        else:
            last = self.hist_mem[0]
            if last.results_added:
                self.p_in_min = last.p_in_min
                self.p_out_max = last.p_out_max
            p_chosen = (self.p_in_min + self.p_out_max)/2.0
            #if (p_chosen == self.p_in_min):
            #    p_chosen -= 0.01 
            #self.logger.info("round %i) state: %s, bid after change (not first round): [%s, %s], p_in_min: %s, p_out_max: %s", self.rnd_mem, str(state), str(p_chosen), str(self.first_rq), str(self.p_in_min), str(self.p_out_max))
            self.logger.info("round %i) state: %s, bid after change (not first round): [%s, [(0,645)]], p_in_min: %s, p_out_max: %s", self.rnd_mem, str(state), str(p_chosen), str(self.p_in_min), str(self.p_out_max))
            #return p_chosen, self.first_rq
            return p_chosen, [(500,1640)]