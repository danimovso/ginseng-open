'''
Created on September 23, 2017

@author: danielle
'''
import logging
import numpy as np
import scipy as sp
from mom.Advisers.AdviserProfitEstimator import AdviserProfitEstimator
from etc.HistoryWindow import NumericHistoryWindow
from random import randint
from mom.Adviser import Adviser, p_digits, p_eps, p_eq

class AdviserAttack3(AdviserProfitEstimator):
    def __init__(self, profiler, base_mem, advice_entry, memory_delta, rev_func, **kwargs):
        AdviserProfitEstimator.__init__(self, profiler, base_mem, advice_entry, memory_delta, rev_func, **kwargs)

    def do_advice_mem(self, state, auction_mem):
        def_bid = 0.0, [(0, auction_mem)]
        if auction_mem == 0:
            return def_bid
        # learn from history
        # calculate target bid value
        self.logger.debug("History: %s", str(self.hist_mem))
        if not self.is_first_round_mem():
            last = self.hist_mem[0]
            if last.results_added:
                self.p_in_min_hist_mem.add(last.p_in_min)
                self.p_out_max_hist_mem.add(last.p_out_max)
                self.won_p_hist_mem.add(last.won_p)
                self.bid_p_hist_mem.add(last.bid_p)
                assert last.bid_p >= last.won_p - p_eps
            else:
                self.logger.warn("do_advice_mem: No data on last bid results")
        self.logger.debug("round %i p_in_min memory history: %s", self.rnd_mem,
                          str(self.p_in_min_hist_mem.dump()))
        self.logger.debug("round %i p_out_max memory history: %s", self.rnd_mem,
                          str(self.p_out_max_hist_mem.dump()))
        self.logger.debug("round %i won_p memory history: %s", self.rnd_mem, 
                          str(self.won_p_hist_mem.dump()))
        ## Generate bids data
        V0 = self.V_func(self.perf(state))
        # all q stations according to the memory for auction
        q = np.asarray(range(self.d_mem, auction_mem, self.d_mem) + [auction_mem])
        # the memory allocation for each q
        self.logger.debug("round %i V0: %s", self.rnd_mem, str(V0))
        # valuation
        V = np.asarray(self.V_func(self.perf([state[0], state[1] + q, 0])))
        self.logger.debug("round %i V: %s", self.rnd_mem, str(V))
        good_V = V > V0
        # if there is no good revenue, return default bid
        try:
            if not any(good_V):
                return def_bid
        except TypeError:
            return def_bid
        # allow only higher valuations
        if not all(good_V):
            V = V[good_V]
            q = q[good_V]
        self.logger.debug("round %i good V: %s", self.rnd_mem, str(V))
        self.logger.debug("round %i good q: %s", self.rnd_mem, str(q))
        # p value is the slope of the revenue-memory graph from the (mem0,V0) point
        # in 3D(bandwidth) this is OK while the other dimension is constant
        with np.errstate(divide = 'ignore', invalid = "ignore"):
            p = (V - V0) / q
        p[np.isinf(p) + np.isnan(p)] = 0
        assert all(p >= 0)
        self.logger.debug("round %i p: %s", self.rnd_mem, str(p))
        _p = np.copy(p) #p[:]  # save the p values for later rq list generation
        # profit according to estimated unit cost
        est_bill = self.get_estimated_bill_mem(q, p)
        U = np.maximum(0, abs(V - est_bill))
        self.last_bill_estimate_mem = dict(map(lambda _q, _p: (_q, _p), q, est_bill))
        self.last_profit_estimate_mem = dict(map(lambda _q, _p: (_q, _p), q, U))
        self.last_valuation_estimate_mem = dict(map(lambda _q, _p: (_q, _p), q, V))
        self.logger.debug("round %i profit: %s", self.rnd_mem, str(U))
        p_chosen = self.choose_p_mem(p, U)
        self.logger.debug("round %i chosen p: %s", self.rnd_mem, str(p_chosen))
        if p_chosen > p_eps:
            return round(p_chosen, p_digits), [(0,1550)]
        else:
            return def_bid

    def choose_p_mem(self, p, U):
        if (self.rnd_mem == 0):
            return 0.05
        else: 
            # clear places where p < p_in_min from utility
            min_p = self.p_in_min_hist_mem[0] if self.p_in_min_hist_mem.len() > 0 else 0
            max_p = self.p_out_max_hist_mem[0] if self.p_out_max_hist_mem.len() > 0 else 0
            won_p = self.won_p_hist_mem[0] if self.won_p_hist_mem.len() > 0 else 0
            bid_p = self.bid_p_hist_mem[0] if self.bid_p_hist_mem.len() > 0 else 0
            self.logger.debug("round %i minimal p: %f", self.rnd_mem, min_p)
            if (won_p > 0):
                good_ps = p < (min_p-p_eps)
            elif (bid_p == min_p and bid_p == max_p):
                good_ps = p > (max_p-p_eps)
            else: 
                good_ps = p >= (((min_p + max_p)/2.0) - p_eps)
            self.logger.debug("round %i min(p): %s, min_p-p_eps: %s", self.rnd_mem, str(np.amin(p)), str(min_p-p_eps))
            self.logger.debug("round %i good_ps: %s", self.rnd_mem, str(good_ps))
            # if the highest p is not enough, choose the highest p possible
            if not any(good_ps):
                self.logger.debug("round %i in any(goog_ps)")
                return np.max(p)
            # keep the places where p >= target
            if (not all(good_ps)) and (any(U[good_ps] > 0)):
                self.logger.debug("round %i in the second is", self.rnd_mem)
                p = p[good_ps]
                U = U[good_ps]
            self.logger.debug("round %i good p: %s", self.rnd_mem, str(p))
            self.logger.debug("round %i good U: %s", self.rnd_mem, str(U))
#            self.logger.debug("round %i maximal profit quantity: %s", self.rnd_mem, q[p_eq(U, max(U))])
            self.logger.debug("round %i prices with maximal profit: %s", self.rnd_mem, p[np.isclose(U, max(U))])
            # choose the highest utility bid from what left
            # with the highest p value conform the highest utility
            return max(p[np.isclose(U, max(U))])            
                