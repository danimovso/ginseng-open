'''
Created on July 03, 2017

@author: danielle
'''
import logging
import numpy as np
import scipy as sp
from mom.Advisers.AdviserProfitEstimator import AdviserProfitEstimator
from etc.HistoryWindow import NumericHistoryWindow
from random import randint


class AdviserConstant(AdviserProfitEstimator):
    def __init__(self, profiler, base_mem, advice_entry, memory_delta, rev_func, **kwargs):
        AdviserProfitEstimator.__init__(self, profiler, base_mem, advice_entry, memory_delta, rev_func, **kwargs)
        
    def do_advice_mem(self, state, auction_mem):
        return 0.5, [(1550,1550)]
        