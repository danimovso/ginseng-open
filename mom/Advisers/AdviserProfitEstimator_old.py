'''
Created on Nov 29, 2011

@author: eyal
'''
import numpy as np
from mom.Advisers.AdviserProfit import AdviserProfit
from collections import namedtuple, defaultdict
from mom.Adviser import p_eps


class BilllHistoryItem_old(object):
    '''
    A single entry in the bills history:
    unit price (p)
    minimal accepted unit price (p_in_min)
    maximal rejected unit price (p_out_max)
    a list of bills with the corresponding p-values (bills)
    '''

    def __init__(self, p, p_in_min, p_out_max, bills=[]):
        self.p = p
        self.p_in_min = p_in_min
        self.p_out_max = p_out_max
        self.bills = bills

    def is_close(self, p, p_in_min, p_out_max, radius):
        return self._is_field_close(self.p, p, radius) \
               and self._is_field_close(self.p_in_min, p_in_min, radius) \
               and self._is_field_close(self.p_out_max, p_out_max, radius)

    def _is_field_close(self, field, value, radius):
        if abs(field) < p_eps:
            return abs(field - value) < radius
        else:
            return abs(field - value) / field < radius

    def is_equal(self, p, p_in_min, p_out_max):
        return self.is_close(p, p_in_min, p_out_max, p_eps)

    def get_bills(self):
        return self.bills[:]

    def add_bill(self, new_bill):
        self.bills.append(new_bill)

    def __str__(self):
        return "BillHistoryItem(p=%.05f, p_in_min=%.05f, p_out_max=%.05f, " \
               "bills=%s)" % (self.p, self.p_in_min, self.p_out_max,
                             str(self.bills))


class BillHistory_old(defaultdict):
    '''
    The bill history record: translates memory quantities to history items
    '''

    def __init__(self, proximity):
        defaultdict.__init__(self, list)
        self.proximity = proximity

    def insert_item(self, q, p, p_in_min, p_out_max):
        '''
        Prepares a history item with the given p-values. If there is another
        item close to the given values, it is used
        @param q:
        @param p:
        @param p_in_min:
        @param p_out_max:
        @return:
        '''
        candidates = filter(lambda item: item.is_equal(p, p_in_min, p_out_max),
                            self[q])
        assert len(candidates) <= 1, "Duplicate candidates"
        if len(candidates) == 0:
            self[q].append(BilllHistoryItem(p, p_in_min, p_out_max))
            return self[q][-1]
        else:
            return candidates[0]

    def insert_bill(self, q, p, p_in_min, p_out_max, bill):
        '''
        Inserts a new bill at the given quantity and p-values.
        If there there is a close entry in the history, the bill
        will be inserted there
        @param q: the quantity to which the item is associated
        @param p:
        @param p_in_min:
        @param p_out_max:
        @param bill:
        '''
        item = self.insert_item(q, p, p_in_min, p_out_max)
        item.add_bill(bill)

    def get_bills(self, q, p, p_in_min, p_out_max):
        '''
        Gets all bills close enough to the point we want
        @param q: quantity required
        @param p: unit price
        @param p_in_min:
        @param p_out_max:
        @return: list of bills, or empty if no data
        '''
        if q not in self:
            return []
        else:
            ret = []
            candidates = filter(lambda item: item.is_close(p, p_in_min,
                                                           p_out_max,
                                                           self.proximity),
                                self[q])
            for c in candidates:
                ret.extend(c.get_bills())
            return ret

    def __str__(self):
        ret = ""
        for k in self.keys():
            ret += "%i => [" % k
            ret += ", ".join(map(lambda item: str(item), self[k]))
            ret += "]\n"
        return ret


class AdviserProfitEstimator_old(AdviserProfit):
    '''
    The super-smart online-learning adviser
    '''

    def __init__(self, profiler, base_mem, advice_entry, memory_delta, rev_func,
                 **kwargs):
        AdviserProfit.__init__(self, profiler, base_mem, advice_entry,
                               memory_delta,
                               rev_func)
        self.bills = BillHistory(0.05)
        self.base_q = 0
        self.base_bill = 0
        self.current_enable_flag = 0
        use_estimator = None
        if kwargs.has_key('use_estimator'):
            use_estimator = kwargs['use_estimator']
        if use_estimator is None:
            self.enable_flags = [True]
        elif not isinstance(use_estimator, list):
            raise ValueError("Use estimator flag must be a list of booleans")
        else:
            self.enable_flags = list(use_estimator)

    def _average(self, vals, weights=None):
        if weights is None:
            return np.average(vals)
        elif abs(sum(weights)) <= 1e-5:
            return 0.0
        else:
            return np.average(vals, weights=weights)

    def _get_p(self, qs, ps, q):
        '''
        Gets the unit price of q given a list of (q, p) pairs
        @param qs: list of memory quantities
        @param ps: list of unit prices
        @param q: the desired quantity
        @return: the unit price of the given q, or the closest q if it is not in the list
        '''
        idx = np.argmin(abs(qs - q))
        return ps[idx]

    def _estimate_bounds(self, qs, ps, p_in_min, p_out_max, q):
        '''
        Estimates the bounds of a given quantity q with given p-values.
        If there are relevant bills in the history, their average is the estimate.
        Otherwise, bounds estimations is used
        @param qs: quantities for which p(q) was calculated
        @param ps: p(q) values
        @param p_in_min:
        @param p_out_max:
        @param q:
        @return:
        '''
        base_bill = self.base_bill
        p = self._get_p(qs, ps, q)
        history = self.bills.get_bills(q, p, p_in_min, p_out_max)
        ret = 0
        if p_in_min < p_out_max:
            p_in_min, p_out_max = p_out_max, p_in_min
        if len(history) == 0:
            self.logger.debug("Using bounds estimate")
            dq = q - self.base_q
            if dq == 0:
                lb = base_bill
                ub = base_bill
            elif dq > 0:
                lb = base_bill + p_in_min * dq
                ub = base_bill + p * dq
            else: # dq < 0
                lb = max(0, base_bill + p_out_max * dq)
                ub = max(0, (base_bill - p_out_max) * (self.base_q - q) / (self.base_q - 1))
            self.logger.debug("Bounds for q=%i: [%.05f, %.05f]", q, lb, ub)
            ret = (min(p_out_max * q, lb) + min(p_out_max * q, ub)) / 2.0
        else:
            self.logger.debug("Using history-based estimation")
            ret = min(q * p_out_max, np.average(history))
        return ret

    def _is_estimator_enabled(self):
        '''
        Indicates whether or not bill estimation is currently enabled.
        This can be used to make the bids stable for warmup purpose
        @return: is the estimator currently enabled
        '''
        return self.enable_flags[self.current_enable_flag]

    def do_reset_mem(self):
        '''
        Clears all of the history
        @return:
        '''
        AdviserProfit.do_reset_mem(self)
        self.bills.clear()

    def next_valuation(self):
        '''
        Changes the valuation flag with the valuation
        @return: the next valuation
        '''
        self.current_enable_flag = (self.current_enable_flag + 1) % len(
                self.enable_flags)
        return AdviserProfit.next_valuation(self)

    def get_estimated_bill_mem(self, qs, ps):
        '''
        Estimates the bills for the given memory quantities
        @param qs: the quantities we want to estimate
        @param ps: the unit prices of the quantities to be estimated
        @return: a list with an estimate for each quantity
        '''
        if not self._is_estimator_enabled():
            self.logger.debug("round %i estimator is not enabled", self.rnd_mem)
            return [0] * len(qs)
        self.logger.debug("round %i estimating bill", self.rnd_mem)
        self.base_q = 0
        self.base_bill = 0
        p_in_min = 0
        p_out_max = 0
        if not self.is_first_round_mem():
            last = self.hist_mem[0]
            p_in_min = last.p_in_min
            p_out_max = last.p_out_max
            if last.results_added:
                self.base_q = last.won_mem - last.base_mem
                self.base_bill = last.bill
                self.bills.insert_bill(self.base_q, p_in_min, p_out_max,
                                       last.bid_p, self.base_bill)
        self.logger.debug("round %i bills history: %s", self.rnd_mem,
                          str(self.bills))
        ret = list(map(lambda q: self._estimate_bounds(qs, ps, p_in_min,
                                                       p_out_max, q), qs))
        self.logger.debug("round %i estimated bills: %s", self.rnd_mem, map(
                lambda _q, _p: (_q, _p), qs, ret))
        return ret


if __name__ == '__main__':
    from mom.Profiler import fromxml

    profiler_file = "doc/profiler-memcached-inside-spare-50-win-500k-tapuz25.xml"
    profiler_entry = "hits_rate"
    revenue_func_str = 'lambda x:x'
    base_mem = 600
    profiler = fromxml(profiler_file, profiler_entry)
    adv = AdviserProfitEstimator(profiler=profiler,
                                 base_mem=base_mem,
                                 advice_entry=profiler_entry,
                                 memory_delta=10,
                                 rev_func=eval(revenue_func_str))

    qs = range(10, 1001, 10)
    ind = np.argmin(map(lambda x: abs(x - 439), qs))
    print(ind, qs[ind])
    try:
        np.argmin([])
    except ValueError:
        pass
