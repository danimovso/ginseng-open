import logging
from mom.Comm.Messages import MessageNotifyMem
import time

costs_map_mem = {}

class Notifier():
    """
    Sends guests notification about auction winning
    """
    def __init__(self, properties):
        self.logger = logging.getLogger('Notifier')
        self.resutls = {}

    # TODO: maybe its best to move the communications to a ParallelInvoker
    # TODO: consider using a switch-case for each resource
    def process(self, host, guests):
        rnd = host.GetVar('results_round_mem')
        if rnd is None:
            return

        msg = {'not_round_mem': rnd, 'p_in_min_mem': host.GetVar('p_in_min_mem'),
               'p_out_max_mem': host.GetVar('p_out_max_mem'),
               "validity_time_mem": time.time() + host.GetVar("mem-notification-interval")}

        for g in guests:
            name = g.Prop('name')
            extra = g.GetControl('control_mem') - g.Prop("base_mem")
            msg['not_mem'] = g.GetControl('control_mem')
            msg['not_bill_mem'] = g.GetControl('control_bill_mem')
            msg['not_won_p_mem'] = float(msg['not_bill_mem']) / extra if extra != 0 else 0
            msg['not_tie_mem'] = g.GetVar('tie_winner_mem')
            msg_class = MessageNotifyMem

            try:
                # tell guest about results
                # specific_timout = 0.9 * (msg["validity_time"] - time.time()))
                data = g.Prop('comm_mem').communicate(msg_class(**msg))
                costs_map_mem.setdefault(g.Prop("name"), {})[g.GetVar('mem_extra')] = msg['not_won_p_mem']  # for costs debugging
                g.SetVar("bill_estimate_mem", data["bill_estimate_mem"])
                g.SetVar("profit_estimate_mem", data["profit_estimate_mem"])
                g.SetVar("valuation_estimate_mem", data["valuation_estimate_mem"])
                g.SetVar("valuation_mem", data["valuation_mem"])
            except Exception as ex:
                self.logger.error(str(ex))
