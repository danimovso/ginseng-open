
from mom.Controllers.Balloon import Balloon
import logging
import threading
import time
from libvirt import libvirtError
from subprocess import call, Popen, PIPE

class SoftBalloon(Balloon):
    """
    Controls the balloon driver through libvirt interface. soft is
    added to the change of balloon, so memory size will be more fluent.
    Requires the following parameters:
       'libvirt_curmem' (control)
       'balloon_target_mb' (statistic)
    Action: sets balloon driver size by libvirt domainSetBalloonTarget
    """
    def __init__(self, properties):
        Balloon.__init__(self, properties)
        self.logger = logging.getLogger('SoftBalloon')
        self.guests = {}
        self.process_lock = threading.Lock()
        self.config = properties["config"]
        self.dmem = self.config.getint("balloon", "max_mem_change_mb")
        self.interval = self.config.getint("balloon", "change_interval")

    def process(self, host, guests):
        with self.process_lock:
            for guest in guests:
                self.update_guests(guest)
            self.apply_changes()

    def update_guests(self, guest):
        target = guest.GetControl('control_mem')
        if target is None:
            return
        gid = guest.Prop('id')
        if gid not in self.guests:
            dom = self.libvirt.getDomainFromID(gid)
            self.guests[gid] = GuestBalloon(dom, guest.Prop("base_mem"),
                                            guest.Prop("name"))
        self.guests[gid].set_target(target)

    @property
    def should_run(self):
        return self.config.getint('__int__', 'running') == 1

    def apply_changes(self):
        while (self.should_run and
               any([g.memory is None or g.memory != g.target
                    for g in self.guests.itervalues()])):
            for g in self.guests.itervalues():
                # limit diff in the range [-dmem, dmem]
                diff = g.target - g.memory
                if not diff:
                    continue
                round_target = g.memory + max(min(diff, self.dmem), -self.dmem)
                try:
                    g.set_memory(round_target)
                except GuestBalloonException as ex:
                    self.logger.error("Error ballooning guest: %s", ex)
                    g.target = g.memory
            time.sleep(self.interval)
            self.logger.info("New memory state: %s",
                             ",".join(["%s: %s" % (g.name, str(g.memory))
                                       for g in self.guests.itervalues()]))

class GuestBalloonException(Exception): pass

class GuestBalloon(object):
    def __init__(self, domain, init_memory, name):
        self.domain = domain
        self.name = name
        self.max_target_mb = self.domain.maxMemory() >> 10
        self.memory = init_memory
        self.target = None

    def set_target(self, target_mb):
        self.target = min(target_mb, self.max_target_mb)

    def set_memory(self, memory_mb):
        try:
            self.domain.setMemory(memory_mb << 10)
        except libvirtError as ex:
            raise GuestBalloonException("Error changing memory: %s" % ex)
        
#        # update min_free_kbytes
#        current = int(run("sysctl -n vm.min_free_kbytes").strip())
#        updated = int(float(current) / self.memory * memory_mb)
#        run("sysctl vm.min_free_kbytes=%i" % updated)
        
        self.memory = memory_mb

def instance(properties):
    return SoftBalloon(properties)

#def main():
#    output_sem = Semaphore()
#
#    class LibvirtDummy():
#        def __init__(self):
#            pass
#        def getDomainFromID(self, pid):
#            return DomainDummy(pid)
#        def domainSetBalloonTarget(self, dom, mem):
#            output_sem.acquire()
#            print 'ballooning dom %i from %i to %i' % \
#                  (dom.pid, dom.mem >> 10, mem >> 10)
#            output_sem.release()
#            dom.setMem(mem)
#        def domainMaxMemory(self, dom):
#            return 100000000
#
#    class DomainDummy():
#        def __init__(self, pid):
#            self.pid = pid
#            self.mem = -1
#        def setMem(self, mem):
#            self.mem = mem
#
#    class GuestDummy():
#        def __init__(self, pid, curr_mem, target_mem):
#            self.pid = pid
#            self.target = target_mem
#            self.curr = curr_mem
#        def Prop(self, key):
#            if key == 'id':
#                return self.pid
#        def GetControl(self, key):
#            if key == 'balloon_target_mb':
#                return self.target
#        def Stat(self, key):
#            if key == 'libvirt_curmem':
#                return self.curr << 10 # in kb...
#
#    props = {}
#    props['libvirt_iface'] = LibvirtDummy()
#
#    soft_ballons = SoftBalloon(props)
#
#    guests = [GuestDummy(0, 100, 150),
#              GuestDummy(1, 100, 100),
#              GuestDummy(2, 100, 20),
#              GuestDummy(3, 100, 300),
#              GuestDummy(4, 100, 200)]
#
#    soft_ballons.process(None, guests)
#
#    sleep(9.9)
#    guests[0].target = 10
#    soft_ballons.process(None, guests)
#
#    sleep(11)
#    guests[1].target = 10
#    soft_ballons.process(None, guests)
#
#if __name__ == "__main__":
#    main()



