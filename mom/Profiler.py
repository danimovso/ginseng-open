'''
Created on Dec 2, 2011

@author: eyal
'''
import logging
from xml.dom.minidom import Document
import numpy as np
from scipy.interpolate.fitpack2 import RectBivariateSpline
from scipy.interpolate.fitpack2 import UnivariateSpline
import os

class Profiler:
    def __init__(self, data, params, entry, k = (1, 1), s = 0):
        # XXX 2d space only!
        zz = np.array(data[unicode(entry)])

        z = np.ndarray(zz.shape[:2])
        for i in range(z.shape[0]):
            for j in range(z.shape[1]):
                z[i, j] = np.average(zz[i, j])
        self.supported_load = None

        if len(params[0]) == 1:
            self.interp = UnivariateSpline(params[1], z, k=k[1], s=s)
            self.supported_load = params[0][0]
        else:
            self.interp = RectBivariateSpline(params[0], params[1], z,
                                              kx=k[0], ky=k[1], s=s)
        self.data = data

        for i in range(1, len(data)):
            for j in range(1, len(data[i])):
                if data[i][j] < data[i][j - 1] or data[i][j] < data[i - 1][j]:
                    raise ValueError("%d, %d" % (i, j))

        self.params_list = params
        self.entry = entry

    def interpolate(self, params):
        if self.supported_load is None:
            return np.squeeze(self.interp(params[0], params[1]))
        else:
            return np.squeeze(self.interp(params[1]))

    def save_xml(self, filename, round_digits = 3):
        from xml.dom import minidom
        doc = minidom.Document()
        doc.appendChild(self.to_xml(round_digits))
        file(filename, 'w').write(doc.toprettyxml())

    def to_xml(self, round_digits):
        doc = Document()
        node = doc.createElement('profiler')

        data = doc.createElement('data')
        for key, val in self.data.iteritems():
            entry = doc.createElement('data-entry')
            entry.setAttribute('name', key)
            ar = np.squeeze(np.array(val.tolist()))
            entry.appendChild(doc.createTextNode(str([map(lambda x: round(x, round_digits), a) for a in ar])))
            data.appendChild(entry)
        node.appendChild(data)

        params_list = doc.createElement('params_list')
        params_list.appendChild(doc.createTextNode(str(self.params_list)))
        node.appendChild(params_list)

        return node

    def draw_surf(self, dx = 20, dy = 1, dz = 1, rev_func = lambda x: x):
        import pylab as pl
        from etc.plots import nice_surf, pl_apply_defaults
        pl_apply_defaults()
        loads = range(self.params_list[0][0], self.params_list[0][-1] + 1)
        mems = pl.arange(self.params_list[1][0], self.params_list[1][-1] + dy, dy)
        perf = self.interpolate([loads, mems])
        z = map(rev_func, perf)
        x = mems
        y = loads
        nice_surf(x, y, z, "Valuation [$]")
        pl.xlim(mems[0], mems[-1])
        pl.ylim(loads[0], loads[-1])
        pl.xlabel("Memory [MB]")
        pl.ylabel("Load [connections]")

    def draw_2d(self, dmem = 100, mem_extend = 0,
                       loads = None, mems = None, rev_func = lambda x: x):
        import pylab as pl
        from etc.plots import pl_apply_defaults
        pl_apply_defaults()
        if not loads:
            loads = self.params_list[0]
        if not mems:
            mems = pl.arange(self.params_list[1][0] - mem_extend,
                        self.params_list[1][-1] + mem_extend, dmem)
        perf = self.interpolate([loads, mems])
        for i, l in enumerate(self.params_list[0]):
            pl.scatter(self.params_list[1], map(rev_func, self.data[self.entry][i]),
                       marker = "o",
                       c = pl.cm.jet(float(l) / loads[-1]),  #@UndefinedVariable
#                       label = "load: %i" % loads[i], alpha = 0.5, s = 40)
                       label = "load: %i" % loads[i], alpha = 1.0, s = 40)
        pl.legend(loc = "best")
        for i, l in enumerate(loads):
            revs = map(rev_func, perf[i])
            pl.plot(mems, revs, c = pl.cm.jet(float(l) / loads[-1]),  #@UndefinedVariable
                    marker = "None")  # @UndefinedVariable
            pl.text(mems[-1] + 50, revs[-1], l)
        pl.grid(True)
        pl.ylabel("Valuation [$]")
        pl.xlabel("Memory [MB]")

    def draw_eps(self, loads, mems, rev_func = lambda x: x):
        import pylab as pl
        from etc.plots import pl_apply_defaults
        from etc.plots import draw_n_save
        pl_apply_defaults()

        perf = self.interpolate([loads, mems])
        mems = np.array(mems) / 1000.
        for i in reversed(range(len(loads))):
            revs = map(rev_func, perf[i])
            pl.plot(mems, revs,
                    label = "load: %i" % loads[i],
                    c = pl.cm.jet(float(i) / len(loads)),  # @UndefinedVariable
                    marker = str(pl.Line2D.filled_markers[i]),
                    markersize = 4)
        pl.legend(loc = "lower right")
        pl.grid(True)
        pl.ylabel("Valuation [$/s]", labelpad = 2)
#        pl.ylabel(r"Performance [Khits/s]", labelpad = 2)
        pl.xlabel("Memory [GB]", labelpad = 2)
        pl.gca().set_position((0.12, 0.12, 0.8, 0.8))
        pl.gca().set_xticks(np.arange(0.600, 2.4, .200))
        pl.gca().set_yticks(np.arange(0, 11, 0.5))
        pl.gca().set_ylim((0, 10))
        pl.gca().set_xlim((0.6, 2.200))
        draw_n_save("profiler.eps", size = (3.2, 3.2))

    def draw_p(self, dmem = 200, mem_extend = 0,
               loads = None, mems = None, rev_func = lambda x: x):
        import pylab as pl
        from etc.plots import pl_apply_defaults
        pl_apply_defaults()
        if not loads:
            loads = self.params_list[0]
        if not mems:
            mems = np.arange(self.params_list[1][0] - mem_extend,
                        self.params_list[1][-1] + mem_extend, dmem)
        perf = self.interpolate([loads, mems])
        pl.legend(loc = "best")
        rev_func = np.vectorize(rev_func)
        for i, l in enumerate(loads):
            revs = rev_func(perf[i])
            q = (mems - mems[0])[1:]
            p = (revs - revs[0])[1:] / q
            pl.plot(mems[1:], p,
                    c = pl.cm.jet(float(l) / loads[-1]),  #@UndefinedVariable
                    marker = "o")  # @UndefinedVariable
            pl.text(mems[-1] + 50, p[-1], l)
        pl.grid(True)
        pl.ylabel("p [$/MB]")
        pl.xlabel("Memory [MB]")

    def draw_dev(self, dmem = 200, mem_extend = 0,
               loads = None, mems = None, rev_func = lambda x: x):
        import pylab as pl
        from etc.plots import pl_apply_defaults
        pl_apply_defaults()
        if not loads:
            loads = self.params_list[0]
        if not mems:
            mems = np.arange(self.params_list[1][0] - mem_extend,
                        self.params_list[1][-1] + mem_extend, dmem)
        perf = self.interpolate([loads, mems])
        pl.legend(loc = "best")
        rev_func = np.vectorize(rev_func)
        for i, l in enumerate(loads):
            revs = rev_func(perf[i])
            dev = (revs[1:] - revs[:-1]) / (mems[1:] - mems[:-1])
            x = (mems[1:] + mems[:-1]) / 2
            pl.plot(x, dev,
                    c = pl.cm.jet(float(l) / loads[-1]),  #@UndefinedVariable
                    marker = "o")  # @UndefinedVariable
            pl.text(x[-1] + 50, dev[-1], l)
        pl.grid(True)
        pl.ylabel("dev")
        pl.xlabel("Memory [MB]")

def fromxml(filename, entry):
    from xml.dom import minidom
    # minidom.parse - must remove white-spaces from data, thus using parseString
    fd = open(filename)
    lines = fd.readlines()
    fd.close()
    content = "".join(l.strip() for l in lines)
    # parsing...
    data = {}
    doc = minidom.parseString(content)
    data_node = doc.getElementsByTagName("data")[0]
    for node in data_node.childNodes:
        node_name = node.getAttribute("name")
        data[node_name] = eval(node.firstChild.data.replace("nan", "0"))

    params_node = doc.getElementsByTagName("params_list")[0]
    params_list = eval(params_node.firstChild.data)

    return Profiler(data, params_list, entry)

if __name__ == "__main__":
    import pylab as pl
#    name = "%s/moc/doc/profiler-mc-spare-50-satur-2000.xml" % os.path.expanduser("~")
#    prf = fromxml(name, entry = "hits_rate")
##    prf.draw_eps(loads = [1, 2, 4, 6, 8, 10], mems = range(600, 2201, 200),
##                 rev_func = lambda p : (p ** 2) / 1000)
#    prf.draw_eps(loads = [1, 2, 4, 6, 8, 10], mems = range(600, 2201, 200),
#                 rev_func = lambda p : p / 10)

    prof = Profiler({'hits_rate': [[1, 2, 3]]}, [[5], [40, 50, 60]], 'hits_rate')
    print prof.interpolate([5, 45])
    print prof.interpolate([5, 70])
    print prof.interpolate([4, 50])
    prof2 = Profiler({'hits_rate': [[1, 2, 3], [4, 5, 6]]}, [[5, 7], [40, 50, 60]], 'hits_rate')
    print prof2.interpolate([5, 45])
    print prof2.interpolate([6, 45])
    print prof2.interpolate([8, 45])


    name = "%s/workspace/moc/doc/profiler-memcached-inside-spare-50-win-500k.xml" % os.path.expanduser("~")
    prf = fromxml(name, entry = "hits_rate")
    prf.draw_2d()
    pl.show()
#    prf.draw_eps(loads = range(11),  #[1, 2, 4, 6, 7, 8, 10],
#                 mems = range(600, 2201, 200),
#                 rev_func = lambda p : p / 1000)
#    prf.draw_eps(loads = [1, 2, 4, 6, 8, 10], mems = range(600, 2201, 200),
#                 rev_func = lambda p : p / 1000)

#    name = "%s/moc-output/testbed-bad/Memcached-90/testbed-profiler-Memcached-vm-4.xml" % os.path.expanduser("~")
#    prf = fromxml(name, entry = "get_hits_total")
#    prf.draw_eps(loads = [1, 2, 4, 6, 8, 10], mems = range(600, 2601, 200),
#                 rev_func = lambda p : p / 1000 / 200)


#    name = "%s/moc-output/testbed-dshad4/MemcachedStatic-2/profiler.xml" % os.path.expanduser("~")
#    prf = fromxml(name, entry = "get_hits_total")
#    prf.draw_eps(loads = [1, 2, 4, 6, 8, 10], mems = range(300, 801, 100),
#                 rev_func = lambda p : p / 1000 / 200)
