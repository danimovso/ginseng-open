/* util.c ....... error message utilities.
 *                C. Scott Ananian <cananian@alumni.princeton.edu>
 *                hacked by Muli Ben-Yehuda <mulix@actcom.co.il> 
 *
 */

#include <stdio.h>
#include <stdarg.h>
#include <syslog.h>
#include <unistd.h>
#include <stdlib.h> 

#include "util.h"

static inline void 
make_string(const char* label, va_list* ap, 
	    const char* format, const char* func, 
	    char* out, size_t outsz)
{
	char buf[8192]; 

	vsnprintf(buf, sizeof(buf), format, *ap);
	snprintf(out, outsz, "%s[%s]: %s", label, func, buf);
}

void _log(const char *func, char *format, ...) 
{
	va_list ap; 
	char string[8192]; 
	va_start(ap, format); 
	make_string("log", &ap, format, func, string, sizeof(string)); 
	va_end(ap); 
	fprintf(stderr, "%s", string);
}

void _warn(const char *func, char *format, ...) 
{
	va_list ap; 
	char string[8192]; 
	va_start(ap, format); 
	make_string("warn", &ap, format, func, string, sizeof(string)); 
	va_end(ap); 
	fprintf(stderr, "%s", string);
}

void _die(int retcode, const char *func, char *format, ...) 
{
	va_list ap; 
	char string[8192]; 
	va_start(ap, format); 
	make_string("die", &ap, format, func, string, sizeof(string)); 
	va_end(ap); 
	fprintf(stderr, "%s", string);
	exit(retcode);
}

