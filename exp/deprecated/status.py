#!/usr/bin/env python
'''
Created on Nov 12, 2011

@author: eyal
'''
import matplotlib
matplotlib.use('WXAgg')
from exp.core.OutputProperties import HOST, MON, POL, def_func, Prop, diff_from_min
from matplotlib.figure import Figure
from matplotlib.backends.backend_wxagg import FigureCanvasWxAgg as FigCanvas
import os
import wx
import numpy as np
import pylab as pl
from mom.Plotter import Plotter

import types
import subprocess

refresh_time = 500

history = {HOST : {POL : {}, MON : {}}}

class WinProp(Prop):
    def __init__(self, var_name, disp_name=None, func=def_func,
                 tp=types.FloatType, width=10):
        Prop.__init__(self, var_name, disp_name, func, tp, width)
        self.checked = None

    def get_plot_arrays(self, res, t0):
        t = []
        y = []
        for i in sorted(res['time'].keys()):
            try:
                if res[self.var_name].has_key(i):
                    ti = res['time'][i] - t0
                    yi = res[self.var_name][i]
                    t.append(ti)
                    y.append(yi)
            except KeyError:
                pass
        y = self.func(y)
        return t, y

class WinPolicyProp(WinProp):
    def get_column(self, res):
        return self._get_column(res.get(POL, {}))

    def get_plot_arrays(self, res, t0):
        t = []
        y = []
        if t0 is not None and res.has_key(POL):
            t, y = WinProp.get_plot_arrays(self, res[POL], t0)
        return t, y

    def add_plot(self, ax, label_pre=""):
        return ax.plot([], [], '-s',
                       label=label_pre + self.disp_name,
                       linestyle='steps-post', markersize=6)[0]

class WinMonitorProp(WinProp):
    def get_column(self, res):
        return self._get_column(res[MON])

    def get_plot_arrays(self, res, t0):
        t = []
        y = []
        if t0 is not None and res.has_key(MON):
            t, y = WinProp.get_plot_arrays(self, res[MON], t0)
        return t, y

    def add_plot(self, ax, label_pre=""):
        return ax.plot([], [], label=label_pre + self.disp_name)[0]

hfields = {# Policies
           "pol-time"    : WinPolicyProp('time', func=diff_from_min),
           "auc-mem" : WinPolicyProp('auction_mem', 'auc-mem'),
           'results_round_mem' : WinPolicyProp('results_round_mem', 'round-res'),
           'auc-round' : WinPolicyProp('auction_round', 'round', tp=types.IntType),
           # Monitors
           'mon-time' : WinMonitorProp('time', func=diff_from_min),
           'mem_avail' : WinMonitorProp('mem_available', 'host mem'),
           'mem_free' : WinMonitorProp('mem_free', 'mem free'),
           'swap_out': WinMonitorProp('swap_out', 'swap out'),
           'swap_in' : WinMonitorProp('swap_in', 'swap in'),
           'anon_pages' : WinMonitorProp('anon_pages', 'anon pages', tp=types.IntType),
           'mem_unused' : WinMonitorProp('mem_unused', 'unused mem', tp=types.IntType)}

gfields = {# Policies
           'pol-time' : WinPolicyProp('time', func=diff_from_min),
           'p' : WinPolicyProp('p'),
           'r' : WinPolicyProp('r'),
           'mem_base' : WinPolicyProp('mem_base', 'base mem'),
           'control-mem' : WinPolicyProp('control_mem', 'alloc'),
           'control-bill' : WinPolicyProp('control_bill', 'bill'),
           'mem_extra': WinPolicyProp('mem_extra', 'mem extra'),
           'mem-extra-cost': WinPolicyProp('mem_extra_cost', 'mem extra cost'),
           'mem-curr' : WinPolicyProp('curr_mem', 'mem curr'),
           'mem-without' : WinPolicyProp('mem_without', 'mem without'),
           'mem-free' : WinPolicyProp('mem_free', 'mem free'),
           # Monitors
           'mon-time' : WinMonitorProp('time', func=diff_from_min),
           'mem-avail' : WinMonitorProp('mem_available', 'mem'),
           'mem-free' : WinMonitorProp('mem_free', 'mem free'),
           'app-perf' : WinMonitorProp('perf', 'perf', func=lambda y: [yi['throughput'] if yi['throughput'] is not None else 0 for yi in y]),
           'app-load' : WinMonitorProp('load', 'load'),
           'app-rev' : WinMonitorProp('rev', 'rev'),
           'swap-out': WinMonitorProp('swap_out', 'swap out'),
           'swap-in' : WinMonitorProp('swap_in', 'swap in'),
           'major-fault' : WinMonitorProp('major_fault', 'major fault', tp=types.IntType),
           'minor-fault' : WinMonitorProp('minor_fault', 'minor fault', tp=types.IntType),
           'host-minor-faults' : WinMonitorProp('host_minor_faults', 'host minor faults', tp=types.IntType),
           'host-major-faults' : WinMonitorProp('host_major_faults', 'host major faults', tp=types.IntType),
           'bid-price' : WinMonitorProp('bid_p', 'bid p'),
           'bid-round' : WinMonitorProp('bid_round', 'bid round'),
           'rss': WinMonitorProp('rss')}

class BoundControlBox(wx.Panel):
    """ A static box with a couple of radio buttons and a text
        box. Allows to switch between an automatic mode and a 
        manual mode with an associated value.
    """
    def __init__(self, parent, ID, label, initval):
        wx.Panel.__init__(self, parent, ID)

        self.value = initval

        box = wx.StaticBox(self, -1, label)
        sizer = wx.StaticBoxSizer(box, wx.VERTICAL)

        self.radio_auto = wx.RadioButton(self, -1, label="Auto",
                                         style=wx.RB_GROUP)
        self.radio_manual = wx.RadioButton(self, -1, label="Manual")
        self.manual_text = wx.TextCtrl(self, -1,
            size=(35, -1),
            value=str(initval),
            style=wx.TE_PROCESS_ENTER)

        self.Bind(wx.EVT_UPDATE_UI, self.on_update_manual_text, self.manual_text)
        self.Bind(wx.EVT_TEXT_ENTER, self.on_text_enter, self.manual_text)

        manual_box = wx.BoxSizer(wx.HORIZONTAL)
        manual_box.Add(self.radio_manual, flag=wx.ALIGN_CENTER_VERTICAL)
        manual_box.Add(self.manual_text, flag=wx.ALIGN_CENTER_VERTICAL)

        sizer.Add(self.radio_auto, 0, wx.ALL, 10)
        sizer.Add(manual_box, 0, wx.ALL, 10)

        self.SetSizer(sizer)
        sizer.Fit(self)

    def on_update_manual_text(self, event):
        self.manual_text.Enable(self.radio_manual.GetValue())

    def on_text_enter(self, event):
        self.value = self.manual_text.GetValue()

    def is_auto(self):
        return self.radio_auto.GetValue()

    def manual_value(self):
        return self.value

class GraphFrame(wx.Frame):
    """ The main frame of the application
    """
    title = 'VM Memory Auction View'

    def __init__(self, h_props, g_props):
        wx.Frame.__init__(self, None, -1, self.title)

        self.plots = []
        self.guests = set([])
        self.t0 = None
        self.paused = False
        self.h_props = h_props
        self.g_props = g_props
        self.tmax = 0

        self.create_menu()
        self.create_main_panel()

        self.redraw_timer = wx.Timer(self)
        self.Bind(wx.EVT_TIMER, self.on_redraw_timer, self.redraw_timer)
        self.redraw_timer.Start(refresh_time)

    def create_menu(self):
        self.menubar = wx.MenuBar()

        menu_file = wx.Menu()
        m_expt = menu_file.Append(-1, "&Save plot\tCtrl-S", "Save plot to file")
        self.Bind(wx.EVT_MENU, self.on_save_plot, m_expt)
        menu_file.AppendSeparator()
        m_exit = menu_file.Append(-1, "E&xit\tCtrl-X", "Exit")
        self.Bind(wx.EVT_MENU, self.on_exit, m_exit)

        self.menubar.Append(menu_file, "&File")
        self.SetMenuBar(self.menubar)

    def create_main_panel(self):
        self.panel = wx.Panel(self)

        self.dpi = 100
        self.fig = Figure((3.0, 3.0), dpi=self.dpi)
        self.init_plot()

        self.canvas = FigCanvas(self.panel, -1, self.fig)

        self.xmin_control = BoundControlBox(self.panel, -1, "X min", 0)
        self.xmax_control = BoundControlBox(self.panel, -1, "X max", 50)
        self.ymin_control = BoundControlBox(self.panel, -1, "Y min", 0)
        self.ymax_control = BoundControlBox(self.panel, -1, "Y max", 100)

        self.pause_button = wx.Button(self.panel, -1, "Pause")
        self.Bind(wx.EVT_BUTTON, self.on_pause_button, self.pause_button)
        self.Bind(wx.EVT_UPDATE_UI, self.on_update_pause_button, self.pause_button)

        self.hbox1 = wx.BoxSizer(wx.HORIZONTAL)
        self.hbox1.Add(self.pause_button, border=5,
                       flag=wx.ALL | wx.ALIGN_CENTER_VERTICAL)

        self.hbox2 = wx.BoxSizer(wx.HORIZONTAL)

        self.vbox_h_props = wx.BoxSizer(wx.VERTICAL)
        self.vbox_h_props.Add(wx.StaticText(self.panel, label="Host Props"),
                              flag=wx.ALL | wx.ALIGN_CENTER_VERTICAL)
        for prop in self.h_props:
            check = wx.CheckBox(self.panel, -1, prop.disp_name, style=wx.ALIGN_LEFT)
            check.SetValue(True)
            self.Bind(wx.EVT_CHECKBOX, self.on_cb_xlab, check)
            self.vbox_h_props.Add(check, flag=wx.ALL | wx.ALIGN_CENTER_VERTICAL)
            prop.checked = check
        self.hbox2.Add(self.vbox_h_props)

        self.hbox2.AddSpacer(10)

        self.vbox_g_props = wx.BoxSizer(wx.VERTICAL)
        self.vbox_g_props.Add(wx.StaticText(self.panel, label="Guest Props"),
                              flag=wx.ALL | wx.ALIGN_CENTER_VERTICAL)
        for prop in self.g_props:
            check = wx.CheckBox(self.panel, -1, prop.disp_name, style=wx.ALIGN_LEFT)
            check.SetValue(True)
#            self.Bind(wx.EVT_CHECKBOX, self.on_cb_xlab, check)
            self.vbox_g_props.Add(check, flag=wx.ALL | wx.ALIGN_CENTER_VERTICAL)
            prop.checked = check
        self.hbox2.Add(self.vbox_g_props)

        self.hbox2.Add(self.xmin_control, border=5, flag=wx.ALL)
        self.hbox2.Add(self.xmax_control, border=5, flag=wx.ALL)
        self.hbox2.Add(self.ymin_control, border=5, flag=wx.ALL)
        self.hbox2.Add(self.ymax_control, border=5, flag=wx.ALL)

        self.vbox = wx.BoxSizer(wx.VERTICAL)
        self.vbox.Add(self.canvas, 1, flag=wx.LEFT | wx.TOP | wx.GROW)
        self.vbox.Add(self.hbox1, 0, flag=wx.ALIGN_LEFT | wx.TOP)
        self.vbox.Add(self.hbox2, 0, flag=wx.ALIGN_LEFT | wx.TOP)

        self.panel.SetSizer(self.vbox)
        self.vbox.Fit(self)

    def __add_subplot(self, curr_plot, num_plots, props, title):
        ax = self.fig.add_subplot(num_plots, 1, curr_plot)
        ax2 = pl.twinx(ax)
        ax.set_axis_bgcolor('white')
        ax.set_title(title, size=12)
        pl.setp(ax.get_xticklabels(), fontsize=8)
        pl.setp(ax.get_yticklabels(), fontsize=8)

        lines = []
        for prop in props:
            prop_ax = ax if prop.var_name not in ["p"] else ax2
            line = prop.add_plot(prop_ax, "")
            lines.append((line, prop))

        self.plots.append({'axes': ax, 'axes2': ax2, 'lines': lines,
                           'name': ax.title._text,
                           'ymin': None, 'ymax': None,
                           'ymin2': None, 'ymax2': None})

    def init_plot(self):
        self.query_moc()
        self.guests = self._all_guests(history)
        self.plots = []
        num_plots = 0
        curr_plot = 0
        if len(self.g_props) > 0: num_plots += len(self.guests)

        if len(self.h_props) > 0:
            num_plots += 1
            curr_plot = 1
            self.__add_subplot(1, num_plots, self.h_props, HOST)

        for guest in self.guests:
            curr_plot += 1
            self.__add_subplot(curr_plot, num_plots, self.g_props, guest)

    def _all_guests(self, res):
        guests = set(res.keys())
        guests.remove(HOST)
        return guests

    def _get_t0(self, data):
        try:
            if len(data[HOST][POL]['time']) == 0:
                return False
        except KeyError:
            return False
        time = data[HOST][MON]['time']
        t0 = time[min(time.keys())]
        for name in data.keys():
            for tp in data[name].keys():
                t0 = min([t0, data[name][tp]['time'].values()])
        self.t0 = t0
        return True

    def draw_plot(self):
        """ Redraws the plot
        """
        data = history
#        if self.guests != self._all_guests(data):
#            self.guests = self._all_guests(data)
#            pl.xticks([0])
#            pl.yticks([0])
#            pl.grid(False)
#            pl.clf()
#            self.init_plot()

        if self.t0 is None:
            if not self._get_t0(data): return

        if data is not None:
            for plot in self.plots:
                ymin = float('inf')
                ymax = -float('inf')
                ymin2 = float('inf')
                ymax2 = -float('inf')
                for line, prop in plot['lines']:
                    if prop.checked.IsChecked() == False:
                        line.set_visible(False)
                        continue
                    line.set_visible(True)
                    t, y = prop.get_plot_arrays(data[plot['name']], self.t0)
                    if len(y) == 0:
                        continue
                    self.tmax = max([self.tmax] + t)
                    if prop.var_name in ["p"]:
                        ymax2 = max([ymax2] + y)
                        ymin2 = min([ymin2] + y)
                    else:
                        ymax = max([ymax] + y)
                        ymin = min([ymin] + y)
                    line.set_xdata(np.array(t))
                    line.set_ydata(np.array(y))
                plot['ymin'] = ymin / 1.1
                plot['ymax'] = ymax * 1.1
                plot['ymin2'] = ymin2 / 1.1
                plot['ymax2'] = ymax2 * 1.1

        for plot in self.plots:
            ax = plot['axes']
            ax2 = plot['axes2']

            ax.grid(True)
            pl.setp(ax.get_xticklabels(), visible=False)

            tmax = self.tmax
            if not self.xmax_control.is_auto():
                tmax = int(self.xmax_control.manual_value())
            tmin = 0
            if not self.xmin_control.is_auto():
                tmin = int(self.xmin_control.manual_value())

            ax.set_xbound(lower=tmin, upper=tmax)
            ax.set_ybound(lower=plot['ymin'], upper=plot['ymax'])
            ax2.set_ybound(lower=plot['ymin2'], upper=plot['ymax2'])

        pl.setp(self.plots[-1]['axes'].get_xticklabels(), visible=True)
        self.canvas.draw()

    def on_pause_button(self, event):
        self.paused = not self.paused

    def on_update_pause_button(self, event):
        label = "Resume" if self.paused else "Pause"
        self.pause_button.SetLabel(label)

    def on_cb_grid(self, event):
        pass
#        self.draw_plot()

    def on_cb_xlab(self, event):
        pass
#        self.draw_plot()

    def on_save_plot(self, event):
        file_choices = "PNG (*.png)|*.png"

        dlg = wx.FileDialog(
            self,
            message="Save plot as...",
            defaultDir=os.getcwd(),
            defaultFile="plot.png",
            wildcard=file_choices,
            style=wx.SAVE)

        if dlg.ShowModal() == wx.ID_OK:
            path = dlg.GetPath()
            self.canvas.print_figure(path, dpi=self.dpi)
            self.flash_status_message("Saved to %s" % path)

    def query_moc(self):
        global history
        data = subprocess.Popen(["python", "../mom_rpc_client.py", "--get-results"],
                                stdout=subprocess.PIPE).communicate()[0]
        if data.startswith("Command 'get_results' failed"):
            return
        data = eval(data)
        for name in data.keys():
            history.setdefault(name, {POL:{}, MON:{}})
            for tp in data[name].keys():
                for prop in data[name][tp].keys():
                    history[name][tp].setdefault(prop, {})
                    t, val = data[name][tp][prop]
                    history[name][tp][prop][t] = val

    def on_redraw_timer(self, event):
        # if paused do not add data, but still redraw the plot
        # (to respond to scale modifications, grid change, etc.)
        if not self.paused:
            self.query_moc()
            self.draw_plot()

    def on_exit(self, event):
        self.Destroy()

    def flash_status_message(self, msg, flash_len_ms=refresh_time):
        self.statusbar.SetStatusText(msg)
        self.timeroff = wx.Timer(self)
        self.Bind(wx.EVT_TIMER, self.on_flash_status_off, self.timeroff)
        self.timeroff.Start(flash_len_ms, oneShot=True)

def_host_plot = [hfields[prop] for prop in ["auc-mem"]]
def_guest_plot = [gfields[prop] for prop in
                  ('mem-avail', 'p', 'app-load', 'app-perf')]

if __name__ == '__main__':
    app = wx.PySimpleApp()
    app.frame = GraphFrame(def_host_plot, def_guest_plot)
    app.frame.Show()
    app.MainLoop()
