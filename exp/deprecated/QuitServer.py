'''
Created on Mar 5, 2012

@author: eyal
'''
from etc.PySock import TcpThreadedServer, Client
import time
import socket

class QuitServer(TcpThreadedServer):
    def __init__(self, port, teminate_func, timeout = None, name = "QuitServer"):
        self.terminate_func = teminate_func
        TcpThreadedServer.__init__(self, "", port, timeout, name)

    def process(self, msg):
        if msg == "finish":
            self.logger.info("GOT REMOTE QUIT SIGNAL")
            self.terminate_func()

def send_quit_request(ip, port):
    client = Client(ip, port, timeout = None, name = "QuitClient-%s" % ip)
    try:
        for i in range(3):
            try:
                client.send("finish")
                break
            except socket.error:
                client.logger.info("Can't send finish request #%i", i + 1)
                time.sleep(3)
    finally:
        client.close()
