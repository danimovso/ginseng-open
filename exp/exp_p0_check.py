'''
Created on Apr 1, 2012

@author: eyal
'''
import exp.core.Loads
from exp.prog.Programs import Guest_Memcached, Host_Memcached, \
    Host_PostgresSQL, Guest_PostgresSQL
from mom.LogUtils import LogUtils
from exp.core.Experiment import set_output_path
import os
from exp.core.VMDesc import get_vm_desc
from exp.core.ExpMachine import ExpMachineProps
from exp.core.Experiment import Experiment
from exp.core.Loads import LoadBinary, LoadPiecewiseLinear
from mom.Collectors.HostMemory import HostMemory

t_auction = [3, 1, 8]
warmup = 60 * 60
duration = 60 * 30
oc = 2
frequency = [60]#[15, 60]
#frequency = [60, 120, 180]
#p0s = map(lambda p: p / 10.0, range(0, 11, 2))
p0s = [0]#[0, 0.1, 0.2, 0.4, 0.5, 1.0]
#alloc_diff = 0

class Configuration(object):
    def __init__(self, prog_args, bm_args, base_mem, saturation_mem,
                 profiler_file, profiler_entry, revenue_func_str,
                 load, load_interval, val_switch_func = None, max_vcpus=1,
                 swappiness=100, vm_class="default"):
        self.prog_args = prog_args
        self.bm_args = bm_args
        self.base_mem = base_mem
        self.saturation_mem = saturation_mem
        self.profiler_file = profiler_file
        self.profiler_entry = profiler_entry
        self.load = load
        self.load_interval = load_interval
        self.revenue_func_str = revenue_func_str
        self.val_switch_func = val_switch_func
        self.max_vcpus = max_vcpus
        self.swappiness = swappiness
        self.vm_class = vm_class
        self.adviser = {'name': 'AdviserProfitEstimator',
                        'profiler': profiler_file,
                        'advice_entry': profiler_entry,
                        'rev_func': revenue_func_str,
                        'base_mem': base_mem, 'memory_delta': 10} #,
#                        'alloc_diff': alloc_diff}

configs = {} # config set pre frequency
for f in frequency:
    configs[f] = [
        Configuration( # psql
            prog_args = Guest_PostgresSQL(
                           checkpoint_mem_ratio=[0.5],
                           autovacuum=[False],
                           checkpoint_completion_target=[0.9],
                           checkpoint_timeout=[300],
                           shared_buffers_MB=[64],
                           checkpoint_mem_static_MB=[4096],
                           work_mem_MB=[1],
                           max_connections=[60],
                           checkpoint_mem_change_threshold=[0.05],
                           temp_buffers_MB=[1]
            ).command_args,
            bm_args = Host_PostgresSQL(
                           output_log=[False],
                           threads_count=[150],
                           connection_per_transaction=[False],
                           measure_frequency=5,
                          ).command_args,
            base_mem = 3500,
            saturation_mem = 5600,  # according to the profiler
            profiler_file = "doc/profiler-postgres-tapuz25.xml",
            profiler_entry = "tps_with_connections_time",
            load = exp.core.Loads.LoadConstant(50),
            load_interval = 5 * 60,
            revenue_func_str = ['lambda x:5*x', 'lambda x: x'], # first valuation is warmup
            val_switch_func = 'lambda t: 0 if t < %i else 1' % (warmup,),
            max_vcpus=4,
            swappiness=1,
           ),
        Configuration( # MCD
            prog_args = Guest_Memcached(
                           spare_mem = 50,
                           init_mem_size = 10,
                           update_interval = 1
                          ).command_args,
            bm_args = Host_Memcached(
                           cmd_get_percent = 0.3,
                           keys_dist = [(249, 249, 1.0)],
                           vals_dist = [(1024, 1024, 1.0)],
                           win_size = "500k",
                          ).command_args,
            base_mem = 600,
            saturation_mem = 2500,
            profiler_file = "doc/profiler-memcached-inside-spare-50-win-500k-tapuz25.xml",
            profiler_entry = "hits_rate",
            load = exp.core.Loads.LoadConstant(8),
            load_interval = warmup + duration - 3 * 60,
            revenue_func_str = ['lambda x:0.5*x', 'lambda x:1.4*x'],
            val_switch_func = LoadBinary(v1 = 0, v2 = 1, T = f, T0 = 0),
           ),
        ]

if __name__ == '__main__':
    LogUtils("debug")
    name = "p0-frequency-nomc"
    output_path = set_output_path("exp", name)
    for f, config in configs.iteritems():
        n = len(config)
        vm_names = ["vm-%i" % (i + 1) for i in range(n)]
        vms = {}

        min_mem = sum([c.base_mem for c in config])
        max_mem = sum([c.saturation_mem for c in config])
        auction_mem = int((max_mem - min_mem) / oc)
#    auction_mem = 500 # let's try this
        total_mem = HostMemory(None).collect()["mem_available"]
#    host_mem = total_mem - auction_mem - min_mem
        host_mem = 500
        print "Overcommitment: %.2f\nTotal memory: %i\nAuction memory: %i\nHost memory: %i\nFrequency: %i" % (oc, total_mem, auction_mem, host_mem, f)

        for name, conf in zip(vm_names, config):
            vms[name] = ExpMachineProps(
                            adviser_args = conf.adviser,
                            prog_args = conf.prog_args,
                            bm_args = conf.bm_args,
                            load_func = conf.load,
                            load_interval = conf.load_interval,
                            desc = get_vm_desc(name, conf.vm_class),
                            vm_class=conf.vm_class,
                            val_switch_func=conf.val_switch_func,
                            max_vcpus=conf.max_vcpus,
                            swappiness=conf.swappiness,
                           )
            vms[name]['desc']['base_mem'] = conf.base_mem
            vms[name]['desc']['max_mem'] = conf.saturation_mem #total_mem

        for p0 in p0s:
            print "batch starting with p0 = %.2f and frequency = %i" % (p0, f)
            exp_out = os.path.join(output_path, "p0-%.2f-freq-%i" % (p0, f))
            os.mkdir(exp_out)
            moc_args = ("MocAuctioneer", (host_mem, p0, t_auction, auction_mem)) # hopefully, will update p0 and not reclaim factor
            Experiment(
                 moc_args = moc_args,
                 vms_desc = vms,
                 output_path = exp_out,
                 duration=warmup + duration,
                 verbosity = "debug",
                ).start()
            print "Results are in %s" % output_path
        '''
        print "starting static alternative with frequency %i" % f
        exp_out = os.path.join(output_path, "p0-static-freq-%i" % f)
        os.mkdir(exp_out)
        vms['vm-1']['desc']['base_mem'] += auction_mem
        
        static_args = ("MocStatic",)

        Experiment(
                  moc_args=static_args,
                  vms_desc=vms,
                  output_path=exp_out,
                  duration=duration,
                  verbosity="debug",
                 ).start()
        '''
