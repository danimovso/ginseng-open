'''
Created on Jul 27, 2014

@author: liran
'''

import sys

from etc.NamedParameters import NamedParameters, \
    ListSingleParameterGroup, ListMultiParameterGroup,\
    SingleParameterGroup
from exp.common.MultiExperiments import MultiExperiments
from exp.common.FunctionOfTime import FunctionOfTime
from exp.common.StepExperiments import StepExperiments

def StaticMemory_FunctionOfTime_Hours(allocation, name):
    return FunctionOfTime([allocation], FunctionOfTime.HOURS, FunctionOfTime.HOURS, FunctionOfTime.SECONDS, "MB", representation = name)

def get_pgbench_parameters():
    return NamedParameters({
        "exp_type" : SingleParameterGroup(dict(
            batch = MultiExperiments.TYPE_BATCH,
            step = MultiExperiments.TYPE_STEP_TEST,
            bed = MultiExperiments.TYPE_TESTBED
         ), "batch"),

        "vm_names" : ListSingleParameterGroup(dict(
            [("vm%s" % i, "vm-%s" % i) for i in range(1,13)]
        ), "vm-1"),

        # Step-test parameters
        "step_timings" : ListSingleParameterGroup(dict(
            # Exp. duration: 3 min + 3 min, + 3 min = 9 min
            step_short = [180,180,180],

            # Exp. duration: 3 h + 3 h, + 3 h = 9 hours
            step_long = [10800, 10800, 10800],

            # Exp. duration: 9 hours (single step)
            step_single_low = [32400],

            # Exp. duration: 9 hours (single step)
            step_single_high = [10, 32400]
        ), show = False),

       "mem_bases" : ListSingleParameterGroup(dict(
            mem_base_low = 256,
            mem_base_medium1 = 512,
            mem_base_medium2 = 512+128,
            mem_base_medium3 = 512+256,
            mem_base_high = 1024
        ), "mem_base_high", show = False),

        "mem_steps" : ListSingleParameterGroup(dict(
            mem_step_low = 1024,
            mem_step_medium = 4096,
            mem_step_high = 8192
        ), "mem_step_high", show = False),

        # Other memory functions
        "memory_functions" : ListSingleParameterGroup({
            # Static memory
            "mem_12G" : StaticMemory_FunctionOfTime_Hours((12288, 1), "12GB"),
            "mem_9G" : StaticMemory_FunctionOfTime_Hours((9216, 1), "9GB"),
            "mem_8G" : StaticMemory_FunctionOfTime_Hours((8192, 1), "8GB"),
            "mem_7.5G" : StaticMemory_FunctionOfTime_Hours((7168+512, 1), "7.5GB"),
            "mem_7G" : StaticMemory_FunctionOfTime_Hours((7168, 1), "7GB"),
            "mem_6.5G" : StaticMemory_FunctionOfTime_Hours((6144+512, 1), "6.5GB"),
            "mem_6G" : StaticMemory_FunctionOfTime_Hours((6144, 1), "6GB"),
            "mem_5.5G" : StaticMemory_FunctionOfTime_Hours((5120+512, 1), "5.5GB"),
            "mem_5G" : StaticMemory_FunctionOfTime_Hours((5120, 1), "5GB"),
            "mem_4.5G" : StaticMemory_FunctionOfTime_Hours((4096+512, 1), "4.5GB"),
            "mem_4G" : StaticMemory_FunctionOfTime_Hours((4096, 1), "4GB"),
            "mem_3.5G" : StaticMemory_FunctionOfTime_Hours((3072+512, 1), "3.5GB"),
            "mem_3G" : StaticMemory_FunctionOfTime_Hours((3072, 1), "3GB"),
            "mem_2.5G" : StaticMemory_FunctionOfTime_Hours((2048+512, 1), "2.5GB"),
            "mem_2G" : StaticMemory_FunctionOfTime_Hours((2048, 1), "2GB"),
            "mem_1.5G" : StaticMemory_FunctionOfTime_Hours((1024+512, 1), "1.5GB"),
            "mem_1G" : StaticMemory_FunctionOfTime_Hours((1024, 1), "1GB"),
            "mem_900M" : StaticMemory_FunctionOfTime_Hours((896, 1), "896MB"),
            "mem_700M" : StaticMemory_FunctionOfTime_Hours((768, 1), "768MB"),
            "mem_600M" : StaticMemory_FunctionOfTime_Hours((640, 1), "640MB"),
            "mem_500M" : StaticMemory_FunctionOfTime_Hours((512, 1), "512MB"),

            "mem_increase" : FunctionOfTime([(4096,90)] + [(m,10) for m in xrange(512,1024,128)] + [(m,10) for m in xrange(1024,6500,512)],
                                            FunctionOfTime.MINUTES, FunctionOfTime.MINUTES, FunctionOfTime.SECONDS, "MB", "Increasing memory from 500M to 6G (4 hrs.)")
        }, "mem_4G"),

        "load_functions" : ListMultiParameterGroup(dict(
            # Static load
            load_highest = StepExperiments.get_load_functions([120], 30),
            load_high = StepExperiments.get_load_functions([110,100], 30),
            load_medium = StepExperiments.get_load_functions([80,60,40,20], 30),
            load_medium_single = StepExperiments.get_load_functions([50], 30),
            load_low = StepExperiments.get_load_functions([5, 10], 30),
            load_lowest = StepExperiments.get_load_functions([4], 30),

            # Dynamic load
            load_increasing = FunctionOfTime(
                            [(1,60*3),(5,60*3),(10,60*3)] + [(i, 30) for i in range(20,121,10)],
                            FunctionOfTime.MINUTES, FunctionOfTime.MINUTES, FunctionOfTime.SECONDS, representation = "Increasing load from 1 to 120 (14.5 hrs.)"),

            load_decreasing = FunctionOfTime(
                            [(120, 60*4)] + [(i, 30) for i in range(110,1,-10)] + [(5,30), (1,30)],
                            FunctionOfTime.MINUTES, FunctionOfTime.MINUTES, FunctionOfTime.SECONDS, representation = "Decreasing load from 120 to 1 (10.5 hrs.)"),

            load_decrease_increase = FunctionOfTime(
                            [(120, 60*4 + 30)] + [(i, 30) for i in range(110,1,-10)] + [(5,30), (1,60), (5,30)] + [(i, 30) for i in range(10,121,10)],
                            FunctionOfTime.MINUTES, FunctionOfTime.MINUTES, FunctionOfTime.SECONDS, representation = "Decreasing-Increasing load 120->1->120 (18 hrs.)"),

            load_decreasing_short = FunctionOfTime(
                            [(100, 90)] + [(i, 10) for i in xrange(90,25,-10)] + [(i, 10) for i in xrange(25,5,-5)] + [(i, 10) for i in range(5,0,-1)],
                            FunctionOfTime.MINUTES, FunctionOfTime.MINUTES, FunctionOfTime.SECONDS, representation = "Decreasing load from 100 to 1 (~4.2 hrs.)"),
        ), "load_highest"),

        "load_intervals" : ListSingleParameterGroup(dict(
            interval_short = 30,     # 30 seconds load
            interval_medium = 600,   # 10 minutes load
            interval_long = 900,     # 15 minutes load

            # When using short steps, must use short interval
            step_short = 30,

            # This loads designed for 10 minutes load interval
            load_decreasing_short = 600,    # 10 minutes load
            mem_increase = 600              # 10 minutes load
        ), "interval_long"),

        "guest_swappiness" : ListMultiParameterGroup(dict(
            swap_all = [1, 10, 20, 30, 40, 50, 60],
            swap_lowest = [1],
            swap_low = [10,20],
            swap_medium = [30, 40],
            swap_high = [50, 60],
            swap_highest = [100],
        ), "swap_lowest"),

        "guest_image_cow" : ListSingleParameterGroup(dict(
            image_cow = True,
            image_copy = False,
        ), "image_cow"),

        "guest_prog_kwargs" : NamedParameters(dict(
            max_connections = ListMultiParameterGroup(dict(
                pg_max_conn_highest = [120],
                pg_max_conn_high = [120,110,100],
                pg_max_conn_medium = [80,60,40,20],
                pg_max_conn_low = [5, 10],
            ), "pg_max_conn_highest"),

            temp_buffersMB = ListSingleParameterGroup(dict(
                pg_buff_large = 10,
                pg_buff_medium = 5,
                pg_buff_small = 1,
            ), "pg_buff_small"),

            work_mem = ListSingleParameterGroup(dict(
                # Aount of memory for sorts (in kB)
                pg_work_mem_default = 1024, # 1MB (postgres defaults)

                pg_work_mem_high = 4096,    # 4MB
                pg_work_mem_medium = 2096,  # 2MB
                pg_work_mem_low = 512,      # 0.5MB
            ), "pg_work_mem_default"),

            autovacuum = ListSingleParameterGroup(dict(
                pg_vacuum_on = True,
                pg_vacuum_off = False,
            ), "pg_vacuum_off"),

            checkpoint_timeout = ListSingleParameterGroup(dict(
                # Maximum time between automatic WAL checkpoints, in seconds
                pg_checkpoint_timeout_default  = 300,  # 5min (postgres default)

                pg_checkpoint_timeout_high     = 900,  # 15min
                pg_checkpoint_timeout_medium   = 600,  # 10min
                pg_checkpoint_timeout_low      = 60,   # 1min
            ), "pg_checkpoint_timeout_default"),

            checkpoint_completion_target = ListSingleParameterGroup(dict(
                # Specifies the target of checkpoint completion, as a fraction of total time between checkpoints.
                pg_checkpoint_comp_trg_default = 0.5, # postgres default
                pg_checkpoint_comp_trg_high    = 0.9, # recommended for large checkpoint segments
                pg_checkpoint_comp_trg_medium  = 0.7,
                pg_checkpoint_comp_trg_low     = 0.3, # not recommended
            ), "pg_checkpoint_comp_trg_high"),

            checkpoint_segments_init_value = ListSingleParameterGroup(dict(
                # Maximum number of log file segments between automatic WAL checkpoints (each segment is normally 16 megabytes).
                # Long intervals causing checkpoints to write too frequently with default value
                pg_checkpoint_segments_default  = 3, # 48MB (postgres default)

                pg_checkpoint_segments_4G    = 256,  # 4GB
                pg_checkpoint_segments_2G    = 128,  # 2GB
                pg_checkpoint_segments_1G    = 64,   # 1GB
                pg_checkpoint_segments_500M  = 32,   # 512MB
                pg_checkpoint_segments_250M  = 16,   # 256MB
            ), "pg_checkpoint_segments_default"),

            checkpoint_mem_ratio = ListSingleParameterGroup(dict(
                # Ratio between total memory to the checkpoint buffer size (checkpoint/memory)
                pg_checkpoint_ratio_small   = 0.2,
                pg_checkpoint_ratio_half    = 0.5,
                pg_checkpoint_ratio_large   = 0.7,
            ), "pg_checkpoint_ratio_half"),

            checkpoint_mem_change_threshold = ListSingleParameterGroup(dict(
                # Ratio between total memory to the checkpoint buffer size (checkpoint/memory)
                pg_checkpoint_mem_change_threshold_small = 0.01,
                pg_checkpoint_mem_change_threshold_medium = 0.05,
                pg_checkpoint_mem_change_threshold_large = 0.1,
            ), "pg_checkpoint_mem_change_threshold_medium"),
        )),

        "host_prog_kwargs" : NamedParameters(dict(
            connection_per_transaction = ListSingleParameterGroup(dict(
                # Create new connection for each transaction
                pgbench_cpt = True,
                pgbench_no_cpt = False,
            ), "pgbench_no_cpt"),

            output_log = ListSingleParameterGroup(dict(
                # Create log files
                pgbench_log = True,
                pgbench_no_log = False,
            ), "pgbench_no_log"),

            threads_count = ListSingleParameterGroup(dict(
                # Create log files
                pgbench_threads_max = 150,  # More than enough
                pgbench_threads_high = 20,
                pgbench_threads_medium = 12,
                pgbench_threads_low = 6
            ), "pgbench_threads_max"),
        ))
    })

if __name__ == '__main__':
    ''' Unit Test '''
    params = get_pgbench_parameters()
    params.inquire_parameters(sys.argv[1:])

    print "Parameters:\n%s\n" % params