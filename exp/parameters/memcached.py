'''
Created on Apr 27, 2015

@author: borispi
'''

import sys

from etc.NamedParameters import NamedParameters, \
    ListSingleParameterGroup, ListMultiParameterGroup
from exp.common.FunctionOfTime import FunctionOfTime
from exp.parameters.memory import MEMORY_PARAMETERS
from exp.parameters.load import LOAD_PARAMETERS
from exp.parameters.generic import get_generic_parameters

def StaticMemory_FunctionOfTime_Hours(allocation, name):
    return FunctionOfTime([allocation], FunctionOfTime.HOURS, FunctionOfTime.HOURS, FunctionOfTime.SECONDS, FunctionOfTime.SECONDS, "MB", representation = name)

def get_memcached_parameters():
    ret = get_generic_parameters()
    ret.add_groups(MEMORY_PARAMETERS)
    ret.add_groups(LOAD_PARAMETERS)

    #guest_prog_kwargs: spare_mem, init_mem_size, update_interval
    #host_prog_kwargs: keys_dist, vals_dist, cmd_get_percent, win_size, seed, remote_ip
    ret.add_groups({
        "guest_prog_kwargs" : NamedParameters(dict(
            spare_mem = ListSingleParameterGroup(dict(
                spare_mem_default = 50,
            ), "spare_mem_default"),

            init_mem_size = ListSingleParameterGroup(dict(
                init_mem_size_default = 10,
                init_mem_size_medium = 1000,
                init_mem_size_large = 2200,
            ), "init_mem_size_default"),

            update_interval = ListSingleParameterGroup(dict(
                update_interval_default = 1,
            ), "update_interval_default"),

            use_dynamic = ListSingleParameterGroup(dict(
                use_dynamic_default = True,
                use_dynamic_false = False,
            ), "use_dynamic_default"),
        )),

        "host_prog_kwargs" : NamedParameters(dict(
            keys_dist = ListSingleParameterGroup(dict(
                # Create new connection for each transaction
                keys_dist_default = [(249, 249, 1.0)],
                keys_dist_small = [(16, 16, 1.0)],
            ), "keys_dist_default"),

            vals_dist = ListSingleParameterGroup(dict(
                # Create log files
                vals_dist_default = [(1024, 1024, 1.0)],
            ), "vals_dist_default"),

            cmd_get_percent = ListSingleParameterGroup(dict(
                # Create log files
                cmd_get_percent_default = 0.1,
                cmd_get_percent_medium  = 0.3,
                cmd_get_percent_high    = 0.5,
            ), "cmd_get_percent_default"),

            win_size = ListSingleParameterGroup(dict(
                # Create log files
                win_size_default = "500k",
                win_size_small = "1k",
            ), "win_size_default"),

            seed = ListSingleParameterGroup(dict(
                # Create log files
                seed_default = 1000,
            ), "seed_default"),

            remote_ip = ListSingleParameterGroup(dict(
                # Create log files
                remote_ip_default = "csl-tapuz26",
                remote_ip_local = "localhost",
            ), "remote_ip_default"),
        ))
    })

    return ret

if __name__ == '__main__':
    ''' Unit Test '''
    params = get_memcached_parameters()
    params.inquire_parameters(sys.argv[1:])

    print "Parameters:\n%s\n" % params
