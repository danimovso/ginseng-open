'''
Created on 14/12/2014

@author: Liran Funaro <fonaro@cs.technion.ac.il>
'''
from exp.common.StepExperiments import StepExperiments
from exp.common.FunctionOfTime import FunctionOfTime
from etc.NamedParameters import ListMultiParameterGroup,\
    ListSingleParameterGroup

LOAD_FUNCTIONS = dict(
            # For testing
            load_short = StepExperiments.get_load_functions([1], 1),

            # Static load
            load_highest = StepExperiments.get_load_functions([120], 30),
            load_high = StepExperiments.get_load_functions([110,100], 30),
            load_high_single = StepExperiments.get_load_functions([100], 10),
            load_medium = StepExperiments.get_load_functions([80,60,40,20], 30),
            load_medium_single = StepExperiments.get_load_functions([50], 10),
            load_low = StepExperiments.get_load_functions([5, 10], 30),
            load_lowest = StepExperiments.get_load_functions([4], 30),
            load_signific = StepExperiments.get_load_functions([50,40,30,20,10,7,5,4,3,2], 30),

            load_medium_pulse = StepExperiments.get_load_functions([50], 1),

            load_memcache_1 = StepExperiments.get_load_functions([20, 40, 60, 80, 100], 30),

            load_memcache_2 = StepExperiments.get_load_functions([9], 30),

            # Dynamic load
            load_increasing = FunctionOfTime(
                            [(1,60*3),(5,60*3),(10,60*3)] + [(i, 30) for i in range(20,121,10)],
                            FunctionOfTime.MINUTES, FunctionOfTime.MINUTES, representation = "Increasing load from 1 to 120 (14.5 hrs.)"),

            load_decreasing = FunctionOfTime(
                            [(120, 60*4)] + [(i, 30) for i in range(110,1,-10)] + [(5,30), (1,30)],
                            FunctionOfTime.MINUTES, FunctionOfTime.MINUTES, representation = "Decreasing load from 120 to 1 (10.5 hrs.)"),

            load_decrease_increase = FunctionOfTime(
                            [(120, 60*4 + 30)] + [(i, 30) for i in range(110,1,-10)] + [(5,30), (1,60), (5,30)] + [(i, 30) for i in range(10,121,10)],
                            FunctionOfTime.MINUTES, FunctionOfTime.MINUTES, representation = "Decreasing-Increasing load 120->1->120 (18 hrs.)"),

            load_decreasing_short = FunctionOfTime(
                            [(100, 90)] + [(i, 10) for i in xrange(90,25,-10)] + [(i, 10) for i in xrange(25,5,-5)] + [(i, 10) for i in range(5,0,-1)],
                            FunctionOfTime.MINUTES, FunctionOfTime.MINUTES, representation = "Decreasing load from 100 to 1 (~4.2 hrs.)"),
        )

LOAD_PARAMETERS = {
       "load_functions" : ListMultiParameterGroup(LOAD_FUNCTIONS, "load_highest"),

        "load_intervals" : ListSingleParameterGroup(dict(
            interval_short = 30,     # 30 seconds load
            interval_1_minute = 60,  # 1 minute load
            interval_2_min = 60*2,   # 2 minutes load
            interval_3_min = 60*3,   # 3 minutes load
            interval_4_min = 60*4,   # 4 minutes load
            interval_5_min = 60*5,   # 5 minutes load
            interval_6_min = 60*6,   # 6 minutes load
            interval_7_min = 60*7,   # 7 minutes load
            interval_8_min = 60*8,   # 8 minutes load
            interval_9_min = 60*9,   # 9 minutes load
            interval_medium = 600,   # 10 minutes load
            interval_long = 900,     # 15 minutes load
            interval_very_long = 3 * 60 * 60, # 11 hours load

            # When using short steps, must use short interval
            step_short = 30,

            # This loads designed for 10 minutes load interval
            load_decreasing_short = 600,    # 10 minutes load
            mem_increase = 600,             # 10 minutes load
            mem_increase_slow = 600,        # 10 minutes load
        ), "interval_long")
    }
