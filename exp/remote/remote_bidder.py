#! /usr/bin/env python

import logging
import os, sys
import signal
from time import sleep

# from exp.deprecated.QuitServer import QuitServer
from mom.LogUtils import LogUtils
import momguestd


port = 1923

def remote_bidder(moc_home, verbosity, program, adviser, bidder_conf):
    LogUtils(verbosity)
    os.chdir(moc_home)

    logger = logging.getLogger("RemoteBidder")

    argv = [
            "-c", bidder_conf,
            "-v", "debug",
            "-p", program,
            "-a", str(adviser),
            ]

    agent = momguestd.momguestd(*argv)

#    # define signals when running on main thread
#    try:
#        def signal_quit(signum = -1, frame = None):
#            logger.info("Received signal %i, shutting down...", signum)
#            agent.terminate()
#        signal.signal(signal.SIGINT, signal_quit)
#        signal.signal(signal.SIGTERM, signal_quit)
#    except ValueError:
#        pass # not running on main thread
#
#    quit_server = QuitServer(port, agent.terminate, timeout = None,
#                             name = "BidderQuitServer")

    try:
        logger.info("Starting bidder for mem auction. adviser = %s ", str(adviser))
#        quit_server.serve_forever()
        agent.start()  # blocking
        logger.info("Bidder ended")
    finally: pass
#        quit_server.shutdown()

if __name__ == "__main__":
    moc_home = sys.argv[1]
    verbosity = sys.argv[2]
    program = sys.argv[3]
    bidder_conf = sys.argv[4]
    adviser_str = eval(" ".join(sys.argv[5:]))
    remote_bidder(moc_home, verbosity, program, adviser_str, bidder_conf)



