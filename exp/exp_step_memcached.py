#! /usr/bin/env python
'''
Created on Mar 9, 2012

@author: eyal
'''
from exp.prog.Programs import Guest_Memcached, Host_Memcached
from exp.common.StepExperiments import StepExperiments

if __name__ == '__main__':
    prog_args = Guest_Memcached(
            spare_mem = 50,
            init_mem_size = 10,
            update_interval = 0.1,
            memcached_port = 11211
            ).command_args

    bm_args = Host_Memcached(
        cmd_get_percent = 0.3,
        keys_dist = ((249, 249, 1),),
        vals_dist = ((1024, 1024, 1),),
        win_size = "2m",
        remote_ip = "ds-had3b",
        memcached_port = 11211
        ).command_args

    StepExperiments(
                    name="memcached-load",
                    vm_name="vm-1",

                    step_timings=[[600, 600, 600]],
                    load_intervals=[10, 50, 100, 200],
                    loads=[10],

                    mem_bases=[500],
                    mem_steps=[1000],

                    prog_args=prog_args,
                    bm_args=bm_args,

                    start_cpu=2,
                    verbosity="info"
                    ).start()
