'''
Created on Dec 3, 2011

@author: eyal
'''

from xml.dom import minidom
from etc.Settings import Settings
import copy
try:
    import libvirt
except:
    pass

global _descs
_descs = {}

NETWORK_NAME = 'ginet'

CLASS_TO_FILE = {
    "default": Settings.machine_descriptor(),
    "psql": Settings.machine_descriptor_psql()
}

def _load_descs(vm_name, vm_class="default"):
    global _descs
    src_file = ""
    try:
        src_file = CLASS_TO_FILE[vm_class]
    except KeyError:
        vm_class = "default"
        src_file = CLASS_TO_FILE['default']
    fd = open(src_file)
    lines = fd.readlines()
    fd.close()
    content = "".join(l.strip() for l in lines)
    doc = minidom.parseString(content)
    for node in doc.firstChild.childNodes:
        desc = {}
        for n in node.childNodes:
            desc[str(n.nodeName)] = (n.firstChild.data)
        _descs[vm_name] = desc

def get_net_attribute(vm_name, attr):
    conn = libvirt.open('qemu:///system')
    net = conn.networkLookupByName(NETWORK_NAME)
    net_xml = net.XMLDesc(0)
    doc = minidom.parseString(net_xml)
    host_elements = doc.getElementsByTagName('host')
    for host in host_elements:
        if str(host.attributes['name'].value) == vm_name:
            return str(host.attributes[attr].value)
    return None

def get_vm_mac(vm_name):
    res = get_net_attribute(vm_name, 'mac')
    if not res:
        assert False, "mac address not found for %s" % vm_name
    return res

def get_vm_ip(vm_name):
    res = get_net_attribute(vm_name, 'ip')
    if not res:
        assert False, "ip address not found for %s" % vm_name
    return res

def get_interface_name(vm_name):
    conn = libvirt.open('qemu:///system')
    vm = conn.lookupByName(vm_name)
    vm_xml = vm.XMLDesc()
    doc = minidom.parseString(vm_xml)
    targets = doc.getElementsByTagName('target')
    for target in targets:
        if ['dev'] == target.attributes.keys():
            return str(target.attributes['dev'].value)
    raise KeyError
        
def get_vm_desc(vm_name, vm_class="default"):
    global _descs
    if not _descs.has_key(vm_name):
        _load_descs(vm_name, vm_class)
    try:
        return copy.deepcopy(_descs[vm_name])
    except KeyError:
        return None

if __name__ == "__main__":
    print get_vm_desc("vm-2")['moc_dir']
    print get_vm_ip("vm-3")
    print get_vm_desc("vm-1", "psql")
    print get_vm_desc("vm-2", "default")
    print get_vm_desc("vm-3", "ffff")
    print get_vm_desc("vm-2", "psql")
