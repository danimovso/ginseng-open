#! /usr/bin/env python
'''
Created on Aug 30, 2011

@author: eyal
'''
from mom.Plotter import Plotter
import os
import time
import logging
from mom.LogUtils import LogUtils
from exp.core.ExpMachine import ExpMachine
from etc.ParallelInvoker import ParallelInvoker
from exp.core.Mocs import get_moc
import shutil

import libvirt

#from mom.Policy.Auctioneer.Auctioneer import total_bills

from etc.FaultyFlag import FaultyFlag
from etc.HostMachine import HostMachine
from etc.decorators import dict_represented

def confirm_moc_path():
    if all((d != "moc" for d in os.getcwd().split("/"))):
        raise OSError("Bad startup location, must be in moc dir")
    while os.path.split(os.getcwd())[1] != "moc":
        os.chdir("..")
    return os.getcwd()

def set_output_path(*dir_name):
    moc_dir = confirm_moc_path()
    output_path = os.path.join(os.path.split(moc_dir)[0], "moc-output", *dir_name)
    if os.path.exists(output_path):
        i = 0
        while os.path.exists(output_path + "-%i" % i):
            i += 1
        output_path += "-%i" % i

    # Create subdirectories also
    os.makedirs(output_path)
    return output_path

@dict_represented
class Experiment(FaultyFlag):
    LOG_DEFAULT_FILE_NAME = "exp-out"

    def __init__(self,
                 moc_args,
                 vms_desc,
                 output_path,
                 duration = None,
                 verbosity = "info",
                 extra_info = {}):
        '''
        Parameters:
        -------------
        moc_args : memory over commitment manager arguments
        vms_desc : descriptor of the virtual machines
        output_path : the experiment output folder path
        duration : the experiment duration
        verbosity : the verbosity of the logger
        extra_info : extra information to write to the info file
        '''

        FaultyFlag.__init__(self)
        self.moc_args = moc_args
        self.output_path = output_path
        self.duration = duration
        self.verbosity = verbosity
        self.extra_info = extra_info

        LogUtils(self.verbosity)
        self.logger = logging.getLogger(self.__class__.__name__)
        self.log_file = os.path.join(self.output_path, self.LOG_DEFAULT_FILE_NAME)
        LogUtils.start_file_logging(self.log_file, self.verbosity)

        self.host_machine = HostMachine()

        Plotter.start_plotting(self.output_path)

        # Start the connection to libvirt
        self.open_libvirt_connection()
        # Get the machines. This will update the available CPU after pinning
        # the CPUs to the VMs
        self.vms = self.get_vms(vms_desc)

        self.moc = get_moc(*moc_args)
        self.invoker = ParallelInvoker(self.vms)

        # copy moc's configs and rules file for documentation
        args = self.moc.get_args()
        for k in ("-r", "-c"):
            if k in args:
                shutil.copy(args[k], self.output_path)

        self.host_machine.begin_experiment(self)

        # save test information file
        try:
            self.write_info(self.output_path)
        except Exception as e:
            self.logger.error("Could not write info file: %s", e)

    @property
    def as_dict(self):
        info = dict(
                duration = self.duration,
                moc_args = self.moc_args,
                vms = dict([ (vm.name, vm.as_dict) for vm in self.vms ]),
                output_path = self.output_path,
                host_machine = self.host_machine.as_dict
                )

        # Add extra_info to info dict (with respect to overlapping keys)
        for inf_key, inf_val in self.extra_info.iteritems():
            if inf_key not in info:
                info[inf_key] = inf_val
            elif info[inf_key] != inf_val:
                info[inf_key + "(extra-info)"] = inf_val

        return info

    def open_libvirt_connection(self):
        self.conn = libvirt.open('qemu:///system')
        self.logger.info('Connection opened at: %s', self.conn.getURI())
        self.logger.info('Host name: %s', self.conn.getHostname())
        self.logger.info('Max qemu vCPUs: %i',
                         self.conn.getMaxVcpus(self.conn.getType()))

    def get_vms(self, vms_desc):
        ret = []

        for name, props in vms_desc.iteritems():
            ret.append(ExpMachine(name, self.conn, self.output_path, verbosity = self.verbosity, **props))

        for vm in ret:
            vm_cpus = self.host_machine.auto_select_cpus( vm.get_cpu_count() )
            bm_cpus = self.host_machine.auto_select_cpus( vm.get_benchmark_cpu_count() )
            vm.set_guest_cpus(vm_cpus)
            vm.set_benchmark_cpus(bm_cpus)

        return ret

    def close(self):
        self.logger.debug("Closing experiment")
        cpus = []

        if self.vms:
            for vm in self.vms:
                if not vm:
                    continue

                vm_cpus = vm.get_guest_cpus()
                bm_cpus = vm.get_benchmark_cpus()

                vm.set_guest_cpus(None)
                vm.set_benchmark_cpus(None)

                if vm_cpus:
                    cpus += vm_cpus

                if bm_cpus:
                    cpus += bm_cpus

            self.host_machine.release_cpus(cpus)

        if self.conn:
            self.conn.close()

        self.host_machine.end_experiment(self)

    def destroy_vms(self):
        try:
            self.logger.info("End experiment for each VMs...")
            self.invoker.start_and_join(lambda vm: vm.end_experiment)
        except Exception as ex:
            self.logger.error("Error ending VMs: %s", ex)
            self.setFaulty()
        finally:
            self.logger.info("Destroying VMs...")
            for vm in self.vms:
                time.sleep(0.1)
                vm.destroy_domain()

    def start(self):
        try:
            start = self.run_everything()
            if start == False:
                raise RuntimeError("Error in startup, resetting experiment")
            else:
                if self.duration is None or self.duration is None:
                    raw_input("Press enter to stop")
                else:
                    self.logger.info("Measure for %.1f min.", float(self.duration) / 60)
                    time.sleep(self.duration)
        except Exception as ex:
            import sys
            import traceback
            self.logger.error("EXCEPTION: %s", ex)
            tb = sys.exc_info()[2]
            tb_list = traceback.format_tb(tb)
            for i in tb_list:
                self.logger.error("EXCEPTION: %s" % i)
            self.invoker.start_and_join(lambda vm: vm.destroy_domain)
            self.setFaulty()
            raise
        finally:
            self.end_everything()
            self.logger.info("Experiment ended")
            if self.isFaulty:
                self.logger.warn("Experiment results may be faulty")
            LogUtils.stop_file_logging(self.log_file)
            Plotter.stop_plotting()
            self.close()

    def run_everything(self):
        self.logger.info("Starting VMs...")

        # libvirt needs domain to start serially:
        for vm in self.vms:
            vm.start_domain()

        for vm in self.vms:
            file(os.path.join(self.output_path, "%s.xml" % vm.name), "w").write(vm.get_xml_desc())

        # Not necessary (bad idea)
        #Machine.set_all_kvms_cpu(''' Should be something ''')

        # all initiations can be done in parallel:
        self.invoker.start_and_join(lambda vm: vm.init_experiment)

        # Assert the all initiation succeeded
        for vm in self.vms:
            if not vm.is_ready_for_experiment():
                return False

        self.logger.info("Starting momd's communication and wait " +
                         "for readiness...")
        self.moc.wait_for_communication(len(self.vms))
        self.logger.info("Starting MOC...")
        self.moc.start()

        start_time = time.time()
        self.logger.info("Starting load simulating...")
        for vm in self.vms:
            vm.run_benchmark(start_time)

        return start_time

    def end_everything(self):
        # self.write_total_bills() # DEPRECATED
        self.end_moc()
        self.destroy_vms()

    def end_moc(self):
        try:
            self.logger.info("Ending MOC")
            if self.moc.is_alive():
                self.moc.terminate()
                self.moc.join(30)
        except Exception as ex:
            self.logger.error("Error ending MOC: %s", ex)
            self.setFaulty()

    def write_total_bills(self):
        ''' DEPRECATED '''
#         try:
#             file(os.path.join(self.output_path, "exp-bills"), "w").write(
#                                     str(total_bills))
#             self.logger.info("\n".join(
#                         ["=== Total bills ==="] +
#                         ["    %s: %.2f" % (name, bill)
#                          for name, bill in total_bills.iteritems()]))
#         except Exception as ex:
#             self.logger.error("Error writing bills: %s", ex)
        pass

