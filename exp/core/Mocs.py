'''
Created on Nov 12, 2011

@author: eyal
'''

import threading
import time
import momd
import logging
from etc.TerminatableThread import Terminatable
from mom.Comm.Messages import MessageNotifyMem
from mom.Plotter import Plotter
from exp.core.VMDesc import get_vm_ip

try:
    import libvirt
except ImportError:
    pass


def get_moc(name, args=(), kwargs={}):
    return eval(name)(*args, **kwargs)


class Moc(threading.Thread):
    def __init__(self, verbosity="info"):
        threading.Thread.__init__(self, name="MocThread")
        self.logger = logging.getLogger("MocThread")
        self.verbosity = verbosity
        args = self.get_args()
        cmdargs = []
        for k, v in args.iteritems():
            cmdargs += [k, v]
        self.moc = momd.momd(*cmdargs)

    def terminate(self):
        self.moc.terminate()

    def guests_state(self):
        if not self.moc:
            return {}
        with self.moc.guest_manager.guests_lock:
            return dict((g.info['name'], g.is_ready)
                    for g in self.moc.guest_manager.guests.values())

    def wait_for_communication(self, number_of_guests):
        """
        Start guest manager, and wait for all guest monitors to be ready
        """
        # start guest manager and wait for guests
        self.moc.config.set('__int__', 'running', '1')
        self.moc.guest_manager.start()
        while (not self._all_guests_ready(number_of_guests)):
            time.sleep(5)

    def _all_guests_ready(self, number_of_guests):
        state = self.guests_state()

        if state == {}:
            self.logger.warn("Checking readiness- no MOC instance!")
            return False

        if len(state) < number_of_guests:
            self.logger.warn("Checking readiness- not enough guests! (%i out of %i)",
                              len(self.moc.guest_manager.guests), number_of_guests)
            return False

        return all(state.values())

    def run(self):
        self.moc.start(start_guest_manager = False)

    def get_args(self):
        args = {'-v': self.verbosity}
        return args


class MocAuctioneer(Moc):
    def __init__(self, host_mem, p0,
                 mem_auction_intervals,  # a list of: bid_collection_interval, calculation_interval, notification_interval,
                 auction_mem=0,
                 conf_file=None,
                 warmup_rounds=-1,
                 *args, **kwrds):

        self.host_mem = int(host_mem)
        # bandwidth in KiloBytes
        self.p0 = float(p0)
        if isinstance(mem_auction_intervals, str):
            self.mem_auction_intervals = eval(mem_auction_intervals)
        else:
            self.mem_auction_intervals = mem_auction_intervals

        self.auction_mem = int(auction_mem)
        self.conf_file = conf_file
        self.warmup_rounds = warmup_rounds

        Moc.__init__(self, *args, **kwrds)

    def get_args(self):
        new_params = {
            '-c': self.conf_file if self.conf_file else 'doc/auction.conf',
            '-r': 'doc/auction.rules',
            '-m': self.host_mem,
            '-M': self.auction_mem,
            '--p0': self.p0,
            '-w': self.warmup_rounds,
            '--mem-auction-intervals': ",".join(map(str, self.mem_auction_intervals)),
        }
        return dict(Moc.get_args(self).items() + new_params.items())

class MocAuctioneerDoubleOvercommit(MocAuctioneer):
    def get_args(self):
        args = MocAuctioneer.get_args(self)
        args['-r'] = 'doc/auction_double.rules'
        return args

class MocMomBalloon(Moc):
    def get_args(self):
        return dict(Moc.get_args(self).items() + {
                     '-c': 'doc/mom-balloon.conf',
                     '-r': 'doc/balloon.rules'
                     }.items())

class MocStatic(Moc, Terminatable):
    UPDATE_INTERVAL = 2 # Seconds
    TIME_TO_PREPARE = 5 # Seconds

    def __init__(self, func_mem=None, *args, **kwrds):
        '''
        func_mem: represents functions that for a specific
            time, returns a key,value dictionary such that the keys are the
            guests names and the values is its matching value.
            It can be an actual function as described above, or a dictionary of
            functions where each item represent a function of a specific guest.

        For func_mem: if a value was not specified for a machine,
        then it will not change.
        '''
        Moc.__init__(self, *args, **kwrds)
        Terminatable.__init__(self)

        self.init_alloc_functions(
                  memory = func_mem,
                  )

        self.init_funcs = dict(
                  memory = self.init_mem,
                  )

        self.notify_msg_funcs = dict(
                  memory = self.notify_message_mem,
                  )

        self.control_funcs = dict(
                  memory = self.control_mem,
                  )

        self.control_threads = []

        if self.is_no_communication():
            self.wait_for_communication = lambda slf: None

        for resource, func in self.alloc_funcs.iteritems():
            if func is not None and resource in self.init_funcs:
                self.init_funcs[resource]()

    @classmethod
    def get_guests_joined_alloc_function(cls, **guests_alloc_functions):
        valid_functions = {}

        for name, func in guests_alloc_functions.iteritems():
            if isinstance(func, str):
                func = eval(func)

            if cls.verify_resource_function(func):
                valid_functions[name] = func

        if valid_functions:
            return lambda t: { name: func(t) for name, func in valid_functions.iteritems() }
        else:
            return None

    @staticmethod
    def verify_resource_function(func, expected_result_type = None):
        if func is None:
            return False

        try:
            res = func(0)
        except Exception as err:
            raise ValueError("Resource function must accept only 1 parameter (time): %s" % err)

        if expected_result_type is not None:
            if not isinstance(res, expected_result_type):
                raise ValueError("Resource function must return %s, result was: %s" % (expected_result_type, res))

        return True

    def init_alloc_functions(self, **alloc_functions):
        self.alloc_funcs = {}

        for resource, func in alloc_functions.iteritems():
            if isinstance(func, str):
                func = eval(func)

            if isinstance(func, dict):
                func = self.get_guests_joined_alloc_function(**func)

            # Might be already a joined function (or None)
            if self.verify_resource_function(func, dict):
                self.alloc_funcs[resource] = func

    def is_no_communication(self):
        used_func = None

        for _, func in self.alloc_funcs.iteritems():
            if func is not None:
                used_func = func
                break

        if used_func is None:
            return True
        elif used_func(0).keys()[0] == "localhost":
            return True
        else:
            return False

    def run(self):
        self.conn = libvirt.open("qemu:///system")

        self.t0 = time.time()
        for resource, alloc_func in self.alloc_funcs.iteritems():
            if alloc_func is not None:
                self.logger.info("Starting %s-control thread" % resource)
                trd = threading.Thread(
                        target = self.resource_control_thread,
                        args = [resource],
                        name = "%s-control" % resource
                        )
                trd.start()
                self.control_threads.append(trd)

        Moc.run(self)

    def terminate(self):
        self.logger.info("Terminating MOC")
        Terminatable.terminate(self)
        Moc.terminate(self)
        for trd in self.control_threads:
            trd.join()
        self.logger.info("MOC terminated successfully")

    def resource_control_thread(self, resource):
        alloc_func = self.alloc_funcs[resource]
        control_func = self.control_funcs[resource]

        # Initiate first allocation (no notifications)
        new_alloc = alloc_func(0)
        control_func(new_alloc, {}, new_alloc)
        last_alloc = new_alloc

        while self.should_run:
            time.sleep(self.UPDATE_INTERVAL)

            new_alloc = alloc_func(time.time() - self.t0)
            changed_alloc = self.get_changed_alloc(new_alloc, last_alloc)
            if not changed_alloc:
                continue

            self.notify_new_alloc(resource, new_alloc)
            time.sleep(self.TIME_TO_PREPARE)

            control_func(new_alloc, last_alloc, changed_alloc)
            last_alloc.update(new_alloc)

    def notify_new_alloc(self, resource, new_alloc):
        notify_msg_func = self.notify_msg_funcs[resource]

        for guest_name, guest in self.moc.guest_manager.guests.items():
            try:
                name = guest.properties['name']
                if name not in new_alloc:
                    continue

                notify_msg = notify_msg_func(new_alloc[name])
                if notify_msg:
                    # TODO: (BUG) Discuss the comm_%s with fonaro so we could fix it
                    guest.properties['comm_mem'].communicate(notify_msg)
            except Exception as err:
                self.logger.warn("Can't send %s notification to guest %s: %s",
                                     resource, guest_name, err)

    @staticmethod
    def get_diff_alloc(new_alloc, last_alloc):
        diff_alloc = {}
        for name, value in new_alloc.iteritems():
            diff_alloc[name] = value - last_alloc.get(name, 0)

        return diff_alloc

    @classmethod
    def get_realloc_order(cls, new_alloc, last_alloc):
        diff_alloc = cls.get_diff_alloc(new_alloc, last_alloc)
        return sorted(diff_alloc, key=lambda k:diff_alloc[k])

    @staticmethod
    def get_changed_alloc(new_alloc, last_alloc):
        changed_alloc = {}
        for name,value in new_alloc.iteritems():
            if name not in last_alloc or last_alloc[name] != value:
                changed_alloc[name] = value

        return changed_alloc


    ###########################################################################
    # Resource specific functions
    ###########################################################################

    def init_mem(self):
        pass

    def control_mem(self, new_alloc, last_alloc, changed_alloc):
        for name in self.get_realloc_order(changed_alloc, last_alloc):
            mem = changed_alloc[name]
            self.logger.debug("%s: About to set memory %i KB", name, mem)

            dom = self.conn.lookupByName(name)
            if dom.isActive():
                dom.setMemory(mem << 10)

    def notify_message_mem(self, mem):
        return MessageNotifyMem(not_round_mem = 0, not_bill_mem = 0,
                not_won_p_mem = 0, not_tie_mem = 0, p_in_min_mem = 0,
                p_out_max_mem = 0, not_mem = mem, validity_time_mem = 0)

    def get_args(self):
        return dict(Moc.get_args(self).items() + {
                    '-c': 'doc/static.conf',
                    '-r': 'doc/static.rules'
                    }.items())

if __name__ == "__main__":
    import os
    os.chdir("..")
    Plotter.start_plotting(".")
    moc = MocStatic(func_mem = {"localhost": None})
    moc.start()

