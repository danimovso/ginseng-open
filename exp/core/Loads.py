'''
Created on Oct 28, 2011

@author: eyal
'''

import math
import numpy as np
import random

def get_load_func(cmd):
    cmd = cmd.split()
    name = cmd[0]
    args = [eval(arg) for arg in cmd[1:]] if len(cmd) > 1 else []
    func_class = eval(name)
    return func_class(*args)


# a load function generator
class LoadFunction(object):
    def __call__(self, t):
        raise NotImplementedError

    @property
    def info(self):
        raise NotImplementedError

    # print
    def __repr__(self):
        return repr(self.info)


class LoadTrace(LoadFunction):
    def __init__(self, time, vals):
        self.t = np.array(time);
        self.vals = np.array(vals);

    def __call__(self, t):
        i = np.argmin(np.abs(self.t - t))
#        i = min(np.argmax(self.t >= t) + 1, len(self.vals) - 1)
        return self.vals[i]
        #return np.interp(t, self.t, self.vals, left = self.vals[0], right = self.vals[-1])

    def __repr__(self):
        return "Loads.LoadTrace(time=%s,vals=%s)" % (str(list(self.t)), str(list(self.vals)))

    @property
    def info(self):
        return {"times": self.t, "vals": self.vals}

class LoadConstant(LoadFunction):
    def __init__(self, val):
        self.value = float(val)

    def __call__(self, t):
        return self.value

    @property
    def info(self):
        return {"type": self.__class__.__name__,
                "value" : self.value}

class LoadSine(LoadFunction):
    def __init__(self, a, T, p, offset):
        self.a = float(a)
        self.T = float(T)
        self.p = float(p)
        self.offset = float(offset)

    def __call__(self, t):
        return max(0, self.offset +
                   self.a * math.sin(math.pi * 2 / self.T * (float(t) - self.p)))

    @property
    def info(self):
        return {"type": self.__class__.__name__,
                "a" : self.a, "T": self.T, "p": self.p, "offset": self.offset}

class LoadBinary(LoadFunction):
    '''
    v1 - first value
    v2 - second value
    T - half period time
    T0 - phase (starting position)
    '''
    def __init__(self, v1, v2, T, T0):
        self.v1 = int(v1)
        self.v2 = int(v2)
        self.T = int(T)
        self.T0 = int(T0)

    def __call__(self, t):
        return self.v1 if ((int(t) - self.T0) / self.T) % 2 == 0 else self.v2

    @property
    def info(self):
        return {"type": self.__class__.__name__,
                "v1" : self.v1, "v2" : self.v2, "T": self.T, "T0": self.T0}

class LoadMultiStep(LoadFunction):
    def __init__(self, steps, step_duration, offset=0):
        self.steps = list(map(lambda t: int(t), steps))
        self.step_duration = list(map(lambda t: int(t), step_duration))
        self.offset = int(offset)
        if len(self.steps) != len(self.step_duration):
            raise InvalidInput("Steps and step durations lists are not of the same size")
        if self.offset < 0:
            raise InvalidInput("Offset must be non-negative")

    def __call__(self, t):
        phase = self._phase(t)
        index = 0
        current_sum = 0
        for i, step in enumerate(self.step_duration):
            current_sum += self.step_duration[i]
            if current_sum > phase:
                index = i
                break
        assert index < len(self.steps), "Index out of bounds"
        print phase, "->", index
        return self.steps[index]

    @property
    def info(self):
        return {"type": self.__class__.__name__,
                "steps": self.steps, "step_duration": self.step_duration,
                "offset": self.offset}
        

    def _phase(self, t):
        return (t + self.offset) % sum(self.step_duration)

class LoadAverage(LoadFunction):
    '''
    every period increase the amplitude
    avg - the expected average
    avg - the maximum cur could go to
    T - half period time
    T0 - phase (starting position)
    '''
    def __init__(self, avg, max, T, T0):
        self.avg = int(avg)
        self.max = int(max)
        self.T = int(T)
        self.T0 = int(T0)
        self.cur = 1
        self.flag = False

    def __call__(self, t):
        if ((int(t) - self.T0) / self.T) % 2 == 0:
            self.flag = True
            return self.avg + self.cur

        if self.flag:
            self.flag = False
            if self.cur < self.max:
                self.cur += 1

        return self.avg - self.cur

    @property
    def info(self):
        return {"type": self.__class__.__name__,
                "avg" : self.avg, "cur" : self.cur, "T": self.T, "T0": self.T0}

class InvalidInput(Exception): pass

class LoadBinaryRandom(LoadFunction):
    def __init__(self, v1, v2, n):
        random.seed()
        self.vals = [int(v1), int(v2)]
        self.n = int(n)
        self.current = 0
        if n <= 0:
            raise InvalidInput

    def __call__(self, t):
        if random.randint(1, self.n) == self.n:
            self.current = 1 - self.current
        return self.vals[self.current]

    @property
    def info(self):
        return {"type": self.__class__.__name__,
                "v1" : self.vals[0], "v2" : self.vals[1], "N": self.n}
    

class LoadConst(LoadFunction):
    '''
    value - constant load
    '''
    def __init__(self, value):
        self.value = int(value)

    def __call__(self, t):
        return self.value

    @property
    def info(self):
        return {"type": self.__class__.__name__, "value" : self.value}

class LoadPiecewiseLinear(LoadFunction):
    def __init__(self, a1, a2, threshold):
        self.a1 = a1
        self.a2 = a2
        self.threshold = threshold

    def __call__(self, t):
        if t < self.threshold:
            return self.a1 * t
        else:
            return self.a2 * t

    @property
    def info(self):
        return {"type": self.__class__.__name__, "a1": self.a1,
                "a2": self.a2, "threshold": self.threshold}

class SeveralStepsValuationSwitcher(LoadFunction):
    def __init__(self, times):
        test_values = lambda l: all(map(lambda t: t > 0, l))
        if len(times) == 0:
           raise ValueError("Empty input")
        elif not test_values(times):
            raise ValueError("All times must be positive")
        else:
            self.times = list(times)

    def __call__(self, t):
        t %= sum(self.times)
        current = 0
        for i in range(len(self.times)):
            current += self.times[i]
            if current >= t:
                return i
        return len(self.times) - 1

    @property
    def info(self):
        return {"type": self.__class__.__name__,
            "times": self.times}

def gen_const(n, T_avg = 0, amp_std = 0, amp_avg = 0, load_avg = 8):
    return [LoadConst(load_avg) for i in range(n)]

def gen_random_sine(n, T_avg = 300,
                    amp_std = 1, amp_avg = 3.5, load_avg = 8):

    T_std = T_avg / 10

    amplitude = amp_avg + amp_std * np.random.randn(n)
    T = T_avg + T_std * np.random.randn(n)
    phase = np.random.uniform(0, T_avg, size = n)

    funcs = []
    for i in range(n):
        funcs.append(LoadSine(amplitude[i], T[i], phase[i], load_avg))

    return funcs

def gen_random_sine_const_sum(n, load_avg, amp_avg, amp_std, T_avg):
    T_std = T_avg / 10
    phase_std = T_std
    m = n / 2
    As = amp_avg + amp_std * np.random.randn(m)
    Ts = T_avg + T_std * np.random.randn(m)
    phase = phase_std * np.random.randn(m)
    funcs = []
    for i in range(m):
        funcs.append(LoadSine(As[i], Ts[i], phase[i], load_avg))
        funcs.append(LoadSine(As[i], Ts[i], phase[i] + 0.5 * Ts[i], load_avg))

    if n % 2 != 0:  # add odd function
        funcs.append(LoadConstant(load_avg))

    return funcs

def gen_random_binary_const_sum(n, load_avg, amp_avg, amp_std, T_avg):
    T_std = T_avg / 10
    m = n / 2
    As = amp_avg + amp_std * np.random.randn(m)
    Ts = T_avg + T_std * np.random.randn(m)
    phases = T_std * np.random.randn(m)
    funcs = []
    for i in range(m):
        funcs.append(LoadBinary(load_avg + As[i], load_avg - As[i], Ts[i], phases[i]))
        funcs.append(LoadBinary(load_avg - As[i], load_avg + As[i], Ts[i], phases[i]))

    if n % 2 != 0:  # add odd function
        funcs.append(LoadConstant(load_avg))

    return funcs

def gen_const_const_sum(n, load_avg, amp_avg, amp_std, T_avg):
    m = n / 2
    As = amp_std * np.random.randn(m)
    funcs = []
    for i in range(m):
        funcs.append(LoadConst(load_avg + As[i]))
        funcs.append(LoadConst(load_avg - As[i]))

    if n % 2 != 0:  # add odd function
        funcs.append(LoadConstant(load_avg))

    return funcs

def gen_const_different(n, load_avg, amp_avg, amp_std, T_avg):
    m = n / 2
    As = [(x + 1) * amp_avg / m for x in range(m)]
    funcs = []
    for i in range(m):
        funcs.append(LoadConst(load_avg + As[i]))
        funcs.append(LoadConst(load_avg - As[i]))

    if n % 2 != 0:  # add odd function
        funcs.append(LoadConstant(load_avg))

    return funcs

def gen_binary_const_sum(n, load_avg, amp_avg, amp_std, T_avg):
    T_std = T_avg / 10
    funcs = []
    m = n / 2
    if m > 0:
        d = float(amp_avg) / m
        As = [int(x) for x in np.linspace(1, amp_avg, m)]
        Ts = T_avg + T_std * np.random.randn(m)
        phases = T_std * np.random.randn(m)
        for i in range(m):
            funcs.append(LoadBinary(load_avg + As[i], load_avg - As[i],
                                    Ts[i], phases[i]))
            funcs.append(LoadBinary(load_avg - As[i], load_avg + As[i],
                                    Ts[i], phases[i]))

    if n % 2 != 0:  # add odd function
        funcs.append(LoadConstant(load_avg))

    return funcs

def gen_binary_low_low_mem_const_sum(n, load_avg, amp_avg, amp_std, T_avg):
    T_std = T_avg / 10
    funcs = []
    m = n / 4
    if m > 0:
        As = [int(x) for x in np.linspace(1, amp_avg, m)]
        Ts = T_avg + T_std * np.random.randn(m)
        phases = T_std * np.random.randn(m)
        for i in range(m):
            funcs.append(LoadBinary(load_avg + As[i], 1, Ts[i], phases[i]))
            funcs.append(LoadBinary(1, load_avg + As[i], Ts[i], phases[i]))

            funcs.append(LoadBinary(load_avg - As[i], 1, Ts[i], phases[i]))
            funcs.append(LoadBinary(1, load_avg - As[i], Ts[i], phases[i]))

    if n % 2 != 0:  # add odd function
        funcs.append(LoadConstant(load_avg))

    return funcs

loadupto7 = {"load_avg": 4, "amp_avg": 3, "amp_std": 0}
loadupto10 = {"load_avg": 6, "amp_avg": 4, "amp_std": 0}
loadupto11 = {"load_avg": 6, "amp_avg": 5, "amp_std": 0}
loadupto20 = {"load_avg": 11, "amp_avg": 9, "amp_std": 0}
loadupto30 = {"load_avg": 15, "amp_avg": 8, "amp_std": 0}
loadupto40 = {"load_avg": 20, "amp_avg": 9, "amp_std": 0}
loadupto60 = {"load_avg": 33, "amp_avg": 20, "amp_std": 2}
loadupto80 = {"load_avg": 40, "amp_avg": 24, "amp_std": 0}
loadupto150 = {"load_avg": 75, "amp_avg": 42, "amp_std": 0}
loadupto200 = {"load_avg": 100, "amp_avg": 69, "amp_std": 0}

#loadupto15 = {"load_avg": 8, "amp_avg": 3.5, "amp_std": 1}
#loadupto20 = {"load_avg": 11, "amp_avg": 4.5, "amp_std": 1.5}
#loadupto30 = {"load_avg": 15, "amp_avg": 8, "amp_std": 2}
#loadupto40 = {"load_avg": 20, "amp_avg": 9, "amp_std": 2}
#loadupto60 = {"load_avg": 32, "amp_avg": 17, "amp_std": 4}
#loadupto80 = {"load_avg": 40, "amp_avg": 24, "amp_std": 5}
#loadupto150 = {"load_avg": 75, "amp_avg": 42, "amp_std": 10}
#loadupto200 = {"load_avg": 100, "amp_avg": 69, "amp_std": 10}

if __name__ == "__main__":
    import pylab as pl

    n = 1
#    funcs = gen_const_different(n, T_avg = 200, **loadupto11)
#    funcs = gen_binary_const_sum(n, 30, 20, 0, T_avg = 200)
    funcs = [LoadBinary(v1 = 1, v2 = 0, T = 10, T0 = 3)]
    t = np.array(range(15))
    tot = np.zeros_like(t)
    for i in range(n):
        y = np.array(map(funcs[i], t))
        tot += y
        pl.plot(t, y, "x")

#    pl.plot(t, tot, linewidth = 3, color = "k")

#    pl.show()

    pl.clf()
    func = LoadMultiStep([1, 2, 3], [5, 10, 20])
    t = 0
    pl.plot(range(100), map(lambda t: func(t), range(100)), 'x')
    pl.show()
