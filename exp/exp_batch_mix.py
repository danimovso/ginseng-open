'''
Created on Apr 1, 2012

@author: eyal
'''
from exp.common.exp_batch import batch
from exp.prog.Programs import Guest_Memcached, Host_Memcached, \
    Guest_MemoryConsumer, Host_MemoryConsumer
from exp.core import Loads
from exp.common.valuations import get_valuation, distributions, shapes_mcd, \
    shapes_mixed
from mom.LogUtils import LogUtils
from exp.core.Experiment import set_out_dir
import os
from exp.common.exp_compare import exp_compare

spare = 50
base0 = 600
init_mem = 600
window_size = "500k"
vms_num = range(4, 14 + 1, 2)
mid_load = 6
max_load = 10
T = 1000  #sec
exp_time = 60  #min
auction_ts = [3, 1, 8]
alpha = 1.0
p0 = 0.0
host_mem = 500

is_mcd = lambda i: i % 2 == 0

mcd_prog_args = Guest_Memcached(
            spare_mem = spare,
            init_mem_size = 10,
            update_interval = 1,
            ).command_args

mcd_bm_args = Host_Memcached(
            cmd_get_percent = 0.3,
            keys_dist = [(249, 249, 1.0)],
            vals_dist = [(1024, 1024, 1.0)],
            win_size = window_size,
            ).command_args
mcd_load_interval = 200
mcd_profiler_file = 'doc/profiler-memcached-inside-spare-50-win-500k.xml'
mcd_profiler_entry = 'hits_rate'

mc_prog_args = Guest_MemoryConsumer(
                 spare_mem = spare,
                 saturation_mem = 2000,
                 update_interval = 0.5,
                 sleep_after_write = 0.1,
                 ).command_args
mc_bm_args = Host_MemoryConsumer().command_args
mc_load_interval = 10
mc_profiler_file = 'doc/profiler-mc-spare-50-satur-2000.xml'
mc_profiler_entry = 'hits_rate'

def mix_exp(n, shape, dist, name, out_dir):
    # make the vms description dictionary
    loads = {}
    for i in range(n / 2):
        a = int(float(max_load - mid_load) * i / (n / 2) + 1)
        T0 = T * i / n / 2
        loads[i * 2] = Loads.LoadBinary(v1 = mid_load - a, v2 = mid_load + a, T = T, T0 = T0)
        loads[i * 2 + 1] = Loads.LoadBinary(v1 = mid_load + a, v2 = mid_load - a, T = T, T0 = T0)

    loads = [loads[i] for i in range(n)]  # loads need to be a list

    mcd_valuations = get_valuation(n, shape, dist, factor = 1)
    mc_valuations = get_valuation(n, shape, dist, factor = 1000)

    advisers = {}
    for i in range(n):
        if is_mcd(i):
            advisers[i] = {'name': 'AdviserProfit',
                           'profiler': mcd_profiler_file, 'advice_entry': mcd_profiler_entry,
                           'rev_func': mcd_valuations[i],
                           'base_mem': base0, 'memory_delta': 10}
        else:
            advisers[i] = {'name': 'AdviserProfit',
                           'profiler': mc_profiler_file, 'advice_entry': mc_profiler_entry,
                           'rev_func': mc_valuations[i],
                           'base_mem': base0, 'memory_delta': 10}

    compare_out = os.path.join(out_dir, "num-%i" % n)
    if not os.path.exists(compare_out):
        os.mkdir(compare_out)

    prog_args = [(mcd_prog_args if is_mcd(i) else mc_prog_args)
                 for i in range(n)]

    bm_args = [(mcd_bm_args if is_mcd(i) else mc_bm_args)
               for i in range(n)]

    load_interval = [(mcd_load_interval if is_mcd(i) else mc_load_interval)
                     for i in range(n)]

    exp_compare(name = name,
                n = n,
                prog_args = prog_args,
                bm_args = bm_args,
                advisers_args = advisers,
                alpha = alpha,
                p0 = p0,
                bid_collection_interval = auction_ts[0],
                calculation_interval = auction_ts[1],
                notification_interval = auction_ts[2],
                rounds = 60 * 60 / 12,
                funcs = loads,
                load_interval = load_interval,
                init_mem = init_mem,
                start_cpu = 1,
                host_mem = host_mem,
                out_dir = compare_out,
                saturation_mem = 2500,
                verbosity = "info")


if __name__ == '__main__':
    LogUtils("info")
    for shape in [shapes_mixed[0]]:
        for dist in [distributions[0]]:
            name = "batch-mix-%s-%s" % (shape, dist)
            out_dir = set_out_dir("exp", name)
            for n in vms_num:
                mix_exp(n, shape, dist, name, out_dir)
