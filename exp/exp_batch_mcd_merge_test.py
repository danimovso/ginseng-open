'''
Created on Apr 1, 2012

@author: eyal
'''
from exp.common.exp_batch import batch
from exp.prog.Programs import Guest_Memcached, Host_Memcached, Guest_MemoryConsumer, Host_MemoryConsumer
from exp.core import Loads
from exp.common.valuations import get_valuation, distributions, shapes_mcd

from mom.Profiler3d import fromxml

'''
base_mem - base guest memory (MB)
spare - memcached
window_size - memcached
vms_num - number of VMs (as list, can be initialized with range() )
mid_load - average load
max_load - maximal load
T - half period time (maximal load duration) in seconds
exp_time - experiment duration (minutes)
auction_mem_ts - auction times: [bid collection duration, calculation time, notification time]
'''
spare = 50
base_mem = 600
base_bw = 1000
window_size = "500k"
vms_num = [9]#range(5, 11 + 1, 1)
mid_load = 6
max_load = 10
T = 1000  #sec
exp_time = 60  #min
auction_mem_ts = [3, 1, 8]
auction_bw_ts = [1, 1, 1]
profiler_file = 'doc/profiler-memcached-inside-spare-50-win-500k-tapuz25-3d.xml'
saturation_mem = 2500
saturation_bw  = 50000

# program arguments (what's running inside the guest)
prog_args = Guest_Memcached(
            spare_mem = spare,
            init_mem_size = 10,
            update_interval = 1,
            ).command_args

# benchmark arguments (that makes the load)
bm_args = Host_Memcached(
            cmd_get_percent = 0.3,
            keys_dist = [(249, 249, 1.0)],
            vals_dist = [(1024, 1024, 1.0)],
            win_size = window_size,
            remote_ip = 'localhost',
#            seed = 1
            ).command_args

# compare floats more carefully (epsilontics)
epsilon = 1e-5
floats_equal = lambda x, y: abs(x - y) < epsilon

# generates a dictionary of loads:
# for each number of machines, there's an array with the loads
# also, generate a scaling factor for the valuations
profiler = fromxml(profiler_file, 'hits_rate')
low_perf = profiler.interpolate([1, saturation_mem, saturation_bw])
high_perf = profiler.interpolate([10, saturation_mem, saturation_bw])
loads = {}
val_switch_func = {}
val_scaling_factor = {}
for n in vms_num:
    loads[n] = []
    val_switch_func[n] = []
    val_scaling_factor[n] = []
    m = n / 2
    for i in range(m):
        a = int(float(max_load - mid_load - 1) / (m - 1) * (i)) + 1
        T0 = T / m * i
        low_load = mid_load - a
        high_load = mid_load + a
#        low_perf = profiler.interpolate([low_load, saturation_mem])
#        high_perf = profiler.interpolate([high_load, saturation_mem])

        # add the loads
        loads[n].append(Loads.LoadConstant(val=low_load))
        loads[n].append(Loads.LoadConstant(val=high_load))

        # switch functions
        val_switch_func[n].append(Loads.LoadBinaryRandom(v1 = low_load, v2 = high_load, n=200))
#                                                   T = T, T0 = T0))
        val_switch_func[n].append(Loads.LoadBinaryRandom(v1 = high_load, v2 = low_load, n=200))
#                                                   T = T, T0 = T0))

        # scaling factor
        val_scaling_factor[n].append([1, high_perf / low_perf if not floats_equal(low_perf, 0) else 1])
        val_scaling_factor[n].append([1, low_perf / high_perf if not floats_equal(high_perf, 0) else 1])
    if n % 2 != 0:
        loads[n].append(Loads.LoadConstant(val = mid_load))
        val_switch_func[n].append(Loads.LoadConstant(val=mid_load))
        val_scaling_factor[n].append([1, 1])

def the_exp():
    for shape in shapes_mcd:
        for dist in distributions:
            if (shape in ("linear", "piecewise1400")):
#            if (shape in ("linear", "secondorder")):
                print "skipping: %s, %s" % (shape, dist)
                continue

            valuations = {}
            for n in vms_num:
                base_valuations = get_valuation(n, shape, dist, individual_factors=map(lambda f: f[0], val_scaling_factor[n]))
                scaled_valuations = get_valuation(n, shape, dist, individual_factors=map(lambda f: f[1], val_scaling_factor[n]))
                valuations[n] = [[b, v] for b, v in zip(base_valuations, scaled_valuations)]

            batch(
                  vms_num = vms_num,
                  alpha = 1.0,
#                  p0 = [0.0, 0.2, 0.5, 0.8, 1.0],#[i / 10.0 for i in range(0, 10 + 1)],
#                  p0 = [0, 0.1, 0.2, 0.3, 0.4],
                  p0 = [0.0],
                  base_mem = base_mem,
                  prifiler_file = profiler_file,
                  profiler_entry = 'hits_rate',
                  revenue_func_dict = valuations,
                  bid_collection_interval_mem = auction_mem_ts[0],
                  calculation_interval_mem = auction_mem_ts[1],
                  notification_interval_mem = auction_mem_ts[2],
                  bid_collection_interval_bw = auction_bw_ts[0],
                  calculation_interval_bw = auction_bw_ts[1],
                  notification_interval_bw = auction_bw_ts[2],
                  rounds_mem = exp_time * 60 / sum(auction_mem_ts),
                  rounds_bw = exp_time * 60 / sum(auction_bw_ts),
                  load_funcs_dict = loads,
                  load_interval = max(1, exp_time * 60 - 3*60),
                  prog_args = prog_args,
                  bm_args = bm_args,
                  host_mem = 1000,
                  host_bw = 1000,
                  saturation_mem = saturation_mem,
                  name = "merge-test-" + shape + "-" + dist,
                  verbosity = "debug",
                  exclude = ["divided", "hinted-host-swapping"],
                  val_switch_func = val_switch_func,
                  base_bw = base_bw
                  )

if __name__ == '__main__':
    the_exp()
