import logging
from subprocess import Popen, PIPE
import signal
from exp.prog.BenchmarkPrograms import BenchmarkProgram, \
    ApacheBench, MemoryConsumerBenchmark, BenchmarkError, RemoteMemslap,\
    PostgresSQLBenchmark, PhoronixTestSuiteBenchmark
import os
import urllib
import time
import re
from etc.PySock import Client
import socket
from exp.prog.DynamicMemCtrl import DynamicMemCtrl
from ast import literal_eval
from etc.HostMachine import HostMachine

load_server_port = 9892

'''
NOTE:
program naming scheme: Host_* Guest_*
(must be named this way, otherwise things will fall apart)
'''

def get_guest_program(args):
    '''
    generates a guest program class instance
    '''
    return eval("Guest_" + args[0])(*args[1:])

def get_guest_program_master_image(args):
    '''
    generates a guest program class instance
    '''
    return eval("Guest_" + args[0]).get_image_name(*args[1:])

def get_host_program(args, ip, alias = None):
    '''
    generates a host program class instance
    (must also get the guest ip - single guest)
    '''
    print "getting host program: " + str(args[0])
    print "params: " + str(args)
    return eval("Host_" + args[0])(*args[1:], ip = ip, alias = alias)

class Program(object):
    """
    Abstract class
    """
    def __init__(self, *args):
        self.args = args
        self.logger = logging.getLogger(self.name + "Program")

    @property
    def name(self):
        """
        return the name of program, without prefix "Guest_"/"Host_"
        """
        return self.__class__.__name__.split("_")[-1]

    @property
    def command_args(self):
        """
        Returns a list of arguments for get_program() function
        Same for host / guest side program (without prefix)
        """
        return [self.name] + map(str, self.args)

    @staticmethod
    def parameter_int(param, default = None):
        try:    return int(param)
        except: return default

    @staticmethod
    def parameter_float(param, default = None):
        try:    return float(param)
        except: return default

    @staticmethod
    def parameter_bool(param, default = None):
        if isinstance(param, bool):
            return param
        elif isinstance(param, str):
            try:    return literal_eval(param)
            except: return default
        else: return default

    @staticmethod
    def parameter_dict(param, default = None):
        if isinstance(param, dict):
            return param
        elif isinstance(param, str):
            try:    return literal_eval(param)
            except: return default
        else: return default

class HostProgram(object):
    """
    Host side methods
    Used by the host to control the remote program on the guest
    """
    def __init__(self, benchmarkclass, args, ip = None, alias = None):
        """
        benchmark class is the running benchmark, and args are the parameters
        that will be sent to the benchmark class on initiation.
        This method should be called by the subclass
        """
        assert issubclass(benchmarkclass, BenchmarkProgram), \
            "benchmark_class must be a subclass of Benchmark"
        self.ip = ip
        self.alias = alias
        self.bm = benchmarkclass(*args, ip = ip, alias = alias)
        self.host_machine = HostMachine()

        self.bm_cpus = None

    def get_benchmark_cpu_count(self):
        return self.bm.getCpuCount()

    def set_benchmark_cpus(self, bm_cpus):
        self.logger.info("Benchmark using CPUs: %s", bm_cpus)
        self.bm_cpus = bm_cpus

    def get_benchmark_cpus(self):
        return self.bm_cpus

    def benchmark(self, load, duration):
        """
        Run the benchmark with load=load for a duration
        """
        if self.bm_cpus:
            auto_allocated_cpus = False
            cpus = self.bm_cpus
        else:
            auto_allocated_cpus = True
            cpus = self.host_machine.auto_select_cpus(self.get_benchmark_cpu_count())
            self.logger.info("Benchmark using CPUs: %s", cpus)

        try:
            ret = self.bm.consume(load, duration, cpus)
        except BenchmarkError as er:
            self.logger.warn("Error in benchmark (load:%i): %s", load, er)
            ret = {}
        finally:
            if auto_allocated_cpus:
                self.host_machine.release_cpus(cpus)

        assert isinstance(ret, dict) or isinstance(ret, list)

        return ret

    def test_benchmark(self):
        """
        Test if the program is alive.
        just a very short and light benchmark
        @return: boolean: True if test passed, else False
        """
        self.logger.debug("testing if benchmark is alive")
        try:
            self.bm.consume(1, 1, None)
        except Exception as ex:
            self.logger.error("Benchmark error: %s", ex)
            return False
        return True

    def terminate(self):
        self.bm.terminate()

    def getFields(self):
        return self.bm.getFields()

class GuestProgram(object):
    """
    Guest side functions
    Used by the guest remote_prog.py script to control the program
    """
    def __init__(self):
        self.prog = None

    def start(self):
        if self.prog is not None:
            return
        return self.do_start()

    def notify_memory(self, mem):
        '''
        notify when the memory is about to change
        '''
        raise NotImplementedError

    def terminate(self):
        if self.prog is not None:
            self.do_terminate()
            self.prog = None

    def do_start(self):
        """
        Start the program
        """
        raise NotImplementedError

    def do_terminate(self):
        """
        Terminate the program
        """
        raise NotImplementedError

    @staticmethod
    def get_image_name(*args):
        return "moc-master.qcow2"

#===============================================================================
# Memcached
#===============================================================================

class MemcachedMemCtrl(DynamicMemCtrl):
    def __init__(self, memcached, init_mem, spare_mem, *args, **kwrds):
        DynamicMemCtrl.__init__(self, *args, **kwrds)
        self.memcached = memcached
        self.mem_usage = init_mem
        self.mem_spare = spare_mem
        self.last_target = None
        self.last_total = None
        self.statm_file = None
        self.memcached_client = Client("localhost", Memcached.memcached_port,
                             timeout = None, name = "MemCtrl-memcached-client")

    def try_open_statm_file(self):
#        try:
        pid = Popen("pidof memcached", shell = True,
#                    stdout = PIPE, stderr = PIPE).communicate(timeout = 10)[0]
                    stdout = PIPE, stderr = PIPE).communicate()[0]
        self.statm_file = open("/proc/%i/statm" % int(pid), "r")
#        except Exception as err:
#            raise

    def __del__(self):
        if self.statm_file:
            self.statm_file.close()

    def terminate(self):
        self.memcached_client.close()
        DynamicMemCtrl.terminate(self)

    @property
    def current_mem(self):
        try:
            output = self.memcached_client.send_recv("stats")
            match = re.search(r"STAT limit_maxbytes (\d*)\r\n", output)
            limit_maxbytes = int(match.groups(1)[0]) >> 20
        except Exception as err:
            self.logger.warn("Error in getting memcached stats: %s", err)
            limit_maxbytes = 0

        try:
            output = self.memcached_client.send_recv("stats slabs")
            match = re.search(r"STAT total_malloced (\d*)\r\n", output)
            total_malloced = int(match.groups(1)[0]) >> 20
            match = re.findall(r"STAT (\d*):total_pages (\d*)\r\n", output)
            total_pages = {}
            for k, v in dict(match).iteritems():
                total_pages[int(k)] = int(v)
        except socket.error as err:
            self.logger.warn("Error in getting memcached stats slabs: %s", err)
            total_malloced = 0
            total_pages = {}

        try:
            # read the statm resident set size (measure in pages), and convert
            # to MB.
            if not self.statm_file:
                self.try_open_statm_file()
            self.statm_file.seek(0)
            info = self.statm_file.read()
            mem_statm = int(info.split(" ")[1]) >> 8
        except Exception as err:
            self.logger.warn("Error reading memcached statm: %s", err)
            mem_statm = 0

        return limit_maxbytes, mem_statm, total_malloced, total_pages

    def change_mem_func(self, mem_total, mem_usage, mem_cache_and_buff):
        unused = mem_total - mem_usage
        diff = unused - self.mem_spare
        mc_max, mc_actual, total_malloced, total_pages = self.current_mem
        total_pages = ", ".join(["slab-%i: %i" % tup
                                 for tup in total_pages.iteritems()])

#        fix = mc_actual - total_malloced
#        fix = 0

#        target = mc_actual + diff - fix + (mem_cache_and_buff - 80)
#        target = mem_cache_and_buf + diff + total_malloced
        target = 0
        if self.last_total is not None and (self.last_total > mem_total or mem_usage > mem_total):
            # if we lose memory, we can't rely on the amount of unused memory anymore
            # if the total malloced memory is higher than the total memory, we haven't yet managed to free the memory
            # just take the last target memory
            assert self.last_target is not None, "kept last total but not last target?!"
            assert self.last_total >= mem_total, "more malloced than total although we added memory?!"
            target = self.last_target - (self.last_total - mem_total)
        else:
            target = total_malloced + mem_cache_and_buff + unused - self.mem_spare - 80

        target = max(1, target)

#        data = {'mcd_total': mem_total, 'mcd_target': target, 'mcd_usage': mem_usage,
#                'mcd_statm': mc_actual, 'mcd_max': mc_max, 'mcd_unused': unused,
#                'mcd_cache': mem_cache_and_buff, 'mcd_diff': diff, 'mcd_total_malloced': total_malloced,
#                'mcd_mem_cache_and_buff': mem_cache_and_buff}
#        plotter.plot(data)
        self.logger.info("total: %i, target: %i, usage: %i, statm: %i, " \
                         "max: %i, unused: %i, cache: %i, diff: %i, " \
                         "total_malloced: %i, %s, " \
                         "mem_cache_and_buff: %i",
                         mem_total, target, mem_usage, mc_actual, mc_max,
                         unused, mem_cache_and_buff, diff, total_malloced, total_pages,
                         mem_cache_and_buff)

        self.last_total = mem_total
        self.last_target = target

        try:
            ans = self.memcached_client.send_recv("m %i" % target)
#            self.logger.debug("Memcached answer: %s", ans.rstrip())
        except socket.error as err:
            self.logger.warn("Error in setting memcached memory to %i: %s",
                             target, err)

class Memcached(Program):
    memcached_port = 11211

class Host_Memcached(Memcached, HostProgram):

    def __init__(self, keys_dist, vals_dist, cmd_get_percent, win_size, seed=0, remote_ip = None, **kwargs):
        '''
        keys_dist - keys distribution (size of keys)
        vals_dist - values distribution (size)
        '''
        if isinstance(keys_dist, str):
            keys_dist = eval(keys_dist)
        self.keys_dist = keys_dist
        if isinstance(vals_dist, str):
            vals_dist = eval(vals_dist)
        self.vals_dist = vals_dist
        self.cmd_get_percent = float(cmd_get_percent)
        self.win_size = str(win_size)
        self.seed = int(seed)
        self.remote_ip = remote_ip
        Memcached.__init__(self, self.keys_dist, self.vals_dist,
                           self.cmd_get_percent, self.win_size, self.seed,
                           self.remote_ip)
        HostProgram.__init__(self, RemoteMemslap,  #MemSlap04406,
                (self.keys_dist, self.vals_dist,
                 self.cmd_get_percent, self.win_size, self.seed,
                 self.remote_ip),
                **kwargs)
        self.logger = logging.getLogger("Memcached-%s-%s" % (self.remote_ip, self.alias))
        self.logger.debug("Host_Memcached initialized")

class Guest_Memcached(Memcached, GuestProgram):
    def_mem_update_interval = 3  # seconds

    def __init__(self, spare_mem, init_mem_size, update_interval, use_dynamic = True):
        self.mem_spare = int(spare_mem)
        self.init_mem_size = int(init_mem_size)
        self.update_interval = int(update_interval)
        Memcached.__init__(self, self.mem_spare, self.init_mem_size,
                           self.update_interval)
        GuestProgram.__init__(self)

    # [BP]: TODO: use these flags only on memory auction
    def get_dynamic_args(self):
        return ["-o", "slab_reassign", "-o", "slab_automove=2"]

    def do_start(self):
        args = ["/usr/local/bin/memcached",
                "-p" , str(Memcached.memcached_port),
                "-u", "nobody"]
        args += self.get_dynamic_args()
        args += ["-m" , str(self.init_mem_size)]
        self.mem_ctrl = MemcachedMemCtrl(self, self.init_mem_size,
                    self.mem_spare, self.update_interval)
        self.prog = Popen(args, stdout = PIPE, stderr = PIPE)
        self.start_mem_ctrl()
        return self.prog.communicate()

    def start_mem_ctrl(self):
        self.mem_ctrl.start()

    def terminate_mem_ctrl(self):
        self.mem_ctrl.terminate()

    def do_terminate(self):
        try:
            self.terminate_mem_ctrl()
            self.prog.send_signal(signal.SIGINT)
            self.prog = None
        except OSError as ex:
            self.logger.error("couldn't kill memcached pid: %i, reason: %s",
                              self.prog.pid, ex)



#===============================================================================
# MemcachedStatic
#===============================================================================

class MemcachedStatic(Memcached):
    pass

class Host_MemcachedStatic(Host_Memcached):
    pass

class Guest_MemcachedStatic(Guest_Memcached):
    def get_dynamic_args(self):
        return [];
    def start_mem_ctrl(self):
        return
    def terminate_mem_ctrl(self):
        return

#===============================================================================
# MemoryConsumer
#===============================================================================

class MemoryConsumer(Program): pass

class Host_MemoryConsumer(MemoryConsumer, HostProgram):
    """
    Wrapper to prog/MemoryConsumer
    """
    def __init__(self, **kwargs):
        MemoryConsumer.__init__(self)
        HostProgram.__init__(self, MemoryConsumerBenchmark, (), **kwargs)

class Guest_MemoryConsumer(MemoryConsumer, GuestProgram):
    def __init__(self, spare_mem, saturation_mem, update_interval,
                 sleep_after_write):
        self.mem_spare = int(spare_mem)
        self.saturation = int(saturation_mem)
        self.update_interval = float(update_interval)
        self.sleep_after_write = float(sleep_after_write)

        MemoryConsumer.__init__(self, self.mem_spare, self.saturation,
                                self.update_interval, self.sleep_after_write)
        GuestProgram.__init__(self)

    def do_start(self):
        from exp.prog.MemoryConsumer import MemoryConsumer as MC
        self.prog = MC(self.mem_spare, self.saturation,
                       self.update_interval, self.sleep_after_write)
        return self.prog.start()

    def do_terminate(self):
        self.prog.terminate()

#===============================================================================
# DayTrader
#===============================================================================

class DayTrader(Program):
    pass

class Host_DayTrader(DayTrader, HostProgram):
    def __init__(self, **kwargs):
        DayTrader.__init__(self)
        HostProgram.__init__(self, ApacheBench, (), **kwargs)
        self.logger = logging.getLogger("DayTrader-%s" % self.alias)

    def get_response(self):
        url = "http://%s:8080/daytrader/" % self.ip
        while True:
            try:
                out = urllib.urlopen(url).read()
                if len(out) > 0 and out.find("HTTP Status 404") == -1:
                    break
            except IOError:
                pass
            time.sleep(1)

    def wait_for_program_init(self):
        self.logger.info("Wait for initialization...")
        self.get_response()

        # reset
        self.logger.info("Reset...")
        reset_url = "http://%s:8080/daytrader/config?action=resetTrade" % self.ip
        urllib.urlopen(reset_url).read()

        # setting options:
        market_summary_interval = 20
        primitive_iterations = 1
        RunTimeMode = "Session (EJB3) To Direct"
        OrderProcessingMode = "Asynchronous_2-Phase"
        AcessMode = "Standard"
        soap_url = "http://%s:8080/daytrader/services/TradeWSServices" % self.ip
        WorkloadMix = "Standard"
        WebInterface = "JSP"
        EnableLongRun = True

        options = {"action": "updateConfig",
                   "RunTimeMode": {"Full EJB3": "0",
                                   "Direct (JDBC)": "1",
                                   "Session (EJB3) To Direct": "2",
                                   "Web JDBC": "3",
                                   "Web JPA": "4"}[RunTimeMode],
                   "OrderProcessingMode": {
                            "Synchronous" : "0", "Asynchronous_2-Phase": "1"
                            }[OrderProcessingMode],
                   "AcessMode": {"Standard": "0", "WebServices": "1"
                                 }[AcessMode],
                   "SOAP_URL": soap_url,
                   "WorkloadMix": {"Standard": "0", "High-Volume": "1"
                                   }[WorkloadMix],
                   "WebInterface": {"JSP": "0", "JSP-Images": "1"
                                    }[WebInterface],
                   "MaxUsers": self.max_users,
                   "MaxQuotes": self.max_quotes,
                   "marketSummaryInterval": market_summary_interval,
                   "primIterations": primitive_iterations,
                   "EnableLongRun": EnableLongRun
                   }

        self.logger.info("applying requested settings...")
        data = urllib.urlencode(options)
        urllib.urlopen("http://%s:8080/daytrader/config" % self.ip, data)
        self.logger.info("Check again for response")
        self.get_response()
        self.logger.info("Ready!")

class Guest_DayTrader(DayTrader, GuestProgram):
    def_heap_size = 2500
    def_max_users = 200
    def_max_quotes = 400

    def __init__(self, heapsize = def_heap_size,
                 max_users = def_max_users, max_quotes = def_max_quotes):
        self.heapsize = int(heapsize)
        self.max_users = int(max_users)
        self.max_quotes = int(max_quotes)
        DayTrader.__init__(self, self.heapsize, self.max_users, self.max_quotes)
        GuestProgram.__init__(self)

    def do_start(self):
        if self.prog is not None:
            return
        os.chdir("/root/geronimo")
        cmd = "/root/geronimo/geronimo-tomcat6-javaee5-2.2/bin/start-server -J"\
              " -Xmx%iM -J -Xms%iM" % (self.heapsize, self.heapsize)

        self.prog = Popen(cmd, shell = True, preexec_fn = os.setsid,
                          stdout = PIPE, stderr = PIPE)
        return self.prog.communicate()

    def do_terminate(self):
        # self.prog.terminate() doesn't work since it must be
        # invoked with shell=True... thus must use pkill java.
        Popen("pkill java", shell = True).communicate()
        self.prog = None



#===============================================================================
# PostgresSQL
#===============================================================================

class PostgresSQL(Program):
    pass

class Host_PostgresSQL(PostgresSQL, HostProgram):
    def __init__(self, threads_count = 1, connection_per_transaction = True,
                 output_log = False, measure_frequency=0, **kwargs):

        self.threads_count = self.parameter_int(threads_count, 1)
        self.connection_per_transaction = self.parameter_bool(connection_per_transaction, True)
        self.output_log = self.parameter_bool(output_log, False)

        self.measure_frequency = self.parameter_int(measure_frequency, 0)

        PostgresSQL.__init__(self,
                             self.threads_count,
                             self.connection_per_transaction,
                             self.output_log,
                             self.measure_frequency)
        HostProgram.__init__(self, PostgresSQLBenchmark,
                             (self.threads_count,
                              self.connection_per_transaction,
                              self.output_log,
                              self.measure_frequency),
                             **kwargs)
        self.logger = logging.getLogger("PostgresSQL-%s" % self.alias)
        self.logger.info("Host_PostgresSQL initialized")

class Guest_PostgresSQL(PostgresSQL, GuestProgram):
# PostgresSQL Options:
#   -B NBUFFERS     number of shared buffers
#   -c NAME=VALUE   set run-time parameter
#   -d 1-5          debugging level
#   -D DATADIR      database directory
#   -e              use European date input format (DMY)
#   -F              turn fsync off
#   -h HOSTNAME     host name or IP address to listen on
#   -i              enable TCP/IP connections
#   -k DIRECTORY    Unix-domain socket location
#   -l              enable SSL connections
#   -N MAX-CONNECT  maximum number of allowed connections
#   -o OPTIONS      pass "OPTIONS" to each server process (obsolete)
#   -p PORT         port number to listen on
#   -s              show statistics after each query
#   -S WORK-MEM     set amount of memory for sorts (in kB)
#   --NAME=VALUE    set run-time parameter
#   --describe-config  describe configuration parameters, then exit
#   --help          show this help, then exit
#   --version       output version information, then exit

    USERNAME = "postgres"
    DATA_DIR = "/usr/local/pgsql/data"

    PG_CTL_GENERIC_TEMPLATE = "sudo -u %s /usr/lib/postgresql/9.1/bin/pg_ctl -D %s %s"
    PG_CTL_CMD_TEMPLATE = PG_CTL_GENERIC_TEMPLATE % (USERNAME,DATA_DIR,"%s")

    CHANGE_SETTING_GENETIC_CMD_TEMPLATE = r"sed -i 's/#\{0,1\}\(%s\s*\(=\|\s\)\s*\)[0-9]\+\(MB\)\{0,1\}\(\s*#.*\)\{0,1\}/\1%s\3\4/' %s/postgresql.conf"
    CHANGE_SETTING_CMD_TEMPLATE = CHANGE_SETTING_GENETIC_CMD_TEMPLATE % ("%s", "%s", DATA_DIR)

    MB_PER_CHECKPOINT_SEGMENT = 16

    def __init__(self,
                 max_connections = None,
                 port_number = None,
                 shared_buffers_MB = None,
                 work_mem_MB = None,
                 temp_buffers_MB = None,
                 autovacuum = None,
                 checkpoint_timeout = None,
                 checkpoint_completion_target = None,
                 checkpoint_mem_static_MB = None,
                 checkpoint_mem_ratio = 0.5,
                 checkpoint_mem_change_threshold = 0.05,
                 mem_update_interval_sec = 3
                 ):

        self.max_connections        = self.parameter_int(max_connections)
        self.port_number            = self.parameter_int(port_number)
        self.shared_buffers_MB      = self.parameter_int(shared_buffers_MB)
        self.work_mem_MB            = self.parameter_int(work_mem_MB)
        self.temp_buffers_MB         = self.parameter_int(temp_buffers_MB)
        self.autovacuum             = self.parameter_bool(autovacuum)
        self.checkpoint_timeout     = self.parameter_int(checkpoint_timeout)
        self.checkpoint_completion_target       = self.parameter_float(checkpoint_completion_target)
        self.checkpoint_mem_static_MB           = self.parameter_int(checkpoint_mem_static_MB)
        self.checkpoint_mem_ratio               = self.parameter_float(checkpoint_mem_ratio, 0.5)
        self.checkpoint_mem_change_threshold    = self.parameter_float(checkpoint_mem_change_threshold, 0.05)
        self.mem_update_interval_sec            = self.parameter_int(mem_update_interval_sec, 3)

        PostgresSQL.__init__(self,
                             self.max_connections,
                             self.port_number,
                             self.shared_buffers_MB,
                             self.work_mem_MB,
                             self.temp_buffers_MB,
                             self.autovacuum,
                             self.checkpoint_timeout,
                             self.checkpoint_completion_target,
                             self.checkpoint_mem_static_MB,
                             self.checkpoint_mem_ratio,
                             self.checkpoint_mem_change_threshold,
                             self.mem_update_interval_sec
                             )
        GuestProgram.__init__(self)

        self.mem_ctrl = None

    def do_start(self):
        if self.prog is not None:
            return

        args = ["sudo",
        		"-u", self.USERNAME,
        		"postgres",
        		"-D", self.DATA_DIR
        		]

        if self.max_connections != None:
            args += ["-N", str(self.max_connections)]
        if self.port_number != None:
            args += ["-p", str(self.port_number)]
        if self.work_mem_MB != None:
            args += ["-c", "work_mem=%sMB" % self.work_mem_MB]
        if self.shared_buffers_MB != None:
            args += ["-c", "shared_buffers=%sMB" % self.shared_buffers_MB]
        if self.temp_buffers_MB != None:
            args += ["-c", "temp_buffers=%sMB" % self.temp_buffers_MB]
        if self.autovacuum != None:
            args += ["-c", "autovacuum=%s" % self.autovacuum]
        if self.checkpoint_timeout != None:
            args += ["-c", "checkpoint_timeout=%s" % self.checkpoint_timeout]
        if self.checkpoint_completion_target != None:
            args += ["-c", "checkpoint_completion_target=%s" % self.checkpoint_completion_target]
        if self.checkpoint_mem_static_MB != None:
            args += ["-c", "checkpoint_segments=%s" % int(self.checkpoint_mem_static_MB / self.MB_PER_CHECKPOINT_SEGMENT)]

        try:
            self.mem_ctrl = PostgresSQLMemCtrl(self.checkpoint_mem_ratio,
                                               self.checkpoint_mem_change_threshold,
                                               self.mem_update_interval_sec)
            self.mem_ctrl.start()
        except Exception as e:
            self.logger.error("Could not start Memory Control: ", e)

        self.prog = Popen(args) #, stdout = PIPE, stderr = PIPE)
        return self.prog.communicate()

    def do_terminate(self):
        self.pg_ctl("stop")
        self.prog = None

        if self.mem_ctrl != None:
            self.mem_ctrl.terminate()

    @staticmethod
    def get_image_name(*args):
        return "postgres-master.qcow2"

    @classmethod
    def pg_ctl(cls, op):
        cmd = cls.PG_CTL_CMD_TEMPLATE % op
        return Popen(cmd, shell = True).communicate()

    @classmethod
    def update_settings(cls, **settings):
        for key, value in settings.iteritems():
            cls.change_configuration_file(key, value)

        cls.pg_ctl("reload")

    @classmethod
    def change_configuration_file(cls, key, value):
        cmd = cls.CHANGE_SETTING_CMD_TEMPLATE % (key, value)
        Popen(cmd, shell = True).communicate()

class PostgresSQLMemCtrl(DynamicMemCtrl):
    GB_IN_MB = 1<<10

    def __init__(self, checkpoint_mem_ratio, checkpoint_mem_change_threshold, mem_update_interval_sec):
        DynamicMemCtrl.__init__(self, mem_update_interval_sec, name = "PostgresSQLMemCtrl")

        self.checkpoint_mem_ratio = checkpoint_mem_ratio
        self.checkpoint_mem_change_threshold = checkpoint_mem_change_threshold

        self.change_ratio_threshold_high = 1.0 + self.checkpoint_mem_change_threshold
        self.change_ratio_threshold_low = 1.0 - self.checkpoint_mem_change_threshold

        self.change_high_threshold = 0
        self.change_low_threshold = 0

        self._current_mem = 0

        # According to experiments
        self.checkpoint_segments_function = lambda mem: max(1,int((mem * self.checkpoint_mem_ratio)/Guest_PostgresSQL.MB_PER_CHECKPOINT_SEGMENT))

        # According to PostgresSQL recommendations
        self.shared_buffers_function = lambda mem: max(1,int(mem/4 if mem > self.GB_IN_MB else int(mem * 0.15)))

    def calc_effective_cache_size_function(self, mem_total, mem_usage, mem_cache_and_buff):
        '''
        According to PostgresSQL recommendations:
        https://wiki.postgresql.org/wiki/Tuning_Your_PostgreSQL_Server
        '''
        return mem_cache_and_buff + (mem_total - mem_usage)

    @property
    def current_mem(self):
        return self._current_mem

    def change_mem_func(self, mem_total, mem_usage, mem_cache_and_buff):
        if self.change_low_threshold < mem_total < self.change_high_threshold:
            return

        self._current_mem = mem_total
        self.change_high_threshold = mem_total * self.change_ratio_threshold_high
        self.change_low_threshold = mem_total * self.change_ratio_threshold_low

        effective_cache_size = self.calc_effective_cache_size_function(mem_total, mem_usage, mem_cache_and_buff)

        Guest_PostgresSQL.update_settings(
                 # change requires restart
                 #shared_buffers = self.shared_buffers_function(mem_total),
                 #checkpoint_segments = self.checkpoint_segments_function(mem_total),
                 effective_cache_size = effective_cache_size
                 )

#===============================================================================
# Phoronix Test Suite
#===============================================================================

class PhoronixTestSuite(Program):
    pass

class Host_PhoronixTestSuite(PhoronixTestSuite, HostProgram):
    def __init__(self, test, test_parameters = {}, **kwargs):
        self.test = str(test)
        self.test_parameters = self.parameter_dict(test_parameters, {})

        PhoronixTestSuite.__init__(self,
                             self.test,
                             self.test_parameters)
        HostProgram.__init__(self, PhoronixTestSuiteBenchmark,
                             (self.test,
                              self.test_parameters),
                             **kwargs)

        self.logger = logging.getLogger("PhoronixTestSuite-%s-%s" % (self.test, self.alias))

class Guest_PhoronixTestSuite(PhoronixTestSuite, GuestProgram):
    def do_start(self):
        pass

    def do_terminate(self):
        pass

    @staticmethod
    def get_image_name(*args):
        return "phoronix-master.qcow2"
