#! /usr/bin/env python

# Ginseng
# Copyright (C) 2012 Eyal Posener, Orna Agmon Ben Yehuda,
# Technion - Israel Institute of Technology
#
# Ginseng is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Ginseng is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Ginseng.  If not, see <http://www.gnu.org/licenses/>.

import time
import random
import itertools
from etc.PySock import TcpThreadedServer
from etc.TerminatableThread import TerminatableThread
import threading
from exp.prog.DynamicMemCtrl import DynamicMemCtrl
import gc

class MemoryConsumerServer(TcpThreadedServer):
    port = 1938

    def __init__(self, prog):
        self.prog = prog
        TcpThreadedServer.__init__(self, "", self.port,
                                   name = "MCServer", timeout = None)

    def process(self, msg):
        """
        Get load as msg.
        Return performance of program.
        """
        if not msg:
            return "bad message"

        msg = eval(msg)

        try:
            load = msg["load"]
            duration = msg["duration"]
        except Exception:
            self.logger.error("While parsing load: %s", msg)
            load = 0
        self.prog.set_load(load)
        self.prog.reset_perf()  # to reset the performance
        time.sleep(duration)
        return self.prog.get_perf()

class Worker(TerminatableThread):

    random_jumpahead_ind = 1

    def __init__(self, write_func, sleep_duration):
        TerminatableThread.__init__(self, name = "Worker")
        self.write_func = write_func
        self.sleep_duration = sleep_duration
        # set a worker own random number generator
        self.random = random.Random()
        self.random.jumpahead(self.random_jumpahead_ind)
        self.random_jumpahead_ind += 1

    def run(self):
        while self.should_run:
            self.write_func(self.random)
            time.sleep(self.sleep_duration)

class MemoryConsumer(DynamicMemCtrl):

    def __init__(self, spare_mem, saturation_mem, update_interval,
                 sleep_after_write):
        '''
        MemoryConsumer is a program which its performance depends on the
        total memory and the load of the program.

        @param spare_mem: amount of memory to leave untouched: memory size
            will be total_memory - spare_memroy.
        @param saturation_mem: saturation point in MB which above additional
            memory won't increase the performance
        @param update_interval: time constant in seconds for updating the
            load and memory allocation of the program.
        @param sleep_after_write: amount of time in seconds that each memory
            writing thread will sleep after writing 1MB of memory.

        The program is composed of a server on port [MemoryConsumerServer.port]
        that can get load and return the current performance of the program.
        The program is constantly controlling an array of 1MB objects such that
        it will occupy (total_memory - spare_mem) of the machine available
        memory.
        According to the current load, the program create threads which
        constantly writing to the memory. this causes the performance to
        increase as the load increases.
        writing to the memory is done by picking a random number between 0 and
        saturation point, and if the number is with the 1MB object limit, the
        program is writing the object to the memory and increasing a counter.
        This causes the performance to increase as the available memory
        increases up to the point it reaches (saturation - spare_mem).
        performance is calculated each time the method get_perf is called by the
        amount of MBs written divided by the elapsed time - kind of throughput.
        '''
        if saturation_mem <= spare_mem:
            raise ValueError("Saturation must be greater then spare memory!")
        self.spare_mem = spare_mem
        self.saturation = saturation_mem
        self.max_rand = self.saturation - self.spare_mem
        self.update_interval = update_interval
        self.sleep_after_write = sleep_after_write
        DynamicMemCtrl.__init__(self, update_interval, name = "MemoryConsumer")
        self.mem_arr = [None] * 2 * saturation_mem
        self.mem_top = 0
        self.workers = []
        self.change_load_lock = threading.Lock()
        self.reset_perf()
        self.last_usage = 0
        self.server = MemoryConsumerServer(self)

    @property
    def current_mem(self):
        return self.mem_top

    def change_mem_func(self, mem_total, mem_usage, mem_cache_and_buff):
        free = mem_total - mem_usage
        diff = free - self.spare_mem
        # update max_rand if no allocation yet
        if self.mem_top == 0:
            self.max_rand = self.saturation - self.spare_mem - mem_usage
            self.logger.info("setting max rand to: %i", self.max_rand)

        target = self.mem_top + diff

        self.logger.info("Changing memory from %i cells to %i" %
                         (self.mem_top, target))
        self.change_mem(target)

    def start(self):
        self.logger.info("Started")
        try:
            self.server.serve_forever()
            DynamicMemCtrl.run(self)  #blocking
        except:
            raise
        finally:
            # closing controlling threads
            self.logger.debug("Shutting down server")
            self.server.shutdown()
            # kill all workers and free memory
            self.logger.debug("Killing all workers")
            self.set_load(0)
            self.logger.debug("Free all memory")
            self.change_mem(0)
            self.logger.info("Ended")
            return None, None

    def change_mem(self, target):
        """
        change memory array size
        """
        if not (0 <= target <= len(self.mem_arr)):
            target = min(max(target, 0), len(self.mem_arr))
            self.logger.info("Setting target within limits: [0,%i]",
                             len(self.mem_arr))

        top_before = self.mem_top

        # add 1MB objects to end
        try:
            while self.mem_top < target:
                self.mem_arr[self.mem_top] = bytearray(1 << 20)
                self.mem_top += 1
        except Exception as ex:
            self.logger.error(
                "Error allocating. current: %i, remained change: %i, Error: %s",
                self.mem_top, self.mem_top - target, str(ex))

        # remove and delete the objects from the beginning
        if self.mem_top > target:
            while self.mem_top > target:
                try:
                    self.mem_arr[self.mem_top] = None
                finally:
                    self.mem_top -= 1
                    gc.collect()

        if __debug__ and top_before != self.mem_top:
            self.logger.debug("Memory changed to %i", self.mem_top)

    def get_perf(self):
        """
        calculate performance
        """
        duration = time.time() - self.measure_start
        if duration == 0:
            return 0, 0
        hits = float(self.hits.next()) / duration
        throughput = float(self.throughput.next()) / duration
        return {"hits_rate": hits, "throughput": throughput,
                "mem-top": self.mem_top, "test_time": duration}

    def reset_perf(self):
        self.measure_start = time.time()
        self.hits = itertools.count()
        self.throughput = itertools.count()

    def write_random_cell(self, random_generator):
        """
        write 1MB into random cell
        """
        # generate random index
        i = random_generator.randrange(0, max(self.max_rand, self.mem_top))
        # increment throughput
        self.throughput.next()
        # check for index validity
        if i >= len(self.mem_arr) or self.mem_arr[i] is None:
            return
        # write to cell
        try:
            self.mem_arr[i][:] = itertools.repeat(0, len(self.mem_arr[i]))
            # if write succeed increment the hits
            self.hits.next()
        except MemoryError as err:
            self.logger.error("While writing memory: %s", err)

    def set_load(self, load):
        """
        change number of workers according to load
        """
        load = max(0, load)  # load must be not-negative
        change = load != self.load

        with self.change_load_lock:
            # add workers
            while load > self.load:
                w = Worker(self.write_random_cell, self.sleep_after_write)
                try:
                    w.start()
                    self.workers.append(w)
                except Exception as ex:
                    self.logger.error(
                        "While starting worker: %s, current load: %i",
                        ex, self.load)
                    break
            # remove workers
            # first terminate needed amount workers and then wait for
            # terminated workers to finish
            terminated = []
            while load < self.load:
                w = self.workers.pop()
                w.terminate()
                terminated.append(w)
            for w in terminated:
                w.join()

        if change:
            self.logger.debug("Load changed to: %i", self.load)

    @property
    def load(self):
        return len(self.workers)

