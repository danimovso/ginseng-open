"""
This experiment should always work. 
If it doesn't work, it means that the changes you made have caused the problem.

Most recnt test: 29/08/2017 12:34 (Liran, tapuz25)

This is the most simple experiments.
There are examples for more advanced parameters in
the other experiments scripts.

@author: Danielle
@author: Liran Funaro <funaro@cs.technion.ac.il>
"""
import os
import time
from collections import OrderedDict

from mom.LogUtils import LogUtils
from mom.Collectors.HostMemory import HostMemory

import exp.core.Loads
from exp.prog.Programs import Guest_Memcached, Host_Memcached, \
     Guest_MemoryConsumer, Host_MemoryConsumer

from exp.core.VMDesc import get_vm_desc
from exp.core.Experiment import Experiment, set_output_path
from exp.core.ExpMachine import ExpMachineProps


class Configuration(object):
    """
    Configuration is a helper class that store a guest configuration
    for an experiment.
    """
    def __init__(self, prog_args, bm_args, base_mem, saturation_mem,
                 profiler_file, profiler_entry, revenue_func_str,
                 load, load_interval, val_switch_func=None,
                 estimator_enable_flag=None,
                 max_vcpus=1, swappiness=100, adviser_class_name=''):
        self.prog_args = prog_args
        self.bm_args = bm_args
        self.base_mem = base_mem
        self.saturation_mem = saturation_mem
        self.profiler_file = profiler_file
        self.profiler_entry = profiler_entry
        self.load = load
        self.load_interval = load_interval
        self.revenue_func_str = revenue_func_str
        self.val_switch_func = val_switch_func
        self.max_vcpus = max_vcpus
        self.swappiness = swappiness

        self.adviser = {'name': adviser_class_name,
                        'profiler': profiler_file,
                        'advice_entry': profiler_entry,
                        'rev_func': revenue_func_str,
                        'base_mem': base_mem,
                        'memory_delta': 10,
                        'use_estimator': estimator_enable_flag,
                        # 'alloc_diff': alloc_diff,
                       }


################################################################################
# General experiment configurations
################################################################################

# verbosity sets the logger verbosity.
# "debug" means log everything.
verbosity = "debug"

# exp_folder sets the experiment results folder to store all the experiments
# of this script
exp_folder = "exp"

# exp_name sets the experiment name. This will define the sub-folder of the 
# experiment followed by an increasing number.
exp_name = "malicious_timing_attack_2_round_interval_attacks"

# conf_file sets the configuration file to use for mom/moc.
# The path is relative to the Ginseng root directory.
# None: sets the default file of the moc/mom instance.
conf_file = None

# auction_intervals specify the timing of each auction step (seconds).
# More details can be found in exp/core/Mocs.py "MocAuctioneer"
auction_intervals = [3, 1, 8]
auction_duration = sum(auction_intervals)

# warmup specify the time (seconds) to allow the benchmark
# to run before starting to work with real valuations.
# When using 'AdviserAttack' make sure the warmup value is set to 0
warmup = 0 # Has no warmup time
warmup_rounds = warmup / auction_duration

# affine_maximizer_p0 is set to 0.0 to turn off the affine-maximizer
# If you are not sure what the affine-maximizer, don't use it.
affine_maximizer_p0 = 0.0

# over_commitment_factor controls the memory that will be offered in the auction.
# It is a factor of the sum of the required memory of all the guests.
# If auction_mem is not None, this will be ignored.
over_commitment_factor = 0.5

# auction_mem define the quantity of memory that will be offered in
# the auction (MB).
# None: use the "over_commitment_factor" instead.
#auction_mem = None
auction_mem = 1550

# host_mem define the quantity of memory that will be reserved
# to the host (MB).
# None: set the host memory to all the unused memory (exluding the auction memory)
host_mem = None

# An ordered key/value where the key is an experiment name
# and the value is a list of configuration.
# Each configuration defines a single virtual machine.
exp_configs = OrderedDict()


################################################################################
# Define all the experiments
################################################################################

# It is possible to define multiple experiments in a loop
# and this script will run them one after the other in that order.
# For example, we define here only one experiment.
for duration in [auction_duration * 151]: # Run for 5 rounds (excluding warmup)
    exp_configs[duration] = [
        Configuration(# Memcached (MCD)
            prog_args=Guest_Memcached(
                spare_mem=50,
                init_mem_size=10,
                update_interval=1
            ).command_args,
            bm_args=Host_Memcached(
                cmd_get_percent=0.3,
                keys_dist=[(249, 249, 1.0)],
                vals_dist=[(1024, 1024, 1.0)],
                win_size="500k",
            ).command_args,
            base_mem=600,
            saturation_mem=2500,  # according to the profiler
            profiler_file="doc/profiler-memcached-inside-spare-50-win-500k-tapuz25.xml",
            profiler_entry="hits_rate",
            load=exp.core.Loads.LoadConstant(8), 
            load_interval=warmup + duration - 10 ,
            #revenue_func_str='lambda x: 10*x**0.5',
            revenue_func_str='lambda x: 3*x',
            estimator_enable_flag=[True],
	        adviser_class_name='AdviserProfitEstimator',
        ),
        Configuration(# Memory Consumer (MC)
            prog_args=Guest_MemoryConsumer(
                spare_mem=50,
                saturation_mem = 2000,
                update_interval=1,
                sleep_after_write = 0.1,
            ).command_args,
            bm_args=Host_MemoryConsumer().command_args,
            base_mem=400,
            saturation_mem=2000,
            profiler_file="doc/profiler-mc-spare-50-satur-2000-tapuz21.xml",
            profiler_entry="hits_rate",
            load=exp.core.Loads.LoadConstant(1),
            load_interval=5,
            revenue_func_str=['lambda x: 2*x'],
            estimator_enable_flag=[True],
	        adviser_class_name='AdviserMaliciousTimingAttack',
        ),
    ]


def run_experiment(output_path, duration, configs):
    # Calculate the required memory by all the guests.
    # If we offer this quantity for auction, everyone will have
    # enough memory and no auction will be needed.
    min_mem = sum([c.base_mem for c in configs])
    max_mem = sum([c.saturation_mem for c in configs])
    total_required_mem = int(max_mem - min_mem)

    if auction_mem is None:
    	cur_auction_mem = int(total_required_mem * over_commitment_factor)
    else:
    	cur_auction_mem = int(auction_mem)

    # Use the HostMemory collector to get the available memory.
    total_mem = HostMemory(None).collect()["mem_available"]

    if host_mem is None:
    	cur_host_mem = total_mem - cur_auction_mem - min_mem
    else:
    	cur_host_mem = int(host_mem)

    # Init the VMs descriptors
    vm_count = len(configs)
    vm_names = ["vm-%i" % (i + 1) for i in range(vm_count)]
    vms_desc = {}

    for name, conf in zip(vm_names, configs):
        vms_desc[name] = ExpMachineProps(
            adviser_args=conf.adviser,
            prog_args=conf.prog_args,
            bm_args=conf.bm_args,
            load_func=conf.load,
            load_interval=conf.load_interval,
            desc=get_vm_desc(name),
            val_switch_func=conf.val_switch_func,
            max_vcpus=conf.max_vcpus,
            swappiness=conf.swappiness,
        )
        vms_desc[name]['desc']['base_mem'] = conf.base_mem
        vms_desc[name]['desc']['max_mem'] = conf.saturation_mem

    # Define the moc instance to use. For more details on the avialable
    # mocs and their parameters, see exp/core/Mocs.py
    moc_auctioneer_args = (cur_host_mem, affine_maximizer_p0, auction_intervals,
    					   cur_auction_mem, conf_file, warmup_rounds)
    moc_args = "MocAuctioneer", moc_auctioneer_args

    # Set a sub forlder the the current experiment
    exp_out = os.path.join(output_path, "duration-%d" % duration)
    os.mkdir(exp_out)

    print "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
    print "~ Starting Experiment..."
    print "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
    print "~ Total Temory:   %i" % total_mem
    print "~ Auction Memory: %i" % cur_auction_mem
    print "~ Host Memory:    %i" % cur_host_mem
    print "~ Warmup Time:    %i" % warmup
    print "~ Duration:       %i" % duration
    print "~ Output Path:    %s" % exp_out
    print "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"

    start_time = time.time()
    Experiment(
        moc_args=moc_args,
        vms_desc=vms_desc,
        output_path=exp_out,
        duration=warmup + duration,
        verbosity=verbosity,
    ).start()
    end_time = time.time()

    print "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
    print "~ Experiment Finished (%d seconds)" % (end_time - start_time)
    print "~ Results are in %s" % output_path
    print "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"


if __name__ == '__main__':
    # Setup the logger output format and the verbosity
    LogUtils(verbosity)

    # Build the output directory full path
    # e.g., ~/moc-output/exp/must-work-76
    output_path = set_output_path("exp", exp_name)
    
    # Run all the experiments
    for duration, configs in exp_configs.iteritems():
        run_experiment(output_path, duration, configs)
