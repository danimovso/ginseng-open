#! /usr/bin/env python
'''
Created on Mar 9, 2012

@author: eyal
'''
from exp.core.Loads import LoadSine
from exp.prog.Programs import Memcached
import os
from mom.Collectors.HostMemory import HostMemory
from exp.core.VMDesc import get_vm_desc
from mom.LogUtils import LogUtils
from exp.core.Experiment import confirm_moc_path
from exp.core.Experiment import Experiment
from etc.Settings import Settings

if __name__ == '__main__':

    LogUtils("info")
    confirm_moc_path()

    exp_name = "static-random-sine"
    n = 2
    host_mem = 2048
    moc_args = ("MocStatic",)

    mem_tot = HostMemory(None).collect()['mem_available']
    mem_guest = int((mem_tot - host_mem) / n)

    exp_dir = "%s/moc-output/exp-dshad4/ginseng-random-sine-2012-03-15-20:05:14" % Settings.user_home()

    funcs_data = eval(file(os.path.join(exp_dir, "funcs"), "r").readline())

    funcs = []
    for i in range(n):
        funcs.append(LoadSine(funcs_data['amplitude'][i], funcs_data['T'][i],
                              funcs_data['phase'][i], funcs_data['offset']))

    out_dir = os.path.join(exp_dir, "static")
    try: os.mkdir(out_dir)
    except OSError: pass

#    import pylab as pl
#    t = range(20000)
#    for i in range(n):
#        pl.plot(t, [funcs[i](k) for k in t])
#    pl.plot(t, [sum([f(k) for f in funcs]) for k in t], "--")
#    pl.show()

    vms = {}
    for i in range(n):
        vms["vm-%i" % (i + 1)] = {
                "prog" : Memcached(memsize = 700, memslap_winsize = 10),
                "adviser_args": "AdviserProfit doc/profiler-memcached.xml throughput def_rev_func 25 1 3",
                "load_func" : funcs[i]
                }

    for vm in vms.keys():
        get_vm_desc(vm)['base0'] = mem_guest

    Experiment(moc_args, vms, funcs_data["load_interval"], out_dir).start()
