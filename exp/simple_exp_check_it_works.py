'''
Created on Apr 1, 2012

@author: eyal
'''
import exp.core.Loads
from exp.prog.Programs import Guest_Memcached, Host_Memcached, \
     Guest_MemoryConsumer, Host_MemoryConsumer
from mom.LogUtils import LogUtils
from exp.core.Experiment import set_output_path
import os
from exp.core.VMDesc import get_vm_desc
from exp.core.ExpMachine import ExpMachineProps
from exp.core.Experiment import Experiment
from exp.core.Loads import LoadBinary, SeveralStepsValuationSwitcher
from mom.Collectors.HostMemory import HostMemory

t_auction = [3, 1, 8]
warmup = 60 * 60
auction_duration = sum(t_auction)
warmup_rounds = warmup / sum(t_auction)
short_step_size = [auction_duration * 2]
oc = 2

#p0s = map(lambda p: p / 10.0, range(0, 11, 2))
#p0s = [0, 1]


# alloc_diff = 0

class Configuration(object):
    def __init__(self, prog_args, bm_args, base_mem, saturation_mem,
                 profiler_file, profiler_entry, revenue_func_str,
                 load, load_interval, val_switch_func=None,
                 estimator_enable_flag=None,
                 max_vcpus=1, swappiness=100):
        self.prog_args = prog_args
        self.bm_args = bm_args
        self.base_mem = base_mem
        self.saturation_mem = saturation_mem
        self.profiler_file = profiler_file
        self.profiler_entry = profiler_entry
        self.load = load
        self.load_interval = load_interval
        self.revenue_func_str = revenue_func_str
        self.val_switch_func = val_switch_func
        self.max_vcpus = max_vcpus
        self.swappiness = swappiness

        self.adviser = {'name': 'AdviserProfitEstimator',
                        'profiler': profiler_file,
                        'advice_entry': profiler_entry,
                        'rev_func': revenue_func_str,
                        'base_mem': base_mem, 'memory_delta': 10,
                        'use_estimator': estimator_enable_flag}  # ,


# 'alloc_diff': alloc_diff}

configs = {}  # config set pre short_step_size
for f in short_step_size:
    switch_times = (warmup, auction_duration * 50, f,
                    auction_duration * 50, auction_duration * 60)
    duration = sum(switch_times[1:])
    configs[f] = [
        Configuration(  # MCD
            prog_args=Guest_Memcached(
                spare_mem=50,
                init_mem_size=10,
                update_interval=1
            ).command_args,
            bm_args=Host_Memcached(
                cmd_get_percent=0.3,
                keys_dist=[(249, 249, 1.0)],
                vals_dist=[(1024, 1024, 1.0)],
                win_size="500k",
            ).command_args,
            base_mem=600,
            saturation_mem=2500,  # according to the profiler
            profiler_file="doc/profiler-memcached-inside-spare-50-win-500k-tapuz25.xml",
            profiler_entry="hits_rate",
            load=exp.core.Loads.LoadConstant(8), 
            load_interval=warmup + duration - 3 * 60,
            revenue_func_str='lambda x: 2.5*x',
            estimator_enable_flag=[True],
        ),
        Configuration(  # MC - this should be the attacker
            prog_args=Guest_MemoryConsumer(
                spare_mem=50,
                saturation_mem = 2000,
                update_interval=1,
                sleep_after_write = 0.1,
            ).command_args,
            bm_args=Host_MemoryConsumer().command_args,
            base_mem=600,
            saturation_mem=2000,
            profiler_file="doc/profiler-mc-spare-50-satur-2000-tapuz21.xml",
            profiler_entry="hits_rate",
            load=exp.core.Loads.LoadConstant(1),
            load_interval=5,
            revenue_func_str=['lambda x:x'],
            estimator_enable_flag=[True],
        ),
    ]

if __name__ == '__main__':
    LogUtils("debug")
    name = "report-p0-demo"
    output_path = set_output_path("exp", name)
    for f, config in configs.iteritems():
        n = len(config)
        vm_names = ["vm-%i" % (i + 1) for i in range(n)]
        vms = {}

        min_mem = sum([c.base_mem for c in config])
        max_mem = sum([c.saturation_mem for c in config])
        auction_mem = int((max_mem - min_mem) / oc)
        #    auction_mem = 500 # let's try this
        total_mem = HostMemory(None).collect()["mem_available"]
        #    host_mem = total_mem - auction_mem - min_mem
        host_mem = 500
        print "Overcommitment: %.2f\nTotal memory: %i\nAuction memory: %i\nHost memory: %i\nFrequency: %i" % (
            oc, total_mem, auction_mem, host_mem, f)

        for name, conf in zip(vm_names, config):
            vms[name] = ExpMachineProps(
                adviser_args=conf.adviser,
                prog_args=conf.prog_args,
                bm_args=conf.bm_args,
                load_func=conf.load,
                load_interval=conf.load_interval,
                desc=get_vm_desc(name),
                val_switch_func=conf.val_switch_func,
                max_vcpus=conf.max_vcpus,
                swappiness=conf.swappiness,
            )
            vms[name]['desc']['base_mem'] = conf.base_mem
            vms[name]['desc']['max_mem'] = conf.saturation_mem  # total_mem

        p0 = 0.0
        print "batch starting with p0 = %.2f and frequency = %i" % (p0, f)
        exp_out = os.path.join(output_path, "p0-%.2f-freq-%i" % (p0, f))
        os.mkdir(exp_out)
        moc_args = ("MocAuctioneer", (host_mem, p0, t_auction, auction_mem, None, warmup_rounds))
        Experiment(
            moc_args=moc_args,
            vms_desc=vms,
            output_path=exp_out,
            duration=warmup + duration,
            verbosity="debug",
        ).start()

        print "Results are in %s" % output_path
