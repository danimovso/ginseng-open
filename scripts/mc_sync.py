#!/usr/bin/env python
'''
Created on Mar 5, 2012

@author: eyal
'''
from subprocess import call
from sys import argv
import time

from mom.LogUtils import LogUtils

def mc_sync(n):
    LogUtils("info")
    for i in range(1, n + 1):
        time.sleep(i * 0.1)
        cmd = "qemu-img create -b /var/lib/libvirt/images/moc-master.qcow2 -f qcow2 /var/lib/libvirt/images/vm-%i.qcow2" % i
        call(cmd, shell = True)

if __name__ == '__main__':
    n = int(argv[1]) if len(argv) > 1 else 12
    mc_sync(n)
