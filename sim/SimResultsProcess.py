'''
Created on Mar 21, 2012

@author: eyal
'''

import numpy as np
import collections

class ProcessResult:
    def process_guest(self, data): pass
    def process_host(self, data): pass
    def finish(self): raise NotImplementedError

class HostRevenue(ProcessResult):
    def __init__(self):
        self.bills = []
    def process_guest(self, data):
        self.bills.extend(data['control_bill'])
    def finish(self):
        return np.sum(self.bills) if len(self.bills) > 0 else 0

class Sw(ProcessResult):
    """
    Social welfare
    """
    def __init__(self):
        self.sw = []
    def process_guest(self, data):
        self.sw.extend(data['rev'])
    def finish(self):
        return np.sum(self.sw) if len(self.sw) > 0 else 0

class Utility(ProcessResult):
    def __init__(self):
        self.util = []
    def process_guest(self, data):
        self.util.extend(data['profit'])
    def finish(self):
        return np.sum(self.util) if len(self.util) > 0 else 0

class Ties(ProcessResult):
    """
    return ties
    """
    def __init__(self):
        self.ties = []
    def process_host(self, data):
        self.ties = [int(tie) for tie in data['ties']]
    def finish(self):
        return np.average(self.ties) if len(self.ties) > 0 else 0

class Waste(ProcessResult):
    """
    waste = max_t(sum_i{contol_mem(i,t)}) - sum_i{min_t(control_mem(i,t))}
    """

    samples = 0.4
    def getvals(self, data):
        return np.array(data[int(-self.samples * len(data)):])

    def __init__(self):
        self.sums = None
        self.mins = []

    def process_guest(self, data):
        if (self.sums is None):
            self.sums = np.zeros(int(self.samples * len(data['control_mem'])))

        arr = self.getvals(data['control_mem'])
        self.sums += arr
        self.mins.append(min(arr))

    def finish(self):
        waste = max(self.sums) - sum(self.mins) if self.sums is not None else 0
        return waste

class PerfWaste(ProcessResult):
    samples = 0.4
    def getvals(self, data):
        return np.array(data[int(-self.samples * len(data)):])

    def __init__(self):
        self.sums = None
        self.mins = []

    def process_guest(self, data):
        if (self.sums is None):
            self.sums = np.zeros(int(self.samples * len(data['control_mem'])))

        arr = self.getvals(data['perf'])
        self.sums += arr
        self.mins.append(min(arr))

    def finish(self):
        waste = max(self.sums) - sum(self.mins) if self.sums is not None else 0
        return waste

class Poa(ProcessResult):
    """
    price-of-anarchy
    """
    def __init__(self):
        self.sw = []
        self.maxsw = []
    def process_guest(self, data):
        self.maxsw.extend(data['opt-val'])
        self.sw.extend(data['rev'])

    def finish(self):
        sw = float(sum(self.sw))
        maxsw = sum(self.maxsw)
        poa = 1 - sw / maxsw if maxsw != 0 else 0
        return poa

class PInMin(ProcessResult):
    def __init__(self):
        self.ps = []
    def process_host(self, data):
        self.ps = [p for p in data['p_in_min_mem']]
    def finish(self):
        return np.average(self.ps) if len(self.ps) > 0 else 0

class POutMax(ProcessResult):
    def __init__(self):
        self.ps = []
    def process_host(self, data):
        self.ps = [p for p in data['p_out_max_mem']]
    def finish(self):
        return np.average(self.ps) if len(self.ps) > 0 else 0

attributes = {
              "host-revenue" : HostRevenue,
              "social-welfare" : Sw,
              "utility" : Utility,
              "ties" : Ties,
              "waste" : Waste,
              "poa": Poa,
              "perf_waste" : PerfWaste,
              'p_in_min_mem' : PInMin,
              'p_out_max_mem' : POutMax
              }

def get_tools():
    return dict((measure, attr()) for measure, attr in attributes.items())
