from mom.Policy.Auctioneer import Auctioneer
from mom.Entity import Entity
from mom.LogUtils import LogUtils
import ConfigParser
from mom.Plotter import Plotter, HOST, POL

verbosity = "debug"
auction_intervals = [3, 1, 8]
step = sum(auction_intervals)
p0s = [1]

plotters = {HOST: Plotter(HOST, POL)}


def setup_guests(n):
    guests = []
    for i in map(lambda x: x + 1, range(n)):
        g = Entity()
        g._set_property('name', 'vm-%i' % (i,))
        g._set_property('base_mem', 0)
        guests.append(g)

    return guests


def setup_host(auction_mem, p0):
    host = Entity()
    host._set_property('config', ConfigParser.SafeConfigParser())
    host.Prop('config').add_section('host')
    host.Prop('config').set('host', 'p0-percent', str(p0))
    host.Prop('config').set('host', 'mem-bid-collection-interval',
                            str(auction_intervals[0]))
    host.Prop('config').set('host', 'mem-calculation-interval',
                            str(auction_intervals[1]))
    host.Prop('config').set('host', 'mem-notification-interval',
                            str(auction_intervals[2]))
    host.SetVar('auction_round_mem', 0)
    host.SetVar('auction_mem', auction_mem)
    host.Prop('config').set('host', 'warmup_rounds', '3')
    return host


def var_from_entity(data, src, key):
    data[key] = src.GetVar(key)


def prop_from_entity(data, src, key):
    data[key] = src.Prop(key)


def get_bids(t, host, guests):
    data = {}
    var_from_entity(data, host, 'auction_round_mem')
    var_from_entity(data, host, 'auction_mem')
    for g in guests:
        prop_from_entity(data, g, 'base_mem')
        prop_from_entity(data, g, 'base_bw')
        try:
            var_from_entity(data, g, 'not_mem')
            var_from_entity(data, g, 'not_bill_mem')
            var_from_entity(data, g, 'not_won_p_mem')
            var_from_entity(data, g, 'not_tie_mem')
            var_from_entity(data, g, 'not_round_mem')
            var_from_entity(data, g, 'p_in_min_mem')
            var_from_entity(data, g, 'p_out_max_mem')
        except KeyError as e:
            if data['auction_round_mem'] > 0:
                print "No previous info and we're not on the first round"
                raise
        data['load'] = g.Prop('load')(t)
        new_valuation = g.Prop('val_switch_func')(t)
        if new_valuation != g.GetVar('current_valuation'):
            g.Prop('adviser').next_valuation()
            g.SetVar('current_valuation', new_valuation)
        p, rqs = g.Prop('adviser').advice(data)
        g.SetVar('bid_round_mem', data['auction_round_mem'])
        g.SetVar('bid_p_mem', p)
        g.SetVar('bid_ranges_mem', rqs)


def next_round(host, guests):
    prev_round = host.GetVar('auction_round_mem')
    round_sw = 0
    for g in guests:
        mem = g.GetControl('control_mem')
        extra = mem - g.Prop("base_mem")
        bill = g.GetControl('control_bill_mem')
        g.SetVar('not_mem', mem)
        g.SetVar('not_bill_mem', bill)
        g.SetVar('not_won_p_mem', float(bill) / extra if extra != 0 else 0)
        g.SetVar('not_tie_mem', g.GetVar('tie_winner_mem'))
        g.SetVar('not_round_mem', prev_round)
        g.SetVar('p_in_min_mem', host.GetVar('p_in_min_mem'))
        g.SetVar('p_out_max_mem', host.GetVar('p_out_max_mem'))
        round_sw += extra * g.GetVar('bid_p_mem')
    host.SetVar('auction_round_mem', prev_round + 1)
    return round_sw


def do_bid(host, guest, p, rqs):
    data = {}
    var_from_entity(data, host, 'auction_round_mem')
    guest.SetVar('bid_round_mem', data['auction_round_mem'])
    guest.SetVar('bid_p_mem', p)
    guest.SetVar('bid_ranges_mem', rqs)


if __name__ == '__main__':
    LogUtils(verbosity)
    guests = setup_guests(3)
    host = setup_host(110, 0)  # 0.2)
    t = 0
    a = Auctioneer(verbosity=verbosity, simulate=True)
    print map(lambda g: g.Prop('base_mem'), guests)

    do_bid(host, guests[0], 1, ([90, 90],))
    do_bid(host, guests[1], 0.95, ([20, 94],))
    do_bid(host, guests[2], 0.9, ([0, 10],))
    a.auction(host, guests)
    sw = next_round(host, guests)

    do_bid(host, guests[0], 1, ([90, 90],))
    do_bid(host, guests[1], 0.95, ([21, 95],))
    do_bid(host, guests[2], 0.9, ([0, 10],))
    a.auction(host, guests)
    sw = next_round(host, guests)

    for i in range(3):
        do_bid(host, guests[0], 0.3, ([1870, 1900],))
        do_bid(host, guests[1], 0.16, ([0, 1650],))
        do_bid(host, guests[2], 0.25, ([0, 1250],))
        a.auction(host, guests)
        sw = next_round(host, guests)
