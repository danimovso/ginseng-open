from exp.core.Loads import LoadBinary, LoadConstant, LoadPiecewiseLinear, \
    LoadFunction, SeveralStepsValuationSwitcher
from mom.Policy.Auctioneer import Auctioneer
from mom.Advisers.AdviserProfitEstimator import AdviserProfitEstimator
from mom.Entity import Entity
from mom.Profiler import fromxml
import logging
from mom.LogUtils import LogUtils
import ConfigParser
from mom.Plotter import Plotter, HOST, POL

verbosity = "debug"
auction_rounds = 40
auction_intervals = [3, 1, 8]
step = sum(auction_intervals)
oc = 2
#frequencies = [30, 60, 120, 180, 360, 600]

class SomeStepsFunction(LoadFunction):
    def __init__(self, times):
        test_values = lambda l: all(map(lambda t: t > 0, l))
        if len(times) == 0:
           raise ValueError("Empty input")
        elif not test_values(times):
            raise ValueError("All times must be positive")
        else:
            self.times = list(times)

    def __call__(self, t):
        t %= sum(self.times)
        current = 0
        for i in range(len(self.times)):
            current += self.times[i]
            if current >= t:
                return i
        return len(self.times) - 1

    @property
    def __info__(self):
        return {"type": self.__class__.__name__,
            "times": self.times}

class Configuration(object):
    def __init__(self, base_mem, saturation_mem, profiler_file, profiler_entry,
                 valuation_funcs, load, load_interval, val_switch_func = None):
        self.base_mem = base_mem
        self.saturation_mem = saturation_mem
        self.profiler_file = profiler_file
        self.profiler_entry = profiler_entry
        self.load = load
        self.load_interval = load_interval
        self.valuation_funcs = valuation_funcs
        if val_switch_func is None:
            self.val_switch_func = lambda t: 0
        else:
            self.val_switch_func = val_switch_func
        self.adviser_params = (fromxml(profiler_file, profiler_entry),
            self.base_mem, self.profiler_entry, 10,
            self.valuation_funcs)

mcd_vals = [lambda t: 2*t, lambda t:0.5 * t]

configs = [
    Configuration( # psql
        base_mem = 600,#3000,
        saturation_mem = 2500,  # according to the profiler
        profiler_file = "doc/profiler-linear.xml",
        profiler_entry = "throughput",
        load = LoadConstant(50),
        load_interval = 10 * 60,
        valuation_funcs = lambda x:x,
    ),
    Configuration( # MCD
        base_mem = 600,
        saturation_mem = 2500,
        profiler_file = "doc/profiler-linear.xml",
        profiler_entry = "throughput",
        load = LoadConstant(8),
        load_interval = 1, # this doesn't really matter...
        valuation_funcs = mcd_vals,
        val_switch_func = LoadBinary(v1=0, v2=1, T=5 * step, T0=0),
    ),
]

plotters = {HOST: Plotter(HOST, POL)}

def setup_guests(config):
    guests = []
    auction_mem = 0
    for i, c in enumerate(config):
        adv = AdviserProfitEstimator(*c.adviser_params)
        g = Entity()
        g._set_property('name', 'vm-%i' % (i+1, ))
        g._set_property('base_mem', c.base_mem)
        g._set_property('adviser', adv)
        g._set_property('load', c.load)
        g._set_property('valuation_funcs', c.valuation_funcs)
        g._set_property('val_switch_func', c.val_switch_func)
        g.SetVar('current_valuation', c.val_switch_func(0))
        guests.append(g)
        auction_mem += c.saturation_mem - c.base_mem

    return guests, auction_mem

def setup_host(auction_mem):
    host = Entity()
    host._set_property('config', ConfigParser.SafeConfigParser())
    host.Prop('config').add_section('host')
    host.Prop('config').set('host', 'mem-bid-collection-interval', str(auction_intervals[0]))
    host.Prop('config').set('host', 'mem-calculation-interval', str(auction_intervals[1]))
    host.Prop('config').set('host', 'mem-notification-interval', str(auction_intervals[2]))
    host.SetVar('auction_round_mem', 0)
    host.SetVar('auction_mem', auction_mem)

    return host

def var_from_entity(data, src, key):
    data[key] = src.GetVar(key)

def prop_from_entity(data, src, key):
    data[key] = src.Prop(key)

def get_bids(t, host, guests):
    data = {}
    var_from_entity(data, host, 'auction_round_mem')
    var_from_entity(data, host, 'auction_mem')
    for g in guests:
        prop_from_entity(data, g, 'base_mem')
        try:
            var_from_entity(data, g, 'not_mem')
            var_from_entity(data, g, 'not_bill_mem')
            var_from_entity(data, g, 'not_won_p_mem')
            var_from_entity(data, g, 'not_tie_mem')
            var_from_entity(data, g, 'not_round_mem')
            var_from_entity(data, g, 'p_in_min_mem')
            var_from_entity(data, g, 'p_out_max_mem')
        except KeyError as e:
            if data['auction_round_mem'] > 0:
                print "No previous info and we're not on the first round"
                raise
        data['load'] = g.Prop('load')(t)
        new_valuation = g.Prop('val_switch_func')(t)
        if new_valuation != g.GetVar('current_valuation'):
            g.Prop('adviser').next_valuation()
            g.SetVar('current_valuation', new_valuation)
        p, rqs = g.Prop('adviser').advice(data)
        g.SetVar('bid_round_mem', data['auction_round_mem'])
        g.SetVar('bid_p_mem', p)
        g.SetVar('bid_ranges_mem', rqs)

def next_round(host, guests):
    prev_round = host.GetVar('auction_round_mem')
    round_sw = 0
    for g in guests:
        mem = g.GetControl('control_mem')
        extra = mem - g.Prop("base_mem")
        bill = g.GetControl('control_bill_mem')
        g.SetVar('not_mem', mem)
        g.SetVar('not_bill_mem', bill)
        g.SetVar('not_won_p_mem', float(bill) / extra if extra != 0 else 0)
        g.SetVar('not_tie_mem', g.GetVar('tie_winner_mem'))
        g.SetVar('not_round_mem', prev_round)
        g.SetVar('p_in_min_mem', host.GetVar('p_in_min_mem'))
        g.SetVar('p_out_max_mem', host.GetVar('p_out_max_mem'))
        round_sw += extra * g.GetVar('bid_p_mem')
    host.SetVar('auction_round_mem', prev_round + 1)
    return round_sw

if __name__ == '__main__':
    assert (type(oc) == int) and (oc > 0), "oc factor is not natural"
    LogUtils(verbosity)
    sw = 0
    guests, auction_mem = setup_guests(configs)
    auction_mem /= oc
    host = setup_host(auction_mem)
    t = 0
    a = Auctioneer(verbosity="info", simulate=True)
    for i in range(auction_rounds):
        get_bids(t, host, guests)
        a.auction(host, guests)
        sw += next_round(host, guests)
        t += step
    print "SW: %.02f" % sw
