'''
Created on Sep 13, 2011

@author: eyal
'''

from optimize import optimized_allocation
import numpy as np
import sys
from time import time

def sim_optimize(guests, rounds, total_mem, total_bw, min_vm_mem, max_vm_mem, min_vm_bw, max_vm_bw, d_mem, d_bw, dt):
    """
    use dt == 0 for static optimization (to save computational time)
    """
    opt = {}
    opt['Host'] = {}
    opt['Host']["total_mem"] = total_mem
    opt['Host']["total_bw"] = total_bw
    opt['Host']["opt-auction-time"] = [0] * rounds
    sorted_guests = sorted(guests, key = lambda g: g.name)
    for name in (g.name for g in guests):
        opt[name] = {"opt-alloc-mem": [0] * rounds,
                     "opt-alloc-bw": [0] * rounds,
                     "opt-val": [0] * rounds,
                     "opt-perf": [0] * rounds}

    # divide memory into chunks
    # assuming all guests has the same minimum memory
    mems = range(min_vm_mem, max_vm_mem, d_mem)
    if max_vm_mem not in mems: mems.append(max_vm_mem)
    total_mem -= len(guests) * min_vm_mem
    total_mem = max(total_mem, 0)


    # divide memory into chunks
    # assuming all guests has the same minimum memory
    bws = range(min_vm_bw, max_vm_bw, d_bw)
    if max_vm_bw not in bws: bws.append(max_vm_bw)
    total_bw -= len(guests) * min_vm_bw
    total_bw = max(total_bw, 0)

    sys.stdout.write("starting optimized simulation for memory: %i and bandwidth %i" % (total_mem, total_bw))
    # if dt ==0 (static optimization), this will only happen once.
    # otherwise it will happen once for every round.
    for t in (range(rounds) if dt != 0 else [0]):
        sys.stdout.write(".")
        sys.stdout.flush()
        # generate guests' revenue functions
        vals = []
        perfs = []
        for g in sorted_guests:
            load = int(g.load_func(t * dt))
            perf = []
            for m in mems:
                length = len(bws)
                perf.append(g.adviser.perf([load,\
                                            m,\
                                            np.asarray(bws)]))
            perfs.append(perf)
            vals.append(g.adviser.V_func(perf))


        # solve optimization - result is a list of (memory, bandwidth) allocation for each guest.
        start_time = time()
        opt_alloc = optimized_allocation(total_mem / d_mem, total_bw / d_bw, vals)
        end_time = time()    

        opt['Host']["opt-auction-time"][t] = end_time - start_time
        # add results to opt
        for i in range(len(sorted_guests)):
            name = sorted_guests[i].name
            mem_index = opt_alloc[i][0]
            bw_index = opt_alloc[i][1]
            opt[name]["opt-alloc-mem"][t] = mems[mem_index]
            opt[name]["opt-alloc-bw"][t] = bws[bw_index]
            opt[name]["opt-val"][t]   = vals[i][mem_index][bw_index]
            opt[name]["opt-perf"][t]  = perfs[i][mem_index][bw_index]

    sys.stdout.write("\n")

    if dt == 0:
        for name in opt['mem'].keys():
            for field in ("opt-alloc-mem", "opt-alloc-bw", "opt-val", "opt-perf"):
                opt[name][field] = rounds * [float(opt[name][field][0])]
                opt[name][field] = rounds * [float(opt[name][field][0])]

    return opt
