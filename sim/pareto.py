import numpy as np
import pylab as pl
from scipy import integrate

def bounded_pareto(x, alpha):
    """
    x - range to calculate the functuin on
    alpha - measured from nature - in wealth - about 1.36 or 1.4
    returns pdf (p) and the cdf (c)
    """
    x = np.array(x)
    L = x[0]
    H = x[-1]
    a = alpha
    assert L > 0 and H > L and a > 0
    p = (a * (L ** a) * (x ** (-a - 1))) / (1 - ((L / H) ** a))
    c = np.array([0] + list(integrate.cumtrapz(p, x)))

    return p, c

def pareto(x, alpha, xm = 1):
    """
    x - range to calculate the functuin on
    alpha - measured from nature - in wealth - about 1.36 or 1.4
    returns pdf (p) and the cdf (c)
    """
    x = np.array(x)
    cond = x >= xm
    c = np.zeros_like(x)
    c[cond] = 1 - (xm / x[cond]) ** alpha
    c[True - cond] = 0
    return c


def rand_x_from_y(x, y, n = 1):
    d = 1. / n
    ys = np.arange(d / 2, 1, d)
    return np.array([x[np.abs(y - yi).argmin()] for yi in ys]), ys

def bounded_pareto_distributed_numbers(L, H, alpha, n, res = 0.1):
    """
    Generates n pareto distributed numbers in the range [L,H]
    L - lower bound of range
    H - upper bound of range
    alpha - coefficient of pareto distribution
    n - number of numbers to generate.
    res- resolution of pareto graph
    """
    x = np.arange(L, H + 0.1 * res, res)
    p, c = bounded_pareto(x, alpha)
    xi, yi = rand_x_from_y(x, c, n)
    return map(int, xi)

def pareto_distributed_numbers(xmin, xmax, alpha, n, res = 0.1, xm = 1):
    """
    Generates n pareto distributed numbers in the range [xmin, xmax]
    Lalpha - coefficient of pareto distribution
    n - number of numbers to generate.
    res- resolution of pareto graph
    """
    x = np.arange(xmin, xmax + 0.1 * res, res)
    c = pareto(x, alpha, xm = 1)
    xi, yi = rand_x_from_y(x, c, n)
    return map(int, xi)

if __name__ == "__main__":
    x = pareto_distributed_numbers(10 ** (-4), 100, 1.1, 10 ** (-5), xm = 10 ** -4)
    print x
    for i in sorted(list(set(x))):
        print i, np.count_nonzero(np.array(x) == i)

    n = 10
    max_num = 20

    x = np.linspace(1, max_num, 1000)
    c = pareto(x, 1.1)

    xi, yi = rand_x_from_y(x, c, n)
    xint = np.floor(xi)

    counts = {}
    for i in range(1, max_num):
        counts[i] = np.count_nonzero(xint == i)

#    pl.plot(x, p, 'r')
    pl.plot(x, c, 'b')
    pl.plot(xi, yi, "r", marker = "x", linestyle = "None")
#     pl.plot(xint, yi, "r", marker = "o", linestyle = "None", alpha = 0.5)
    pl.plot(xint, yi, "r", marker = "o", linestyle = "None", alpha = 1.0)
#
#    for i in set(xint):
#        pl.text(i, 0, str(counts[int(i)]))
#    pl.grid(True)
    pl.show()

