'''
Created on Sep 13, 2011

@author: eyal
'''

from mom.Entity import Entity
from mom.Controllers.Announcer import Announcer
from mom.Controllers.Notifier import Notifier
from mom.Comm.GuestServer import DictDefaultNone
from mom.Policy.Auctioneer import Auctioneer
import ConfigParser
from mom.Comm.Messages import get_message
import time
import numpy as np
from mom.Plotter import HOST
import sys
import threading
from mom.Controllers.Notifier import costs_map_mem

def_rev_func = lambda perf: perf

results = {}

class DummyCommunicator:
    def __init__(self, data, mon, pol):
        """
        @param data - 
        @param mon - monitor - Guest instance. target: gather statistics
        @param pol - policy
        """
        self.data = data
        self.mon = mon
        self.pol = pol
    def communicate(self, msg, timeout = None):
        msg = get_message(repr(msg))  # test the message passing method
        return msg.process(self.data, self.mon, self.pol)

class PolicyDummy:
    def __init__(self, adviser):
        self.adviser = adviser
    def do_controls(self, data):
        start = time.time()
        p, rq = self.adviser.advice(data)
        duration = time.time() - start
        return {'bid_round_mem': data['auction_round_mem'],
                'bid_p_mem': p, 'bid_ranges_mem': rq,
                "bid_duration": duration}

class Guest(Entity):
    def __init__(self, name, adviser, base_mem, load_func, load_interval):
        Entity.__init__(self)
        self.name = name
        self.properties['name'] = name
        self.statistics.append({})
        self.adviser = adviser
        self.policy_mem = PolicyDummy(self.adviser)
        self.properties['base_mem'] = base_mem
        self.SetVar('base_mem', base_mem)
        self.statistics[-1]['mem_available'] = base_mem
        self.statistics[-1]['mem_free'] = base_mem
        self.load_func = np.vectorize(load_func)
        self.load_interval = load_interval
        # DictDefaultNone is an empty dict with default for missing keys
        #probably done for performance
        self.data = DictDefaultNone()
        self.properties['comm_mem'] = DummyCommunicator(self.data, self, self.policy)

    def interrogate(self):
        return self

    def collect(self):
        return self.statistics[-1]

def show_guests_graph(guests, mem_range, loads):
    import pylab as pl
    for i in range(len(guests)):
        perf = []
        for mem in mem_range:
            perf.append(guests[i].adviser.profiler.interpolate([loads[i], mem], 'perf'))
        pl.plot(mem_range, perf, label = 'guest %i, load: %i' % (i, loads[i]))

    pl.grid(True)
    pl.legend(loc = 'best')
    pl.xlabel('memory [MB]')
    pl.ylabel('Throughput [req/sec]')

class AuctionRunner(threading.Thread):
    def __init__(self, _auctioneer, _host, _guests, _auctions_per_round, _auction_round, _Tauction, _mem_available):
        threading.Thread.__init__(self, name = "AuctionRunner")
        self.auctioneer = _auctioneer
        self.host = _host
        self.guests = _guests
        self.auctions_per_round = _auctions_per_round
        self.auction_round = _auction_round
        self.mem_available = _mem_available
        self.Tauction = _Tauction

    def run(self):
        try:
            for i in xrange(self.auctions_per_round):
                self.auction_subround = i
                # preparations
                for g in self.guests:
                    # the time equals [current_auction_round * time_for_each_auction]
                    g.statistics[-1]['load'] = g.load_func((self.auctions_per_round * self.auction_round + i) * self.Tauction)
                # the amount of resources available doesn't change between rounds
                self.host.statistics[-1]['mem_available'] = self.mem_available  

                # run the auction
                print 'starting auction'
                self.auctioneer.auction(self.host, self.guests)

                # fake memory ballooning
                for g in self.guests:
                    # control_mem - how much memory the guest got in the auction
                    g.statistics[-1]['mem_available'] = g.GetControl('control_mem')
                
                self.collect_results()
        except Exception as ex:
            print "exception in notifier: %s", ex
            import traceback
            traceback.print_exc()
                
    def collect_results(self):
        global results
        i = self.auction_subround
        host = self.host
        cur_auction_round = self.auction_round * self.auctions_per_round + i

        results[HOST]['time'][cur_auction_round] = cur_auction_round * self.Tauction
        results[HOST]['auction_round'][cur_auction_round] = cur_auction_round
        results[HOST]['auction_round_mem'][cur_auction_round] = host.GetVar('auction_round_mem')
        results[HOST]['auction_mem'][cur_auction_round] = self.mem_available
        results[HOST]['ties_mem'][cur_auction_round] = any([g.GetVar('tie_winner_mem') for g in self.guests])
        results[HOST]['p_in_min_mem'][cur_auction_round] = host.GetVar('p_in_min_mem')
        results[HOST]['p_out_max_mem'][cur_auction_round] = host.GetVar('p_out_max_mem')
        results[HOST]['auction_time'][cur_auction_round] = host.GetVar('auction_time_mem')

        for g in self.guests:
            load = g.statistics[-1]['load']
            mem = g.GetControl('control_mem')
            perf = float(g.adviser.profiler.interpolate((load, mem)))
            rev = g.adviser.V_func(perf)

            results[g.name]['base_mem'][cur_auction_round] = g.GetVar('base_mem')
            results[g.name]['control_mem'][cur_auction_round] = mem
            results[g.name]['load'][cur_auction_round] = load
            results[g.name]['perf'][cur_auction_round] = perf
            results[g.name]['rev'][cur_auction_round] = rev
            results[g.name]['auction_round'][cur_auction_round] = cur_auction_round
            results[g.name]['time'][cur_auction_round] = cur_auction_round * self.Tauction
            results[g.name]['adviser_time'][cur_auction_round] = g.data['bid_duration']
            bill_mem = g.GetControl('control_bill_mem')
            profit_mem = rev - bill_mem
            results[g.name]['p_mem'][cur_auction_round] = g.GetVar('p_mem')
            results[g.name]['r_mem'][cur_auction_round] = g.GetVar('rq_mem')[-1][0]
            results[g.name]['q_mem'][cur_auction_round] = g.GetVar('rq_mem')[-1][1]
            # TODO fix in case history is empty
            results[g.name]['p_target_mem'][cur_auction_round] = g.adviser.p_tar_hist_mem.max()
            results[g.name]['last_won_p'][cur_auction_round] = float(bill_mem) / mem if mem != 0 else 0
            results[g.name]['control_bill_mem'][cur_auction_round] = bill_mem
            results[g.name]['profit_mem'][cur_auction_round] = profit_mem
            results[g.name]['mem_available'][cur_auction_round] = self.mem_available

# TODO: replace rounds with time
def simulate(guests, mem_available, rounds, Tauction_mem, show=False,
             plot_costs=True):
    """
    @param rounds = number of auction runs
    @param dt = granularity of time(minimum between Tauction_mem and Tauction_mem
    """
    global results
    results = {}
    dt = Tauction_mem
    round_len = Tauction_mem
    assert (round_len % Tauction_mem == 0)
    # the number of memory rounds that be accomplished during a single round of all auctions
    rounds_mem = rounds * (round_len / Tauction_mem)
    host = Entity()
    config = ConfigParser.SafeConfigParser()
    config.add_section("host")
    config.set('host', 'mem-bid-collection-interval', '0')
    config.set('host', 'mem-calculation-interval', '0')
    config.set('host', 'mem-notification-interval', '0')
    config.set('host', 'host-mem', '0')
    host.properties['config'] = config
    host.statistics.append({})
    host.statistics[-1]['mem_free'] = 0
    host.statistics[-1]['mem_available'] = mem_available
    auctioneer_mem = Auctioneer()
    auctioneer_mem.wait_for_time = lambda seconds_to_wait, start: 0
    #props means properties
    props = ['auction_mem', 'auction_round', 'auction_round_mem', 'time',
             'ties_mem', 'p_in_min_mem', 'p_out_max_mem', 'auction_time']
    #initialize results dictionary for host
    results[HOST] = {}
    for k in props:
        results[HOST][k] = [0] * rounds_mem

    props = ['base_mem', 'p_mem', 'r_mem', 'q_mem', 'control_mem',
             'control_bill_mem', 'load', 'perf', 'rev', 'profit_mem',
             'time', 'auction_round', 'last_won_p', 'p_target_mem',
             'adviser_time', 'rev_min', 'perf_min', 'mem_available']
    # initialize results dictionary for guests
    for guest in guests:
        guest.Control('control_mem', guest.Prop("base_mem"))
        results[guest.name] = {}
        for k in props:
            results[guest.name][k] = [0] * rounds_mem

    for t in range(rounds):  # ROUNDS LOOP
        sys.stdout.write(".")
        sys.stdout.flush()

        ar_mem = AuctionRunner(auctioneer_mem, host, guests,
                               (round_len / Tauction_mem), t, Tauction_mem,
                               mem_available)
        ar_mem.start()
        ar_mem.join()

    sys.stdout.write("\n")
    sys.stdout.flush()
    # END ROUNDS LOOP

    # set the performance and valuation according to the minimal value in the
    # load interval.
    for g in guests:
        for t in range(rounds_mem):
            imin = (t * dt) / g.load_interval * (g.load_interval / dt)
            imax = imin + (g.load_interval / dt)
            for g in guests:
                results[g.name]['perf_min'][t] = min(results[g.name]['perf'][imin:imax - 1])
                results[g.name]['rev_min'][t] = min(results[g.name]['rev'][imin:imax - 1])

    results[HOST]["costs_mem"] = costs_map_mem

    return results
