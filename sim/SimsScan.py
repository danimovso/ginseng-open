'''
Created on Dec 25, 2011

@author: eyal
'''
import os
from simulate import simulate
from sim.simulate import Guest
from mom.Adviser import get_adviser

def confirm_moc_path():
    if all((d != "moc" for d in os.getcwd().split("/"))):
        raise OSError("Bad startup location, must be in moc dir")
    while os.path.split(os.getcwd())[1] != "moc":
        os.chdir("..")
    return os.getcwd()

def guests_generator(n, adviser_args, load_funcs, load_interval):
    return lambda: [Guest(str(i), get_adviser(**adviser_args),
                          base_mem = adviser_args["base_mem"],
                          base_bw     = adviser_args["base_bw"],
                          load_func = load_funcs[i],
                          load_interval = load_interval)
            for i in range(n)]

class SimsScan:
    def __init__(self, name):
        self._name = name
        self.__dir = None

    def get_test_dir(self):
        moc_dir = confirm_moc_path()
        out_dir = os.path.join(os.path.split(moc_dir)[0], "moc-output", "sim")
        if self.__dir is None:
            dr = os.path.join(out_dir, self._name + "-")
            i = 0
            while True:
                if not os.path.exists(dr + str(i)):
                    dr += str(i)
                    break
                i += 1
            os.mkdir(dr)
            self.__dir = dr

        return self.__dir

    def draw_n_save(self, name, size = (10, 16)):
        from etc.plots import draw_n_save
        draw_n_save(os.path.join(self.get_test_dir(), name + ".png"), size)

    def save_to_file(self, obj, name):
        fd = open(os.path.join(self.get_test_dir(), name), "w")
        fd.write(str(obj))
        fd.close()

    def show_perf_func(self, func, tot_mem, d_mem):
        import pylab as pl
        pl.clf()
        pl.hold(True)
        for i in range(len(func)):
            x = []
            y = []
            for mem in range(0, tot_mem + d_mem, d_mem):
                x.append(mem)
                y.append(func[i](mem))

            pl.plot(x, y, label = 'guest %i' % i)
        pl.legend(loc = 'lower right')
        pl.xlabel("Memory")
        pl.ylabel("Performance")
        pl.grid(True)
        pl.hold(False)
        pl.show()

    def run(self, guests_generator,
            min_mem, max_mem,
            rounds, Tauction_mem, Tauction_bw,
            ocs, alphas,
            host_mem, mem_diff,
            p0, 
            do_optimized = True, static_optimization = False,
            show = False, auction_mem = 0, auction_bw = 0):
	"""
	@param guests_generator - function that returns a list of Guest instances
	@param min_mem - total of base memories (of all guests) ?
	@param max_mem - total memory (in MB?) when all the guests reach saturation
	@param rounds - how many auction rounds
	@param Tauction_mem - time for each memory auction
   @param Tauction_bw - time for each bandwidth auction
	@param ocs - over commitment coefficients list
	a simulation of a few rounds will be run for each of them
	@param alphas - reclaim factor coefficients list.alpha in (0,1]. keep it 1. DEPRECATED.
	@param host_mem - memory reserved for the host
   @param host_bw - bandwidth reserved for the host
	@param mem_diff - difference between the memory reported to the guest 
	and the actual memory the guest sees
	@param p0 -(float) affine maximizer coefficient #TODO: should be a list of p0's
	@param do_optimized - maximize the social welfare under the total memory constraint.
	@param static_optimization - should the optimization be done once (as opposed to once per auction round)
	@param show - looks DEPRECATED. maybe remove?
	@param auction_mem - ?
   @param auction_bw - TODO document
        """
        guests = guests_generator()
        n = len(guests)

        res = []

        for oc in ocs:  # MEMORY LOOP
            res.append([])

#            memory = int(min_mem + (max_mem - min_mem) / oc)
            wasted_mem = host_mem + n * mem_diff
            memory = int(float(wasted_mem + max_mem) / oc) - wasted_mem
            if auction_mem != 0:
                assert auction_mem > 0, "auction_mem must be non-negative"
                memory = int(auction_mem) + min_mem
            print "memory is: %i" % auction_mem
            if memory < min_mem:
                print "memory %i, minimum %i" % (memory, min_mem)
                return []

            if do_optimized:
                from sim_optimized import sim_optimize
                # the optimization is maximizing the social welfare
                # under the constraint of not giving more memory than there is
                opt_sim_res = sim_optimize(guests = guests,
                                           rounds = rounds,
                                           total_mem = memory,
                                           min_vm_mem = 600,
                                           max_vm_mem = 2600,
                                           d_mem = 50,
                                           dt = Tauction if not static_optimization else 0
                                           )
                self.save_to_file(opt_sim_res, "opt-oc-%.2f" % oc)
            #TODO: deprecated. change this to affine maximizer or remove!
            for a in alphas:  # ALPHA LOOP

                print "step: oc: %.2f, alpha: %.2f" % (oc, a)
                guests = guests_generator()

                sim_res = simulate(guests,
                                   alpha = a,
                                   p0 = p0,
                                   mem_available = memory,
                                   bw_available  = bw,
                                   rounds = rounds,
                                   dt = Tauction)

                # add optimization results
                if do_optimized:
                    for name, opt_res in opt_sim_res.items():
                        for key, val in opt_res.items():
                            sim_res[name][key] = val

                self.save_to_file(sim_res, "sim-oc-%.2f-a-%.2f" % (oc, a))

                res[-1].append(sim_res)

            # END ALPHA LOOP
        # END MEMORY LOOP

        return res

