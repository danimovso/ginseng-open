from exp.core.Loads import LoadConstant, \
    LoadFunction, SeveralStepsValuationSwitcher
from mom.Policy.Auctioneer import Auctioneer
from mom.Advisers.AdviserProfitEstimator import AdviserProfitEstimator
from mom.Entity import Entity
from mom.Profiler import fromxml
from mom.LogUtils import LogUtils
from mom.Adviser import p_eps
from collections import defaultdict, OrderedDict
import ConfigParser
from mom.Plotter import HOST
import logging
from matplotlib import use

use('cairo')
import pylab as pl

verbosity = "debug"
rounds = (50, 2, 50, 50)
auction_intervals = [3, 1, 8]
step = sum(auction_intervals)
p0s = map(lambda x: x / 10.0, range(11))
# p0s = [0, 1]
# p0s = [0]
oc = 2

LogUtils(verbosity)
logger = logging.getLogger("Simulation")


class DummyPlotter():
    def __init__(self, name, p0, verbosity="info"):
        self.name = name
        self.p0 = p0
        LogUtils(verbosity)
        self.logger = logging.getLogger("Plotter-%s-%.02f" % (name, p0))
        self.data = []

    def plot(self, data):
        self.logger.debug(str(data))
        self.data.append(dict(data))

    def __getitem__(self, item):
        return self.data[item]


class SomeStepsFunction(LoadFunction):
    def __init__(self, times):
        test_values = lambda l: all(map(lambda t: t > 0, l))
        if len(times) == 0:
            raise ValueError("Empty input")
        elif not test_values(times):
            raise ValueError("All times must be positive")
        else:
            self.times = list(times)

    def __call__(self, t):
        t %= sum(self.times)
        current = 0
        for i in range(len(self.times)):
            current += self.times[i]
            if current >= t:
                return i
        return len(self.times) - 1

    @property
    def __info__(self):
        return {"type": self.__class__.__name__,
                "times": self.times}


class Configuration(object):
    def __init__(self, base_mem, saturation_mem, profiler_file, profiler_entry,
                 valuation_funcs, load, load_interval, val_switch_func=None,
                 use_flags=None):
        self.base_mem = base_mem
        self.saturation_mem = saturation_mem
        self.profiler_file = profiler_file
        self.profiler_entry = profiler_entry
        self.load = load
        self.load_interval = load_interval
        self.valuation_funcs = valuation_funcs
        if val_switch_func is None:
            self.val_switch_func = lambda t: 0
        else:
            self.val_switch_func = val_switch_func
        self.adviser_params = (fromxml(profiler_file, profiler_entry),
                               self.base_mem, self.profiler_entry, 10,
                               self.valuation_funcs)


configs = (
    Configuration(  # psql
            base_mem=3500 + 170,
            saturation_mem=5400,  # according to the profiler
            profiler_file="doc/profiler-postgres-tapuz25.xml",
            profiler_entry="tps_with_connections_time",
            load=LoadConstant(50),
            load_interval=10 * 60,
            valuation_funcs=lambda x: 2.5 * x,
    ),
    Configuration(  # MCD
            base_mem=600 + 53,
            saturation_mem=2500,
            profiler_file="doc/profiler-memcached-inside-spare-50-win-500k-tapuz25.xml",
            profiler_entry="hits_rate",
            load=LoadConstant(8),
            load_interval=1,  # this doesn't really matter...
            valuation_funcs=[lambda x: x, lambda x: 5 * x, lambda x: x,
                             lambda x: 5 * x],
            val_switch_func=SeveralStepsValuationSwitcher(
                    times=map(lambda x: 12 * x, rounds)),
    ),
)


def setup_guests(config, p0, plotters):
    guests = []
    auction_mem = 0
    for i, c in enumerate(config):
        name = 'vm-%i' % (i + 1)
        adv = AdviserProfitEstimator(*c.adviser_params)
        g = Entity()
        g._set_property('name', name)
        g._set_property('base_mem', c.base_mem)
        g._set_property('adviser', adv)
        g._set_property('load', c.load)
        g._set_property('valuation_funcs', c.valuation_funcs)
        g._set_property('val_switch_func', c.val_switch_func)
        g.SetVar('current_valuation', c.val_switch_func(0))
        guests.append(g)
        auction_mem += c.saturation_mem - c.base_mem
        plotters[name] = DummyPlotter(name, p0, verbosity=verbosity)

    return guests, auction_mem


def setup_host(auction_mem, p0):
    host = Entity()
    host._set_property('config', ConfigParser.SafeConfigParser())
    host.Prop('config').add_section('host')
    host.Prop('config').set('host', 'p0-percent', str(p0))
    host.Prop('config').set('host', 'mem-bid-collection-interval',
                            str(auction_intervals[0]))
    host.Prop('config').set('host', 'mem-calculation-interval',
                            str(auction_intervals[1]))
    host.Prop('config').set('host', 'mem-notification-interval',
                            str(auction_intervals[2]))
    host.Prop('config').set('host', 'warmup_rounds', str(-1))
    host.SetVar('auction_round_mem', 0)
    host.SetVar('auction_mem', auction_mem)
    return host


def var_from_entity(data, src, key):
    data[key] = src.GetVar(key)


def prop_from_entity(data, src, key):
    data[key] = src.Prop(key)


def get_bids(t, host, guests):
    data = {}
    var_from_entity(data, host, 'auction_round_mem')
    var_from_entity(data, host, 'auction_mem')
    for g in guests:
        prop_from_entity(data, g, 'base_mem')
        try:
            var_from_entity(data, g, 'not_mem')
            var_from_entity(data, g, 'not_bill_mem')
            var_from_entity(data, g, 'not_won_p_mem')
            var_from_entity(data, g, 'not_tie_mem')
            var_from_entity(data, g, 'not_round_mem')
            var_from_entity(data, g, 'p_in_min_mem')
            var_from_entity(data, g, 'p_out_max_mem')
        except KeyError as e:
            if data['auction_round_mem'] > 0:
                logger.error(
                        "No previous info and we're not on the first round")
                raise
        data['load'] = g.Prop('load')(t)
        new_valuation = g.Prop('val_switch_func')(t)
        if new_valuation != g.GetVar('current_valuation'):
            g.Prop('adviser').next_valuation()
            g.SetVar('current_valuation', new_valuation)
        p, rqs = g.Prop('adviser').advice(data)
        g.SetVar('bid_round_mem', data['auction_round_mem'])
        g.SetVar('bid_p_mem', p)
        g.SetVar('bid_ranges_mem', rqs)


def next_round(host, guests):
    prev_round = host.GetVar('auction_round_mem')
    round_sw = 0
    for g in guests:
        mem = g.GetControl('control_mem')
        extra = mem - g.Prop("base_mem")
        bill = g.GetControl('control_bill_mem')
        g.SetVar('not_mem', mem)
        g.SetVar('not_bill_mem', bill)
        g.SetVar('not_won_p_mem', float(bill) / extra if extra != 0 else 0)
        g.SetVar('not_tie_mem', g.GetVar('tie_winner_mem'))
        g.SetVar('not_round_mem', prev_round)
        g.SetVar('p_in_min_mem', host.GetVar('p_in_min_mem'))
        g.SetVar('p_out_max_mem', host.GetVar('p_out_max_mem'))
        round_sw += extra * g.GetVar('bid_p_mem')
    host.SetVar('auction_round_mem', prev_round + 1)
    return round_sw


def plot_round(t, host, guests, plotters):
    host_items = ('p_in_min_mem', 'p_out_max_mem', 'reduction_low_mem',
                  'reduction_high_mem', 'auction_mem')
    guest_items = ('bid_ranges_mem', 'bid_p_mem', 'mem_extra',
                   'mem_extra_alternate')
    guest_controls = ('control_bill_mem',)
    data = {'time': t}
    for p in host_items:
        var_from_entity(data, host, p)
    plotters[HOST].plot(data)
    data = {'time': t}
    for g in guests:
        name = g.Prop('name')
        for p in guest_items:
            data[p] = g.GetVar(p)
        for p in guest_controls:
            data[p] = g.GetControl(p)
        extra = data['mem_extra']
        data['bill_estimate_mem'] = g.Prop('adviser') \
            .get_last_bill_estimate_mem(extra)
        data['profit_estimate_mem'] = g.Prop('adviser') \
            .get_last_profit_estimate_mem(extra)
        data['valuation'] = g.Prop('adviser') \
            .get_last_profit_estimate_mem(extra)
        plotters[name].plot(data)


def reset_history(host, guests):
    for g in guests:
        g.Prop('adviser').reset_mem()
    host.SetVar('auction_round_mem', 0)


def get_data(data, name, x_field, y_field):
    x = map(lambda p: p[x_field], data[name])
    y = map(lambda p: p[y_field], data[name])
    for i in range(len(y)):
        try:
            if abs(y[i]) < p_eps:
                y[i] = 0
        except TypeError:
            break
    return x, y


def plot_host(data, fname):
    size = (15, 8)
    host_fields = ('p_in_min_mem', 'p_out_max_mem', 'reduction_low_mem',
                   'reduction_high_mem')
    host_design = defaultdict(dict)
    host_design.update({
        'p_in_min_mem': {'marker': '^'},
        'p_out_max_mem': {'marker': 'v'},
    })
    pl.figure(figsize=size)
    for f in host_fields:
        x, y = get_data(data, HOST, 'time', f)
        pl.plot(x, y, **host_design[f])
    pl.legend(host_fields)
    pl.savefig(fname % 'p-vals')
    pl.close(pl.gcf())
    lines = []
    labels = []
    fig = pl.figure(figsize=size)
    ax = pl.gca()
    ax2 = ax.twinx()
    x1, y1 = get_data(data, HOST, 'time', 'auction_mem')
    lines.extend(ax.plot(x1, y1, color='b'))
    labels.append('auction_mem')
    ax.set_ylim([0, max(y1) * 1.01])
    x2, y2 = get_data(data, HOST, 'time', 'time')
    y2 = [0] * len(x1)
    for g in filter(lambda k: k != HOST, data.keys()):
        for i, e in enumerate(data[g]):
            y2[i] += e['mem_extra']
    lines.extend(ax.plot(x2, y2, color='g'))
    labels.append('mem_extra')
    y3 = map(lambda u, t: float(u) / t, y2, y1)
    logger.debug("utlization: %s", y3)
    lines.extend(ax2.plot(x1, y3, linestyle='--', color='r'))
    labels.append('utilization')
    ax2.set_ylim([0, 1.01])
    pl.legend(lines, labels, loc='best')
    pl.savefig(fname % 'mem')
    pl.close(fig)


def plot_auctions(data, guests, n, fname, size):
    guest_fields = OrderedDict((('bid_p_mem', 0), ('control_bill_mem', 1),
                                ('bill_estimate_mem', 1), ('mem_extra', 2),
                                ('mem_extra_alternate', 2)))
    ranges = OrderedDict((('r', 2), ('q', 2)))
    guest_design = defaultdict(dict)
    guest_design.update({
        'r': {'linestyle': 'none', 'marker': '^', 'color': 'g', 'alpha': 0.5},
        'q': {'linestyle': 'none', 'marker': 'v', 'color': 'y', 'alpha': 0.5},
        'bid_p_mem': {'color': 'b', 'alpha': 0.5},
        'mem_extra': {'color': 'r', 'alpha': 0.5},
        'mem_extra_alternate': {'color': 'c', 'alpha': 0.5, 'linestyle': '--'},
        'control_bill_mem': {'color': 'g', 'alpha': 0.5},
        'bill_estimate_mem': {'alpha': 0.5, 'color': 'k', 'marker': 'x'},
    })
    fig = pl.figure(figsize=size)
    in_single = max(max(guest_fields.values()), max(ranges.values())) + 1
    subaxes = [None] * in_single * n
    subaxe_idx = lambda i, j: i * in_single + j
    gname = [None] * in_single * n
    max_y = [1] * in_single
    ylabels = map(lambda i: list(), range(in_single))
    lines = []
    labels = []
    for i, g in enumerate(guests):
        pos = [0.05, (float(i) / n) + 0.02, 0.8, 1. / (n + 0.15)]
        ax = fig.add_axes(pos)
        ax.grid()
        subaxes[subaxe_idx(i, 0)] = ax
        gname[subaxe_idx(i, 0)] = g
        for j in range(1, in_single):
            subaxes[subaxe_idx(i, j)] = ax.twinx()
            gname[subaxe_idx(i, j)] = g
        for f, group in guest_fields.iteritems():
            x, y = get_data(data, g, 'time', f)
            max_y[group] = max(max_y[group], max(y))
            line = subaxes[subaxe_idx(i, group)].plot(x, y, **guest_design[f])
            if i == 0:
                lines.extend(line)
                labels.append(f)
                ylabels[group].append(f)
        x, rqs = get_data(data, g, 'time', 'bid_ranges_mem')
        x_rq = []
        y_r = []
        y_q = []
        for t, rq in zip(x, rqs):
            new_rs = map(lambda t: t[0], rq)
            new_qs = map(lambda t: t[1], rq)
            assert len(new_rs) == len(new_qs)
            x_rq.extend([t] * len(new_rs))
            y_r.extend(new_rs)
            y_q.extend(new_qs)
        line = subaxes[subaxe_idx(i, ranges['r'])].plot(x_rq, y_r,
                                                        **guest_design['r'])
        max_y[ranges['r']] = max(max_y[ranges['r']], max(y_r))
        if i == 0:
            lines.extend(line)
            labels.append('r')
            ylabels[ranges['r']].append('r')
        line = subaxes[subaxe_idx(i, ranges['q'])].plot(x_rq, y_q,
                                                        **guest_design['q'])
        max_y[ranges['q']] = max(max_y[ranges['q']], max(y_q))
        if i == 0:
            lines.extend(line)
            labels.append('q')
            ylabels[ranges['q']].append('q')
    ylabels = map(lambda ls: "/".join(ls), ylabels)
    for j, subaxe in enumerate(subaxes):
        ylabel = ylabels[j % in_single]
        if j % in_single == 0:
            ylabel = ": ".join([gname[j], ylabel])
        subaxe.set_ylim([0, max_y[j % in_single] * 1.01])
        subaxe.set_ylabel(ylabel)
        subaxe.spines['right'].set_position(
                ('outward', 50 * ((j - 1) % in_single)))
    pl.gca().legend(lines, labels, loc='best')
    fig.savefig(fname)
    pl.close(fig)


def plot_profits(data, guests, n, fname, size):
    labels = ('valuation', 'estimated_profit', 'profit', 'bill')
    ylabel = "/".join(labels)
    fig = pl.figure(figsize=size)
    limits = [0, 0]
    for i, g in enumerate(guests):
        pos = [0.05, (float(i) / n) + 0.02, 0.8, 1. / (n + 0.15)]
        ax = fig.add_axes(pos)
        ax.grid()
        ax.set_ylabel(": ".join([g, ylabel]))
        x, y = get_data(data, g, 'time', 'valuation')
        ax.plot(x, y)
        limits = [min(limits[0], min(y)), max(limits[1], max(y))]
        x, y = get_data(data, g, 'time', 'profit_estimate_mem')
        ax.plot(x, y)
        limits = [min(limits[0], min(y)), max(limits[1], max(y))]
        x, y = get_data(data, g, 'time', 'valuation')
        x2, y2 = get_data(data, g, 'time', 'control_bill_mem')
        y = map(lambda v, b: v - b, y, y2)
        ax.plot(x, y, linestyle='--')
        ax.plot(x2, y2, linestyle=':')
        limits = [min(limits[0], min(y)), max(limits[1], max(y))]
        ax.set_ylim([limits[0], limits[1] * 1.01])
    pl.gca().legend(labels, loc='best')
    fig.savefig(fname)
    pl.close(fig)


def plot_guests(data, fname):
    guests = sorted(filter(lambda k: k != HOST, data.keys()))
    n = len(guests)
    size = (15, 5 * n)
    plot_auctions(data, guests, n, fname % 'bids', size)
    plot_profits(data, guests, n, fname % 'profits', size)


def plot(plotters):
    for p0, data in plotters.iteritems():
        logger.info("plotting %.02f", p0)
        plot_host(data, 'host-%%s-%.02f.png' % p0)
        plot_guests(data, 'guest-%%s-%.02f.png' % p0)


def main():
    assert (type(oc) == int) and (oc > 0), "oc factor is not natural"
    plotters = {}
    for p0 in p0s:
        plotters[p0] = {HOST: DummyPlotter(HOST, p0, verbosity=verbosity)}
        logger.info("p0=%.02f", p0)
        sw = 0
        guests, auction_mem = setup_guests(configs, p0, plotters[p0])
        auction_mem /= oc
        host = setup_host(2000, p0)
        t = 0
        a = Auctioneer(verbosity="debug", simulate=True)
        for i in range(sum(rounds)):
            get_bids(t, host, guests)
            a.auction(host, guests)
            logger.debug("plotting round %i p0=%.02f", i, p0)
            plot_round(t, host, guests, plotters[p0])
            sw += next_round(host, guests)
            t += step
        logger.info("SW=%.02f", sw)
    plot(plotters)


if __name__ == '__main__':
    main()
