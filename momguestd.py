#! /usr/bin/env python
# Memory Overcommitment Manager
# Copyright (C) 2010 Adam Litke, IBM Corporation
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

import signal
import logging
import ConfigParser
from mom.Comm.GuestServer import GuestServer
from optparse import OptionParser
from mom.HostMonitor import HostMonitor
from mom.GuestPolicyEngine import GuestPolicyEngine
from mom.LogUtils import LogUtils


class momguestd():
    def __init__(self, *args):
        # parse command line args
        args = list(args) if len(args) != 0 else None
        cmdline = get_cmdline_options()
        options, args = cmdline.parse_args(args)
        config = read_config(options.config_file, options)
        self.config = config

        # set logging
        logfile = config.get('logging', 'log')
        LogUtils(config.get('logging', 'verbosity'), logfile == "stdio")
        if logfile != "stdio":
            LogUtils.start_file_logging(logfile,
                        config.get('logging', 'verbosity'),
                        config.getint('logging', 'max-bytes'),
                        config.getint('logging', 'backup-count'))
        self.logger = logging.getLogger("momguestd")

        # creating the server
        if self.is_memory_auction_available():
            monitor = HostMonitor(config) # for collecting data, not running as a thread
            policy = GuestPolicyEngine(config)
            ports = {'mem' : GuestServer.port_mem}
            self.server = GuestServer(config, monitor, policy, ports['mem']) # server to communicate with host
            self.logger.warn("Running memory auction")

    def is_memory_auction_available(self):
        return self.config.has_section('mem')

    def start(self):
        self.logger.info("Starting Server")
        self.server.serve_forever()
        try:
            self.server.join()
        finally:
            self.server.shutdown()
            self.logger.info("Ended")

    def terminate(self):
        self.server.shutdown()


def get_cmdline_options():
    # command line argument parsing
    cmdline = OptionParser()
    cmdline.add_option('-c', '--config-file', dest='config_file',
                       help='Load configuration from FILE', metavar='FILE',
                       default='/etc/mom.conf')
    cmdline.add_option('-l', '--log', dest='log', metavar='TARGET',
                       help='Set the log to TARGET (stdout, or <file>')
    cmdline.add_option('-v', '--verbose', dest='verbosity', metavar='LEVEL',
                       help='Set logging verbosity to LEVEL (0-4)')
    cmdline.add_option('-p', '--program', dest='program', metavar='TYPE',
                       help='Set program name')
    cmdline.add_option('-a', '--adviser', dest='adviser', metavar='TYPE',
                       help='Set adviser type')
    return cmdline


def read_config(fname, options):
    config = ConfigParser.SafeConfigParser()
    # Set defaults
    config.add_section('main')
    config.set('main', 'host', '')
    config.set('main', 'messages', 'MessageStats')
    config.add_section('connection')
    config.set('connection', 'port', '2187')
    config.set('connection', 'timeout', '10')
    config.add_section('props')
    config.set('props', 'min_free', '0.20')  # These two variables
    config.set('props', 'max_free', '0.50')  #  are currently unused
    config.add_section('logging')
    config.set('logging', 'log', 'stdio')
    config.set('logging', 'verbosity', 'debug')
    config.set('logging', 'max-bytes', '2097152')
    config.set('logging', 'backup-count', '5')
    config.read(fname)
    # Process command line overrides    
    if options.log is not None:
        config.set('logging', 'log', options.log)
    if options.verbosity is not None:
        config.set('logging', 'verbosity', options.verbosity)
    if options.program is not None:
        config.set('main', 'program', options.program)
    if options.adviser is not None:
        config.set('main', 'adviser', options.adviser)

    return config

if __name__ == "__main__":
    m = momguestd()

    def signal_quit(signum= -1, frame=None):
        m.logger.info("Received signal %i, shutting down server.", signum)
        m.terminate()

    signal.signal(signal.SIGINT, signal_quit)
    signal.signal(signal.SIGTERM, signal_quit)
    # if we don't intend to run the auction we should just sleep
    m.start()


