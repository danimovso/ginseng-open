#! /usr/bin/env python
'''
Created on Oct 21, 2011

@author: eyal
'''
from mom.LogUtils import LogUtils
from exp.prog.Programs import get_guest_program
from mom.Collectors.HostMemory import HostMemory
from exp.deprecated.QuitServer import QuitServer
import time
import signal
import sys
from mom.Collectors.GuestCpu import GuestCpu
import threading
from numpy import average
from Queue import Queue
from ConfigParser import ConfigParser
import logging
from mom.Comm.GuestServer import GuestServer

def collector(collectors, stop_event, output, collect_interval = 1):
    data = {}

    while not stop_event.is_set(): # wait for stop request
        for c in collectors:
            res = c.collect()
            for key, val in res.iteritems():
                data.setdefault(key, []).append(val)
            time.sleep(collect_interval)

    ret = {}
    for key, lst in data.iteritems():
        ret[key] = {'avg' : average(lst) if len(lst) > 0 else 0.0,
                    'sum' : sum(lst), 'len' : len(lst)}
    output.put(ret)

class RemoteTestbedServer(QuitServer):

    port = GuestServer.port#1926

    def __init__(self, prog):
        self.prog = prog
        config = ConfigParser()
        config.add_section("main")
        config.set("main", "program", prog.name)

        self.host_mem = HostMemory(None)
        self.host_cpu = GuestCpu({'config': config})

        self.output = Queue()
        self.stop_event = threading.Event()
        self.memory = 0

        QuitServer.__init__(self, self.port, prog.terminate,
                            timeout = None, name = "RemoteTestbedServer")

    def process(self, msg):

        if msg.startswith("get-mem"):
            return self.host_mem.collect()['mem_available']

        # collect-end when testbed starts a round
        elif msg.startswith("collect-start"):
            self.stop_event.clear()
            self.trd = threading.Thread(target = collector,
                                        args = [(self.host_mem, self.host_cpu),
                                              self.stop_event, self.output])
            self.trd.start()
            return "started"

        # collect-end when testbed finishes a round and wants to know the
        # results.
        elif msg.startswith("collect-end"):
            self.stop_event.set()
            self.trd.join()
            return self.output.get()

        # total-mem message is when host hint the client before he is
        # actually changes the memory
        elif msg.startswith("total-mem"):
            try:
                mem = int(msg.split(":")[-1])
            except Exception:
                self.logger.error("While parsing memory: %s", msg)
                mem = 0
            self.memory = mem
            return "continue" # make client start counting seconds of load

        # MessageTargetMemory message is when DynamicMemoryController requests
        # hint on the new memory allocation.
        elif msg.startswith("MessageTargetMemory"):
            return {"target_mem": self.memory}

        else:
            return QuitServer.process(self, msg)

def remote_testbed(verbosity, *prog_args):
    LogUtils(verbosity)
    logger = logging.getLogger("RemoteTestbed")

    prog = get_guest_program(args = prog_args)

    def signal_quit(signum, frame):
        prog.terminate()

    try:
        signal.signal(signal.SIGINT, signal_quit)
        signal.signal(signal.SIGTERM, signal_quit)
    except ValueError:
        pass # when not main thread..

    server = RemoteTestbedServer(prog)
    try:
        server.serve_forever()
        logger.info("Starting %s", prog.name)
        out, err = prog.start()
        logger.info("Program ended")
        logger.info("=== stdout:\n%s\n=== stderr:\n%s", out, err)
    finally:
        server.shutdown()

if __name__ == "__main__":
    """
    Run with prog_args as should expect in get_guest_program
    For test mode run with test as an argument
    """
    remote_testbed(*sys.argv[1:])
