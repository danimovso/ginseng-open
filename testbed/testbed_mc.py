#! /usr/bin/env python
'''
Created on Oct 21, 2011
@author: eyal
'''

import time
from exp.prog.Programs import Guest_MemoryConsumer, Host_MemoryConsumer
from testbed.core.Testbed import Testbed
from exp.prog.BenchmarkPrograms import MemoryConsumerBenchmark

def testbed():
    n = 4

    prog_args = Guest_MemoryConsumer(
                    spare_mem = 50,
                    saturation_mem = 2000,
                    update_interval = 0.5,
                    sleep_after_write = 0.1,
                    ).command_args

    bm_args = Host_MemoryConsumer().command_args

    start_time = time.time()

    Testbed(vm_names = ["vm-%i" % i for i in range(1, n + 1)],
            prog_args = prog_args,
            bm_args = bm_args,
            mem_range = range(600, 2201, 400),
            load_range = [1, 8, 10], #[5, 7], #range(1, 11),  #[1, 4, 6, 8, 10],
            entries = MemoryConsumerBenchmark.getFields(),
            samples_num = 5,
            repetitions = 1,
            length = 60,
            mem_scan_up_and_down = True,
            set_notify_sleep = 5,
            verbosity = "info").start()

    print "testbed duration: ", (time.time() - start_time)

if __name__ == "__main__":
    testbed()
