'''
Created on May 2, 2013

@author: eyal
'''
import subprocess
import os
from mom.Plotter import Plotter, PERF, POL, MON, HOST, INFO
import numpy as np
from exp.core import Loads
import sys
from etc.plots import multifunction
import numpy
from cmath import log
from functools import partial

def sync_files(remote, local, excludes, remote_server = None):
    '''
    remote_server includes the user name (e.g dl8@ds-out0)
    '''
    if not remote.endswith("/"):
        remote += "/"

    if remote_server is not None:
        remote = remote_server + ":" + remote


    cmd = " ".join(["rsync -azW"] + ["--exclude=%s" % e for e in excludes] +
                    [remote, local])

    subprocess.Popen(cmd.split(), stdout = subprocess.PIPE,
                     stderr = subprocess.PIPE).communicate()

def copy_files(remote, local, excludes):
    if not remote.endswith("/"):
        remote += "/"

    cmd = " ".join(["cp -rsf"] + [remote, local])

    err = subprocess.Popen(cmd.split(), stdout = subprocess.PIPE,
                     stderr = subprocess.PIPE).communicate()[1]
    if err is not None and len(err) > 0:
        print "ERROR COPY: %s" % err


def iter_vals(dict_iterable, *properties, **kwargs):
    if len(properties) == 0:
        return dict_iterable

    default_value = kwargs.get("default_value", 0)

    prop = properties[0]
    return get_vals( iter(t.get(prop, default_value) for t in dict_iterable), *properties[1:], **kwargs)

def iter_vms(data):
    return ["vm-%i" % n for n in sorted([int(k.split("-")[1]) for k in data.iterkeys() if k != HOST])]

def get_vals(dict_list, *properties, **kwargs):
    return list(iter_vals(dict_list, *properties, **kwargs))

def iter_processed_vals(dict_iterable, proccess_func, *properties):
    for d in iter_vals(dict_iterable, *properties):
        yield proccess_func(d)

def get_processed_vals(dict_list, proccess_func, *properties):
    return list( iter_processed_vals(dict_list, proccess_func, *properties) )

def get_perf(dict_list, *properties, **kwargs):
    reverse = kwargs.get("reverse", False)
    scale = "unknown"

    for d in dict_list:
        scale = d.get("scale", None)
        if scale is not None:
            break

    if scale in ["Seconds", "Microseconds"]:
        reverse = True

    vals = get_vals(dict_list, *properties)

    if not reverse:
        m = min(vals)
        return [ float(v)/m for v in vals]
    else:
        m = max(vals)
        return [ m/float(v) for v in vals]

def get_x_against_y_foreach_z_vals(x_vals, y_vals, z_vals,
                    x_sort_func = None, z_sort_func = None,
                    x_select_func = max, y_select_func = max):
    z_res = {}

    x_history = {}

    for x, y, z in zip(x_vals, y_vals, z_vals):
        if x != None:
            x_history.setdefault(x, 0)
            x_history[x] += 1

        if y != None and z != None:
            # Select the proper x value based on history count
            x_val = x_select_func(x_history, key = lambda val: x_history[val])

            x_res = z_res.setdefault(z, {})

            if x_val not in x_res:
                x_res[x_val] = y
            else:
                x_res[x_val] = y_select_func(x_res[x_val], y)

            x_history = {}

    res = {}
    iter_order = sorted(z_res, cmp = z_sort_func)

    for z in iter_order:
        x_res = z_res[z]

        x_sorted_vals = sorted(x_res, cmp = x_sort_func)
        y = [ x_res[x] for x in x_sorted_vals ]

        res[z] = [(x_sorted_vals, y)]

    return res, iter_order

def get_x_against_y_vals(x_vals, y_vals, x_sort_func = None, x_select_func = max, y_select_func = max, x_samples_threshold = 10):
    res = {}

    x_history = {}

    for x, y in zip(x_vals, y_vals):
        if x != None:
            x_history.setdefault(x, 0)
            x_history[x] += 1

        if y != None:
            # Select the proper x value based on history count
            x_val = x_select_func(x_history, key = lambda val: x_history[val])

            if x_history[x_val] < x_samples_threshold:
                continue

            if x_val not in res:
                res[x_val] = y
            else:
                res[x_val] = y_select_func(res[x_val], y)

            x_history = {}

    x_vals = sorted(res, cmp = x_sort_func)
    y_vals = [ res[x] for x in x_vals ]

    return x_vals, y_vals

def get_stable_x_against_y_vals(x_vals, y_vals, x_cmp_func = None, y_select_func = max, samples_threshold = 10):
    ''' Only use samples where X value havn't changed for a full Y value sample '''

    res = {}

    valid = False
    cur_x = None
    cur_y = None
    samples_count = 0

    for x, y in zip(x_vals, y_vals):
        if y != cur_y:
            if valid and \
               cur_x and cur_y is not None and \
               samples_count >= samples_threshold:
                res[cur_x] = y_select_func(res[cur_x], cur_y) if cur_x in res else cur_y

            valid = True
            cur_x = x
            cur_y = y
            samples_count = 0
        elif x != cur_x:
            valid = False
        else:
            samples_count += 1

    x_vals = sorted(res, cmp = x_cmp_func)
    y_vals = [ res[x] for x in x_vals ]

    return x_vals, y_vals

def get_plot_values(dct, dct_type, prop, old = False):
    result = [t[prop] if t.has_key(prop) else 0 for t in dct[dct_type]]

    if old: result = numpy.cumsum([p if p != None else 0 for p in result])

    return result

def get_vals_change_per_time(dct, dct_type, prop,
                             timeprop = "time", per_time_sec = 60.0, old = False):
    assert per_time_sec > 0

    times = get_time_from_type(dct, dct_type, timeprop)
    vals = get_plot_values(dct, dct_type, prop, old)

    assert len(times) == len(vals)

    low = 0
    ret = []

    for i in range(len(times)):
        # Find the data entry from a minute ago
        while times[low] + per_time_sec < times[i]: low += 1

        # Find the first value from this time to the current time
        while vals[low] == None and low < i: low += 1

        # Find the last value until the current time
        while vals[i] == None and low < i: i -= 1

        # Append values added per time
        time_diff = float(times[i] - times[low]) / float(per_time_sec)
        vals_diff = float(vals[i] - vals[low])

        try: val_per_time = vals_diff / time_diff
        except: val_per_time = 0.

        ret.append(val_per_time)

    return ret

def get_time(dct, timeprop = "time"):
    return [ float(t[timeprop]) / 60.0 for t in dct ]

def get_time_from_type(dct, plot_type, timeprop = "time", **kwargs):
    return get_time(dct[plot_type], timeprop)

def get_time_and_vals(dct, plot_type, *properties, **kwargs):
    none_object = {}
    time = get_time_from_type(dct, plot_type, **kwargs)
    vals = get_vals(dct[plot_type], *properties, default_value=none_object)
    new_time = []
    new_vals = []

    proccess_func = kwargs.get("proccess_func", None)

    for t, v in zip(time, vals):
        if v is not none_object:
            new_time.append(t)
            if proccess_func:
                v = proccess_func(v)
            new_vals.append(v)

    perf_improvement = kwargs.get("perf_improvement", False)
    if not perf_improvement:
        return new_time, new_vals

    reverse = kwargs.get("reverse", False)
    scale = "unknown"

    for d in dct[plot_type]:
        scale = d.get("scale", None)
        if scale is not None:
            break

    if scale in ["Seconds", "Microseconds"]:
        reverse = True

    if not reverse:
        m = min(new_vals)
        return new_time, [ float(v)/m for v in new_vals]
    else:
        m = max(new_vals)
        return new_time, [ m/float(v) for v in new_vals]

pol_time = partial(get_time_from_type, dct_type=POL)
mon_time = partial(get_time_from_type, dct_type=MON)
perf_time = partial(get_time_from_type, dct_type=PERF)

k_per_sec = lambda x: float(x) / 1000

def extra_mem(dct):
    base = get_vals(dct[POL], 'mem_base')
    mem = get_vals(dct[POL], 'control_mem')
    assert len(base) == len(mem)
    return [_mem - _base for _mem, _base in zip(mem, base)]

def correct_time(data, info):
    for n in data.keys():
        for test in data[n].keys():
            t0 = 99999999999
            for vm in data[n][test].keys():
                for tp in data[n][test][vm].keys():
                    try: t0 = min(t0, data[n][test][vm][tp][0]['time'])
                    except KeyError: pass

            for vm in data[n][test].keys():
                for tp in data[n][test][vm].keys():
                    for dct in data[n][test][vm][tp]:
                        dct['time'] -= t0

def get_exp_data(path, perf_field, rev_funcs):
    folders = os.listdir(path)
    nums = sorted([int(f.split("-")[-1])
                   for f in folders if f.startswith("num-") and os.path.isdir(os.path.join(path, f))])
    data = {}
    info = {}

    for n in nums:
        read_dir = os.path.join(path, "num-%i" % n)
        tests = [f for f in os.listdir(read_dir) if f not in ("info", "loads-and-valuation")]
        # read the experiment output
        data[n] = {}
        info[n] = {}
        for test in tests:
            content = file(os.path.join(read_dir, test, "exp-plotter")).readlines()
            data[n][test] = Plotter.parse(content)
            info[n][test] = eval(file(os.path.join(read_dir, test, "info")).readline())

    correct_time(data, info)

    p = perf_field
    for n in data.keys():
        for test in data[n].keys():
            for vm in data[n][test].keys():
                if vm == HOST: continue
                if rev_funcs != None and rev_funcs != {}:
                    try:
                        rev_func = rev_funcs[n][vm]
                    except KeyError:
                        sys.stderr.write("no rev func for %s, n=%s, test=%s, vm=%s\n" % (
                                            path, str(n), str(test), str(vm)))
                        sys.stderr.write("rev_funcs: %s\n" % str(rev_funcs))
                        raise
                    if isinstance(rev_func, str):
                        rev_func = eval(rev_func)
                    if not isinstance(rev_func, list):
                        rev_func = [rev_func]
                    rev_func = map(lambda f: eval(f) if isinstance(f, str) else f, rev_func)
                else:
                    rev_func = None
                if not data[n][test][vm].has_key(PERF):
                    print "NO PERF: exp:", path, "num:", n, "test:", test, "vm:", vm
                    continue
                data[n][test][vm]['raw_perf'] = [] # back up raw performance

                for i in range(len(data[n][test][vm][PERF])):  # go over all the performance samples
                    try:
                        data[n][test][vm]['raw_perf'].append(dict(data[n][test][vm][PERF][i]))

                        data[n][test][vm][PERF][i]['perf'] = data[n][test][vm][PERF][i]['perf'][p]
                        perf = data[n][test][vm][PERF][i]['perf']

                        try:
                            current_valuation = [x for x in data[n][test][vm][MON] if x['time'] <= data[n][test][vm][PERF][i]['time']][-1]['current_valuation']
                        except KeyError:
                            current_valuation = 0
                        except IndexError:
                            current_valuation = 0
                        if rev_func != None:
                            data[n][test][vm][PERF][i]['rev'] = rev_func[current_valuation](perf)
                    except KeyError:
                        data[n][test][vm][PERF][i]['perf'] = 0
                        data[n][test][vm][PERF][i]['rev'] = 0

    return data, info

def process_description_file(input_paths_list, output_path, sat_dict = None, exp_name = "ginseng"):
    rev_funcs = {}
    loads = {}
    sat = {}
    for path in input_paths_list:
        folders = os.listdir(path)
        nums = sorted([int(f.split("-")[-1])
                   for f in folders if f.startswith("num-") and os.path.isdir(os.path.join(path, f))])

        for n in nums:
            if n in rev_funcs.keys():
                continue
            read_dir = os.path.join(path, "num-%i" % n)
            # read the experiment output
            try:
                info = eval(file(os.path.join(read_dir, exp_name, "info")).readline())
            except:
                continue
            rev_funcs[n] = {}
            loads[n] = {}
            sat[n] = 0
            for vm in info['vms_desc'].keys():
                try:
                    func = info['vms_desc'][vm]['adviser_args']['rev_func']
                    rev_funcs[n][vm] = func
                except:
                    pass
                load_desc = info['vms_desc'][vm]['load_func']
                if load_desc['type'] == "LoadBinary":
                    loads[n][vm] = {'v1': load_desc['v1'], 'v2': load_desc['v2'],
                                    'T': load_desc['T'], 'T0': load_desc['T0']}
                    if sat_dict is not None:
                        sat[n] += sat_dict[load_desc['v1']]
                else:
                    loads[n][vm] = {'v1': load_desc['value'],
                                    'v2': load_desc['value'],
                                    'T': 1, 'T0': 0}
                    if sat_dict is not None:
                        sat[n] += sat_dict[load_desc['value']]
    if sat_dict is not None:
        file(os.path.join(output_path, "sat.txt"), "w").write(str(sat))
    file(os.path.join(output_path, "valuations.txt"), "w").write(str(rev_funcs))
    file(os.path.join(output_path, "loads.txt"), "w").write(str(loads))

def process_traces(read_path, out_path, perf_field, rev_funcs, exp_name = "ginseng"):
    load = {}
    perf = {}
    val = {}
    time = {}

    folders = os.listdir(read_path)
    nums = sorted([int(f.split("-")[-1])
    for f in folders if f.startswith("num-") and os.path.isdir(os.path.join(read_path, f))])

    for n in nums:
        read_dir = os.path.join(read_path, "num-%i" % n)
        # read the experiment output
        try:
            data = file(os.path.join(read_dir, exp_name, "exp-plotter")).readlines()
            data = Plotter.parse(data)
        except:
            continue
        load[n] = {}
        perf[n] = {}
        val[n] = {}
        time[n] = {}
        for vm in data.keys():
            if vm == HOST: continue
            try:
                time[n][vm] = [x["time"] for x in data[vm][PERF]]
                load[n][vm] = Loads.LoadTrace([x["time"] for x in data[vm][PERF]], [x["load"] for x in data[vm][PERF]])
                perf[n][vm] = [x["perf"][perf_field] for x in data[vm][PERF]]

                if isinstance(rev_funcs[n][vm], str):
                    funcs = [rev_funcs[n][vm]]

                funcs = map(lambda f: np.vectorize(eval(f) if isinstance(f, str) else f), rev_funcs[n][vm])
                val[n][vm] = []
                for x in data[vm][PERF]:
                    try:
                        current_valuation = [y for y in data[vm][MON] if y['time'] <= x['time']][-1]['current_valuation']
                    except KeyError:
                        current_valuation = 0
                    except IndexError:
                        current_valuation = 0
                    func = funcs[current_valuation]
                    val[n][vm].append(func(x['perf'][perf_field]))

#                func = np.vectorize(eval(rev_funcs[n][vm]))
#                val[n][vm] = func(np.array([x["perf"][perf_field] for x in data[vm][PERF]]))
            except KeyError:
                print "no reults in experiment ", n, vm

    file(os.path.join(out_path, "trace_time.txt"), "w").write(str(time))
    file(os.path.join(out_path, "trace_load.txt"), "w").write(str(load))
    file(os.path.join(out_path, "trace_perf.txt"), "w").write(str(perf))
    file(os.path.join(out_path, "trace_val.txt"), "w").write(str(val))

def exp_plot_as_is(data, funcs, plot_kwargs, y_labels, axes,
                 ylim, xlim):
    if not data:
        return

    xmin = sys.maxint
    xmax = 0

    all_xy = []

    for vm_i, vm in enumerate(iter_vms(data)):
        all_xy.append([])
        for xy_func in funcs:
            try:
                x = xy_func[0](data[vm])
                y = xy_func[1](data[vm])
            except Exception as ex:
                print ("error in guest: %i, %s" % (vm_i, ex))
                y = x = [0]
                raise
            if len(x) != 0 and len(y) != 0:
                xmin = min(xmin, min(x))
                xmax = max(xmax, max(x))
            all_xy[-1].append((x, y))

    multifunction(all_xy,
                  axes = axes,
                  plot_kwargs = plot_kwargs,
                  xlim = xlim if xlim is not None else (xmin, xmax),
                  ylim = ylim,
                  ylabels = y_labels)

def exp_plot(data, funcs, plot_kwargs, axes = None,
                 ylim = None, xlim = None):
    if not data:
        return

    funcs.append((perf_time, lambda dct: get_vals(dct[PERF], 'load')))
    plot_kwargs.append({"label": "Load", "marker": "None", "color": "k",
                        "alpha": 0.5, "linestyle": "-", "linewidth": 1})
    if axes is not None:
        axes.append(max(axes) + 1)
    else:
        axes = range(len(funcs))

    y_labels = [kw["label"] for kw in plot_kwargs]

    exp_plot_as_is(data, funcs, plot_kwargs, y_labels, axes, ylim, xlim)

def align_data_to_time(time_data_pairs):
    first_timestamp = max([ time[0] for time,_ in time_data_pairs ])
    last_timestamp = min([ time[-1] for time,_ in time_data_pairs ])

    pos = [0] * len(time_data_pairs)

    for i, (time,_) in enumerate(time_data_pairs):
        while time[pos[i]] < first_timestamp:
            pos[i] += 1

    fixed_time = [ ]
    fixed_data = [ [] for _ in time_data_pairs ]

    while True:
        cur_time = min( [ time[p] for p, (time,_) in zip(pos, time_data_pairs) ] )
        fixed_time.append(cur_time)

        for i, (time,data) in enumerate(time_data_pairs):
            if time[pos[i]] > cur_time:
                fixed_data[i].append(data[pos[i] - 1])
            else:
                fixed_data[i].append(data[pos[i]])
                pos[i] += 1

        if cur_time >= last_timestamp:
            break

    return fixed_time, fixed_data

def align_data_to_time_backwards(time_data_pairs):
    min_last_timestamp = min([ time[-1] for time,_ in time_data_pairs ])

    pos = [0] * len(time_data_pairs)

    fixed_time = [ ]
    fixed_data = [ [] for _ in time_data_pairs ]

    while True:
        cur_time = min( [ time[p] for p, (time,_) in zip(pos, time_data_pairs) ] )
        fixed_time.append(cur_time)

        for i, (time,data) in enumerate(time_data_pairs):
            fixed_data[i].append(data[pos[i]])

            if time[pos[i]] <= cur_time:
                pos[i] += 1

        if cur_time >= min_last_timestamp:
            break

    return fixed_time, fixed_data

def get_revenue(dct):
    revenues = []
    for i in range(len(dct[PERF])):
        mon_entries = [x for x in dct[MON] if x['time'] <= dct[PERF][i]['time']]
        rev_func_index = mon_entries[-1]['current_valuation']
        rev_func = eval(dct[INFO]['adviser_args']['rev_func'][rev_func_index])
        revenues.append(rev_func(dct[PERF][i]['perf']['hits_rate']))

    return revenues
