'''
Created on Jun 1, 2014

@author: Liran Funaro
'''

import pylab as pl
import matplotlib.gridspec as gridspec
import math
import copy
import sys, os
from out.common.common import get_time, get_vals
from etc.plots import draw_n_save

from mom.Plotter import MON, HOST

import matplotlib
from out.common.MultiFunctionsPlotter import MultiFunctionsPlotter

class OutputView(object):
    '''
    Represent a view conducted of a few visuals
    '''

    PAPER_OUTPUT_EXT = "eps"
    WORKING_OUTPUT_EXT = "pdf"
    IMAGE_OUTPUT_EXT = "png"

    output_format = dict(
                         default = WORKING_OUTPUT_EXT,

                         paper = PAPER_OUTPUT_EXT,
                         eps = PAPER_OUTPUT_EXT,

                         work = WORKING_OUTPUT_EXT,
                         working = WORKING_OUTPUT_EXT,
                         pdf = WORKING_OUTPUT_EXT,

                         image = IMAGE_OUTPUT_EXT,
                         png = IMAGE_OUTPUT_EXT
                         )

    colororder = [
        # R     G     B     A
        (0.00, 0.00, 1.00, 1.0), # 1
        (0.00, 0.50, 0.00, 1.0), # 2
        (1.00, 0.00, 0.00, 1.0), # 3
        (0.00, 0.75, 0.75, 1.0), # 4
        (0.75, 0.00, 0.75, 1.0), # 5
        (0.75, 0.75, 0.00, 1.0), # 6
        (0.25, 0.25, 0.25, 1.0), # 7
        (0.75, 0.25, 0.25, 1.0), # 8
        (0.95, 0.95, 0.00, 1.0), # 9
        (0.25, 0.25, 0.75, 1.0), # 10
        (0.75, 0.75, 0.75, 1.0), # 11
        (0.00, 1.00, 0.00, 1.0), # 12
        (0.76, 0.57, 0.17, 1.0), # 13
        (0.54, 0.63, 0.22, 1.0), # 14
        (0.34, 0.57, 0.92, 1.0), # 15
        (1.00, 0.10, 0.60, 1.0), # 16
        (0.88, 0.75, 0.73, 1.0), # 17
        (0.10, 0.49, 0.47, 1.0), # 18
        (0.66, 0.34, 0.65, 1.0), # 19
        (0.99, 0.41, 0.23, 1.0), # 20
    ]

    markersorder = ['.', '+', 'v', 's', 'o', '*', "D", "p","^","8","h","x", matplotlib.markers.CARETDOWN, matplotlib.markers.TICKLEFT, matplotlib.markers.CARETUP, matplotlib.markers.TICKUP]

    def __init__(self,
                 name,
                 visuals = [],
                 compare_visuals = [],
                 size = (16, 9),
                 ylim = None,
                 xlim = None,
                 default_x_label = "Time [min.]"):
        '''
        Constructor
        '''

        self.__name = name
        self.visuals = visuals if visuals else []
        self.compare_visuals = compare_visuals if compare_visuals else []

        self.x_label = None

        for vis in self.visuals:
            if not self.x_label:
                self.x_label = vis.args["x_label"]
            else:
                assert self.x_label == vis.args["x_label"]

        for vis in self.compare_visuals:
            if not self.x_label:
                self.x_label = vis.args["x_label"]
            else:
                assert self.x_label == vis.args["x_label"]

        if not self.x_label:
            self.x_label = default_x_label

        self.size = size

        self.ylim = ylim
        self.xlim = xlim

    @property
    def name(self):
        return self.__name

    @classmethod
    def getAutoColor(cls, plot_number):
        return cls.colororder[plot_number % len(cls.colororder)]

    @classmethod
    def getAutoMarker(cls, plot_number):
        return cls.markersorder[plot_number % len(cls.markersorder)]

    @classmethod
    def initColors(cls, plot_kwargs):
        for i, args in enumerate(plot_kwargs):
            if "color" not in args:
                args["color"] = cls.getAutoColor(i)

    @classmethod
    def initMarkers(cls, plot_kwargs):
        for i, args in enumerate(plot_kwargs):
            if "marker" not in args:
                args["marker"] = cls.getAutoMarker(i)

    @classmethod
    def initAxes(cls, plot_kwargs):
        axes_units = []
        axes_yscale = []

        for args in plot_kwargs:
            found_an_axe = False

            cur_unit = args["units"] if "units" in args else None
            cur_yscale = args["yscale"] if "yscale" in args else None

            if cur_unit:
                for axe,(unit,yscale) in enumerate(zip(axes_units, axes_yscale)):
                    if cur_unit == unit and cur_yscale == yscale:
                        found_an_axe = True
                        args["axe"] = axe
                        break

            if not found_an_axe:
                next_axe = len(axes_units)
                assert next_axe == len(axes_yscale)

                axes_units.append(cur_unit)
                axes_yscale.append(cur_yscale)

                args["axe"] = next_axe

    def drawAndSave(self, output_path, data, draw_args = {}):
        if "output_format" in draw_args:
            selected_out_format = draw_args["output_format"].lower()
        else:
            selected_out_format = "default"

        if selected_out_format in self.output_format:
            out_ext = self.output_format[selected_out_format]
        else:
            out_ext = self.output_format["default"]

        pl.clf()
        draw_ret = self.draw(data, draw_args)
        if not draw_ret:
            return None

        nrows, ncols = draw_ret
        draw_n_save(
                    name = os.path.join( output_path, "%s.%s" % (self.__name, out_ext) ),
                    size = (self.size[0] * ncols, self.size[1] * nrows))

        return out_ext

    def draw(self, data, info = None, draw_args = {}):
        return None

class VMsView(OutputView):
    '''
    Represent a view conducted of a few visuals
    '''

    def __init__(self,
                 name,
                 visuals = [],
                 compare_visuals = [],
                 size = (16, 9),
                 ylim = None,
                 xlim = None):
        '''
        Constructor
        '''
        OutputView.__init__(self, name, visuals, compare_visuals, size, ylim, xlim)

    def get_all_xy(self, data):
        all_xy = {}

        vis_iter_order = None

        is_single_visual = len(self.visuals) == 1

        for vis in self.visuals:
            raw_xys, iter_order = vis.get_all_xy(data, is_single_visual, False)

            if vis_iter_order:
                assert iter_order == vis_iter_order
            else:
                vis_iter_order = iter_order

            for key in iter_order:
                all_xy.setdefault(key, []).extend(raw_xys[key])

        if vis_iter_order == None:
            vis_iter_order = []

        if self.compare_visuals:
            is_single_visual = len(self.compare_visuals) == 1

            res_compare = []
            for vis in self.compare_visuals:
                raw_xys, _ = vis.get_all_xy(data, is_single_visual, True)
                res_compare += raw_xys

            all_xy["compare"] = res_compare
            vis_iter_order.append("compare")

        return all_xy, vis_iter_order

    def prepare_data(self, data):
        if not data:
            return None

        all_xy, iter_order = self.get_all_xy(data)

        xmin = sys.maxint
        xmax = 0

        for xys in all_xy.itervalues():
            for x, y, _ in xys:
                if len(x) != 0 and len(y) != 0:
                    xmin = min(xmin, min(x))
                    xmax = max(xmax, max(x))

            plot_kwargs = [ xy[2] for xy in xys ]
            self.initColors(plot_kwargs)
            self.initMarkers(plot_kwargs)
            self.initAxes(plot_kwargs)

        return dict(
                    all_xy = all_xy,
                    iter_order = iter_order,

                    xlim = self.xlim if self.xlim is not None else (xmin, xmax),
                    ylim = self.ylim
                    )

    def draw(self, data, draw_args = {}):
        pdata = self.prepare_data(data)
        if not pdata:
            return None

        MultiFunctionsPlotter(x_label = self.x_label, **pdata).plot()

        return (len(pdata["all_xy"]), 1)

'''
Special Views Classes
'''
class CPUView(OutputView):
    CORES_IN_ROW = 4
    CATEGORIES_HSPACE = 0.5
    NODES_HSPACE = 0.1
    CORES_SPACE = 0.1

    def __init__(self, name, size = (16,9)):
        OutputView.__init__(self, name = name, size = size)

    @classmethod
    def get_monitoed_cpu(cls, data):
        keys = [k.replace("cpu_", "") for k in data[0].keys()
            if k.startswith("cpu") and k != "cpu_tot"]

        return sorted( [int(cpu) for cpu in keys] )

    @classmethod
    def get_cpu_topology(cls, info, all_monitored_cpus):
        # Fetch the hypervisor topology
        if info and isinstance(info, dict):
            if "host_machine" in info and "cpus_hierarcy" in info["host_machine"]:
                return info["host_machine"]["cpus_hierarcy"]
        else:
            # Fallback in-case there is no relevant information in the info
            return [[ [cpu] for cpu in all_monitored_cpus ]]

    @classmethod
    def get_cpus_categories(cls, info, all_monitored_cpus):
        categories = {}

        if info and isinstance(info, dict):
            if "host_machine" in info and "system_cpus" in info["host_machine"]:
                categories["System"] = info["host_machine"]["system_cpus"]

            if "vms" in info:
                for vm, vm_info in info["vms"].iteritems():
                    vm_title = "no title"
                    if "title" in vm_info:
                        vm_title = vm_info["title"]

                    if "guest_cpus" in vm_info:
                        categories["%s Guest (%s)" % (vm, vm_title)] = vm_info["guest_cpus"]
                    if "bm_cpus" in vm_info:
                        categories["%s Benchmark (%s)" % (vm, vm_title)] = vm_info["bm_cpus"]

        # Find all CPUs that doesn't belong to any category
        non_categorized_cpus = copy.copy(all_monitored_cpus)
        for cat in categories:
            for cpu in categories[cat]:
                try: non_categorized_cpus.remove(cpu)
                except: pass

        if non_categorized_cpus:
            categories["Others"] = non_categorized_cpus

        return categories

    @classmethod
    def get_cores_rows_count(cls, core_count):
        return int(math.ceil(float(core_count) / cls.CORES_IN_ROW))

    @classmethod
    def get_categories_topology(cls, categories, cpus_topology):
        categories_topology = {}

        for cat in categories:
            cur_cat = categories[cat]
            categories_topology[cat] = {}

            for i_node, node in cpus_topology.iteritems():
                for i_socket, socket in node.iteritems():
                    for i_core, core in socket.iteritems():
                        for cpu in core:
                            if cpu in cur_cat:
                                categories_topology[cat] \
                                    .setdefault(i_node, {}) \
                                    .setdefault(i_socket, {}) \
                                    .setdefault(i_core, []) \
                                    .append(cpu)

        return categories_topology

    @classmethod
    def get_categories_topology_heights(cls, categories_topology):
        category_socket_rows = {}
        category_socket_heights = {}
        category_heights = {}

        for cat, cat_topo in categories_topology.iteritems():
            category_socket_rows[cat] = {}
            category_socket_heights[cat] = []

            for _,node_topo in sorted(cat_topo.iteritems()):
                for sock,sock_topo in sorted(node_topo.iteritems()):
                    sock_rows = cls.get_cores_rows_count(len(sock_topo))
                    cur_sock_height = cls.calc_socket_height( sock_rows )

                    category_socket_rows[cat][sock] = sock_rows
                    category_socket_heights[cat].append(cur_sock_height)

            category_heights[cat] = cls.calc_category_height(category_socket_heights[cat])

        return category_socket_rows, category_socket_heights, category_heights


    @classmethod
    def calc_socket_height(cls, node_rows_count):
        return (1.0 * node_rows_count) + (cls.CORES_SPACE * (node_rows_count - 1))

    @classmethod
    def calc_category_height(cls, node_rows_height):
        return sum(node_rows_height) + ( len(node_rows_height) * cls.NODES_HSPACE )

    def draw(self, data, draw_args = {}):
        if HOST not in data or MON not in data[HOST]:
            return None

        host_monitor_data = data[HOST][MON]

        all_monitored_cpus = self.get_monitoed_cpu(host_monitor_data)
        cpus_count = len(all_monitored_cpus)

        cpus_topology = self.get_cpu_topology(data.info, all_monitored_cpus)
        categories = self.get_cpus_categories(data.info, all_monitored_cpus)
        categories_topology = self.get_categories_topology(categories, cpus_topology)

        category_socket_rows, category_socket_heights, category_heights = self.get_categories_topology_heights(categories_topology)
        max_cpus_in_core = max([ len(core) for node in cpus_topology.itervalues() for socket in node.itervalues() for core in socket.itervalues() ])
        categories_heights_array = [ height for cat, height in sorted(category_heights.iteritems())]

        fig = pl.figure()
        fig.suptitle("Hypervisor CPU Usage", fontsize = 80)

        category_grid = gridspec.GridSpec(len(categories_topology), 1,
                                          wspace = 0.0, hspace = self.CATEGORIES_HSPACE,
                                          height_ratios = categories_heights_array)

        for i_cat, (cat,cat_topo) in enumerate( sorted( categories_topology.iteritems() ) ):
            cur_sockets_heights = category_socket_heights[cat]
            cur_socket_count = len(cur_sockets_heights)

            sock_grid = gridspec.GridSpecFromSubplotSpec(
                            cur_socket_count, 1,
                            subplot_spec = category_grid[i_cat],
                            wspace = 0.0, hspace = self.NODES_HSPACE,
                            height_ratios = cur_sockets_heights)

            first_core = True
            sock_grid_pos = 0

            for node, node_topo in sorted( cat_topo.iteritems() ):
                for sock, sock_topo in sorted( node_topo.iteritems() ):
                    core_grid = gridspec.GridSpecFromSubplotSpec(
                                category_socket_rows[cat][sock], self.CORES_IN_ROW,
                                subplot_spec = sock_grid[sock_grid_pos],
                                wspace = self.CORES_SPACE, hspace = self.CORES_SPACE)

                    sock_grid_pos += 1

                    for i_core, (core, core_topo) in enumerate( sorted( sock_topo.iteritems() ) ):
                        core_cpu_count = len(core_topo)

                        cpu_grid = gridspec.GridSpecFromSubplotSpec(
                                max_cpus_in_core, 1,
                                subplot_spec = core_grid[i_core],
                                wspace = 0.0, hspace = 0.0)

                        for i_cpu in range(max_cpus_in_core):

                            ax = pl.Subplot(fig, cpu_grid[i_cpu])
                            if i_cpu < core_cpu_count:
                                cpu = core_topo[i_cpu]
                                key = "cpu_%s" % cpu

                                ax.plot(
                                        get_time(host_monitor_data),
                                        get_vals(host_monitor_data, key),
                                        marker = "None",
                                        color = pl.cm.jet(float(cpu) / cpus_count),  # @UndefinedVariable
                                        alpha = 1,
                                        label = "CPU %s" % cpu)

                                ax.grid(True)
                                ax.legend(loc = "upper right")

                            ax.label_outer()


                            if first_core:
                                ax.set_title(cat.upper(), fontsize = 40)
                            first_core = False

                            ax.set_ylim(0.0, 1.0)
                            fig.add_subplot(ax)

        return (sum(categories_heights_array), self.CORES_IN_ROW)

class VMsOutputCSV(VMsView):
    CSV_EXP = "csv"

    def __init__(self, name, visuals, compare_visuals, ylim = None, xlim = None):
        VMsView.__init__(self, name, visuals, compare_visuals, None, ylim, xlim)

    @classmethod
    def fromView(cls, view):
        return cls(view.name, view.visuals, view.compare_visuals, view.ylim, view.xlim)

    def drawAndSave(self, output_path, data, draw_args = {}):
        pdata = self.prepare_data(data)

        all_xy = pdata["all_xy"]

        for subplot_name, subplot_xy in all_xy.iteritems():
            titles = ['"%s"' % self.x_label] + [ '"%s [%s]"' % (kwargs["y_label"], kwargs["units"]) for _,_,kwargs in subplot_xy ]
            titles_str = ",".join(titles) + "\n"

            out_path = os.path.join( output_path, "%s-%s.%s" % (self.name, subplot_name, self.CSV_EXP) )

            pos = [0] * len(subplot_xy)
            values = [0] * (len(subplot_xy) + 1)

            with open(out_path, "w+") as f:
                f.write(titles_str)

                while True:
                    min_time = float("inf")
                    min_xy = []

                    for i, p in enumerate(pos):
                        if p >= len(subplot_xy[i][0]):
                            continue

                        x_time = subplot_xy[i][0][p]

                        if x_time < min_time:
                            min_time = x_time
                            min_xy = [i]
                        elif x_time == min_time:
                            min_xy.append(i)

                    if not min_xy:
                        break

                    values[0] = min_time
                    for i in min_xy:
                        values[i+1] = subplot_xy[i][1][pos[i]]
                        pos[i] += 1

                    f.write(",".join( map(str, values) ) )
                    f.write("\n")

        return self.CSV_EXP

