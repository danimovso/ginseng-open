'''
Common Visuals

@author: Liran Funaro <fonaro@cs.technion.ac.il>
'''
from out.common.OutputVisual import OutputVisual, GlobalVisual
from out.common.common import get_vals, k_per_sec, extra_mem,\
    get_vals_change_per_time, get_time_from_type, get_x_against_y_vals,\
    get_x_against_y_foreach_z_vals, \
    align_data_to_time_backwards, get_stable_x_against_y_vals
from mom.Plotter import PERF, MON, POL

def VISUAL_LOAD_U(units = None):
    return OutputVisual(
             xy_function = lambda dct: [(get_time_from_type(dct,PERF), get_vals(dct[PERF], 'load'))],
             x_label = "Time [m]",
             y_label = "Load",
             units = units,
             linestyle = "-",
             marker = "None",
             linewidth = 2,
             alpha = 0.5,
             drawstyle = "steps-post"
             )

VISUAL_LOAD = VISUAL_LOAD_U()

'''
Hitrate performance
'''
VISUAL_PERFORMANCE = OutputVisual(
             xy_function = lambda dct: [(get_time_from_type(dct,PERF),  get_vals(get_vals(dct[PERF], 'perf'), "hits_rate"))],
             x_label = "Time [m]",
             y_label = "Performance",
             units = "Khits/s",
             linestyle = "--",
             marker = "None",
             linewidth = 2,
             alpha = 0.5,
             color = "r",
             drawstyle = "steps-post"
             )

VISUAL_PERFORMANCE_K = OutputVisual(
             xy_function = lambda dct: [(get_time_from_type(dct,PERF), map(k_per_sec,
                                          get_vals(get_vals(dct[PERF], 'perf'),
                                                   "hits_rate")))],
             x_label = "Time [m]",
             y_label = "Performance",
             units = "Khits/s",
             linestyle = "--",
             marker = "None",
             linewidth = 2,
             alpha = 0.5,
             color = "r",
             drawstyle = "steps-post"
             )

'''
Common Bandwidth Visuals 
'''

VISUAL_BANDWIDTH_RATE = OutputVisual(
             xy_function = lambda dct: [(get_time_from_type(dct,MON), get_vals(dct[MON], 'net_rate'))],

             x_label = "Time [m]",
             y_label = "Bandwidth Rate",
             units = "bps",
             linestyle = "-",
             marker = "None",
             linewidth = 2,
             alpha = 0.5,
             drawstyle = "steps-post"
             )

'''
Common Memory Visuals
'''
VISUAL_MEMORY = OutputVisual(
             xy_function = lambda dct: [(get_time_from_type(dct,MON), get_vals(dct[MON], 'mem_available'))],

             x_label = "Time [m]",
             y_label = "Available Memory",
             units = "MB",
             linestyle = "-",
             marker = "None",
             linewidth = 2,
             alpha = 0.5,
             drawstyle = "steps-post"
             )

VISUAL_MEMORY_ANON_PAGES = OutputVisual(
             xy_function = lambda dct: [(get_time_from_type(dct,MON), get_vals(dct[MON], 'anon_pages'))],
             x_label = "Time [m]",
             y_label = "Anonymous Pages",
             units = "MB",
             linestyle = "-",
             marker = "None",
             linewidth = 2,
             alpha = 0.5
             )

VISUAL_MEMORY_UNUSED = OutputVisual(
             xy_function = lambda dct: [(get_time_from_type(dct,MON), get_vals(dct[MON], 'mem_unused'))],
             x_label = "Time [m]",
             y_label = "Unused Memory",
             units = "MB",
             linestyle = "-",
             marker = "None",
             linewidth = 2,
             alpha = 0.5
             )

VISUAL_MEMORY_FREE = OutputVisual(
             xy_function = lambda dct: [(get_time_from_type(dct,MON), get_vals(dct[MON], 'mem_free'))],
             x_label = "Time [m]",
             y_label = "Free Memory",
             units = "MB",
             linestyle = "-",
             marker = "None",
             linewidth = 2,
             alpha = 0.5
             )

VISUAL_MEMORY_CACHE_BUFF = OutputVisual(
             xy_function = lambda dct: [(get_time_from_type(dct,MON), get_vals(dct[MON], 'cache_and_buff'))],
             x_label = "Time [m]",
             y_label = "Cache and Buffer",
             units = "MB",
             linestyle = "-",
             marker = "None",
             linewidth = 2,
             alpha = 0.5
             )

VISUAL_MEMORY_QEMU_RSS = OutputVisual(
             xy_function = lambda dct: [(get_time_from_type(dct,MON), get_vals(dct[MON], 'rss'))],
             x_label = "Time [m]",
             y_label = "Resident set size",
             units = "MB",
             linestyle = "-",
             marker = "None",
             linewidth = 2,
             alpha = 0.5
             )

VISUAL_EXTRA_MEMORY = OutputVisual(
             xy_function = lambda dct: [(get_time_from_type(dct,POL), extra_mem(dct))],
             x_label = "Time [m]",
             y_label = "Extra Memory",
             units = "MB",
             linestyle = "-",
             marker ="None",
             linewidth = 2,
             alpha = 0.5,
             color = "r",
             drawstyle = "steps-post"
             )

VISUAL_MEMORY_BASE = OutputVisual(
             xy_function = lambda dct: [(get_time_from_type(dct,POL), get_vals(dct[POL], 'mem_base'))],
             x_label = "Time [m]",
             y_label = "Base Memory",
             units = "MB",
             linestyle = "None",
             linewidth = 2,
             marker ="o",
             alpha = 0.5,
             drawstyle = "steps-post"
             )

def _VISUAL_PG_FAULT_MAJOR_PER_MIN(old = False):
    return OutputVisual(
             xy_function = lambda dct: [(get_time_from_type(dct,MON), get_vals_change_per_time(dct, MON, 'major_fault', old = old))],
             x_label = "Time [m]",
             y_label = "Major Faults",
             units = "Faults/min.",
             linestyle = "-",
             marker = "None",
             linewidth = 2,
             alpha = 0.5,
             yscale = "symlog",
             drawstyle = "default"
             )

VISUAL_PG_FAULT_MAJOR_PER_MIN = _VISUAL_PG_FAULT_MAJOR_PER_MIN(False)
VISUAL_PG_FAULT_MAJOR_PER_MIN_OLD = _VISUAL_PG_FAULT_MAJOR_PER_MIN(True)

def _VISUAL_PG_FAULT_MINOR_PER_MIN(old = False):
    return OutputVisual(
             xy_function = lambda dct: [(get_time_from_type(dct,MON), get_vals_change_per_time(dct, MON, 'minor_fault', old = old))],
             x_label = "Time [m]",
             y_label = "Minor Faults",
             units = "Faults/min.",
             linestyle = "-",
             marker = "None",
             linewidth = 2,
             alpha = 0.5,
             yscale = "symlog",
             drawstyle = "default"
             )

VISUAL_PG_FAULT_MINOR_PER_MIN = _VISUAL_PG_FAULT_MINOR_PER_MIN(False)
VISUAL_PG_FAULT_MINOR_PER_MIN_OLD = _VISUAL_PG_FAULT_MINOR_PER_MIN(True)

def _VISUAL_MEMORY_SWAP_OUT_PER_MIN(old = False):
    return OutputVisual(
             xy_function = lambda dct: [(get_time_from_type(dct,MON), get_vals_change_per_time(dct, MON, 'swap_out', old = old))],
             x_label = "Time [m]",
             y_label = "Swapped out Pages",
             units = "Pages/min.",
             linestyle = "-",
             marker = "None",
             linewidth = 2,
             alpha = 0.5,
             yscale = "symlog",
             drawstyle = "default"
             )

VISUAL_MEMORY_SWAP_OUT_PER_MIN = _VISUAL_MEMORY_SWAP_OUT_PER_MIN(False)
VISUAL_MEMORY_SWAP_OUT_PER_MIN_OLD = _VISUAL_MEMORY_SWAP_OUT_PER_MIN(True)

def _VISUAL_MEMORY_SWAP_IN_PER_MIN(old = False):
    return OutputVisual(
             xy_function = lambda dct: [(get_time_from_type(dct,MON), get_vals_change_per_time(dct, MON, 'swap_in', old = old))],
             x_label = "Time [m]",
             y_label = "Swapped in Pages",
             units = "Pages/min.",
             inestyle = "-",
             marker = "None",
             linewidth = 2,
             alpha = 0.5,
             yscale = "symlog",
             drawstyle = "default"
             )

VISUAL_MEMORY_SWAP_IN_PER_MIN = _VISUAL_MEMORY_SWAP_IN_PER_MIN(False)
VISUAL_MEMORY_SWAP_IN_PER_MIN_OLD = _VISUAL_MEMORY_SWAP_IN_PER_MIN(True)

VISUAL_MEMORY_PAGE_OUT_PER_MIN = OutputVisual(
             xy_function = lambda dct: [(get_time_from_type(dct,MON), get_vals_change_per_time(dct, MON, 'page_out', old = False))],
             x_label = "Time [m]",
             y_label = "Paged out Pages",
             units = "Pages/min.",
             inestyle = "-",
             marker = "None",
             linewidth = 2,
             alpha = 0.5,
             yscale = "symlog",
             drawstyle = "default"
             )

VISUAL_MEMORY_PAGE_IN_PER_MIN = OutputVisual(
             xy_function = lambda dct: [(get_time_from_type(dct,MON), get_vals_change_per_time(dct, MON, 'page_in', old = False))],
             x_label = "Time [m]",
             y_label = "Paged in Pages",
             units = "Pages/min.",
             inestyle = "-",
             marker = "None",
             linewidth = 2,
             alpha = 0.5,
             yscale = "symlog",
             drawstyle = "default"
             )

VISUAL_PYTHON1 = OutputVisual(
             xy_function = lambda dct: [(get_time_from_type(dct,MON),  get_vals(dct[MON], 'python1_usage'))],
             x_label = "Time [m]",
             y_label = "Python1",
             units = "CPU %",
             linestyle = "-",
             marker ="None",
             linewidth = 2,
             alpha = 0.5,
             drawstyle = "steps-post"
             )

VISUAL_PYTHON2 = OutputVisual(
             xy_function = lambda dct: [(get_time_from_type(dct,MON),  get_vals(dct[MON], 'python2_usage'))],
             x_label = "Time [m]",
             y_label = "Python2",
             units = "CPU %",
             linestyle = "-",
             marker ="None",
             linewidth = 2,
             alpha = 0.5,
             drawstyle = "steps-post"
             )

VISUAL_CPU_USAGE = OutputVisual(
             xy_function = lambda dct: [(get_time_from_type(dct,MON),  get_vals(dct[MON], 'cpu_usage'))],
             x_label = "Time [m]",
             y_label = "CPU usage",
             units = "CPU %",
             linestyle = "-",
             marker ="None",
             linewidth = 1,
             alpha = 0.5,
             color = "b",
             drawstyle = "steps-post"
             )

'''
Common Bidding Visuals
'''
VISUAL_SW = OutputVisual(
             xy_function = lambda dct: [(get_time_from_type(dct,PERF),  get_vals(dct[PERF], 'rev'))],
             x_label = "Time [m]",
             y_label = "SW",
             units = "K/s",
             linestyle = "-",
             marker = "None",
             linewidth = 3,
             alpha = 0.5,
             color = "r",
             drawstyle = "steps-post"
             )

VISUAL_P_VALUE = OutputVisual(
             xy_function = lambda dct: [(get_time_from_type(dct,POL),  get_vals(dct[POL], 'p'))],
             x_label = "Time [m]",
             y_label = "p",
             units = "value",
             linestyle = "-",
             marker ="None",
             linewidth = 2,
             alpha = 1,
             drawstyle = "steps-post"
             )

VISUAL_R_VALUE = OutputVisual(
             xy_function = lambda dct: [(get_time_from_type(dct,POL), [x[0][0] for x in get_vals(dct[POL], 'rq')])],
             x_label = "Time [m]",
             y_label = "r",
             units = "value",
             linestyle = "None",
             marker ="^",
             alpha = 0.5,
             drawstyle = "steps-post"
             )

VISUAL_Q_VALUE = OutputVisual(
             xy_function = lambda dct: [(get_time_from_type(dct,POL), [x[0][1] for x in get_vals(dct[POL], 'rq')])],
             x_label = "Time [m]",
             y_label = "q",
             units = "value",
             linestyle = "None",
             marker ="v",
             alpha = 0.5,
             drawstyle = "steps-post"
             )

VISUAL_BILL = OutputVisual(
             xy_function = lambda dct: [(get_time_from_type(dct,POL), get_vals(dct[POL], 'control_bill'))],
             x_label = "Time [m]",
             y_label = "Bill",
             units = "value",
             linestyle = "-",
             marker ="None",
             linewidth = 2,
             alpha = 0.5,
             color = "g",
             drawstyle = "steps-post"
             )

'''
MCD Extended Performance
'''
VISUAL_MCD_PERFORMANCE = VISUAL_PERFORMANCE_K # khits/s

VISUAL_MCD_TOTAL_GETS = OutputVisual(
             xy_function = lambda dct: [(get_time_from_type(dct,PERF), get_vals(get_vals(dct[PERF], 'perf'), 'get_total'))],
             x_label = "Time [m]",
             y_label = "Total Gets",
             linestyle = "-",
             marker = "None",
             linewidth = 2,
             alpha = 0.5,
             color = "y",
             drawstyle = "steps-post"
             )

VISUAL_MCD_TOTAL_GETS_FOR_BANDWIDTH = OutputVisual(
             xy_function = lambda dct: [
                                align_data_to_time_backwards([
                                    ( get_time_from_type(dct,MON),  [int(x) for x in get_vals(dct[MON], 'net_rate')] ),
                                    ( get_time_from_type(dct,PERF), get_vals(get_vals(dct[PERF], 'perf'), 'get_hits_total') )
                                ])[1]],
             x_label = "Bandwidth [bps]",
             y_label = "Total Gets",
             units = "OPS",
             #linestyle = "-",
             #marker = "None",
             #linewidth = 2,
             #alpha = 0.5,
             #color = "y",
             #drawstyle = "steps-post"
             )


VISUAL_MCD_TOTAL_HITS = OutputVisual(
             xy_function = lambda dct: [(get_time_from_type(dct,PERF), get_vals(get_vals(dct[PERF], 'perf'), "get_hits_total"))],
             x_label = "Time [m]",
             y_label = "Total Hits",
             linestyle = "-",
             marker = "None",
             linewidth = 2,
             alpha = 0.5,
             color = "r",
             drawstyle = "steps-post"
             )

VISUAL_MCD_TOTAL_MISSES = OutputVisual(
             xy_function = lambda dct: [(get_time_from_type(dct,PERF), get_vals(get_vals(dct[PERF], 'perf'), 'get_misses'))],
             x_label = "Time [m]",
             y_label = "Total Misses",
             linestyle = "-",
             marker = "None",
             linewidth = 2,
             alpha = 0.5,
             color = "c",
             drawstyle = "steps-post"
             )

VISUAL_MCD_MISS_RATE = OutputVisual(
             xy_function = lambda dct: [(get_time_from_type(dct,PERF), map(k_per_sec, get_vals(get_vals(dct[PERF], 'perf'), 'miss_rate')))],
             x_label = "Time [m]",
             y_label = "Miss Rate",
             linestyle = "--",
             marker = "None",
             linewidth = 2,
             alpha = 0.5,
             color = "b",
             drawstyle = "steps-post"
             )

VISUAL_MCD_HIT_RATE = VISUAL_PERFORMANCE

VISUAL_MCD_HIT_PERCENT = OutputVisual(
             xy_function = lambda dct: [(get_time_from_type(dct,PERF), get_vals(get_vals(dct[PERF], 'perf'), 'get_hits_percent'))],
             x_label = "Time [m]",
             y_label = "Hits Percent",
             linestyle = "-",
             marker = "None",
             linewidth = 2,
             alpha = 0.5,
             color = "b",
             drawstyle = "steps-post"
             )

VISUAL_MCD_THROUGHPUT = OutputVisual(
             xy_function = lambda dct: [(get_time_from_type(dct,PERF), get_vals(get_vals(dct[PERF], 'perf'), 'throughput'))],
             x_label = "Time [m]",
             y_label = "Throughput",
             linestyle = "-",
             marker = "None",
             linewidth = 2,
             alpha = 0.5,
             color = "b",
             drawstyle = "steps-post"
             )

'''
Common SQL Performance Visuals
'''
VISUAL_SQL_TPS_WITH_COMM = OutputVisual(
             xy_function = lambda dct: [(get_time_from_type(dct,PERF), get_vals(get_vals(dct[PERF], 'perf'), 'tps_with_connections_time'))],
             x_label = "Time [m]",
             y_label = "Performance (including comm)",
             units = "TPS",
             linestyle = "-",
             marker = "None",
             linewidth = 2,
             alpha = 0.5,
             drawstyle = "steps-post"
             )

VISUAL_SQL_TPS_WITHOUT_COMM = OutputVisual(
             xy_function = lambda dct: [(get_time_from_type(dct,PERF), get_vals(get_vals(dct[PERF], 'perf'), 'tps_without_connections_time'))],
             x_label = "Time [m]",
             y_label = "Performance (not including comm)",
             units = "TPS",
             linestyle = "--",
             marker = "None",
             linewidth = 2,
             alpha = 0.5,
             drawstyle = "steps-post"
             )

VISUAL_SQL_PERF_FOR_LOAD = OutputVisual(
             xy_function = lambda dct: [get_x_against_y_vals(get_vals(dct[PERF], 'load'), get_vals(get_vals(dct[PERF], 'perf'), 'tps_with_connections_time'))],
             x_label = "Load (clients)",
             y_label = "Performance",
             units = "TPS",
             #linestyle = "-",
             #marker = "None",
             #linewidth = 2,
             #alpha = 0.5
             )

VISUAL_SQL_PERF_FOR_MEM = OutputVisual(
             xy_function = lambda dct: [get_stable_x_against_y_vals(
                                *align_data_to_time_backwards([
                                    ( get_time_from_type(dct,MON),  get_vals(dct[MON], 'mem_available') ),
                                    ( get_time_from_type(dct,PERF), get_vals(get_vals(dct[PERF], 'perf'), 'tps_with_connections_time') )
                                ])[1])],
             x_label = "Available Memory (MB)",
             y_label = "Performance",
             units = "TPS",
             #linestyle = "-",
             #marker = "None",
             #linewidth = 2,
             #alpha = 0.5
             )

def VISUAL_SQL_QUERY_LATENCY(number):
    return OutputVisual(
             xy_function = lambda dct: [(get_time_from_type(dct,PERF), get_vals(get_vals(dct[PERF], 'perf'), 'query%02d_latency' % number))],
             x_label = "Time [m]",
             y_label = "Query %02d Latency" % number,
             units = "Milliseconds",
             linestyle = "--",
             marker = "None",
             linewidth = 2,
             alpha = 0.5,
             drawstyle = "steps-post",
             yscale = "log"
             )

def get_perf_to_bw_foreach_load_mcd(data):
    print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!test!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
    vms = data.itervms()

    load = []
    perf = []
    bw = []

    for vm in vms:
        cur_perf_time = get_time_from_type(data[vm],PERF)
        cur_load = get_vals(data[vm][PERF], 'load')
        cur_perf = get_vals(get_vals(data[vm][PERF], 'perf'), 'hits_rate')
        assert len(cur_load) == len(cur_perf) == len(cur_perf_time)

        cur_mon_time = get_time_from_type(data[vm],MON)
        cur_bw = get_vals(data[vm][MON], 'net_rate')

        cur_fixed_bw = [ 0 ] * len(cur_perf_time)

        mon_pos = 0

        for i in xrange(len(cur_perf_time)):
            if cur_perf_time[i] > cur_mon_time[mon_pos]:
                mon_pos += 1

            cur_fixed_bw[i] = cur_bw[mon_pos]

        load += cur_load
        perf += cur_perf
        bw += cur_fixed_bw

    res, iter_order = get_x_against_y_foreach_z_vals(bw, perf, load)

    named_iter_order = []
    named_res = {}

    for key in iter_order:
        named_key = "%s clients" % key
        named_res[named_key] = res[key]
        named_iter_order.append(named_key)

    print named_res
    print 
    print named_iter_order

    return named_res, named_iter_order


def get_perf_to_mem_foreach_load(data):
    vms = data.itervms()

    load = []
    perf = []
    mem = []

    for vm in vms:
        cur_perf_time = get_time_from_type(data[vm],PERF)
        cur_load = get_vals(data[vm][PERF], 'load')
        cur_perf = get_vals(get_vals(data[vm][PERF], 'perf'), 'tps_with_connections_time')
        assert len(cur_load) == len(cur_perf) == len(cur_perf_time)

        cur_mon_time = get_time_from_type(data[vm],MON)
        cur_mem = get_vals(data[vm][MON], 'mem_available')

        cur_fixed_mem = [ 0 ] * len(cur_perf_time)

        mon_pos = 0

        for i in xrange(len(cur_perf_time)):
            if cur_perf_time[i] > cur_mon_time[mon_pos]:
                mon_pos += 1

            cur_fixed_mem[i] = cur_mem[mon_pos]

        load += cur_load
        perf += cur_perf
        mem += cur_fixed_mem

    res, iter_order = get_x_against_y_foreach_z_vals(mem, perf, load)

    named_iter_order = []
    named_res = {}

    for key in iter_order:
        named_key = "%s clients" % key
        named_res[named_key] = res[key]
        named_iter_order.append(named_key)

    return named_res, named_iter_order

VISUAL_SQL_PERF_TO_MEM_FOREACH_LOAD = GlobalVisual(
             xy_function = get_perf_to_mem_foreach_load,
             x_label = "Available Memory (MB)",
             y_label = "Performance",
             units = "TPS",)

VISUAL_MCD_PERF_TO_BW_FOREACH_LOAD = GlobalVisual(
             xy_function = get_perf_to_bw_foreach_load_mcd,
             x_label = "Available Bandwidth (bps)",
             y_label = "Performance",
             units = "Hits per sec",)
