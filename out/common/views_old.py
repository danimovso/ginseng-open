'''
Common Views

@author: Liran Funaro <fonaro@cs.technion.ac.il>
'''

from OutputView import VMsView, CPUView
from out.common.visuals import *

VIEW_CPU = CPUView(name = "cpu-view")

VIEW_PRQ = VMsView(
               name = "prq-view",
               visuals = [VISUAL_P_VALUE, VISUAL_EXTRA_MEMORY, VISUAL_R_VALUE,
                          VISUAL_Q_VALUE, VISUAL_BILL],
               size = (20,8))

VIEW_BANDWIDTH_RATE = VMsView(
               name = "net-rate-view",
               visuals = [VISUAL_BANDWIDTH_RATE])

VIEW_MEMORY_PGFAULTS = VMsView(
               name = "mem-pgfaults-view",
               visuals = [VISUAL_MEMORY,
                          VISUAL_PG_FAULT_MAJOR_PER_MIN,
                          VISUAL_PG_FAULT_MINOR_PER_MIN])

VIEW_MEMORY_SWAP = VMsView(
               name = "mem-swap-view",
               visuals = [VISUAL_MEMORY,
                          VISUAL_PG_FAULT_MAJOR_PER_MIN,
                          VISUAL_PG_FAULT_MINOR_PER_MIN,
                          VISUAL_MEMORY_SWAP_OUT_PER_MIN,
                          VISUAL_MEMORY_SWAP_IN_PER_MIN,
                          VISUAL_MEMORY_PAGE_OUT_PER_MIN,
                          VISUAL_MEMORY_PAGE_IN_PER_MIN])

VIEW_MEMORY_SWAP_OLD = VMsView(
               name = "mem-swap-old-view",
               visuals = [VISUAL_MEMORY,
                          VISUAL_PG_FAULT_MAJOR_PER_MIN_OLD,
                          VISUAL_PG_FAULT_MINOR_PER_MIN_OLD,
                          VISUAL_MEMORY_SWAP_OUT_PER_MIN_OLD,
                          VISUAL_MEMORY_SWAP_IN_PER_MIN_OLD])

VIEW_MEMORY_SUMMERY = VMsView(
               name = "mem-summery-view",
               visuals=[VISUAL_MEMORY, VISUAL_MEMORY_UNUSED, VISUAL_MEMORY_FREE,
                        VISUAL_MEMORY_CACHE_BUFF])

VIEW_PYTHON = VMsView(
               name = "python-view",
               visuals = [VISUAL_MEMORY, VISUAL_PYTHON1,
                          VISUAL_PYTHON2])

VIEW_TRACE = VMsView(
                name = "exp-view",
                visuals = [VISUAL_LOAD, VISUAL_SW, VISUAL_PERFORMANCE_K,
                           VISUAL_MEMORY, VISUAL_PG_FAULT_MAJOR_PER_MIN])

VIEW_PERFORMANCE = VMsView(
               name = "perf-view",
               visuals = [VISUAL_MCD_TOTAL_GETS, VISUAL_MCD_TOTAL_HITS,
               VISUAL_MCD_TOTAL_MISSES, VISUAL_MCD_HIT_PERCENT,
               VISUAL_PG_FAULT_MAJOR_PER_MIN])

VIEW_MCD_PREFORMANCE = VMsView(
               name = "mcd-perf",
               visuals = [VISUAL_MCD_TOTAL_HITS, VISUAL_BANDWIDTH_RATE,
                          VISUAL_LOAD_U("Clients")])

VIEW_MCD_HIT_RATE = VMsView(
               name = "mcd-throughput",
               visuals = [VISUAL_MCD_HIT_RATE, VISUAL_BANDWIDTH_RATE,
                          VISUAL_LOAD_U("Clients")])

VIEW_MCD_THROUGHPUT = VMsView(
               name = "mcd-throughput",
               visuals = [VISUAL_MCD_THROUGHPUT, VISUAL_BANDWIDTH_RATE,
                          VISUAL_LOAD_U("Clients")])

VIEW_MCD_PERF_FOR_BW = VMsView(
               name = "mcd-perf-bw",
               visuals = [VISUAL_MCD_TOTAL_GETS_FOR_BANDWIDTH])

VIEW_MCD_PERF_FOR_BW_COMPARE = VMsView(
               name = "mcd-perf-bw-compare-view",
               visuals = [],
               compare_visuals = [VISUAL_MCD_TOTAL_GETS_FOR_BANDWIDTH])

VIEW_MCD_PERF_TO_BW_FOREACH_LOAD = VMsView(
               name = "mcd-performance-to-bw-foreach-load-view",
               visuals = [VISUAL_MCD_PERF_TO_BW_FOREACH_LOAD],
               compare_visuals = [])

VIEW_MCD_PERF_TO_BW_FOREACH_LOAD_COMPARE = VMsView(
               name = "mcd-performance-to-bw-foreach-load-compare-view",
               visuals = [],
               compare_visuals = [VISUAL_MCD_PERF_TO_BW_FOREACH_LOAD])

##########################################################
#
VIEW_SQL_EVALUATION = VMsView(
               name = "sql-evaluation-view",
               visuals = [VISUAL_SQL_TPS_WITH_COMM, VISUAL_MEMORY,
                          VISUAL_PG_FAULT_MAJOR_PER_MIN, VISUAL_LOAD_U("Clients")])

VIEW_SQL_PERFORMANCE = VMsView(
               name = "sql-performance-view",
               visuals = [VISUAL_SQL_TPS_WITH_COMM, VISUAL_SQL_TPS_WITHOUT_COMM,
                          VISUAL_MEMORY, VISUAL_LOAD_U("Clients")])

VIEW_SQL_PERF_FOR_LOAD = VMsView(
               name = "sql-performance-for-load-view",
               visuals = [VISUAL_SQL_PERF_FOR_LOAD])

VIEW_SQL_PERF_FOR_LOAD_COMPARE = VMsView(
               name = "sql-performance-for-load-compare-view",
               visuals = [],
               compare_visuals = [VISUAL_SQL_PERF_FOR_LOAD])

VIEW_SQL_PERF_FOR_MEM = VMsView(
               name = "sql-performance-for-mem-view",
               visuals = [VISUAL_SQL_PERF_FOR_MEM])

VIEW_SQL_PERF_FOR_MEM_COMPARE = VMsView(
               name = "sql-performance-for-mem-compare-view",
               visuals = [],
               compare_visuals = [VISUAL_SQL_PERF_FOR_MEM])

VIEW_SQL_PERF_TO_MEM_FOREACH_LOAD = VMsView(
               name = "sql-performance-to-mem-foreach-load-view",
               visuals = [VISUAL_SQL_PERF_TO_MEM_FOREACH_LOAD],
               compare_visuals = [])

VIEW_SQL_PERF_TO_MEM_FOREACH_LOAD_COMPARE = VMsView(
               name = "sql-performance-to-mem-foreach-load-compare-view",
               visuals = [],
               compare_visuals = [VISUAL_SQL_PERF_TO_MEM_FOREACH_LOAD])

VIEW_SQL_COMPARE_PERFORMANCE = VMsView(
               name = "sql-compare-performance-view",
               visuals = [VISUAL_LOAD_U("Clients")],
               compare_visuals = [VISUAL_SQL_TPS_WITH_COMM])

VIEW_SQL_QUERIES = VMsView(
               name = "sql-queries-view",
               visuals = [VISUAL_SQL_QUERY_LATENCY(n) for n in range(1,15)] + [VISUAL_MEMORY, VISUAL_LOAD_U("Clients")],
               size = (40,8))