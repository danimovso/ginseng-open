'''
Created on Mar 19, 2013

@author: eyal
'''
from mom.Adviser import get_adviser
from sim.simulate import Guest, simulate
import os
from sim.sim_optimized import sim_optimize
from exp.core import Loads
from numpy import array

def calc_best_valuation(loads, rev_funcs,
                        profiler_file,
                        profiler_entry,
                        base_mem,
                        load_interval,
                        optimization_res = 100,
                        total_mem = 10012,
                        auction_time = 12,
                        rounds = 60 * 60 / 12
                        ):

    nums = sorted(rev_funcs.keys())

    opt = {}
    ginseng = {}
    no_mem_limit = {}
    static = {}
    for n in nums:
        guests = []
        for i, vm in enumerate(rev_funcs[n].keys()):
            adviser_args = {
                "name": "AdviserProfit",
                "profiler": profiler_file,
                "base_mem": base_mem,
                "entry": profiler_entry,
                "memory_delta": 10,
                "rev_func": eval(rev_funcs[n][vm]),
                }
            g = Guest(name = vm,
                      adviser = get_adviser(**adviser_args),
                      base_mem = adviser_args["base_mem"],
                      load_func = loads[n][vm],
                      load_interval = load_interval)
            guests.append(g)

        mem_available = total_mem - n * 55 - 500
#
#        opt[n] = sim_optimize(guests,
#                       rounds = rounds,
#                       total_mem = mem_available,
#                       d_mem = optimization_res,
#                       max_vm_mem = 2000,
#                       min_vm_mem = base0,
#                       dt = auction_time)

        ginseng[n] = simulate(guests = guests,
                           alpha = 1,
                           p0 = 0,
                           mem_available = mem_available,
                           rounds = rounds,
                           dt = auction_time)

#        no_mem_limit[n] = simulate(guests = guests,
#                           alpha = 1,
#                           mem_available = 2500 * n,
#                           rounds = rounds,
#                           dt = auction_time)

#        static[n] = simulate_static(guests = guests,
#                           alpha = 1,
#                           mem_available = mem_available,
#                           rounds = rounds,
#                           dt = auction_time)

#        file("/home/eyal/moc-output/opt-%i.txt" % n, "w").write(str(opt[n]))
    return opt, ginseng, no_mem_limit, static

if __name__ == '__main__':

    name = "mcd_v_e_p"
    user_dir = os.path.expanduser("~")

    profiler_file = "%s/workspace/moc/doc/profiler-memcached-inside-spare-50-win-500k.xml" % user_dir
    profiler_entry = "hits_rate"
    rev_funcs = eval("".join(file("%s/moc-output/%s_valuations.txt" % (user_dir, name))))
    loads = rev_funcs = eval("".join(file("%s/moc-output/%s_trace_load.txt" % (user_dir, name))))

    opt_results, ginseng_results, no_mem_limit, static = calc_best_valuation(loads, rev_funcs,
                                         optimization_res = 10, base_mem = 600,
                                         profiler_file = profiler_file,
                                         profiler_entry = profiler_entry,
                                         load_interval = 200
                                         )
    file(os.path.join("%s/moc-output/" % user_dir, "%s_opt_results.txt" %  name), "w").write(str(opt_results))
    file(os.path.join("%s/moc-output/" % user_dir, "%s_ginseng_results.txt" % name), "w").write(str(ginseng_results))
#    file(os.path.join("%s/moc-output/" % user_dir, "mcd%i_no_mem_limit.txt" % num), "w").write(str(no_mem_limit))
#    file(os.path.join("%s/moc-output/" % user_dir, "mcd%i_static.txt" % num), "w").write(str(static))
#    profiler_file = "%s/workspace/moc/doc/profiler-mc-spare-50-satur-2000.xml" % user_dir
#    profiler_entry = "hits_rate"
#    loads, rev_funcs = eval("".join(file("%s/moc-output/mc_loads_valuations.txt" % user_dir)))
#    opt_results, ginseng_results, no_mem_limit = calc_best_valuation(loads, rev_funcs,
#                                         optimization_res = 10, base_mem = 600,
#                                         profiler_file = profiler_file,
#                                         profiler_entry = profiler_entry,
#                                         load_interval = 200
#                                         )
#    file(os.path.join("%s/moc-output/" % user_dir , "mc_opt_results.txt"), "w").write(str(opt_results))
#    file(os.path.join("%s/moc-output/" % user_dir, "mc_ginseng_results.txt"), "w").write(str(ginseng_results))
#    file(os.path.join("%s/moc-output/" % user_dir, "mc_no_mem_limit.txt"), "w").write(str(no_mem_limit))
