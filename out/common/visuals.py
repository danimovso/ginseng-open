'''
Common Visuals

@author: Liran Funaro <fonaro@cs.technion.ac.il>
'''
from out.common.OutputVisual import OutputVisual, GlobalVisual, GlobalVMsVisual
from out.common.common import get_vals, k_per_sec, extra_mem, \
    get_vals_change_per_time, get_time_from_type, get_x_against_y_vals, \
    get_x_against_y_foreach_z_vals, \
    align_data_to_time_backwards, get_stable_x_against_y_vals, get_perf, \
    get_processed_vals, get_time_and_vals, get_revenue
from mom.Plotter import PERF, MON, POL, HOST
import numpy as np
from scipy.interpolate.interpolate import interp1d
from mom.Advisers.ValuationFunction import ValuationFunction
from numpy import argmin
from scipy import interp
from _collections import defaultdict

def get_visual(visuals_names):
    visuals = []
    unknown = []
    all_visuals = globals()

    for v in visuals_names:
        cur_visual = v.upper().replace("-", "_")

        if cur_visual in all_visuals:
            visuals.append(all_visuals[cur_visual])
            continue

        cur_visual = "VISUAL_" + cur_visual
        if cur_visual in all_visuals:
            visuals.append(all_visuals[cur_visual])
            continue

        unknown.append(v)

    return visuals, unknown

def VISUAL_LOAD_U(units=None):
    return OutputVisual(
             xy_function=lambda dct: [(get_time_from_type(dct, PERF), get_vals(dct[PERF], 'load'))],
             x_label="Time [m]",
             y_label="Load",
             units=units,
             linestyle="-",
             marker="None",
             linewidth=2,
             alpha=0.5,
             drawstyle="steps-post"
             )

VISUAL_LOAD = VISUAL_LOAD_U()

'''
Hitrate performance
'''
VISUAL_PERFORMANCE = OutputVisual(
             xy_function=lambda dct: [(get_time_from_type(dct, PERF), get_vals(get_vals(dct[PERF], 'perf'), "hits_rate"))],
             x_label="Time [m]",
             y_label="Performance",
             units="Khits/s",
             linestyle="--",
             marker="None",
             linewidth=2,
             alpha=0.5,
             color="r",
             drawstyle="steps-post"
             )

VISUAL_PERFORMANCE_K = OutputVisual(
             xy_function=lambda dct: [(get_time_from_type(dct, PERF), map(k_per_sec,
                                          get_vals(get_vals(dct[PERF], 'perf'),
                                                   "hits_rate")))],
             x_label="Time [m]",
             y_label="Performance",
             units="Khits/s",
             linestyle="--",
             marker="None",
             linewidth=2,
             alpha=0.5,
             color="r",
             drawstyle="steps-post"
             )

'''
Common Bandwidth Visuals
'''

VISUAL_BANDWIDTH_RATE = OutputVisual(
             xy_function=lambda dct: [(get_time_from_type(dct, MON), get_vals(dct[MON], 'net_rate'))],

             x_label="Time [m]",
             y_label="Bandwidth Rate",
             units="bps",
             linestyle="-",
             marker="None",
             linewidth=2,
             alpha=0.5,
             drawstyle="steps-post"
             )

'''
Common Cache Visuals
'''

VISUAL_LLC_ALLOC_WITHOUT_PIT = OutputVisual(
             xy_function=lambda dct: [get_time_and_vals(dct, MON, 'cache_alloc_size_without_pit')],

             x_label="Time [m]",
             y_label="LLC Allocation without PIT",
             units="ways",
             linestyle="-",
             marker="None",
             linewidth=2,
             alpha=0.5,
             drawstyle="steps-post"
             )

VISUAL_LLC_ALLOC_WITH_PIT = OutputVisual(
             xy_function=lambda dct: [get_time_and_vals(dct, MON, 'cache_alloc_size')],

             x_label="Time [m]",
             y_label="LLC Allocation with PIT",
             units="ways",
             linestyle="-",
             marker="None",
             linewidth=2,
             alpha=0.5,
             drawstyle="steps-post"
             )

VISUAL_LLC_ALLOC_WITH_OR_WITHOUT_PIT = OutputVisual(
             xy_function=lambda dct: [cache_alloc_with_or_with_out_pit(dct, MON)],

             x_label="Time [m]",
             y_label="LLC Allocation without PIT",
             units="ways",
             linestyle="-",
             marker="None",
             linewidth=2,
             alpha=0.5,
             drawstyle="steps-post"
             )

VISUAL_LLC_COS = OutputVisual(
             xy_function=lambda dct: [get_time_and_vals(dct, MON, 'cache_cos')],

             x_label="Time [m]",
             y_label="LLC COS",
             units="choice",
             linestyle="-",
             marker="None",
             linewidth=2,
             alpha=0.5,
             drawstyle="steps-post"
             )

def cache_alloc_with_or_with_out_pit(dct, plot_type):
    two, vwo = get_time_and_vals(dct, MON, 'cache_alloc_size_without_pit')
    tw, vw = get_time_and_vals(dct, MON, 'cache_alloc_size')

    assert len(two) == len(tw)

    for i in xrange(len(tw)):
        if vw[i] == 2:
            vw[i] = vwo[i]

    return tw, vw

BYTES_TO_MB = lambda x: float(x) / float(1 << 20) if x is not None else None

VISUAL_LLC_OCCUPENCY = OutputVisual(
             xy_function=lambda dct: [get_time_and_vals(dct, MON, 'llc_occupency', proccess_func=BYTES_TO_MB)],

             x_label="Time [m]",
             y_label="LLC Occupency",
             units="MB",
             linestyle="-",
             marker="None",
             linewidth=2,
             alpha=0.5,
             )

VISUAL_LLC_ALLOC = OutputVisual(
             xy_function=lambda dct: [get_time_and_vals(dct, MON, 'cache_alloc')],

             x_label="Time [m]",
             y_label="LLC Alloc",
             units="Ways",
             linestyle="-",
             marker="None",
             linewidth=2,
             alpha=0.5,
             )

VISUAL_LLC_LOCAL_EXTERNAL_BANDWIDTH = OutputVisual(
             xy_function=lambda dct: [get_time_and_vals(dct, MON, 'llc_local_external_bandwidth', proccess_func=BYTES_TO_MB)],

             x_label="Time [m]",
             y_label="LLC Local External Bandwidth",
             units="MB(1)",
             linestyle="-",
             marker="None",
             linewidth=2,
             alpha=0.5,
             )

VISUAL_LLC_TOTAL_EXTERNAL_BANDWIDTH = OutputVisual(
             xy_function=lambda dct: [get_time_and_vals(dct, MON, 'llc_total_external_bandwidth', proccess_func=BYTES_TO_MB)],

             x_label="Time [m]",
             y_label="LLC Total External Bandwidth",
             units="MB(1)",
             linestyle="-",
             marker="None",
             linewidth=2,
             alpha=0.5,
             )

'''
Common Memory Visuals
'''
VISUAL_MEMORY = OutputVisual(
             xy_function=lambda dct: [(get_time_from_type(dct, MON), get_vals(dct[MON], 'mem_available'))],

             x_label="Time [m]",
             y_label="Available Memory",
             units="MB",
             linestyle="-",
             marker="None",
             linewidth=2,
             alpha=0.5,
             drawstyle="steps-post"
             )

VISUAL_MEMORY_ANON_PAGES = OutputVisual(
             xy_function=lambda dct: [(get_time_from_type(dct, MON), get_vals(dct[MON], 'anon_pages'))],
             x_label="Time [m]",
             y_label="Anonymous Pages",
             units="MB",
             linestyle="-",
             marker="None",
             linewidth=2,
             alpha=0.5
             )

VISUAL_MEMORY_UNUSED = OutputVisual(
             xy_function=lambda dct: [(get_time_from_type(dct, MON), get_vals(dct[MON], 'mem_unused'))],
             x_label="Time [m]",
             y_label="Unused Memory",
             units="MB",
             linestyle="-",
             marker="None",
             linewidth=2,
             alpha=0.5
             )

VISUAL_MEMORY_FREE = OutputVisual(
             xy_function=lambda dct: [(get_time_from_type(dct, MON), get_vals(dct[MON], 'mem_free'))],
             x_label="Time [m]",
             y_label="Free Memory",
             units="MB",
             linestyle="-",
             marker="None",
             linewidth=2,
             alpha=0.5
             )

VISUAL_MEMORY_CACHE_BUFF = OutputVisual(
             xy_function=lambda dct: [(get_time_from_type(dct, MON), get_vals(dct[MON], 'cache_and_buff'))],
             x_label="Time [m]",
             y_label="Cache and Buffer",
             units="MB",
             linestyle="-",
             marker="None",
             linewidth=2,
             alpha=0.5
             )

VISUAL_MEMORY_QEMU_RSS = OutputVisual(
             xy_function=lambda dct: [(get_time_from_type(dct, MON), get_vals(dct[MON], 'rss'))],
             x_label="Time [m]",
             y_label="Resident set size",
             units="MB",
             linestyle="-",
             marker="None",
             linewidth=2,
             alpha=0.5
             )

VISUAL_EXTRA_MEMORY = OutputVisual(
             xy_function=lambda dct: [(get_time_from_type(dct, POL), extra_mem(dct))],
             x_label="Time [m]",
             y_label="Extra Memory",
             units="MB",
             linestyle="-",
             marker="None",
             linewidth=2,
             alpha=0.5,
             color="r",
             drawstyle="steps-post"
             )

VISUAL_MEMORY_BASE = OutputVisual(
             xy_function=lambda dct: [(get_time_from_type(dct, POL), get_vals(dct[POL], 'mem_base'))],
             x_label="Time [m]",
             y_label="Base Memory",
             units="MB",
             linestyle="None",
             linewidth=2,
             marker="o",
             alpha=0.5,
             drawstyle="steps-post"
             )

def _VISUAL_PG_FAULT_MAJOR_PER_MIN(old=False):
    return OutputVisual(
             xy_function=lambda dct: [(get_time_from_type(dct, MON), get_vals_change_per_time(dct, MON, 'major_fault', old=old))],
             x_label="Time [m]",
             y_label="Major Faults",
             units="Faults/min.",
             linestyle="-",
             marker="None",
             linewidth=2,
             alpha=0.5,
             yscale="symlog",
             drawstyle="default"
             )

VISUAL_PG_FAULT_MAJOR_PER_MIN = _VISUAL_PG_FAULT_MAJOR_PER_MIN(False)
VISUAL_PG_FAULT_MAJOR_PER_MIN_OLD = _VISUAL_PG_FAULT_MAJOR_PER_MIN(True)

def _VISUAL_PG_FAULT_MINOR_PER_MIN(old=False):
    return OutputVisual(
             xy_function=lambda dct: [(get_time_from_type(dct, MON), get_vals_change_per_time(dct, MON, 'minor_fault', old=old))],
             x_label="Time [m]",
             y_label="Minor Faults",
             units="Faults/min.",
             linestyle="-",
             marker="None",
             linewidth=2,
             alpha=0.5,
             yscale="symlog",
             drawstyle="default"
             )

VISUAL_PG_FAULT_MINOR_PER_MIN = _VISUAL_PG_FAULT_MINOR_PER_MIN(False)
VISUAL_PG_FAULT_MINOR_PER_MIN_OLD = _VISUAL_PG_FAULT_MINOR_PER_MIN(True)

def _VISUAL_MEMORY_SWAP_OUT_PER_MIN(old=False):
    return OutputVisual(
             xy_function=lambda dct: [(get_time_from_type(dct, MON), get_vals_change_per_time(dct, MON, 'swap_out', old=old))],
             x_label="Time [m]",
             y_label="Swapped out Pages",
             units="Pages/min.",
             linestyle="-",
             marker="None",
             linewidth=2,
             alpha=0.5,
             yscale="symlog",
             drawstyle="default"
             )

VISUAL_MEMORY_SWAP_OUT_PER_MIN = _VISUAL_MEMORY_SWAP_OUT_PER_MIN(False)
VISUAL_MEMORY_SWAP_OUT_PER_MIN_OLD = _VISUAL_MEMORY_SWAP_OUT_PER_MIN(True)

def _VISUAL_MEMORY_SWAP_IN_PER_MIN(old=False):
    return OutputVisual(
             xy_function=lambda dct: [(get_time_from_type(dct, MON), get_vals_change_per_time(dct, MON, 'swap_in', old=old))],
             x_label="Time [m]",
             y_label="Swapped in Pages",
             units="Pages/min.",
             inestyle="-",
             marker="None",
             linewidth=2,
             alpha=0.5,
             yscale="symlog",
             drawstyle="default"
             )

VISUAL_MEMORY_SWAP_IN_PER_MIN = _VISUAL_MEMORY_SWAP_IN_PER_MIN(False)
VISUAL_MEMORY_SWAP_IN_PER_MIN_OLD = _VISUAL_MEMORY_SWAP_IN_PER_MIN(True)

VISUAL_MEMORY_PAGE_OUT_PER_MIN = OutputVisual(
             xy_function=lambda dct: [(get_time_from_type(dct, MON), get_vals_change_per_time(dct, MON, 'page_out', old=False))],
             x_label="Time [m]",
             y_label="Paged out Pages",
             units="Pages/min.",
             inestyle="-",
             marker="None",
             linewidth=2,
             alpha=0.5,
             yscale="symlog",
             drawstyle="default"
             )

VISUAL_MEMORY_PAGE_IN_PER_MIN = OutputVisual(
             xy_function=lambda dct: [(get_time_from_type(dct, MON), get_vals_change_per_time(dct, MON, 'page_in', old=False))],
             x_label="Time [m]",
             y_label="Paged in Pages",
             units="Pages/min.",
             inestyle="-",
             marker="None",
             linewidth=2,
             alpha=0.5,
             yscale="symlog",
             drawstyle="default"
             )

VISUAL_PYTHON1 = OutputVisual(
             xy_function=lambda dct: [(get_time_from_type(dct, MON), get_vals(dct[MON], 'python1_usage'))],
             x_label="Time [m]",
             y_label="Python1",
             units="CPU %",
             linestyle="-",
             marker="None",
             linewidth=2,
             alpha=0.5,
             drawstyle="steps-post"
             )

VISUAL_PYTHON2 = OutputVisual(
             xy_function=lambda dct: [(get_time_from_type(dct, MON), get_vals(dct[MON], 'python2_usage'))],
             x_label="Time [m]",
             y_label="Python2",
             units="CPU %",
             linestyle="-",
             marker="None",
             linewidth=2,
             alpha=0.5,
             drawstyle="steps-post"
             )

VISUAL_CPU_USAGE = OutputVisual(
             xy_function=lambda dct: [(get_time_from_type(dct, MON), get_vals(dct[MON], 'cpu_usage'))],
             x_label="Time [m]",
             y_label="CPU usage",
             units="CPU %",
             linestyle="-",
             marker="None",
             linewidth=1,
             alpha=0.5,
             color="b",
             drawstyle="steps-post"
             )

VISUAL_CPU_COUNT = OutputVisual(
             xy_function=lambda dct: [get_time_and_vals(dct, MON, 'cpu-count')],
             x_label="Time [m]",
             y_label="CPU Count",
             units="CPUs",
             linestyle="-",
             marker="None",
             linewidth=1,
             alpha=0.5,
             drawstyle="steps-post"
             )

'''
Common Bidding Visuals
'''
VISUAL_SW = OutputVisual(
             xy_function=lambda dct: [(get_time_from_type(dct, PERF), get_vals(dct[PERF], 'rev'))],
             x_label="Time [m]",
             y_label="SW",
             units="$/s",
             linestyle="-",
             marker="None",
             linewidth=3,
             alpha=0.5,
            color="r",
            drawstyle="steps-post"
             )

VISUAL_VALUATION = OutputVisual(
             xy_function=lambda dct: [get_time_and_vals(dct, MON, "cache", 'valuation')],
             x_label="Time [m]",
             y_label="Revenue Function",
             units="ValuationFunction",
             linestyle="-",
             marker="None",
             linewidth=2,
             alpha=0.5,
             drawstyle="steps-post"
             )

VISUAL_P_VALUE = OutputVisual(
             xy_function=lambda dct: [(get_time_from_type(dct, POL), get_vals(dct[POL], 'bid_p_mem'))],
             x_label="Time [m]",
             y_label="p",
             units="p value",
             linestyle="-",
             marker="None",
             linewidth=2,
             alpha=1,
             drawstyle="steps-post"
             )

VISUAL_R_VALUE = OutputVisual(
             xy_function=lambda dct: [(get_time_from_type(dct, POL), [x[0][0] for x in get_vals(dct[POL], 'bid_ranges_mem')])],
             x_label="Time [m]",
             y_label="r",
             units="value",
             linestyle="None",
             marker="^",
             alpha=0.5,
             drawstyle="steps-post"
             )

VISUAL_Q_VALUE = OutputVisual(
             xy_function=lambda dct: [(get_time_from_type(dct, POL), [x[0][1] for x in get_vals(dct[POL], 'bid_ranges_mem')])],
             x_label="Time [m]",
             y_label="q",
             units="value",
             linestyle="None",
             marker="v",
             alpha=0.5,
             drawstyle="steps-post"
             )

VISUAL_BILL = OutputVisual(
             xy_function=lambda dct: [(get_time_from_type(dct, POL), get_vals(dct[POL], 'control_bill_mem'))],
             x_label="Time [m]",
             y_label="Bill",
             units="value",
             linestyle="-",
             marker="None",
             linewidth=2,
             alpha=0.5,
             color="g",
             drawstyle="steps-post"
             )

VISUAL_SP = OutputVisual(
             xy_function=lambda dct: [(get_time_from_type(dct, POL), get_vals(dct[POL], 'side_payments'))],
             x_label="Time [m]",
             y_label="Side Payments",
             units="$",
             linestyle="-",
             marker="None",
             linewidth=2,
             alpha=1,
             drawstyle="steps-post"
             )


# VISUAL_VALUATION = OutputVisual(
#     xy_function = lambda dct: [(get_time_from_type(dct, PERF),
#                                 get_vals(dct[PERF], "rev"))],
#     x_label = "Time [m]",
#     y_label = "Valuation",
#     units = "k$/sec",
#     linestyle = "-",
#     marker = "None",
#     linewidth = 2,
#     alpha = 0.5,
#     color = "b",
#     drawstyle = "steps-post"
# )

VISUAL_REVENUE_NEGOTIATIONS = OutputVisual(
    xy_function=lambda dct: [(get_time_from_type(dct, PERF), get_revenue(dct))],
    x_label="Time [m]",
    y_label="Valuation",
    units="$/sec",
    linestyle="-",
    marker="None",
    linewidth=2,
    alpha=0.5,
    color="b",
    drawstyle="steps-post"
)

###################################################################3
# Cache Auction Visuals
###################################################################3
VISUAL_CACHE_EXPECTED_SW = OutputVisual(
             xy_function=lambda dct: [ {"xyz_tup": get_time_and_vals(dct, POL, 'cache-results-sw'),
                                          "legend_label": "Avg: %s" % np.average(get_vals(dct[POL], 'cache-results-sw')) } ],
             x_label="Time [m]",
             y_label="Expected Social Welfare",
             units="$/s",
             legend_label="SW",
             linestyle="-",
             marker="o",
             linewidth=3,
             alpha=0.5,
             is_host=True,
#              color = "r",
#              drawstyle = "steps-post"
             )

VISUAL_CACHE_BILL = OutputVisual(
             xy_function=lambda dct: [get_time_and_vals(dct, POL, 'cache-control-bill')],
             x_label="Time [m]",
             y_label="Bill",
             units="$/s",
             linestyle="-",
             marker="None",
             linewidth=2,
             alpha=0.5,
             drawstyle="steps-post"
             )

VISUAL_CACHE_ALLOC = OutputVisual(
             xy_function=lambda dct: [get_time_and_vals(dct, POL, 'cache-control-alloc')],
             x_label="Time [m]",
             y_label="LLC Allocation without PIT",
             units="ways",
             linestyle="-",
             marker="None",
             linewidth=2,
             alpha=0.5,
             drawstyle="steps-post"
             )

VISUAL_CACHE_REV = OutputVisual(
             xy_function=lambda dct: [get_revenues(get_time_and_vals(dct, PERF, 'performance'),
                                                     get_time_and_vals(dct, MON, "cache", 'valuation'))],
             x_label="Time [m]",
             y_label="Revenue",
             units="$/s",
             linestyle="-",
             marker="None",
             linewidth=2,
             alpha=0.5,
             drawstyle="steps-post"
             )

VISUAL_CACHE_EXPECTED_REV = OutputVisual(
             xy_function=lambda dct: [get_revenues(get_time_and_vals(dct, MON, "cache", 'estimated-performance'),
                                                     get_time_and_vals(dct, MON, "cache", 'valuation'))],
             x_label="Time [m]",
             y_label="Expected Revenue",
             units="$/s",
             linestyle="-",
             marker="None",
             linewidth=2,
             alpha=0.5,
             drawstyle="steps-post"
             )

VISUAL_CACHE_PROFIT = OutputVisual(
             xy_function=lambda dct: [get_profits(get_time_and_vals(dct, PERF, 'performance'),
                                                     get_time_and_vals(dct, MON, "cache", 'valuation'),
                                                     get_time_and_vals(dct, POL, 'cache-control-bill')
                                                     )],
             x_label="Time [m]",
             y_label="Profit",
             units="$/s",
             linestyle="-",
             marker="None",
             linewidth=2,
             alpha=0.5,
             drawstyle="steps-post"
             )

VISUAL_CACHE_EXPECTED_PROFIT = OutputVisual(
             xy_function=lambda dct: [get_profits(get_time_and_vals(dct, MON, "cache", 'estimated-performance'),
                                                     get_time_and_vals(dct, MON, "cache", 'valuation'),
                                                     get_time_and_vals(dct, POL, 'cache-control-bill')
                                                     )],
             x_label="Time [m]",
             y_label="Profit",
             units="$/s",
             linestyle="-",
             marker="None",
             linewidth=2,
             alpha=0.5,
             drawstyle="steps-post"
             )

def allign_times_numbers(main_time, t, d):
    return interp1d(t, d, kind='nearest')(main_time)

def joined_times(*params):
    assert len(params) % 2 == 0
    count = len(params) / 2

    t = []
    d = []
    p = []
    l = []
    for i in xrange(len(params) / 2):
        t.append(params[i * 2])
        d.append(params[i * 2 + 1])
        p.append(0)
        l.append(None)

    joined_time = []
    joined_d = [ [] for i in xrange(count) ]

    while any(p[i] < len(t[i]) for i in xrange(count)):
        cur_t = min([ t[i][p[i]] if p[i] < len(t[i]) else float("inf") for i in xrange(count) ])
        for i in xrange(count):
            if  p[i] < len(t[i]) and t[i][p[i]] == cur_t:
                if d[i][p[i]] is not None:
                    l[i] = d[i][p[i]]
                p[i] += 1

        if all(l[i] is not None for i in xrange(count)):
            joined_time.append(cur_t)
            for i in xrange(count):
                joined_d[i].append(l[i])

    return joined_time, joined_d

def get_valuation_functions(valuations_str):
    for v in valuations_str:
        try: res = ValuationFunction(v)
        except: res = None
        yield res

def get_revenues(time_perf, time_valuation):
    perf_time, perf = time_perf
    valuation_time, valuations_str = time_valuation

    valuations = list(get_valuation_functions(valuations_str))
    t, (perf, valuations) = joined_times(perf_time, perf, valuation_time, valuations)
    return t, [ v(p) for p, v in zip(perf, valuations)]

def get_profits(time_perf, time_valuation, time_bill):
    perf_time, perf = time_perf
    valuation_time, valuations_str = time_valuation
    bill_time, bills = time_bill

    valuations = list(get_valuation_functions(valuations_str))

    t, (perf, valuations, bills) = joined_times(perf_time, perf, valuation_time, valuations, bill_time, bills)
    return t, [ v(p) - b for p, v, b in zip(perf, valuations, bills)]

def get_sub_iter_order(iter_order):
    if not isinstance(iter_order[0], tuple):
        return {None: iter_order}

    sub_order = defaultdict(lambda: [])
    for key in iter_order:
        sub_order[key[0]].append(key)

    return sub_order

def get_real_sw(all_raw_xys, iter_order):
    t_max = 0

    for k in iter_order:
        t = all_raw_xys[k][0][0]
        t_max = max(t[-1], t_max)

    new_t = np.linspace(0, t_max, t_max * 60 * 10)

    sub_order = get_sub_iter_order(iter_order)

    result = {}
    new_iter_order = []
    for sub_key, sub_iter in sub_order.iteritems():
        new_d = {}
        for k in sub_iter:
            new_d[k] = interp(new_t, all_raw_xys[k][0][0], all_raw_xys[k][0][1])

        res = [0] * len(new_t)
        for k in sub_iter:
            res = np.add(res, new_d[k])

        avg = np.average(res)
        if sub_key is None:
            sub_key = "SW"

        key = "%s: avg. of %s $/s" % (sub_key, avg)
        result[key] = [(new_t, res)]
        new_iter_order.append(key)

    return result, new_iter_order


VISUAL_CACHE_REAL_SW = GlobalVMsVisual.from_visual(VISUAL_CACHE_REV, get_real_sw,
             y_label="Social Welfare",
             legend_label=None,  # "SW",
             linestyle="-",
             marker="None",
             linewidth=3,
             alpha=0.5)

'''
MCD Extended Performance
'''
VISUAL_MCD_PERFORMANCE = VISUAL_PERFORMANCE_K  # khits/s

VISUAL_MCD_TOTAL_GETS = OutputVisual(
             xy_function=lambda dct: [(get_time_from_type(dct, PERF), get_vals(get_vals(dct[PERF], 'perf'), 'get_total'))],
             x_label="Time [m]",
             y_label="Total Gets",
             linestyle="-",
             marker="None",
             linewidth=2,
             alpha=0.5,
             color="y",
             drawstyle="steps-post"
             )

VISUAL_MCD_TOTAL_GETS_FOR_BANDWIDTH = OutputVisual(
             xy_function=lambda dct: [
                                align_data_to_time_backwards([
                                    (get_time_from_type(dct, MON), [int(x) for x in get_vals(dct[MON], 'net_rate')]),
                                    (get_time_from_type(dct, PERF), get_vals(get_vals(dct[PERF], 'perf'), 'get_hits_total'))
                                ])[1]],
             x_label="Bandwidth [bps]",
             y_label="Total Gets",
             units="OPS",
             # linestyle = "-",
             # marker = "None",
             # linewidth = 2,
             # alpha = 0.5,
             # color = "y",
             # drawstyle = "steps-post"
             )


VISUAL_MCD_TOTAL_HITS = OutputVisual(
             xy_function=lambda dct: [(get_time_from_type(dct, PERF), get_vals(get_vals(dct[PERF], 'perf'), "get_hits_total"))],
             x_label="Time [m]",
             y_label="Total Hits",
             linestyle="-",
             marker="None",
             linewidth=2,
             alpha=0.5,
             color="r",
             drawstyle="steps-post"
             )

VISUAL_MCD_TOTAL_MISSES = OutputVisual(
             xy_function=lambda dct: [(get_time_from_type(dct, PERF), get_vals(get_vals(dct[PERF], 'perf'), 'get_misses'))],
             x_label="Time [m]",
             y_label="Total Misses",
             linestyle="-",
             marker="None",
             linewidth=2,
             alpha=0.5,
             color="c",
             drawstyle="steps-post"
             )

VISUAL_MCD_MISS_RATE = OutputVisual(
             xy_function=lambda dct: [(get_time_from_type(dct, PERF), map(k_per_sec, get_vals(get_vals(dct[PERF], 'perf'), 'miss_rate')))],
             x_label="Time [m]",
             y_label="Miss Rate",
             linestyle="--",
             marker="None",
             linewidth=2,
             alpha=0.5,
             color="b",
             drawstyle="steps-post"
             )

VISUAL_MCD_HIT_RATE = VISUAL_PERFORMANCE

VISUAL_MCD_HIT_PERCENT = OutputVisual(
             xy_function=lambda dct: [(get_time_from_type(dct, PERF), get_vals(get_vals(dct[PERF], 'perf'), 'get_hits_percent'))],
             x_label="Time [m]",
             y_label="Hits Percent",
             linestyle="-",
             marker="None",
             linewidth=2,
             alpha=0.5,
             color="b",
             drawstyle="steps-post"
             )

VISUAL_MCD_THROUGHPUT = OutputVisual(
             xy_function=lambda dct: [(get_time_from_type(dct, PERF), get_vals(get_vals(dct[PERF], 'perf'), 'throughput'))],
             x_label="Time [m]",
             y_label="Throughput",
             units="Kops/s",
             linestyle="-",
             marker="None",
             linewidth=2,
             alpha=0.5,
             color="b",
             drawstyle="steps-post"
             )

'''
Common Phoronix Performance Visuals
'''
VISUAL_PHORONIX_RESULTS = OutputVisual(
             xy_function=lambda dct: [ {"xyz_tup": get_time_and_vals(dct, PERF, 'performance'),
                                          "legend_label": "%s: %s" % (dct[PERF][0].get("title", ""), dct[PERF][0].get("arguments-description", "")),
                                          "units": dct[PERF][0].get("scale", "")} ],
             x_label="Time [m]",
             y_label="Performance",
             units="?",
             linestyle="-",
             marker="None",
             linewidth=2,
             alpha=0.5,
             drawstyle="steps-post"
             )

VISUAL_PHORONIX_PERFORMANCE = OutputVisual(
             xy_function=lambda dct: [get_time_and_vals(dct, PERF, 'performance', perf_improvement=True)],
             x_label="Time [m]",
             y_label="Performance",
             units="compared to min.",
             linestyle="-",
             marker="None",
             linewidth=2,
             alpha=0.5,
             drawstyle="steps-post"
             )

'''
Common SQL Performance Visuals
'''
VISUAL_SQL_TPS_WITH_COMM = OutputVisual(
             xy_function=lambda dct: [(get_time_from_type(dct, PERF), get_vals(get_vals(dct[PERF], 'perf'), 'tps_with_connections_time'))],
             x_label="Time [m]",
             y_label="Performance (including comm)",
             units="TPS",
             linestyle="-",
             marker="None",
             linewidth=2,
             alpha=0.5,
             drawstyle="steps-post"
             )

VISUAL_SQL_TPS_WITHOUT_COMM = OutputVisual(
             xy_function=lambda dct: [(get_time_from_type(dct, PERF), get_vals(get_vals(dct[PERF], 'perf'), 'tps_without_connections_time'))],
             x_label="Time [m]",
             y_label="Performance (not including comm)",
             units="TPS",
             linestyle="--",
             marker="None",
             linewidth=2,
             alpha=0.5,
             drawstyle="steps-post"
             )

VISUAL_SQL_PERF_FOR_LOAD = OutputVisual(
             xy_function=lambda dct: [get_x_against_y_vals(get_vals(dct[PERF], 'load'), get_vals(get_vals(dct[PERF], 'perf'), 'tps_with_connections_time'))],
             x_label="Load (clients)",
             y_label="Performance",
             units="TPS",
             # linestyle = "-",
             # marker = "None",
             # linewidth = 2,
             # alpha = 0.5
             )

VISUAL_SQL_PERF_FOR_MEM = OutputVisual(
             xy_function=lambda dct: [get_stable_x_against_y_vals(
                                *align_data_to_time_backwards([
                                    (get_time_from_type(dct, MON), get_vals(dct[MON], 'mem_available')),
                                    (get_time_from_type(dct, PERF), get_vals(get_vals(dct[PERF], 'perf'), 'tps_with_connections_time'))
                                ])[1])],
             x_label="Available Memory (MB)",
             y_label="Performance",
             units="TPS",
             # linestyle = "-",
             # marker = "None",
             # linewidth = 2,
             # alpha = 0.5
             )

def VISUAL_SQL_QUERY_LATENCY(number):
    return OutputVisual(
             xy_function=lambda dct: [(get_time_from_type(dct, PERF), get_vals(get_vals(dct[PERF], 'perf'), 'query%02d_latency' % number))],
             x_label="Time [m]",
             y_label="Query %02d Latency" % number,
             units="Milliseconds",
             linestyle="--",
             marker="None",
             linewidth=2,
             alpha=0.5,
             drawstyle="steps-post",
             yscale="log"
             )

def get_perf_to_bw_foreach_load_mcd(data):
    print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!test!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
    vms = data.itervms()

    load = []
    perf = []
    bw = []

    for vm in vms:
        cur_perf_time = get_time_from_type(data[vm], PERF)
        cur_load = get_vals(data[vm][PERF], 'load')
        cur_perf = get_vals(get_vals(data[vm][PERF], 'perf'), 'hits_rate')
        assert len(cur_load) == len(cur_perf) == len(cur_perf_time)

        cur_mon_time = get_time_from_type(data[vm], MON)
        cur_bw = get_vals(data[vm][MON], 'net_rate')

        cur_fixed_bw = [ 0 ] * len(cur_perf_time)

        mon_pos = 0

        for i in xrange(len(cur_perf_time)):
            if cur_perf_time[i] > cur_mon_time[mon_pos]:
                mon_pos += 1

            cur_fixed_bw[i] = cur_bw[mon_pos]

        load += cur_load
        perf += cur_perf
        bw += cur_fixed_bw

    res, iter_order = get_x_against_y_foreach_z_vals(bw, perf, load)

    named_iter_order = []
    named_res = {}

    for key in iter_order:
        named_key = "%s clients" % key
        named_res[named_key] = res[key]
        named_iter_order.append(named_key)

    print named_res
    print
    print named_iter_order

    return named_res, named_iter_order


def get_perf_to_mem_foreach_load(data):
    vms = data.itervms()

    load = []
    perf = []
    mem = []

    for vm in vms:
        cur_perf_time = get_time_from_type(data[vm], PERF)
        cur_load = get_vals(data[vm][PERF], 'load')
        cur_perf = get_vals(get_vals(data[vm][PERF], 'perf'), 'tps_with_connections_time')
        assert len(cur_load) == len(cur_perf) == len(cur_perf_time)

        cur_mon_time = get_time_from_type(data[vm], MON)
        cur_mem = get_vals(data[vm][MON], 'mem_available')

        cur_fixed_mem = [ 0 ] * len(cur_perf_time)

        mon_pos = 0

        for i in xrange(len(cur_perf_time)):
            if cur_perf_time[i] > cur_mon_time[mon_pos]:
                mon_pos += 1

            cur_fixed_mem[i] = cur_mem[mon_pos]

        load += cur_load
        perf += cur_perf
        mem += cur_fixed_mem

    res, iter_order = get_x_against_y_foreach_z_vals(mem, perf, load)

    named_iter_order = []
    named_res = {}

    for key in iter_order:
        named_key = "%s clients" % key
        named_res[named_key] = res[key]
        named_iter_order.append(named_key)

    return named_res, named_iter_order

import re
def get_mesurment_to_cache_foreach_mem(data, mesurment="hits_rate"):
    vms = data.itervms()

    key_value_data = {}
    vm_regexp = re.compile("vm-[0-9]+")
    for vm in vms:
        m = vm_regexp.search(vm)
        if not m:
            continue
        cur_vm = m.group(0)

        info = data.info[vm]
        vm_params = info["parameters"][cur_vm]
        mem = vm_params.get("memcached_mem_size", None)
        vals_size = vm_params.get("memcached_vals_size", 1024)
        cache = vm_params.get("cache_alloc", None)
        if mem is None or cache is None or vals_size is None:
            continue
        cur_perf = get_vals(get_vals(data[vm][PERF], 'perf'), mesurment)

        m = np.median(cur_perf)
        d = np.abs(cur_perf - m)
        mdev = np.median(d)
        relavent = np.argmax(d < mdev)

        avg = np.average(cur_perf[relavent:])
        err = np.std(cur_perf[relavent:])

        key = mem, vals_size
        cur_data = key_value_data.setdefault(key, {})
        cur_data[cache] = (avg, err)

    named_iter_order = []
    named_res = {}

    for key in sorted(key_value_data):
        mem, vals_size = key
        named_key = "memory: %s - value size: %s" % (mem, vals_size)

        x = []
        y = []
        err = []

        cur_data = key_value_data[key]
        for cache in sorted(cur_data):
            x.append(cache)
            cur_y, cur_err = cur_data[cache]
            y.append(cur_y)
            err.append(cur_err)

        named_res[named_key] = [(x, y, err)]
        named_iter_order.append(named_key)

    return named_res, named_iter_order

VISUAL_MCD_HITRATE_TO_CACHE_FOREACH_MEM = GlobalVisual(
             xy_function=lambda d: get_mesurment_to_cache_foreach_mem(d, "hits_rate"),
             x_label="Cache Ways Allocation",
             y_label="Performance",
             units="Khits/s",)

VISUAL_MCD_THROUGHPUT_TO_CACHE_FOREACH_MEM = GlobalVisual(
             xy_function=lambda d: get_mesurment_to_cache_foreach_mem(d, "throughput"),
             x_label="Cache Ways Allocation",
             y_label="Throughput",
             units="Kops/s",
             )

VISUAL_SQL_PERF_TO_MEM_FOREACH_LOAD = GlobalVisual(
             xy_function=get_perf_to_mem_foreach_load,
             x_label="Available Memory (MB)",
             y_label="Performance",
             units="TPS",)

VISUAL_MCD_PERF_TO_BW_FOREACH_LOAD = GlobalVisual(
             xy_function=get_perf_to_bw_foreach_load_mcd,
             x_label="Available Bandwidth (bps)",
             y_label="Performance",
             units="Hits per sec",)
