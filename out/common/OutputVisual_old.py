'''
Created on Jun 1, 2014

@author: Liran Funaro
'''

import copy

class OutputVisual(object):
    '''
    Represent a visual aspect of a trace plot
    '''

    def __init__(self, xy_function, **kwargs):
        '''
        Constructor
        '''
        self.__xy_function = xy_function
        self.__kwargs = kwargs

        if "legend_label" not in self.__kwargs and "y_label" in self.__kwargs:
            self.__kwargs["legend_label"] = self.__kwargs["y_label"]

    def assert_arg(self, arg):
        if arg not in self.__kwargs:
            self.__kwargs[arg] = None

    @property
    def xy_function(self):
        return self.__xy_function

    @property
    def args(self):
        return copy.deepcopy(self.__kwargs)

    def get_all_raw_xys(self, data):
        vms = data.itervms()

        all_raw_xys = {}

        for vm in vms:
            all_raw_xys[vm] = self.xy_function(data[vm])

        return all_raw_xys, vms

    def get_plot_data(self, x, y, sub_label = None, single_plot = False):
        kwargs = self.args

        vis_label = kwargs["legend_label"] if "legend_label" in kwargs else None

        legend_label = None

        if vis_label and not sub_label:
            legend_label = str(vis_label)
        elif sub_label and not vis_label:
            legend_label = str(sub_label)
        elif vis_label and sub_label:
            if not single_plot and vis_label != sub_label:
                legend_label = "%s: %s" % (vis_label, sub_label)
            else:
                legend_label = str(sub_label)

        kwargs["legend_label"] = legend_label
        kwargs["label"] = legend_label

        return (x, y, kwargs)

    def get_xys(self, xys, sub_label = None, single_visual = False):
        res_xys = []

        for x, y in xys:
            res_xys.append(self.get_plot_data(x, y, sub_label, single_visual))

        return res_xys

    def get_all_xy(self, data, single_visual = False, joined = False):
        all_raw_xys, iter_order = self.get_all_raw_xys(data)

        if joined:
            all_xy = []
        else:
            all_xy = {}

        for key in iter_order:
            xys = self.get_xys(all_raw_xys[key], key if joined else None, single_visual)

            if joined:
                all_xy += xys
            else:
                all_xy[key] = xys

        return all_xy, iter_order

class GlobalVisual(OutputVisual):

    def __init__(self, xy_function, **kwargs):
        OutputVisual.__init__(self, xy_function, **kwargs)

    def get_all_raw_xys(self, data):
        return self.xy_function(data)
