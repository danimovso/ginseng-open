'''
Created on Jun 1, 2014

@author: Liran Funaro
'''
import copy
import traceback
from mom.Plotter import INFO


class OutputVisual(object):
    '''
    Represent a visual aspect of a trace plot
    '''

    def __init__(self, xy_function, **kwargs):
        '''
        Constructor
        '''
        self.__xy_function = xy_function
        self.__kwargs = kwargs

        if "legend_label" not in self.__kwargs and "y_label" in self.__kwargs:
            self.__kwargs["legend_label"] = self.__kwargs["y_label"]

    def assert_arg(self, arg):
        if arg not in self.__kwargs:
            self.__kwargs[arg] = None

    @property
    def xy_function(self):
        return self.__xy_function

    @property
    def args(self):
        return copy.deepcopy(self.__kwargs)

    def get_all_raw_xys(self, data):
        is_host = self.__kwargs.get("is_host", False)
        if not is_host:
            vms = data.itervms()
        else:
            vms = data.iterhosts()

        all_raw_xys = {}

        for vm in vms:
            try:
                cur_data = data[vm]
                cur_data[INFO] = data.info['vms'][vm]
            except:
                print "Error retrieving data for VM:",vm," -- available:",data.keys()
                all_raw_xys[vm] = []
            else:
                try:
                    all_raw_xys[vm] = self.xy_function(cur_data)
                except Exception as e:
                    print "Error retrieving data for VM:",vm," -- with:",e
                    traceback.print_exc()
                    all_raw_xys[vm] = []
    #                 raise

        return all_raw_xys, vms

    def get_plot_data(self, x, y, err, sub_label = None, single_plot = False, more_args = {}):
        kwargs = self.args
        kwargs.update(more_args)

        vis_label = kwargs.get("legend_label", None)

        legend_label = None

        if vis_label and not sub_label:
            legend_label = str(vis_label)
        elif sub_label and not vis_label:
            legend_label = str(sub_label)
        elif vis_label and sub_label:
            #if not single_plot and vis_label != sub_label:
            if vis_label != sub_label:
                legend_label = "%s: %s" % (vis_label, sub_label)
            else:
                legend_label = str(sub_label)

        kwargs["legend_label"] = legend_label
        kwargs["label"] = legend_label

        return (x, y, err, kwargs)

    def get_xys(self, xys, sub_label = None, single_visual = False):
        res_xys = []

        for xyz_tup in xys:
            if isinstance(xyz_tup, dict):
                more_args = xyz_tup
                xyz_tup = more_args.pop("xyz_tup")
            else:
                more_args = {}

            x = xyz_tup[0]
            y = xyz_tup[1]
            if len(xyz_tup) > 2:
                err = xyz_tup[2]
            else:
                err = None
            res_xys.append(self.get_plot_data(x, y, err, sub_label, single_visual, more_args))

        return res_xys

    def get_all_xy(self, data, single_visual = False, joined = False):
        all_raw_xys, iter_order = self.get_all_raw_xys(data)

        if joined:
            all_xy = []
        else:
            all_xy = {}

        for key in iter_order:
            xys = self.get_xys(all_raw_xys[key], key if joined else None, single_visual)

            if joined:
                all_xy += xys
            else:
                all_xy[key] = xys

        return all_xy, iter_order

class GlobalVisual(OutputVisual):

    def __init__(self, xy_function, **kwargs):
        OutputVisual.__init__(self, xy_function, **kwargs)

    def get_all_raw_xys(self, data):
        return self.xy_function(data)

class GlobalVMsVisual(OutputVisual):

    @staticmethod
    def from_visual(vis, global_func, **kwargs):
        kwargs = dict(vis.args, **kwargs)
        return GlobalVMsVisual(vis.xy_function, global_func, **kwargs)

    def __init__(self, xy_function, global_func, **kwargs):
        self.global_func = global_func
        OutputVisual.__init__(self, xy_function, **kwargs)

    def get_all_raw_xys(self, data):
        all_raw_xys, iter_order = OutputVisual.get_all_raw_xys(self, data)
        return self.global_func(all_raw_xys, iter_order)
