"""
Created on Jun 15, 2016

@author: Sharon
@author: Yarden
@author: Amit
"""

try:
    import imageio
except:
    print "imageio package is missing. Run the following command line to get it:"
    print "git clone https://github.com/imageio/imageio.git"
    sys.exit()

import argparse
import os
import re
from os.path import expanduser
from tempfile import mkdtemp

from etc.publish import remote_sync
from mom.Plotter import Plotter  # , POL, MON
from out.common.OutputPlotter import EvaluationData

import matplotlib.cm as cmx
import matplotlib.colors as colors

from glob import glob

class ExperimentData:
    """
    This class has been copied and modified from the OutputPlotter.py file. This
    class is responsible for receiving an information file and parsing it to a
    readable format.

    To get the information about the experiment, use the evalDataSet field which
    contains a dictionary of dictionaries of dictionaries of lists of
    dictionaries (yeah...).
    """

    PLOTTER_FILE = "exp-plotter"
    INFO_FILE = "info"

    CHECKIN_DATA_FILES = [PLOTTER_FILE, INFO_FILE]

    DICT_OBJ_PATTERN = re.compile("<[^<>]*>")

    def __init__(self, source_dir):
        self.source_dir = expanduser(source_dir)
        self.work_dir = mkdtemp()
        self.target_dir = self.source_dir

        self.draw_args = {}
        self.debug = False
        self.subfolders_list = []
        self.output_to_sub_folders = True

        if not os.path.exists(self.source_dir):
            raise IOError("Source path not found: %s" % self.source_dir)

        if not os.path.exists(self.target_dir):
            os.makedirs(self.target_dir)

        self.checkinData()
        self.retrieveEvaluationDataSet()

    def checkinData(self):
        remote_sync(
            src_path=self.source_dir,
            dst_path=self.work_dir,
            include=self.CHECKIN_DATA_FILES,

            include_subfolders=len(self.subfolders_list) == 0,
            delete=True,
            delete_excluded=True,
            show_progress=False,
            verbose=False)

    def retrieveEvaluationDataSet(self):
        self.evalDataSet = {}

        for work_path, work_dir in [(os.path.join(self.work_dir, f), f) for f in
                                    os.listdir(self.work_dir)]:
            if not os.path.isdir(work_path):
                continue
            if work_dir.startswith("."):
                continue

            plotter_file_path = os.path.join(work_path, self.PLOTTER_FILE)
            info_file_path = os.path.join(work_path, self.INFO_FILE)

            if not os.path.isfile(plotter_file_path):
                print "Plotter file does not exist:", plotter_file_path
                continue

            if not os.path.isfile(info_file_path):
                print "Info file does not exist:", info_file_path
                continue

            # read the evaluation output
            plotter_content = file(plotter_file_path).readlines()
            info_content = file(info_file_path).read()

            try:
                data = Plotter.parse(plotter_content)
                info = self.__evalInfo(info_content)
            except:
                print "Failed loading", work_dir
                raise

            self.evalDataSet[work_dir] = EvaluationData(data, info, work_dir)
            self.evalDataSet[work_dir].correct_time()

            if "long_titles" in info:
                print "Load:", work_dir, " - Title: ", info["long_titles"]
            else:
                print "Load:", work_dir

    @classmethod
    def __evalInfo(cls, info):
        """Remove objects from info text. Something like "< Bla Bla >"
        """
        count = -1
        while count != 0:
            info, count = cls.DICT_OBJ_PATTERN.subn("None", info)

        return eval(info)


def cmp_vms_data(vm_data1, vm_data2):
    if vm_data1[1] > vm_data2[1]:
        return 1
    elif vm_data1[1] == vm_data2[1]:
        if vm_data1[0] > vm_data2[0]: return 1
        if vm_data1[0] == vm_data2[0]: return 0
        return -1

    return -1


def get_cmap(N):
    """Returns a function that maps each index in 0, 1, ... N-1 to a distinct
    RGB color.
    """
    color_norm = colors.Normalize(vmin=0, vmax=N - 1)
    scalar_map = cmx.ScalarMappable(norm=color_norm, cmap='hsv')

    def map_index_to_rgb_color(index):
        return scalar_map.to_rgba(index)

    return map_index_to_rgb_color

def make_gif(source_dir):
    source_dir = expanduser(source_dir)
    rounds = len(glob(source_dir + "/partition/Memory*.png"))
    images = []
    print "Generating gif from %d images" % rounds
    for i in range(rounds):
        image = imageio.imread(source_dir+"/partition/Memory"+str(i)+".png")
        images.append(image)
    imageio.mimsave(source_dir + "/partition/movie.gif", images, duration=1.25)
    

"""
This script generates, given an experiment as source, the distribution of memory
between the virtual machines in the experiment, for each round.
The x axis displays the amount of memory each virtual machine was assigned while
the y axis displays the price the virtual machine is willing to pay for each
memory unit.

An example run command looks like this:

    python out/common/MemoryPartition -s ../moc-output/exp/negotiations-187

This will generate a partition graph for the first graph of the 187th
experiment.
"""
if __name__ == '__main__':
    import matplotlib.pyplot as plt
    import matplotlib.patches as patches
    from matplotlib.backends.backend_pdf import PdfPages

    # Initialize program arguments parser
    parser = argparse.ArgumentParser(description='Plot experiments.')
    parser.add_argument("-s", "--source", required=True)

    args = parser.parse_args()
    outdir = args.source + "/partition"

    if not os.path.exists(outdir):
        os.mkdir(outdir)

    # Create a new ExperimentData object which will hold the information about
    # the experiment
    cp = ExperimentData(args.source)

    data = cp.evalDataSet['p0-0.00-freq-60000']

    vms_num = len(data) - 1

    memories = []
    prices = []
    ranges = []

    for i in range(1, vms_num + 1):
        memories.append([dic['mem_extra']
                         for dic in data['vm-{}'.format(i)]['Policy']])
        prices.append([dic['bid_p_mem']
                       for dic in data['vm-{}'.format(i)]['Policy']])
        ranges.append([dic['bid_ranges_mem']
                       for dic in data['vm-{}'.format(i)]['Policy']])
    max_p = max([max(guest_prices) for guest_prices in prices])
    max_sum_ranges = sum([max([r[0][1] for r in guest_ranges]) for guest_ranges in ranges])
    print "max y should be : ", max_p
    print "max x should be: ", max_sum_ranges

    # Calculate how much memory the system can give to the guests
    totalMems = [dic['auction_mem'] for dic in data['Host']['Policy']]

    # Get the color map
    cmap = get_cmap(vms_num + 1)

    for i in range(len(totalMems)):
    #for i in range(50):
        # Initialize a plotter and pdf exporter
        #pp = PdfPages(outdir + '/Memory partition' + str(i) + '.pdf')
        fig = plt.figure(figsize=(20, 5))

        plt.xlim([0, max_sum_ranges])
        plt.ylim([0, max_p])
        ax = fig.add_subplot(111)

        # Draw the total memory line
        ax.plot([totalMems[i], totalMems[0]], [0, 200], 'k', lw=2)

        vms = data.keys()
        vms.remove('Host')

        round_memories = [l[i] for l in memories]
        round_prices = [l[i] for l in prices]
        round_ranges = [l[i] for l in ranges]

        vms_data = zip(round_memories, round_prices, round_ranges, [j for j in range(1,len(round_memories)+1)])
        vms_data = sorted(vms_data, cmp=cmp_vms_data, reverse=True)
        print vms_data

        rightMost = 0
        maxPrice = 0
        for j, vm_data in enumerate(vms_data):
            price = vm_data[1]
            memory = max(vm_data[0], vm_data[2][0][1])

            if price == 0.0: price = 0.01

            ax.add_patch(
                patches.Rectangle(
                    (rightMost, 0),
                    memory,
                    price,
                    facecolor=cmap(vm_data[3]),
                    edgecolor="#000000",
                    linewidth=1
                )
            )

            rightMost += memory
            maxPrice = max(maxPrice, price)

        #plt.xlim([0, rightMost + 100])
        #plt.ylim([0, maxPrice * 1.2])

        ax.text(totalMems[i],
                0.75*max_p,
                'Total Memory: {}MB'.format(totalMems[i]),
                horizontalalignment='left',
                verticalalignment='bottom')

        ax.set_title("Memory partition in round {}".format(i))
        ax.set_xlabel("Memory (MB)")
        ax.set_ylabel("Price ($/s)")

        #plt.savefig(pp, format='pdf')
        #pp.close()
        plt.savefig(outdir + '/Memory' + str(i) + '.png')
        plt.close(fig)
    make_gif(args.source)