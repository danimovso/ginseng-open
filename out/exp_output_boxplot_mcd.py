'''
Created on Mar 4, 2013

@author: eyal
'''

from numpy import array  #@UnusedImport
from etc.plots import pl_apply_defaults
import os
from out.common.common import sync_files
from out.exp_output_boxplot import boxplot_create_file, boxplot_plot_file

if __name__ == "__main__":
    pl_apply_defaults()
    filenames = ["batch-Memcached-%i" % i for i in [252, 255, 256, 257]]
    localdir = "%s/moc-output/exp-tapuz21/mcd-boxplot" % os.path.expanduser("~")
    remotedir = "%s/moc-output/exp" % os.path.expanduser("~")
    paths = [os.path.join(localdir, f) for f in filenames]

    profiler_file = "/home/eyal/moc/doc/profiler-memcached-inside-spare-50-win-500k.xml"
    field = "hits_rate"

    start_min = 20
    end_min = 50

    limit = 3
    step = 0.5
    func = lambda x: x * 1e-3
    k = "k"

    create_files = True


    if not os.path.exists(localdir): os.mkdir(localdir)
    for f in filenames:
        sync_files(os.path.join(remotedir, f), os.path.join(localdir, f),
                   ["bidder-*", "exp-out*", "prog-*"])
    if create_files:
        for f in paths:
            boxplot_create_file(f, profiler_file, field, start_min, end_min)
    boxplot_plot_file(paths = paths, outdir = localdir, axis_max = limit, axis_step = step, func, k, name = "mcd")
