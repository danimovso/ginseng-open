'''
Created on Mar 5, 2012
@author: eyal
'''

from matplotlib import use

use('cairo')
import pylab as pl
import numpy as np
import os
from etc.plots import draw_n_save, pl_apply_defaults, multifunction
import sys
import collections
from out.common.common import sync_files, get_vals, get_vals2, perf_time, \
    pol_time, \
    mon_time, process_description_file, k_per_sec
from mom.Plotter import Plotter, PERF, MON, HOST, POL
from mom import Profiler
from etc.Settings import Settings

prefix = os.path.join(Settings.user_home(), Settings.output_dir())
remotedir_base = os.path.join(prefix, "exp")

localdir_base = os.path.join(prefix, "exp-%s" % Settings.hostname())
if not os.path.exists(localdir_base): os.mkdir(localdir_base)

paper_mode = False
discard_min = 61
end_min = 85
position = (0.1, 0.22, 0.88, 0.72)
# filetype = ".eps"
filetype = "png" if not paper_mode else "eps"
font_default = {'legend.fontsize': 10,
                'axes.labelsize': 10,
                'font.size': 10,
                'xtick.labelsize': 10,
                'ytick.labelsize': 10, }

if paper_mode:
    font_default = {'legend.fontsize': 10,
                    'axes.labelsize': 10,
                    'font.size': 9,
                    'xtick.labelsize': 9,
                    'ytick.labelsize': 9, }

pl_apply_defaults(font_default)

bars_size = (7.0, 2.4) if paper_mode else (10.0, 2.4)
trace_size = (9, 4.8) if paper_mode else (40, 8)

iter_vms = lambda data: ["vm-%i" % n for n in sorted(
    [int(k.split("-")[1]) for k in data.iterkeys() if k != HOST])]


def plot_name(name, p0, filetype):
    if isinstance(p0[0], str):
        return "%s-p0-%s-freq-%i.%s" % (name, p0[0], p0[1], filetype)
    else:
        return "%s-p0-%.02f-freq-%i.%s" % (name, p0[0] / 100., p0[1], filetype)


def draw_bars(data, info, freq):
    # collect the performance data
    all_perf = {}
    all_rev = {}

    relevant_keys = filter(lambda k: k[1] == freq, data.keys())
    numeric_vals = map(lambda x: x[0],
                       filter(lambda x: isinstance(x[0], (float, int)),
                              relevant_keys))
    static_idx = max(numeric_vals) + 10.0 if len(numeric_vals) > 0 else 0.0

    for a, f in filter(lambda k: k[1] == freq, data.iterkeys()):
        if a == "static":
            all_perf[static_idx] = {}
            all_rev[static_idx] = {}
        else:
            all_perf[a] = {}
            all_rev[a] = {}
        for vm in iter_vms(data[a, f]):
            try:
                #                vals = get_vals(get_vals(data[a, f][vm][PERF], 'perf'), "hits_rate")
                vals = get_vals2(get_vals(data[a, f][vm][PERF], 'perf'),
                                 "tps_with_connections_time", "hits_rate")
                times = perf_time(data[a, f][vm])
                vals = [float(v) for t, v in zip(times, vals) if
                        discard_min <= t <= end_min]
                if a == "static":
                    all_perf[static_idx][vm] = vals[1:]
                else:
                    all_perf[a][vm] = vals[1:]

                vals = get_vals(data[a, f][vm][PERF], 'rev')
                vals = [float(v) for t, v in zip(times, vals) if
                        discard_min <= t <= end_min]
                if a == "static":
                    all_rev[static_idx][vm] = vals[1:]
                else:
                    all_rev[a][vm] = vals[1:]

                ls = get_vals(data[a, f][vm][PERF], 'load')
            except KeyError as ex:
                print "error: %s" % ex
                pass

    p0s = np.array(sorted(all_perf.keys()))

    def calc_sw(a):
        rev = []
        for vm in all_perf[a].keys():
            r = all_rev[a][vm]
            rev.append(sum(r))
        return sum(np.array(rev))

    revs = map(calc_sw, p0s)

    pl.clf()

    w = 0.05
    h = revs / min(revs) if min(revs) != 0 else revs

    x = p0s / 100.
    pl.bar(left=x - w / 2,
           height=h,
           width=w,
           bottom=0,
           color="w")  # (0.5, 0.5, 1.0))
    for xi, hi in zip(x, h):
        text = "%0.2f" % hi if hi != static_idx else "static"
        pl.text(xi, hi / 2, text,
                rotation=-90,
                ha="center", va="center")
    pl.xticks(x)
    pl.xlim((-0.05, max(1.05, max(x + 0.05))))
    pl.ylim((0, max(2.5, max(h + 0.05))))
    pl.gca().set_position(position)
    pl.xlabel("p0", labelpad=2)
    pl.ylabel("Relative SW", labelpad=2)
    pl.gca().set_position(position)
    draw_n_save(
        os.path.join(localdir, "p0-compare-freq-%s.%s" % (freq, filetype)),
        size=bars_size)


def correct_time(data, info):
    for a in data.keys():
        t0 = 99999999999
        for vm in data[a].keys():
            for tp in data[a][vm].keys():
                try:
                    t0 = min(t0, data[a][vm][tp][0]['time'])
                except KeyError:
                    pass

            if vm == HOST or not data[a][vm].has_key(PERF):
                continue

            rev_funcs = info[a]['vms'][vm]['adviser_args']['rev_func']
            if isinstance(rev_funcs, str):
                rev_funcs = [eval(rev_funcs)]

            rev_funcs = map(lambda f: eval(f) if isinstance(f, str) else f,
                            rev_funcs)

            for i in range(len(data[a][vm][PERF])):
                if data[a][vm][PERF][i]['perf'].has_key(
                        "get_hits_total") and not data[a][vm][PERF][i][
                    'perf'].has_key('hits_rate'):
                    try:
                        data[a][vm][PERF][i]['perf']["hits_rate"] = \
                        data[a][vm][PERF][i]['perf']["get_hits_total"] / \
                        data[a][vm][PERF][i]['perf']["test_time"]
                    except KeyError:
                        pass

                try:
                    current_valuation = [x for x in data[a][vm][MON] if
                                         x['time'] <= data[a][vm][PERF][i][
                                             'time']][-1]['current_valuation']
                except KeyError:
                    current_valuation = 0
                except IndexError:
                    current_valuation = 0

                rev_func = rev_funcs[current_valuation]

                perf_key = 'hits_rate'
                if data[a][vm][PERF][i]['perf'].has_key(
                        'tps_with_connections_time'):
                    perf_key = 'tps_with_connections_time'
                if not data[a][vm][PERF][i]['perf'].has_key(perf_key):
                    data[a][vm][PERF][i]['perf'][perf_key] = 0

                data[a][vm][PERF][i]['rev'] = rev_func(
                    data[a][vm][PERF][i]['perf'][perf_key])

        for vm in data[a].keys():
            for tp in data[a][vm].keys():
                for dct in data[a][vm][tp]:
                    dct['time'] -= t0


def exp_plot(data, funcs, plot_kwargs, axes=None, tests=None,
             ylim=None, xlim=None):
    if not data:
        return

    funcs.append((perf_time, lambda dct: get_vals(dct[PERF], 'load')))
    plot_kwargs.append({"label": "Load", "marker": "None", "color": "k",
                        "alpha": 0.5, "linestyle": "-", "linewidth": 1})
    if axes is not None:
        axes.append(max(axes) + 1)

    y_labels = [kw["label"] for kw in plot_kwargs]

    xmin = sys.maxint
    xmax = 0

    all_xy = []

    for vm_i, vm in enumerate(iter_vms(data)):
        all_xy.append([])
        for xy_func in funcs:
            try:
                x = xy_func[0](data[vm])
                y = xy_func[1](data[vm])
            except Exception as ex:
                print ("error in guest: %s, %s" % (vm, ex))
                print xy_func
                y = x = [0]
                raise
            if len(x) != 0 and len(y) != 0:
                xmin = min(xmin, min(x))
                xmax = max(xmax, max(x))
            all_xy[-1].append((x, y))
    multifunction(all_xy,
                  axes=axes if axes is not None else range(len(funcs)),
                  plot_kwargs=plot_kwargs,
                  xlim=xlim if xlim is not None else (xmin, xmax),
                  ylim=ylim,
                  ylabels=y_labels)


def draw_exp(p0, data, info):
    check_com = lambda d: (d['p'] == 0 and len(d['rq']) == 1
                           and d['rq'][0][0] == 0 and d['rq'][0][1] == 0)

    def no_communication_markers(data):
        data = data[POL]
        if not data[0].has_key("rq"):
            return []
        ret = []
        for d in data:
            if check_com(d):
                ret.append(0)
        return ret

    def no_communication_time(data):
        data = data[POL]
        if not data[0].has_key("rq"):
            return []
        ret = []
        for d in data:
            if check_com(d):
                ret.append(d['time'] / 60.0)
        return ret

    #    duration = info['load_interval']
    pl.clf()
    exp_plot(data,
             [(perf_time,
               lambda dct: map(k_per_sec, get_vals(dct[PERF], 'rev'))),
              (perf_time, lambda dct: map(k_per_sec,
                                          get_vals2(get_vals(dct[PERF], 'perf'),
                                                    "tps_with_connections_time",
                                                    "hits_rate"))),
              (mon_time, lambda dct: get_vals(dct[MON], 'mem_available')),
              #              (mon_time, lambda lst: get_vals(lst[MON], 'major_fault')),
              #              (mon_time, lambda lst: get_vals(lst[MON], 'cpu_usage')),
              #              (no_communication_time, no_communication_markers),
              ], [
                 {"label": "Valution", "units": "K/s", "linestyle": "-",
                  "marker": "None", "linewidth": 1, "alpha": 0.5, "color": "r"},
                 {"label": "performance", "units": "Khits/s'",
                  "linestyle": "--", "marker": "None", "linewidth": 1,
                  "alpha": 0.5, "color": "r"},
                 {"label": "Memory", "units": "MB", "linestyle": "-",
                  "marker": "None", "linewidth": 1, "alpha": 0.5, "color": "g"},
                 #              {"label": "Page faults", "linestyle": "None", "marker": "o", "alpha": 0.5},
                 #              {"label": "CPU usage", "linestyle": "-", "marker": "None", "linewidth": 1, "alpha": 0.5, "color": "b"},
                 #              {"label": "No comm", "linestyle": "None", "marker": "x", "color": "b"},
             ])
    draw_n_save(os.path.join(localdir, plot_name("exp-view", p0, filetype)),
                size=trace_size)

    pl.clf()
    exp_plot(data,
             [
                 (perf_time, lambda dct: get_vals(get_vals(dct[PERF], 'perf'),
                                                  'get_total')),
                 (perf_time, lambda dct: get_vals(get_vals(dct[PERF], 'perf'),
                                                  "get_hits_total")),
                 (perf_time, lambda dct: get_vals(get_vals(dct[PERF], 'perf'),
                                                  'get_misses')),
                 #              (perf_time, lambda dct: map(k_per_sec, get_vals(get_vals(dct[PERF], 'perf'), 'miss_rate'))),
                 (perf_time, lambda dct: map(k_per_sec, get_vals(
                     get_vals(dct[PERF], 'perf'), 'hits_rate'))),
                 (perf_time, lambda dct: get_vals(get_vals(dct[PERF], 'perf'),
                                                  'get_hits_percent')),
                 #              (mon_time, lambda dct: get_vals(dct[MON], 'mem_available')),
                 (mon_time, lambda lst: get_vals(lst[MON], 'major_fault')),
             ], [
                 {"label": "Total gets", "linestyle": "-", "marker": "None",
                  "linewidth": 2, "alpha": 0.5, "color": "y"},
                 {"label": "Total hits", "linestyle": "-", "marker": "None",
                  "linewidth": 2, "alpha": 0.5, "color": "r"},
                 {"label": "Total misses", "linestyle": "-", "marker": "None",
                  'linewidth': 2, "alpha": 0.5, 'color': 'c'},
                 #              {"label": "Miss rate", "linestyle": "--", "marker": "None", "linewidth": 2, "alpha": 0.5, "color": "b"},
                 {"label": "Hits rate", "linestyle": "--", "marker": "None",
                  "linewidth": 2, "alpha": 0.5, "color": "r"},
                 {"label": "Hits percent", "linestyle": "-", "marker": "None",
                  "linewidth": 2, "alpha": 0.5, "color": "b"},
                 #              {"label": "Memory", "linestyle": "-", "marker": "None", "linewidth": 2, "alpha": 0.5, "color": "g"},
                 {"label": "Major faults", "linestyle": "None", "marker": "o",
                  "alpha": 0.5},
             ],
             [0, 0, 0, 1, 2, 3])
    #              [0, 0, 0, 1, 1, 2, 3, 4])
    draw_n_save(os.path.join(localdir, plot_name("perf-view", p0, filetype)),
                size=trace_size)

    pl.clf()
    exp_plot(data, [
        (mon_time, lambda dct: get_vals(dct[MON], 'mem_available')),
        (mon_time, lambda lst: get_vals(lst[MON], 'major_fault')),
        (mon_time, lambda lst: get_vals(lst[MON], 'minor_fault')),
        (mon_time, lambda lst: get_vals(lst[MON], 'python1_usage')),
        (mon_time, lambda lst: get_vals(lst[MON], 'python2_usage')),
    ], [
                 {"label": "memory", "linestyle": "-", "marker": "None",
                  "linewidth": 2, "alpha": 0.5, "color": "g"},
                 {"label": "Major faults", "linestyle": "None", "marker": "o",
                  "alpha": 0.5},
                 {"label": "Minor faults", "linestyle": "None", "marker": "^",
                  "alpha": 0.5},
                 {"label": "python1", "linestyle": "-", "marker": "None",
                  "alpha": 0.5, "color": "r"},
                 {"label": "python2", "linestyle": "-", "marker": "None",
                  "alpha": 0.5, "color": "b"},
             ],
             [0, 1, 2, 3, 3])
    draw_n_save(os.path.join(localdir, plot_name("mem-view", p0, filetype)),
                size=trace_size)

    def get_payment(dct):
        bill = get_vals(dct[POL], 'control_mem')
        mem = get_vals(dct[POL], 'control_mem')

    def extra_mem(dct):
        base = get_vals(dct[POL], 'base_mem')
        mem = get_vals(dct[POL], 'control_mem')
        assert len(base) == len(mem)
        return [_mem - _base for _mem, _base in zip(mem, base)]

    try:
        pl.clf()
        exp_plot(data,
                 [(pol_time, lambda lst: get_vals(lst[POL], 'bid_p_mem')),
                  #                  (pol_time, lambda dct: get_vals(dct[POL], 'control_mem')),
                  (pol_time, lambda dct: extra_mem(dct)),
                  (pol_time,
                   lambda dct: get_vals(dct[POL], 'mem_extra_alternate')),
                  (pol_time, lambda lst: [x[0][0] for x in get_vals(lst[POL],
                                                                    'bid_ranges_mem')]),
                  # 'rq_mem')]),
                  (pol_time, lambda lst: [x[0][1] for x in get_vals(lst[POL],
                                                                    'bid_ranges_mem')]),
                  # 'rq_mem')]),
                  #                  (pol_time, lambda lst: get_vals(lst[POL], 'mem_base')),
                  (
                  pol_time, lambda dct: get_vals(dct[POL], 'control_bill_mem')),
                  ],
                 [{"label": "p", "linestyle": "-", "linewidth": 2,
                   "marker": "None", "alpha": 1},
                  {"label": "Extra Memory", "linestyle": "-", "marker": "None",
                   "linewidth": 2, "alpha": 0.5, "color": "r"},
                  {"label": "Alternate Memory", "linestyle": "-",
                   "marker": "None", "linewidth": 2, "alpha": 0.5,
                   "color": "c"},
                  {"label": "r", "linestyle": "None", "marker": "^",
                   "alpha": 0.5},
                  {"label": "q", "linestyle": "None", "marker": "v",
                   "alpha": 0.5},
                  #                   {"label": "base", "linestyle": "None", "marker": "o", "alpha": 0.5},
                  {"label": "bill", "linestyle": "-", "marker": "None",
                   "linewidth": 2, "alpha": 0.5, "color": "g"},
                  ],
                 [0, 1, 1, 1, 1, 2])
        draw_n_save(os.path.join(localdir, plot_name("prq", p0, filetype)),
                    size=trace_size)

        pl.clf()
        exp_plot(data,
                 [(pol_time, lambda dct: extra_mem(dct)),
                  (pol_time, lambda lst: [x[0][0] for x in get_vals(lst[POL],
                                                                    'bid_ranges_mem')]),
                  # 'rq_mem')]),
                  (pol_time, lambda lst: [x[0][1] for x in get_vals(lst[POL],
                                                                    'bid_ranges_mem')]),
                  # 'rq_mem')]),
                  (
                  pol_time, lambda dct: get_vals(dct[POL], 'control_bill_mem')),
                  (pol_time,
                   lambda dct: get_vals(dct[POL], 'bill_estimate_mem')),
                  ],
                 [
                     {"label": "Extra Memory", "linestyle": "-",
                      "marker": "None", "linewidth": 2, "alpha": 0.5,
                      "color": "r"},
                     {"label": "r", "linestyle": "None", "marker": "^",
                      "alpha": 0.5},
                     {"label": "q", "linestyle": "None", "marker": "v",
                      "alpha": 0.5},
                     {"label": "bill", "linestyle": "-", "marker": "None",
                      "linewidth": 2, "alpha": 0.5, "color": "g"},
                     {"label": "bill estimate", "linestyle": "-",
                      "marker": "None", "linewidth": 2, "alpha": 0.5,
                      "color": "b"},
                 ],
                 [0, 0, 0, 1, 1])
        draw_n_save(os.path.join(localdir, plot_name("bills", p0, filetype)),
                    size=trace_size)
    except Exception as e:
        print "No auctions (error: %s)" % e
    #    for i, test in enumerate(all_tests):
    #        pl.subplot(len(all_tests), 1, i + 1)
    pl.clf()
    try:
        keys = sorted([k.replace("cpu_", "") for k in data[HOST][MON][0].keys()
                       if k.startswith("cpu") and k != "cpu_tot"],
                      cmp=lambda x, y: int(x) - int(y))

        for j, k in enumerate(keys):
            k = "cpu_" + k
            pl.subplot(len(keys), 1, j + 1)
            pl.plot(mon_time(data[HOST]),
                    get_vals(data[HOST][MON], k),
                    color=pl.cm.jet(float(j) / len(keys)),  # @UndefinedVariable
                    marker="None", alpha=1, label=k)
            pl.grid(True)
            pl.legend(loc="best")
            if j == len(keys) - 1:
                pl.xlabel("Time [min]")
        draw_n_save(os.path.join(localdir, plot_name("host-cpu", p0, filetype)),
                    size=(trace_size[0], trace_size[1] * len(keys)))
    except KeyError:
        pass

    # p values
    pl.clf()
    for p, marker in zip(
            ["p_in_min_mem", "p_out_max_mem", "p0_mem", "p_reduction_mem"],
            ["^", "v", "*", "o"]):
        pl.plot(pol_time(data[HOST]), get_vals(data[HOST][POL], p),
                marker=marker, alpha=0.5, label="%s" % p)
        pl.legend(loc="best")
        pl.grid(True)
        pl.xlabel("Time [min]")
    draw_n_save(os.path.join(localdir, plot_name("host-p-vals", p0, filetype)),
                size=trace_size)


def draw_composed_exp(data, info):
    p0s = [0, 20]

    k_per_sec = lambda x: x / 1000.
    to_GB = lambda x: x / 1024.
    d = collections.defaultdict(dict)

    for a in p0s:
        for vm in data[a].keys():
            for k in data[a][vm].keys():
                d[vm][k + "-" + str(a)] = [x.copy() for x in data[a][vm][k]]
                d[vm][k] = data[0][vm][k]
    funcs = []
    props = []

    linestyles = {0: "-",
                  20: ":"}
    markers = {0: "None", 20: "None"}
    colors = {"mem": "b", "sw": "r"}

    funcs.append(
        (lambda dct: map(lambda t: t['time'] / 60., dct[PERF + "-" + '0']),
         lambda dct: map(k_per_sec, get_vals(dct[PERF + "-" + '0'], 'rev')))
    )
    props.append(
        {"label": r"SW p0=0.0",
         "linestyle": linestyles[0],
         "marker": markers[0],
         "linewidth": 1,
         "color": colors["sw"]}
    )
    funcs.append(
        (lambda dct: map(lambda t: t['time'] / 60., dct[MON + "-" + '0']),
         lambda dct: map(to_GB,
                         get_vals(dct[MON + "-" + '0'], 'mem_available')))
    )
    props.append(
        {"label": r"Memory p0=0.0",
         "linestyle": linestyles[0],
         "marker": "None",
         "linewidth": 1,
         "color": colors["mem"]}
    )

    funcs.append(
        (lambda dct: map(lambda t: t['time'] / 60., dct[PERF + "-" + '20']),
         lambda dct: map(k_per_sec, get_vals(dct[PERF + "-" + '20'], 'rev')))
    )
    props.append(
        {"label": r"SW p0=0.2",
         "linestyle": linestyles[20],
         "marker": markers[20],
         "linewidth": 1,
         "color": colors["sw"]}
    )
    funcs.append(
        (lambda dct: map(lambda t: t['time'] / 60., dct[MON + "-" + '20']),
         lambda dct: map(to_GB,
                         get_vals(dct[MON + "-" + '20'], 'mem_available')))
    )
    props.append(
        {"label": r"Memory p0=0.2",
         "linestyle": linestyles[20],
         "marker": "None",
         "linewidth": 1,
         "color": colors["mem"]}
    )

    pl.clf()
    exp_plot(d, funcs, props, axes=[0, 1, 0, 1], xlim=(0, 16))
    draw_n_save(os.path.join(localdir, "exp-view.eps"),
                size=(7, 3))


def get_exp_data(path):
    folders = os.listdir(path)
    parsed_folders = map(lambda f: f.split('-'),
                         filter(lambda f: f.startswith('p0-') and os.path.isdir(
                             os.path.join(path, f)),
                                folders))
    for i in range(len(parsed_folders)):
        try:
            parsed_folders[i][1] = float(parsed_folders[i][1])
        except ValueError:
            pass
    p0s = sorted(map(lambda p: (
    float(p[1]) if isinstance(p[1], (int, float)) else p[1], int(p[3])),
                     parsed_folders))
    static_exp = os.path.join(path, "static")

    p0s = map(lambda a: (
    round(a[0] * 100) if isinstance(a[0], (int, float)) else a[0], a[1]), p0s)
    data = {}
    info = {}
    for a, f in p0s:
        read_dir = os.path.join(path, "p0-%s-freq-%i" % (
        "%.02f" % (a / 100.,) if isinstance(a, (int, float)) else a, f))
        if not os.path.isdir(read_dir):
            read_dir = os.path.join(path, "p0-%.2f" % a / 100.)
        # read the experiment output
        content = file(os.path.join(read_dir, "exp-plotter")).readlines()
        data[a, f] = Plotter.parse(content)
        info[a, f] = eval(file(os.path.join(read_dir, "info")).readline())
        print a, f, [(k, info[a, f]['vms'][k]['adviser_args']['rev_func']) for k
                     in info[a, f]['vms'].keys()]

    if os.path.isdir(static_exp):
        content = file(os.path.join(static_exp, "exp-plotter")).readlines()
        data['static'] = Plotter.parse(content)
        info['static'] = eval(file(os.path.join(static_exp, "info")).readline())
        print 'static', [
            (k, info['static']['vms'][k]['adviser_args']['rev_func']) for k in
            info['static']['vms'].keys()]

    correct_time(data, info)
    return data, info


# def boxplot_create_file(path, profiler_file, field, start_min, end_min, main_exp = "ginseng"):
def boxplot_create_file(p0, data, info):
    #    profiler_file = "%s/moc/doc/profiler-memcached-inside-spare-50-win-500k-tapuz25.xml" % (Settings.user_home())
    profiler_file = "%s/moc/doc/profiler-postgres-tapuz25.xml" % (
    Settings.user_home())
    field = 'tps_with_connections_time'  # 'hits_rate'
    profiler = Profiler.fromxml(profiler_file, field)
    start_min = 15

    P = []
    L = []
    M = []

    for j, vm in enumerate(iter_vms(data)):
        if vm != 'vm-1': continue
        perf_data = data[vm][PERF]
        try:
            mon_data = data[vm][MON]
        except KeyError:
            print "ERROR! No monitor data for num: %i, guest: %s" % (j, vm)
            mon_data = []

        p = np.array(get_vals(get_vals(perf_data, 'perf'), field))
        l = np.array(get_vals(perf_data, 'load'))
        t = np.array(get_vals(perf_data, 'time'))
        #        dt = np.array(get_vals(perf_data, 'duration'))
        #        dt = np.array(get_vals(get_vals(perf_data, 'perf'), 'test_time'))
        dt = np.array(get_vals(get_vals(perf_data, 'perf'), 'measure_duration'))
        mt = np.array(get_vals(mon_data, 'time'))
        #        p = p  # / dt

        allm = np.array(get_vals(mon_data, 'mem_available'), float)

        def avg(arg):
            try:
                return np.average(arg) if len(arg) > 0 else 0
            except AttributeError as er:
                return 0

        limit = [avg(allm[list(
            set.intersection(set(np.where(ti - .5 * dti <= mt)[0]),
                             set(np.where(mt < ti + .5 * dti)[0])))])
                 for ti, dti in zip(t, dt)]
        limit = np.array(limit)

        t = (t - t[0]) / 60

        trim = (start_min <= t) & (np.isnan(p) == 0) & \
               (np.isnan(l) == 0) & (np.isnan(limit) == 0)

        p = p[trim]
        l = l[trim]
        limit = limit[trim]

        P.extend(p)
        L.extend(l)
        M.extend(limit)

        print "%i - %s" % (len(P), vm)

    P = np.array(P)
    L = np.array(L)
    M = np.array(M)

    # perf(load,mem) - debug the boxplot
    loads = set(L)
    for k, li in enumerate(sorted(loads)):
        pl.clf()
        ind = np.where(L == li)
        pl.plot(M[ind], P[ind],
                markersize=6,
                marker="o",
                linestyle="None",
                label="load: " + str(li),
                alpha=0.5,
                color=pl.cm.jet(float(li - 1) / max(L)))  # @UndefinedVariable

        pl.xlabel("Memory [MB]")
        pl.ylabel("Performance [hits/s]")
        draw_n_save(os.path.join(localdir, plot_name("boxplot-dbg-%i" % li, p0,
                                                     filetype)), (10, 10))

    # predicted performance from testbed vs. actual performance
    pl.clf()
    p_eval = np.array(map(lambda t: float(profiler.interpolate(t)), zip(L, M)))

    p_eval_sorted = np.array(sorted(list(set(p_eval))))
    xvals = []
    yvals = []
    for pi in p_eval_sorted:
        lst = P[p_eval == pi]
        xvals.append(pi)
        yvals.append(list(lst))
    file(os.path.join(localdir, "boxplot_data-%s-%s" % (p0[0], p0[1])),
         "w").write(str({"x": list(xvals), "y": yvals}))


def boxplot_plot_file(p0):
    path = os.path.join(localdir, "boxplot_data-%s-%s" % (p0[0], p0[1]))
    dcts = []
    dcts.append(eval("".join(file(path, "r").readlines())))
    ox = []
    oys = []
    for d in dcts:
        ox.extend(d["x"])
        oys.extend(d["y"])
    ox = np.array(ox)
    oys = np.array(oys)

    func = lambda x: x * 1e-3
    axis_max = 0.75
    axis_step = 0.25
    x = []
    ys = []
    for xi, y in zip(ox, oys):
        if len(y) > 3:
            ys.append(func(np.array(y)))
            x.append(func(xi))

        #    all_bad = 0
        #    all = 0
        #    point = 1.4
        #
        #    for xi, ysi in zip(x, ys):
        #        if xi < point:
        #            pl.plot([xi] * len(ysi), ysi, "ob")
        #            continue
        #        ysi = np.array(ysi)
        #        ybad = ysi[ysi < point]
        #        ygood = ysi[ysi >= point]
        #        all += len(ysi)
        #        if len(ybad) > 0:
        #            pl.plot([xi] * len(ybad), ybad, "or")
        #            all_bad += len(ybad)
        #        if len(ygood) > 0:
        #            pl.plot([xi] * len(ygood), ygood, "og")
        #
        #    print float(all_bad) / all
        #    pl.show()

    ax = pl.gca()
    pl.plot((0, axis_max), (0, axis_max), "k-", label="Theoretical")
    bp = ax.boxplot(ys, positions=x, sym='k+',
                    widths=0.2, bootstrap=5000)
    pl.xticks(np.arange(0, axis_max + axis_step / 2, axis_step))
    pl.yticks(np.arange(0, axis_max + axis_step / 2, axis_step))
    pl.xlim((0, axis_max))
    pl.ylim((0, axis_max))
    pl.legend(loc="upper left")
    pl.setp(bp['boxes'], color='black', linewidth=1)
    pl.setp(bp['whiskers'], color='black', linewidth=1, linestyle="-")
    pl.setp(bp['fliers'], markersize=3.0)

    #    cnt = 0
    #    tot = 0
    #    for xi, ysi in zip(x, ys):
    #        if xi < 3.4: continue
    #        tot += len(ysi)
    #        yi = ysi[ysi < 3.4]
    #        cnt += len(yi)
    #        pl.plot([xi] * len(yi), yi, "x", linestyle = "None")
    #    print float(cnt) / tot

    k = "k"
    pl.xlabel("Predicted [%shits/s]" % k, labelpad=2)
    pl.ylabel("Actual [%shits/s]" % k, labelpad=2)
    pl.gca().set_position((0.13, 0.13, 0.8, 0.8))
    #    draw_n_save(os.path.join(outdir, "exp-boxplot-%s.eps" % name), (3.2, 3.2))
    #    draw_n_save(os.path.join(outdir, "exp-boxplot-%s.%s" % name), (3.2, 3.2))
    draw_n_save(os.path.join(localdir, plot_name("exp-boxplot", p0, filetype)),
                (10, 10))


def draw_memcached(remotedir, localdir, p0):
    for i in range(1, 1 + 1):
        data = {}
        if isinstance(p0[0], (int, float)):
            dirname = "p0-%.02f-freq-%i" % (p0[0] / 100., p0[1])
        else:
            dirname = dirname = "p0-%s-freq-%i" % (p0[0], p0[1])
        print dirname
        for line in file(os.path.join(remotedir, dirname, "prog-vm-%i" % i)):
            if line.find("target") == -1 or line.find("usage") == -1:
                continue
            for tup in line.split(" - ")[-1].split(", "):
                if not tup.strip():
                    continue
                try:
                    k, v = tup.split(": ")
                    data.setdefault(k, []).append(v)
                except:
                    pass

        interval = 1  # plot every interval-th point
        pl.figure(figsize=(60, 20))
        #        data['fix'] = [int(x) - int(y) for x, y in zip(data['total_malloced'], data['statm'])]
        for k, v in data.iteritems():
            #            if k not in ["fix"]: continue
            if k not in ["target", "total", "usage", "diff", "statm"]:
                continue
            pl.plot([float(j) / 60 for j in range(len(v[::interval]))],
                    v[::interval], label=k)
        pl.legend(loc=0)
        pl.grid()

        #        pl.ylim(-50, 0)
        #        pl.xlim(0, 18800)
        pl.show()

        pl.ylabel("MB")
        pl.xlabel("Approx. time [min]")
        draw_n_save(os.path.join(localdir, plot_name("vm-%i-memcached" % i, p0,
                                                     filetype)), (40, 8))


if __name__ == '__main__':

    #    files = ("p0-compare-%i" % (i) for i in [139])#range(134, 140 + 1))#[140])#[139])#range(134, 138+1))#[136])#range(127, 133 + 1))#[133])#[128])#[127])#[125, 126])#[119, 120])# + [102] + range(78, 82) + [84, 85, 86, 100]) #range(74, 78))
    #    files = ('p0-hunt-2',)
    #    files = ('p0-average', 'p0-average-0')
    #    files = ('p0-compare-new-7',)
    #    files = ('p0-compare-147',)
    #    files = ('random-seed-mixed-5',)
    #    files = ('p0-compare-147', 'p0-compare-new-7')
    #    files = ('p0-compare-149',)
    #    files = ('mcd-check-boxplot',)
    #    files = ('p0-frequency-nomc-12',)
    #    files = ('p0-frequency-nomc-17',)
    files = ('p0-psql-winning-14',)

    for filename in files:
        remotedir = os.path.join(remotedir_base, filename)
        localdir = os.path.join(localdir_base, filename)
        sync_files(remotedir, localdir, ["bidder-*", "exp-out*", "prog-*"])
        if not os.path.exists(localdir): os.mkdir(localdir)
        data, info = get_exp_data(localdir)
        for freq in set(map(lambda k: k[1], data.keys())):
            print freq
        #            draw_bars(data, info, freq)

        # draw_composed_exp(data, info)
        normalize = lambda a: (a[0] / 100., a[1]) if isinstance(a, (
        int, float)) else a
        for a in data.keys():
            print "plotting %s" % str(a)
            draw_exp(normalize(a), data[a], info[a])
        #            boxplot_create_file(normalize(a), data[a], info[a])
        #            boxplot_plot_file(normalize(a))
        # draw_memcached(remotedir, localdir, normalize(a))
