'''
Created on Mar 5, 2012

@author: eyal
'''
import matplotlib
import sys
matplotlib.use('Agg')
import os
import pylab as pl
import numpy as np
from numpy import array  #@UnusedImport
from etc.plots import draw_n_save, pl_apply_defaults, nice_surf2
from mom.Plotter import HOST

out_dir = "%s/sim-output/" % os.path.expanduser("~")

t0 = 0

def draw_exp(fname):
    data = eval(file(fname, "r").readline())
    return data[HOST]['p_in_min_mem'][-1], np.average(data[HOST]['p_in_min_mem']), np.std(data[HOST]['p_in_min_mem']), \
        data[HOST]['p_out_max_mem'][-1], np.average(data[HOST]['p_out_max_mem']), np.std(data[HOST]['p_out_max_mem'])


if __name__ == '__main__':
    pl_apply_defaults()
    num = ""
    file_template = "%s/moc-output/sim/sim-p0-adaptive-mcd%s/sim-oc-%.2f-p0-%.2f" % os.path.expanduser("~")
    out_dir = os.path.join(out_dir, os.path.split(os.path.split(file_template)[0])[1] % num)
    print "outdir:", out_dir
    ocs = list(np.arange(1.0, 1.95, 0.1))
    p0s = list(np.arange(0.0, 1.05, 0.1))

    names = ["p_in_min_last", "p_in_min_avg", "p_in_min_std", "p_out_max_last", "p_out_max_avg", "p_out_max_std"]

    data = dict((n, np.ndarray((len(ocs), len(p0s)))) for n in names)

    for i, o in enumerate(ocs):
        for j, p in enumerate(p0s):
            out = draw_exp(file_template % (num, o, p))
            for k, n in enumerate(names):
                data[n][i, j] = out[k]

    for n in names:
        pl.clf()
        nice_surf2(p0s, ocs, data[n], cbar_pos = (0.82, 0.17, 0.1, 0.75))
        pl.xlabel(r"$p_0$")
        pl.ylabel("Memory Overcommitment")
        pl.gca().set_position((0.2, 0.17, 0.6, 0.75))
        draw_n_save(os.path.join(out_dir, "%s.png" % n), (3.6, 3.6 * 2.0 / 2.6))
        draw_n_save(os.path.join(out_dir, "%s.eps" % n), (3.6, 3.6 * 2.0 / 2.6))

