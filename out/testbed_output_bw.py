import matplotlib
import sys
matplotlib.use('Agg')
import os
import pylab as pl
import numpy as np
from numpy import array
from etc.plots import draw_n_save, pl_apply_defaults
from mom.Plotter import HOST, Plotter
from mpl_toolkits.mplot3d import Axes3D
from mom.Profiler3d import fromxml
from collections import OrderedDict 
from sim.fake_valution_generator import xml_format
from matplotlib.font_manager import FontProperties
from scipy.interpolate import interp1d

# load all markers
markers = []
for m in matplotlib.lines.Line2D.markers:
    try:
        if len(m) == 1 and m != ' ':
            markers.append(m)
    except TypeError:
        pass

# set output directory
out_dir = None
base_dir = None
try:
    # throws exception "AttributeError" on windows    
    os.uname()
    out_dir = os.environ['HOME'] + "/sim-output/traces/%s"
    base_dir = os.environ['HOME'] + "/moc/%s"
except Exception:
    out_dir = os.environ['HOMEPATH'] + r"\Documents\moc-output\testbed"

size = (10, 5)  #(2.5, 2)
t0 = 0
start = 8
end = 12

auction_types = ['mem', 'bw']

pl_apply_defaults({
         'axes.labelsize': 6,
         'text.fontsize': 6,
         'legend.fontsize': 5,
         'font.size': 8,
         'xtick.labelsize': 6,
         'ytick.labelsize': 6,
         })

colors = ["r", "g", "b", "k"]

pol_time = lambda lst: [t - lst['time'][0] for t in lst['time']]

def exp_plot_valuation(profiler, load):
    fig = pl.figure()
    ax = fig.gca(projection='3d')
    # mem
    X = np.asarray(profiler.y)
    # bw
    Y = np.asarray(profiler.z)
    X1, Y1 = np.meshgrid(X, Y)
    Z = profiler.interpolate([load * np.ones(X1.shape), np.copy(X1), np.copy(Y1)])
    surf = ax.plot_surface(X1, Y1, Z, rstride=1, cstride=1, cmap=matplotlib.cm.coolwarm,
        linewidth=0, antialiased=False)
    ax.set_xlabel('Memory')
    ax.set_ylabel('Bandwidth')
    ax.set_zlabel('Value')
    max_val = profiler.interpolate([profiler.x[-1], X[-1], Y[-1]])
    max_val *= 1.1
    ax.set_zlim(profiler.interpolate([0,0,0]), max_val)

def exp_plot(keys, data, func, ylabel, marker = "None", linestyle = "-", xlabel = "round"):
    ret_y = {}
    #guests = sorted(set(data.keys()).difference(set((HOST,))))
    #n = len(guests) + 1
    n = len(keys) + 1
    sp = 1
    maxy = 0
    lines = []

    # draw total load in first subplot
    _xmin = 100000000
    _xmax = -1

    ys = []
    xs = []
    xfunc, yfunc = func
    #for vm in guests:
    for vm in keys:
        try:
            xi = xfunc(data[vm])
            yi = yfunc(data[vm])
            xi = xi[-len(yi):]
            _xmin = min(_xmin, xi[0])
            _xmax = max(_xmax, xi[-1])
            maxy = max(maxy, max(yi))
            ymin = min(min(yi), 0)
            ret_y.setdefault("test", []).extend(yi)
        except Exception as ex:
            print 'Exception in evaluating vm %s' % vm
            continue
        ys.append(yi)
        xs.append(xi)

    for i,k in enumerate(keys):
        l = pl.plot(xs[i], ys[i], markersize = 4, alpha = 1.0,
                    linestyle = linestyle,
                    marker = markers[i % len(markers)],
                    color = pl.cm.jet(float(i) / len(ys)),  #@UndefinedVariable
                    label = "%s" % (k))
        lines.append(l[0])
        pl.grid(True)

    # set a more pleasent font for the legend
    fontP = FontProperties()
    fontP.set_size('small')
    
    # Don't use 1e8 and such
    pl.ticklabel_format(style = 'plain', axis = 'both')

    pl.legend(lines, [l.get_label() for l in lines], prop = fontP, bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.0, mode="expand")
    pl.xlabel(xlabel)
    pl.ylabel(ylabel)
    pl.ylim(ymin = ymin, ymax = float(maxy) * 1.1)

    return ret_y

def get_max_gradient(data, vm_name, max_x, keyword, prev_x, prev_keyword):
    time_max_x = sorted( [t['time'] for t in data[vm_name]['Monitor'] if t['net_rate'] == max_x] )
    time_range = ( min(time_max_x), max(time_max_x) )
    #print max_x, vm_name, time_range
    max_hits_gradient = (np.average([t['perf']['get_hits_total'] for t in data[vm_name]['Performance'] \
        if t['time'] >= time_range[0] and t['time'] <= time_range[1] ]) - prev_keyword) /\
                            (max_x - prev_x)
    return max_hits_gradient

def generate_valuation(doc_string, hits_rate, load, bw, mem):
    xml = xml_format % (doc_string, hits_rate, load, mem, bw)
    xml = xml.replace('], [', '],\n [')
    xml = xml.replace('[[', '\n[[')
    return xml

'''
The code is similar to draw_exp1 but here we take the result of $NUM_SCORES draw_exp1 graphs.
From all those graphs we take the maximum points and derive a final valid graph.
'''
def draw_exp2(fname_exp):
    # the goal of this code is to take the output of a static experiment(exp-plotter)
    # combined from multiple guest and to produce a perforamce for bandwidth graph 
    # by taking the performance and bandwidth in each step and combining it using the time
    # (it's like having 2 sql tables one with (bandwidth, time) and one with (performance, time)
    data = Plotter.parse(open(fname_exp, 'rb').readlines())

    x_keys = sorted(list(set([x['net_rate'] for x in data['vm-10']['Monitor']])))[:-1] # remove the last one(it is used for initialization)
    copy_x_keys = [x for x in x_keys]

    # filter out empty value sets - those where bandwidth wasn't measured
    for x in copy_x_keys:
        time_x = sorted( [t['time'] for t in data['vm-10']['Monitor'] if t['net_rate'] == x] )
        time_range = ( min(time_x), max(time_x) )
        vals_hits = [t['perf']['get_hits_total'] for t in data['vm-10']['Performance'] if t['time'] >= time_range[0] and \
                                                                             t['time'] <= time_range[1] ]
        if len(vals_hits) == 0:
            x_keys.remove(x)

    #prepare data
    '''
    {'vm-1' : {
    'Policy' : {Empty},
    'Performance' : {'load' : 1, 'perf' : {'get_hits_total' : 123, 'time' : 1234}},
    'Monitor' : {'net_rate' : 1234, 'time' : 1234},   
    }}
    '''
    x_keys = sorted(list(set([x['net_rate'] for x in data['vm-10']['Monitor']])))[:-1] # remove the last one(it is used for initialization)
    copy_x_keys = [x for x in x_keys]

    # filter out empty value sets - those where bandwidth wasn't measured
    for x in copy_x_keys:
        time_x = sorted( [t['time'] for t in data['vm-10']['Monitor'] if t['net_rate'] == x] )
        time_range = ( min(time_x), max(time_x) )
        vals_hits = [t['perf']['get_hits_total'] for t in data['vm-10']['Performance'] if t['time'] >= time_range[0] and \
                                                                                     t['time'] <= time_range[1] ]
        if len(vals_hits) == 0:
            x_keys.remove(x)

    x_func = lambda lst : lst['keys']
    y_func_hits_rate = lambda lst : lst['hits_rate']
    y_func_hits = lambda lst : lst['hits']
    keys = sorted(set(data.keys()).difference(set(('Host',))))
    # fix vm-10 to be the last key (sorting puts it between vm-1 and vm-2
    if 'vm-10' in keys:
        keys.remove('vm-10')
        keys.append('vm-10')
    print 'keys: %s' % (str(keys))
    print 'x_keys: %s' % str(x_keys)

    bw = x_keys
    scores = {}
    SCORES_NUM = 20
    load = None
    mem = [600, 800, 1000, 1200, 1400, 1600, 1800, 2000, 2200] # currently this is fake
    for s in xrange(1, SCORES_NUM):
        scores[s] = {}
        load = []
        #tmp_x_keys = x_keys
        #x_keys = [tmp_x_keys[i] for i in xrange(0, len(tmp_x_keys), s)]
        
        #hits_rate = []
        for key in keys:
            print key
            scores[s][key] = {}
            # prepare the gradiant to the maximal point
            prev_x = 0
            prev_hits = 0
            prev_hits_gradient = float("inf")
            prev_hits_rate = 0
            prev_hits_rate_gradient = float("inf")

            load.append(data[key]['Performance'][0]['load'])
            #hits_rate.append([[] for i in mem])
            for x_i in xrange(0, len(x_keys), s):
                x = x_keys[x_i]
                #print x_keys
                time_x = sorted( [t['time'] for t in data[key]['Monitor'] if t['net_rate'] == x] )
                #print key, x, len(time_x)
                time_range = ( min(time_x), max(time_x) )
                #print 'time_range: %s' % str(time_range)
                vals_hits = [t['perf']['get_hits_total'] for t in data[key]['Performance'] if t['time'] >= time_range[0] and \
                                                                                         t['time'] <= time_range[1] ]
                vals_hits_rate = [t['perf']['hits_rate'] for t in data[key]['Performance'] if t['time'] >= time_range[0] and \
                                                                                         t['time'] <= time_range[1] ]
                #print len(vals_hits), len(vals_hits_rate)

                # if some bug caused this step to fail skip it
                if len(vals_hits) == 0:
                    assert 'errror!!!'

                new_hits_rate = np.average(vals_hits_rate)
                new_hits = np.average(vals_hits)
                #new_data[key]['keys'].append(x / 1000)

                new_hits_rate = prev_hits_rate if np.isnan(new_hits_rate) else  new_hits_rate
                new_hits = prev_hits if np.isnan(new_hits) else new_hits

                # verify gradient is non-increasing
                #print key, x, ((new_hits - prev_hits) / ((x - prev_x) / 1000)), prev_hits_gradient
                if ((new_hits - prev_hits) / ((x - prev_x) / 1000)) > prev_hits_gradient:
                    print 'decreasing', x
                    new_hits = prev_hits + prev_hits_gradient * ((x - prev_x) / 1000)

                if ((new_hits_rate - prev_hits_rate) / ((x - prev_x) / 1000)) > prev_hits_rate_gradient:
                    new_hits_rate = prev_hits_rate + prev_hits_rate_gradient * ((x - prev_x) / 1000)

                # verify that gradient is not negative
                if ((new_hits - prev_hits) / ((x - prev_x) / 1000)) < 0:
                    print 'neg', x
                    new_hits = prev_hits

                if ((new_hits_rate - prev_hits_rate) / ((x - prev_x) / 1000)) < 0:
                    new_hits_rate = prev_hits_rate

                #new_data[key]['hits'].append(new_hits)
                #new_data[key]['hits_rate'].append(new_hits_rate)
                #for i in xrange(len(mem)):
                #    hits_rate[-1][i].append(new_hits_rate)

                # update prev
                prev_hits_gradient = (new_hits - prev_hits) / ((x - prev_x) / 1000)
                prev_hits_rate_gradient = ((new_hits_rate - prev_hits_rate) / ((x - prev_x) / 1000))
                prev_x = x
                prev_hits = new_hits
                prev_hits_rate = new_hits_rate
                #print prev_x, prev_hits, prev_hits_gradient
                scores[s][key][x / 1000] = new_hits_rate
                #for i in xrange(len(mem)):
                #    hits_rate[-1][i].append(new_hits_rate)

                # fix previous intermideate points
                for i in xrange(1,s):
                    if x_i <= i:
                        continue
                    new_x = x_keys[x_i - i]
                    scores[s][key][new_x / 1000] = prev_hits_rate - prev_hits_rate_gradient * ((x - new_x) / 1000.0)
                    #scores[s][key][new_x] = new_hits

            # add missing values assuming the valuation is flat at the highest bandwidth value
            #for i in xrange(len(mem)):
            #    while len(hits_rate[-1][i]) < len(bw):
            #        hits_rate[-1][i].append(hits_rate[-1][i][-1])

    new_data  = {k : {'keys' : [], 'hits_rate' : []} for k in keys}
    hits_rate = []
    for vm in keys:
        improved_hits_rate = 0
        prev_improved_hits_rate = 0
        gradient_improved_hits_rate = np.Infinity
        final_improved_hits_rate = None
        prev_b = 0
        hits_rate.append([[] for i in mem])
        for b in sorted(scores[1][vm].keys())[:-SCORES_NUM]:
            if final_improved_hits_rate is not None:
                improved_hits_rate = final_improved_hits_rate
            else:
                improved_hits_rate = round(max([scores[s][vm][b] for s in xrange(1, SCORES_NUM)]), 5)

            if final_improved_hits_rate is None and improved_hits_rate == prev_improved_hits_rate:
                print 'Setting final value for %s at %dKbits' % (vm, b)
                final_improved_hits_rate = improved_hits_rate
            if ((improved_hits_rate - prev_improved_hits_rate) / (b - prev_b)) > gradient_improved_hits_rate:
                improved_hits_rate = round(prev_improved_hits_rate + gradient_improved_hits_rate * (b - prev_b),5)
            new_data[vm]['keys'].append(b)
            new_data[vm]['hits_rate'].append(improved_hits_rate)
            gradient_improved_hits_rate = (improved_hits_rate - prev_improved_hits_rate) / (b - prev_b)
            prev_improved_hits_rate = improved_hits_rate
            prev_b = b
            for i in xrange(len(mem)):
                hits_rate[-1][i].append(improved_hits_rate)

    for vm in xrange(len(hits_rate)):
        for m in xrange(len(mem)):
            while len(hits_rate[vm][m]) < len(bw):
                hits_rate[vm][m].append(hits_rate[vm][m][-1])

    # end loop
    valuation = generate_valuation(fname_exp, hits_rate, load, map(lambda i : i / 1000.0, bw), mem)
    path = os.path.join(out_dir, "new_valuation.xml")
    open(path, 'wb').write(valuation)

    #pl.clf()
    #exp_plot(keys, new_data, (x_func, y_func_hits), 'total-get-hits[hits]', "pasten", "-", "bandwidth[Kbps]")
    #draw_n_save(os.path.join(out_dir, "total-hits-rate-bw"), size)

    pl.clf()
    exp_plot(keys, new_data, (x_func, y_func_hits_rate), 'hits-rate[Khits\s]', "pasten", "-", "bandwidth[Kbps]")
    draw_n_save(os.path.join(out_dir, "total-hits-bw"), size)


def draw_exp1(fname_exp):
    # the goal of this code is to take the output of a static experiment(exp-plotter)
    # combined from multiple guest and to produce a perforamce for bandwidth graph 
    # by taking the performance and bandwidth in each step and combining it using the time
    # (it's like having 2 sql tables one with (bandwidth, time) and one with (performance, time)
    data = Plotter.parse(open(fname_exp, 'rb').readlines())

    x_keys = sorted(list(set([x['net_rate'] for x in data['vm-10']['Monitor']])))[:-1] # remove the last one(it is used for initialization)
    copy_x_keys = [x for x in x_keys]

    # filter out empty value sets - those where bandwidth wasn't measured
    for x in copy_x_keys:
        time_x = sorted( [t['time'] for t in data['vm-10']['Monitor'] if t['net_rate'] == x] )
        time_range = ( min(time_x), max(time_x) )
        vals_hits = [t['perf']['get_hits_total'] for t in data['vm-10']['Performance'] if t['time'] >= time_range[0] and \
                                                                             t['time'] <= time_range[1] ]
        if len(vals_hits) == 0:
            x_keys.remove(x)


    x_func = lambda lst : lst['keys']
    y_func_hits = lambda lst : lst['hits']
    y_func_hits_rate = lambda lst : lst['hits_rate']

    #prepare data
    '''
    {'vm-1' : {
    'Policy' : {Empty},
    'Performance' : {'load' : 1, 'perf' : {'get_hits_total' : 123, 'time' : 1234}},
    'Monitor' : {'net_rate' : 1234, 'time' : 1234},   
    }}
    '''
    x_keys = sorted(list(set([x['net_rate'] for x in data['vm-10']['Monitor']])))[:-1] # remove the last one(it is used for initialization)
    copy_x_keys = [x for x in x_keys]

    # filter out empty value sets - those where bandwidth wasn't measured
    for x in copy_x_keys:
        time_x = sorted( [t['time'] for t in data['vm-10']['Monitor'] if t['net_rate'] == x] )
        time_range = ( min(time_x), max(time_x) )
        vals_hits = [t['perf']['get_hits_total'] for t in data['vm-10']['Performance'] if t['time'] >= time_range[0] and \
                                                                                     t['time'] <= time_range[1] ]
        if len(vals_hits) == 0:
            x_keys.remove(x)

    keys = sorted(set(data.keys()).difference(set(('Host',))))
    # fix vm-10 to be the last key (sorting puts it between vm-1 and vm-2
    if 'vm-10' in keys:
        keys.remove('vm-10')
        keys.append('vm-10')
    new_data  = {k : {'keys' : [], 'hits' : [], 'hits_rate' : [], 'throughput' : []} for k in keys}
    print 'keys: %s' % (str(keys))
    print 'x_keys: %s' % str(x_keys)

    load = []
    mem = [600, 800, 1000, 1200, 1400, 1600, 1800, 2000, 2200] # currently this is fake
    tmp_x_keys = x_keys
    #x_keys = [tmp_x_keys[i] for i in xrange(0, len(tmp_x_keys), 5)]
    bw = x_keys
    hits_rate = []

    for key in keys:
        print key
        # prepare the gradiant to the maximal point
        prev_x = 0
        prev_hits = 0
        prev_hits_gradient = float("inf")
        prev_hits_rate = 0
        prev_hits_rate_gradient = float("inf")

        load.append(data[key]['Performance'][0]['load'])
        hits_rate.append([[] for i in mem])
        for x in x_keys:
            #print x_keys
            time_x = sorted( [t['time'] for t in data[key]['Monitor'] if t['net_rate'] == x] )
            #print key, x, len(time_x)
            time_range = ( min(time_x), max(time_x) )
            #print 'time_range: %s' % str(time_range)
            vals_hits = [t['perf']['get_hits_total'] for t in data[key]['Performance'] if t['time'] >= time_range[0] and \
                                                                                     t['time'] <= time_range[1] ]
            vals_hits_rate = [t['perf']['hits_rate'] for t in data[key]['Performance'] if t['time'] >= time_range[0] and \
                                                                                     t['time'] <= time_range[1] ]
            #print len(vals_hits), len(vals_hits_rate)

            # if some bug caused this step to fail skip it
            if len(vals_hits) == 0:
                assert 'errror!!!'

            new_hits_rate = np.average(vals_hits_rate)
            new_hits = np.average(vals_hits)
            new_data[key]['keys'].append(x / 1000)

            new_hits_rate = prev_hits_rate if np.isnan(new_hits_rate) else  new_hits_rate
            new_hits = prev_hits if np.isnan(new_hits) else new_hits

            # verify gradient is non-increasing
            #print key, x, ((new_hits - prev_hits) / ((x - prev_x) / 1000)), prev_hits_gradient
            if ((new_hits - prev_hits) / ((x - prev_x) / 1000)) > prev_hits_gradient:
                print 'decreasing', x
                new_hits = prev_hits + prev_hits_gradient * ((x - prev_x) / 1000)

            if ((new_hits_rate - prev_hits_rate) / ((x - prev_x) / 1000)) > prev_hits_rate_gradient:
                new_hits_rate = prev_hits_rate + prev_hits_rate_gradient * ((x - prev_x) / 1000)

            # verify that gradient is not negative
            if ((new_hits - prev_hits) / ((x - prev_x) / 1000)) < 0:
                print 'neg', x
                new_hits = prev_hits

            if ((new_hits_rate - prev_hits_rate) / ((x - prev_x) / 1000)) < 0:
                new_hits_rate = prev_hits_rate

            new_data[key]['hits'].append(new_hits)
            new_data[key]['hits_rate'].append(new_hits_rate)
            for i in xrange(len(mem)):
                hits_rate[-1][i].append(new_hits_rate)

            # update prev
            prev_hits_gradient = ((new_hits - prev_hits) / ((x - prev_x) / 1000))
            prev_hits_rate_gradient = ((new_hits_rate - prev_hits_rate) / ((x - prev_x) / 1000))
            prev_x = x
            prev_hits = new_hits
            prev_hits_rate = new_hits_rate
            print prev_x, prev_hits, prev_hits_gradient

        # add missing values assuming the valuation is flat at the highest bandwidth value
        for i in xrange(len(mem)):
            while len(hits_rate[-1][i]) < len(bw):
                hits_rate[-1][i].append(hits_rate[-1][i][-1])
    # end loop
    valuation = generate_valuation(fname_exp, hits_rate, load, map(lambda i : i / 1000.0, bw), mem)
    path = os.path.join(out_dir, "new_valuation.xml")
    open(path, 'wb').write(valuation)

    #import pprint
    #pprint.pprint(new_data)
    pl.clf()
    exp_plot(keys, new_data, (x_func, y_func_hits), 'total-get-hits[hits]', "pasten", "-", "bandwidth[Kbps]")
    draw_n_save(os.path.join(out_dir, "total-hits-rate-bw"), size)
    
    pl.clf()
    exp_plot(keys, new_data, (x_func, y_func_hits_rate), 'hits-rate[Khits\s]', "pasten", "-", "bandwidth[Kbps]")
    draw_n_save(os.path.join(out_dir, "total-hits-bw"), size)
    
    #pl.clf()
    #exp_plot(keys, new_data, (x_func, y_func_throughput), 'throughput', "None", "-", "bandwidth")
    #draw_n_save(os.path.join(out_dir, "throughput-bw"), size)
    
def draw_social_wellfare(fname_exp):
    # the goal of this code is to take the output of a static experiment(exp-plotter)
    # combined from multiple guest and to produce a perforamce for bandwidth graph 
    # by taking the performance and bandwidth in each step and combining it using the time
    # (it's like having 2 sql tables one with (bandwidth, time) and one with (performance, time)
    data = Plotter.parse(open(fname_exp, 'rb').readlines())
    info_name = fname_exp[:-len("exp-plotter")] + "info"
    info = eval(open(info_name, 'rb').read())

    x_func = lambda lst : lst['time']
    y_func_hits_rate_sum = lambda lst : lst['hits_rate_sum']

    #prepare data
    '''
    {'vm-1' : {
    'Policy' : {Empty},
    'Performance' : {'load' : 1, 'perf' : {'get_hits_total' : 123, 'time' : 1234}},
    'Monitor' : {'net_rate' : 1234, 'time' : 1234},   
    }}
    '''
    keys = sorted(set(data.keys()).difference(set(('Host',))))
    
    # fix vm-10 to be the last key (sorting puts it between vm-1 and vm-2
    if 'vm-10' in keys:
        keys.remove('vm-10')
        keys.append('vm-10')

    times = map(lambda x: int(x)/5*5, [entry['perf']['test_time'] for entry in data[keys[0]]['Performance']])
    print times 

    new_data  = {'social_wellfare' : {'time' : [], 'hits_rate_sum' : []}}

    for t in times:
        social_wellfare_t = 0
        for key in keys:
            #print 'Running on vm %s using revenue "%s"' % (key, info['vms'][key]['adviser_args']['revenue_function'])
            revenue_func = eval(info['vms'][key]['adviser_args']['rev_func'])
            print [entry['perf']['hits_rate'] for entry in data[key]['Performance'] if int(entry['perf']['test_time'])/5*5 == t]
            value = [entry['perf']['hits_rate'] for entry in data[key]['Performance'] if int(entry['perf']['test_time'])/5*5 == t][0]
            social_wellfare_t += revenue_func(value)

        new_data['social_wellfare']['time'].append(t)
        new_data['social_wellfare']['hits_rate_sum'].append(social_wellfare_t)



    # end loop
    
    import pprint
    pprint.pprint(new_data)
    pl.clf()
    exp_plot(['social_wellfare'], new_data, (x_func, y_func_hits_rate_sum), 'total social wellfare', "pasten", "-", "times[secs]")
    draw_n_save(os.path.join(out_dir, "social-wellfare"), size)

def draw_performance(fname_exp):
    # the goal of this code is to take the output of a static experiment(exp-plotter)
    # combined from multiple guest and to produce a perforamce for bandwidth graph 
    # by taking the performance and bandwidth in each step and combining it using the time
    # (it's like having 2 sql tables one with (bandwidth, time) and one with (performance, time)
    data = Plotter.parse(open(fname_exp, 'rb').readlines())
    info_name = fname_exp[:-len("exp-plotter")] + "info"
    info = eval(open(info_name, 'rb').read())

    x_func = lambda lst : lst['time']
    y_func_hits_rate = lambda lst : lst['hits_rate']
    y_func_net_rate = lambda lst : lst['net_rate']
    y_func_actual_net_rate = lambda lst : lst['actual_net_rate']

    #prepare data
    '''
    {'vm-1' : {
    'Policy' : {Empty},
    'Performance' : {'load' : 1, 'perf' : {'get_hits_total' : 123, 'time' : 1234}},
    'Monitor' : {'net_rate' : 1234, 'time' : 1234},   
    }}
    '''
    keys = sorted(set(data.keys()).difference(set(('Host',))))

    # fix vm-10 to be the last key (sorting puts it between vm-1 and vm-2
    if 'vm-10' in keys:
        keys.remove('vm-10')
        keys.append('vm-10')

    times = map(lambda x: (int(x[0])/5*5, x[1]), [(entry['perf']['test_time'], entry['time']) for entry in data[keys[0]]['Performance']])
    print times 

    new_data = {k : {'time' : [], 'hits_rate' : [], 'net_rate' : [], 'actual_net_rate' : []} for k in keys}

    for t,real_time in times:
        print t, real_time
        for key in keys:
            #print 'Running on vm %s using revenue "%s"' % (key, info['vms'][key]['adviser_args']['revenue_function'])
            #print [entry['perf']['hits_rate'] for entry in data[key]['Performance'] if int(entry['perf']['test_time'])/5*5 == t]
            hits_rate = [entry['perf']['hits_rate'] for entry in data[key]['Performance'] if int(entry['perf']['test_time'])/5*5 == t][0]
            net_rate = [entry['net_rate'] for entry in data[key]['Monitor'] if entry['time'] >= real_time and entry['time'] <= real_time + 5][0]
            actual_net_rate = [entry['net_cur_rate'] for entry in data[key]['Monitor'] if entry['time'] >= real_time and entry['time'] <= real_time + 5][0]
            #print net_rate
            new_data[key]['hits_rate'].append(hits_rate)
            new_data[key]['net_rate'].append(net_rate / (1000.0 * 1000.0))
            new_data[key]['actual_net_rate'].append(actual_net_rate/ (1000.0 * 1000.0))
            new_data[key]['time'].append(t)

    print new_data
    # end loop
    
    #import pprint
    #pprint.pprint(new_data)
    pl.clf()
    exp_plot(keys, new_data, (x_func, y_func_hits_rate), 'hits rate (#hits per second)', "pasten", "-", "time[secs]")
    draw_n_save(os.path.join(out_dir, "performance"), size)    

    pl.clf()
    exp_plot(keys, new_data, (x_func, y_func_net_rate), 'net rate (Mbps)', "pasten", "-", "time[secs]")
    draw_n_save(os.path.join(out_dir, "allocate_net_rate"), size) 

    pl.clf()
    exp_plot(keys, new_data, (x_func, y_func_actual_net_rate), 'actual net rate(Mbps)', "pasten", "-", "time[secs]")
    draw_n_save(os.path.join(out_dir, "actual_net_rate"), size) 

if __name__ == '__main__':
    if len(sys.argv)>3:
        print "Usage: testbed_output_bw.py <results_output_file>\
                \nAssume we are running from the base directory.\n"
        exit() 
    print "plotting results for %s. Output will be available in %s.\n" % (sys.argv[1],out_dir)
    arg1 = sys.agrv[1]
    draw_exp1(arg1)
