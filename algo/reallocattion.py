'''
Created on Apr 6, 2015

@author: Liran Funaro <fonaro@cs.technion.ac.il>
'''
from copy import copy
import logging

class GoodsReallocattionSteps(object):
    DEFAULT_PIT = "GOODS-SHARE-PIT"
    DEFAULT_CLEAN = "CLEAN-GOODS"

    def __init__(self, pit_name = DEFAULT_PIT, clean_name = DEFAULT_CLEAN):
        self.logger = logging.getLogger( self.__class__.__name__ )
        self.pit_name = pit_name
        self.clean_name = clean_name

        self.dependent = None
        self.independent = None
        self.owner = None

        self.prev_intervals = {}

    def init_dependency(self):
        # Initiate the owners of each good
        self.goods_owner = {}
        for player_name, (start, end) in self.prev_intervals.iteritems():
            if player_name in self.next_intervals:
                for i in xrange(start, end+1):
                    self.goods_owner.setdefault(i, set()).add(player_name)

        self.dependent = {}
        self.independent = { player for player in self.next_intervals }
        self.owner = {}
        self.cur_intervals = copy(self.prev_intervals)

        pit_interval = self.next_intervals.get(self.pit_name, None)
        old_pit_interval = self.prev_intervals.get(self.pit_name, None)

        for dependent_player, interval in self.next_intervals.iteritems():
            # If the player's interval have not changed (including staying in
            # the PIT) then it's remains independent
            if dependent_player in self.prev_intervals and self.prev_intervals[dependent_player] == interval:
                continue

            if dependent_player == self.pit_name or interval != pit_interval:
                start, end = interval

                for i in xrange(start, end+1):
                    # If the player is one of the owners, he is independent
                    # of the other owners
                    if dependent_player not in self.goods_owner.get(i, set()):
                        for owner in self.goods_owner.get(i, set()):
                            self.add_dependency(dependent_player, owner)
            else:
                # If it moves to the PIT, it only dependent on the PIT's
                # relocation (not dependent by all the players in the PIT)
                self.add_dependency(dependent_player, self.pit_name)

        if self.pit_name in self.dependent:
            # PIT is not dependent on players who will move to the PIT
            for player_name, interval in self.next_intervals.iteritems():
                if self.pit_name not in self.dependent:
                    break

                if interval == pit_interval and player_name in self.dependent[self.pit_name]:
                    self._release_dependency(self.pit_name, player_name)

            # PIT is not dependent on players that are currently in the PIT
            for player_name, interval in self.prev_intervals.iteritems():
                if self.pit_name not in self.dependent:
                    break

                if interval == old_pit_interval and player_name in self.dependent[self.pit_name]:
                    self._release_dependency(self.pit_name, player_name)

    def add_dependency(self, dependent_player, owner_player):
        if owner_player is None or dependent_player == owner_player:
            return

        if dependent_player in self.independent:
            self.independent.remove(dependent_player)

        self.dependent.setdefault(dependent_player, set()).add(owner_player)
        self.owner.setdefault(owner_player, set()).add(dependent_player)

    def _release_dependency(self, dependent_player, owner_player):
        if dependent_player in self.dependent and owner_player in self.dependent[dependent_player]:
            self.dependent[dependent_player].remove(owner_player)

            if not self.dependent[dependent_player]:
                self.dependent.pop(dependent_player)
                self.independent.add(dependent_player)

        if owner_player in self.owner and dependent_player in self.owner[owner_player]:
            self.owner[owner_player].remove(dependent_player)

    def release_dependency(self, owner_player):
        dependes_on_owner = self.owner.pop(owner_player, set())

        for dependent_player in dependes_on_owner:
            self._release_dependency(dependent_player, owner_player)

    # DEPRECATED FOR NOW
    def _find_cycle(self, cur_player, markers):
        is_marked = markers.get(cur_player, False)
        if is_marked:
            return True, [cur_player]
        else:
            markers[cur_player] = True

        for dependent_player in self.dependent[cur_player]:
            in_cycle, the_cycle = self._find_cycle(dependent_player, markers)
            if in_cycle:
                if cur_player in the_cycle:
                    return False, the_cycle
                else:
                    the_cycle.insert(0, cur_player)
                    return True, the_cycle
            elif the_cycle is not None:
                return False, the_cycle

        return False, None

    # DEPRECATED FOR NOW
    def find_cycle(self):
        if self.pit_name in self.dependent:
            start_player = self.pit_name
        else:
            start_player = self.dependent.keys()[0]

        _, cycle = self._find_cycle(start_player, {})
        return cycle

    def find_max_intersection_min_clean(self, *players):
        min_clean = None
        max_intersection = -1

        player = None

        for p in players:
            # Use prev_intervals (not cur) to prevent accumulating losses
            old_start,old_end = self.prev_intervals[p]
            new_start,new_end = self.next_intervals[p]

            inter_start = max(old_start, new_start)
            inter_end = min(old_end, new_end)

            new_len = new_end-new_start+1
            inter_len = max(0,inter_end-inter_start+1)
            to_clean = new_len - inter_len

            if inter_len > max_intersection or (inter_len == max_intersection and to_clean < min_clean):
                player = p
                max_intersection = inter_len
                min_clean = to_clean

        return player

    def add_step(self, player, interval = None):
        if interval is None:
            interval = self.next_intervals[player]

        player_cur_interval = self.cur_intervals.get(player, None)

        # If the player current interval is the same as the new, we don't need
        # to change its allocation
        if player_cur_interval is None or player_cur_interval != interval:
            self.steps.append( (player, interval) )

        # Add a clean step if the player evacuated some goods
        if player_cur_interval:
            cur_start,cur_end = player_cur_interval
            new_start,new_end = interval
            pit_start,pit_end = self.next_intervals[self.pit_name]
            old_pit_start,old_pit_end = self.prev_intervals[self.pit_name]

            clean_intervals = []
            for i in xrange(cur_start,cur_end+1):
                # Don't clean the new pit interval
                if pit_start <= i <= pit_end:
                    continue

                # Don't clean the remaining player's interval unless it used to
                # be part of the old pit (then it was shared and should be cleaned)
                if new_start <= i <= new_end and not old_pit_start <= i <= old_pit_end:
                    continue

                self.goods_owner.get(i, set()).remove(player)

                # Only clean a good if we got rid of all its former owners
                if self.goods_owner.get(i, set()):
                    continue

                if clean_intervals:
                    last_int = clean_intervals[-1]
                    if i == last_int[1]+1:
                        last_int[1] = i
                    else:
                        clean_intervals.append([i,i])
                else:
                    clean_intervals.append([i,i])

            for inter in clean_intervals:
                self.steps.append( (self.clean_name, inter) )

        self.release_dependency(player)
        self.cur_intervals[player] = interval

    def get_real_intervals(self, intervals):
        real_intervals = copy(intervals)
        pit_interval = intervals.get(self.pit_name, None)

        for player, inter in real_intervals.iteritems():
            if player == self.pit_name:
                continue

            if inter is None:
                real_intervals[player] = pit_interval

        return real_intervals

    def find_reallocation_steps(self, next_intervals):
        self.next_intervals = self.get_real_intervals(next_intervals)
        self.init_dependency()

        self.steps = []

        while self.dependent or self.independent:
            while self.independent:
                # Release the PIT ASAP
                if self.pit_name in self.independent:
                    self.add_step(self.pit_name)
                    self.independent.remove(self.pit_name)
                else:
                    self.add_step(self.independent.pop())

            if self.dependent:
                cycle = self.find_cycle()
                if cycle is None:
                    self.logger.debug("No cycle found, but there is still dependent players: %s",self.dependent)
                    break

                player_name = self.find_max_intersection_min_clean(*cycle)
                if player_name is None:
                    self.logger.debug("Max intersection returned with no player out of the cycle: %s - where dependent players are: %s", cycle, self.dependent)
                    break

                self.add_step(player_name)

        self.prev_intervals = copy(self.next_intervals)
        self.next_intervals = None

        return self.steps
