#! /usr/bin/env python
# Memory Overcommitment Manager
# Copyright (C) 2010 Adam Litke, IBM Corporation
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

import sys
import signal
import time
import os
import atexit
import re
from optparse import OptionParser
import ConfigParser
from mom.LogUtils import LogUtils
from mom.libvirtInterface import libvirtInterface
from mom.HostMonitor import HostMonitor
from mom.GuestManager import GuestManager
from mom.PolicyEngine import PolicyEngine
import logging

class LocalVirt(object):
    class LocalDom(object):
        def __init__(self):
            self.mem = 0
        def maxMemory(self):
            return 10 << 10 << 10  #10GB in kB
        def setMemory(self, mem):
            self.mem = mem

    def listDomainsID(self):
        return set(['localhost'])
    def getDomainFromID(self, id):
        return self.LocalDom()
    def domainGetUUID(self, dom):
        return str(os.getpid())
    def domainGetName(self, dom):
        return "localhost"
    def domainGetMemoryStats(self, dom):
        return {'libvirt_state': 1, 'libvirt_maxmem': dom.mem,
                'libvirt_curmem': dom.maxMemory()}

def daemonize(pid_file):
    try:
        pid = os.fork()
        if pid > 0:
            sys.exit(0)
    except OSError as e:
        sys.stderr.write("momd: fork failed: %d (%s)\n" % (e.errno, e.strerror))
        sys.exit(1)

    os.chdir("/")
    os.setsid()
    os.umask(0)

    try:
        pid = os.fork()
        if pid > 0:
            sys.exit(0)
    except OSError as e:
        sys.stderr.write("momd: fork failed: %d (%s)\n" % (e.errno, e.strerror))
        sys.exit(1)

    pid = str(os.getpid())
    try:
        file(pid_file, 'w+').write("%s\n" % pid)
    except EnvironmentError as e:
        sys.stderr.write("momd: failed to write pid file: %d (%s)\n" %
                         (e.errno, e.strerror))
        sys.exit(1)
    atexit.register(delpid, pid_file)

    sys.stdout.flush()
    sys.stderr.flush()
    si = file('/dev/null', 'r')
    so = file('/dev/null', 'a+')
    se = file('/dev/null', 'a+', 0)
    os.dup2(si.fileno(), sys.stdin.fileno())
    os.dup2(so.fileno(), sys.stdout.fileno())
    os.dup2(se.fileno(), sys.stderr.fileno())

def delpid(pid_file):
    try:
        os.remove(pid_file)
    except OSError as e:
        logger = logging.getLogger('mom')
        logger.error("Unable to remove pid file (%s): %s", pid_file, e.strerror)

def get_plot_subdir(basedir):
    """
    Create a new directory for plot files inside basedir.  The name is in the
    format: momplot-NNN where NNN is an ascending sequence number.
    Return: The new directory name or '' on error.
    """
    if basedir == '':
        return ''

    logger = logging.getLogger('mom')
    regex = re.compile('^momplot-(\d{3})$')
    try:
        names = os.listdir(basedir)
    except OSError as e:
        logger.warn("Cannot read plot-basedir %s: %s", basedir, e.strerror)
        return ''
    seq_num = -1
    for name in names:
        m = regex.match(name)
        if m is not None:
            num = int(m.group(1))
            if num > seq_num:
                seq_num = num
    seq_num = seq_num + 1
    dir = "%s/momplot-%03d" % (basedir, seq_num)
    if seq_num > 999:
        logger.warn("Cannot create plot-dir because the sequence number "\
              "is out of range.  Clear the directory or choose a different one")
        return ''
    try:
        os.mkdir(dir)
    except OSError as e:
        logger.warn("Cannot create plot-dir %s: %s", dir, e.strerror)
        return ''
    return dir

def threads_ok(threads):
    """
    Check to make sure a list of expected threads are still alive
    """
    return all(threads_ok(t) if type(t) is list else t.is_alive() for t in threads)

def wait_for_thread(t, timeout = None):
    """
    Join a thread only if it is still running
    """
    if t.is_alive():
        t.join(timeout)

class momd():
    def __init__(self, *args):
        # read options
        args = list(args) if len(args) != 0 else None
        cmdline = get_cmdline_options()
        (options, args) = cmdline.parse_args(args)
        self.config = read_config(options.config_file, options)
        if options.daemonize:
            daemonize(self.config.get('main', 'pid-file'))

        # set logging
        logfile = self.config.get('logging', 'log')
        LogUtils(self.config.get('logging', 'verbosity'), logfile == "stdio")
        if logfile != "stdio":
            LogUtils.start_file_logging(logfile,
                        self.config.get('logging', 'verbosity'),
                        self.config.getint('logging', 'max-bytes'),
                        self.config.getint('logging', 'backup-count'))
        self.logger = logging.getLogger('mom')

        # Set up a shared libvirt connection
        uri = self.config.get('main', 'libvirt-hypervisor-uri')
        libvirt_iface = libvirtInterface(uri)

        if self.config.get('main', 'local-test').lower() == "true":
            self.logger.warn("RUNNING LOCAL TEST")
            libvirt_iface = LocalVirt()

        self.host_monitor = HostMonitor(self.config)
        self.guest_manager = GuestManager(self.config, libvirt_iface)
        # synchronization mechanisms will be required.
        self.policy_engines = []
        self.auction_available = {'mem' : self.is_memory_auction_available()}
        for auction_type in self.auction_available.keys():
            if self.auction_available[auction_type]:
                self.logger.info("Running %s auction" % auction_type)
                self.policy_engines.append(PolicyEngine(self.config, options.rules_file,
                                                        libvirt_iface, self.host_monitor,
                                                        self.guest_manager, auction_type))
            else:
                self.logger.info("Skipping %s auction" % auction_type)

#        self.rpc_server = RPCServer(self.config, self.host_monitor,
#                    self.guest_manager, self.policy_engine)

    def is_memory_auction_available(self):
        return self.config.has_section('mem')

    def start(self, start_guest_manager = True):
        # Start threads
        self.logger.debug("Daemon starting")
        self.config.set('__int__', 'running', '1')

        if start_guest_manager:
            self.guest_manager.start()
        self.host_monitor.start()
        for policy_engine in self.policy_engines:
            policy_engine.start()
#        self.rpc_server.start()

        interval = self.config.getint('main', 'main-loop-interval')
        while self.should_run:
            time.sleep(interval)
            if not threads_ok((self.host_monitor, self.guest_manager,
                               self.policy_engines)):
                break
            # Check the RPC server separately from the other threads since it
            # can be disabled.
#            if not self.rpc_server.thread_ok():
#                break

        self.terminate()
#        self.rpc_server.shutdown()

        # wait for all threads
#        wait_for_thread(self.rpc_server)
        for policy_engine in self.policy_engines:
            wait_for_thread(policy_engine)
        wait_for_thread(self.guest_manager)
        wait_for_thread(self.host_monitor)
        self.logger.info("Daemon ending")

    def terminate(self):
        self.config.set('__int__', 'running', '0')

    @property
    def should_run(self):
        return self.config.getint('__int__', 'running') == 1

def get_cmdline_options():
    cmdline = OptionParser()
    cmdline.add_option('-c', '--config-file', dest = 'config_file',
                       help = 'Load configuration from FILE', metavar = 'FILE',
                       default = '/etc/mom.conf')
    cmdline.add_option('-r', '--rules-file', dest = 'rules_file', default = '',
                       help = 'Load rules from FILE', metavar = 'FILE')
    cmdline.add_option('-p', '--plot-dir', dest = 'plot_dir',
                       help = 'Save data plot files in DIR', metavar = 'DIR')
    cmdline.add_option('-l', '--log', dest = 'log', metavar = 'TARGET',
                       help = 'Set the log to TARGET (stdout, or <file>')
    cmdline.add_option('-v', '--verbose', dest = 'verbosity', metavar = 'LEVEL',
                       help = 'Set logging verbosity to LEVEL (0-4)')
    cmdline.add_option('-d', '--daemon', action = 'store_true', dest = 'daemonize')
    cmdline.add_option('', '--pid-file', dest = 'pid_file', metavar = 'FILE',
                       help = 'When running as a daemon, write pid to FILE')
    cmdline.add_option('-m', '--host-mem', dest = 'host_mem', metavar = 'INT',
                       help = 'Memory left for host')
    cmdline.add_option('-0', '--p0', dest = 'p0_percent', metavar = 'FLOAT',
                       help = 'Set p0 percent out of minimal accepted unit price')
    cmdline.add_option('-w', '--warmup-rounds', dest='warmup_rounds',
                       metavar='INT', default=-1,
                       help='Sets the number of warmup auction rounds')

    cmdline.add_option('-i', '--interval', dest = 'policy_interval', metavar = 'INT',
                       help = 'Set policy interval')
    cmdline.add_option('--mem-auction-intervals', dest = 'mem_auction_intervals', metavar = 'STR',
                       help = "a list of intervals: bid collection interval, calculation interval, and notification interval for the memory auction")
    cmdline.add_option('-M', '--auction-mem', dest = 'auction_mem', metavar = 'INT',
                       help = 'Set auction memory (0 for default mode)')
    return cmdline

def read_config(fname, options):
    config = ConfigParser.SafeConfigParser()
    # Set defaults
    config.add_section('main')
    config.set('main', 'main-loop-interval', '60')
    config.set('main', 'host-monitor-interval', '5')
    config.set('main', 'guest-manager-interval', '5')
    config.set('main', 'guest-monitor-interval', '5')
    config.set('main', 'policy-engine-interval', '10')
    config.set('main', 'sample-history-length', '10')
    config.set('main', 'libvirt-hypervisor-uri', '')
    config.set('main', 'controllers', 'Balloon')
    config.set('main', 'plot-dir', '')
    config.set('main', 'pid-file', '/var/run/momd.pid')
    config.set('main', 'rpc-port', '-1')
    config.set('main', 'local-test', 'False')
    config.add_section('logging')
    config.set('logging', 'log', 'stdio')
    config.set('logging', 'verbosity', 'info')
    config.set('logging', 'max-bytes', '2097152')
    config.set('logging', 'backup-count', '5')
    config.add_section('host')
    config.set('host', 'collectors', 'HostMemory')
    config.set('host', 'auction-mem', '0')
    config.set('host', 'warmup_rounds', '-1')
    config.add_section('guest')
    config.set('guest', 'collectors', 'GuestQemuProc, GuestLibvirt')
    config.read(fname)

    # Process command line overrides
    if options.plot_dir is not None:
        config.set('main', 'plot-dir', options.plot_dir)
    if options.pid_file is not None:
        config.set('main', 'pid-file', options.pid_file)
    if options.log is not None:
        config.set('logging', 'log', options.log)
    if options.verbosity is not None:
        config.set('logging', 'verbosity', options.verbosity)
    if options.host_mem is not None:
        config.set('host', 'host-mem', str(options.host_mem))
    if options.p0_percent is not None:
        config.set('host', 'p0-percent', str(options.p0_percent))
    if options.warmup_rounds is not None:
        config.set('host', 'warmup_rounds', str(options.warmup_rounds))
    if options.policy_interval is not None:
        config.set('main', 'policy-engine-interval', str(options.policy_interval))
    if options.mem_auction_intervals is not None:
        try:
            mem_bid_collection_interval, mem_calculation_interval, mem_notification_interval = map(int, options.mem_auction_intervals.split(","))
        except:
            sys.stderr.write("Error in memory auction intervals argument")
            sys.exit(1)
        config.set('host', 'mem-bid-collection-interval', str(mem_bid_collection_interval))
        config.set('host', 'mem-calculation-interval', str(mem_calculation_interval))
        config.set('host', 'mem-notification-interval', str(mem_notification_interval))
        config.set('main', 'mem-policy-engine-interval', str(mem_bid_collection_interval + mem_calculation_interval + mem_notification_interval))

    if options.auction_mem is not None:
        config.set('host', 'auction-mem', str(options.auction_mem))

    # Add non-customizable thread-global variables
    # The supplied config file must not contain a '__int__' section
    if config.has_section('__int__'):
        config.remove_section('__int__')
    config.add_section('__int__')
    config.set('__int__', 'running', '0')
    plot_subdir = get_plot_subdir(config.get('main', 'plot-dir'))
    config.set('__int__', 'plot-subdir', plot_subdir)

    return config

if __name__ == "__main__":
    m = momd()

    def signal_quit(signum, frame):
        m.logger.info("Received signal %i shutting down.", signum)
        m.terminate()

    signal.signal(signal.SIGINT, signal_quit)
    signal.signal(signal.SIGTERM, signal_quit)

    m.start()
